package com.zealtech.dld.dldregist.model.newFarmer;


/**
 * Created by up on 28/9/2558.
 */


public class ResponseBase<T1> {
    private String responseCode;
    private String responseMessage;
    private T1 data;



    public T1 getData() {
        return data;
    }

    public void setData(T1 data) {
        this.data = data;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
