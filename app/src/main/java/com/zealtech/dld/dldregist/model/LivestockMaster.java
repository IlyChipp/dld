package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by fanqfang on 9/26/2017 AD.
 */
@Parcel
public class LivestockMaster implements APIResponse{
    //DBHelper
    public static final String TABLE = "list_livestock";

    String responseCode;
    String responseMessage;
    ArrayList<LivestockEntity> data;

    public LivestockMaster(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<LivestockEntity> getData() {
        return data;
    }

    public void setData(ArrayList<LivestockEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class LivestockEntity{
        int livestock_Type_ID;
        String createBy;
        String createDate;
        String livestock_Type_Code;
        String livestock_Type_Flgtail;
        String livestock_Type_FullName;
        String livestock_Type_Name;
        String livestock_Type_Old_ID;
        int livestock_Type_Parent;
        BigDecimal livestock_Type_Price;
        String livestock_Type_Unit;
        int livestock_Type_UnitLimit;
        String updateBy;
        String updateDate;

        int livestock_Amount;
        Double livestock_Values;

        int orderIndex;
        int status_ID;
        int livestock_Type_Root;

        public LivestockEntity(){ }

        public LivestockEntity(LivestockEntity livestockEntity) {
            this.livestock_Type_ID = livestockEntity.getLivestock_Type_ID();
            this.createBy = livestockEntity.getCreateBy();
            this.createDate = livestockEntity.getCreateDate();
            this.livestock_Type_Code = livestockEntity.getLivestock_Type_Code();
            this.livestock_Type_Flgtail = livestockEntity.getLivestock_Type_Flgtail();
            this.livestock_Type_FullName = livestockEntity.getLivestock_Type_FullName();
            this.livestock_Type_Name = livestockEntity.getLivestock_Type_Name();
            this.livestock_Type_Old_ID = livestockEntity.getLivestock_Type_Old_ID();
            this.livestock_Type_Parent = livestockEntity.getLivestock_Type_Parent();
            this.livestock_Type_Price = livestockEntity.getLivestock_Type_Price();
            this.livestock_Type_Unit = livestockEntity.getLivestock_Type_Unit();
            this.livestock_Type_UnitLimit = livestockEntity.getLivestock_Type_UnitLimit();
            this.updateBy = livestockEntity.getUpdateBy();
            this.updateDate = livestockEntity.getUpdateDate();
            this.livestock_Amount = livestockEntity.getLivestock_Amount();
            this.livestock_Values = livestockEntity.getLivestock_Values();
            this.status_ID = livestockEntity.getStatus_ID();
            this.orderIndex = livestockEntity.getOrderIndex();
            this.livestock_Type_Root = livestockEntity.getLivestock_Type_Root();
        }

        public int getLivestock_Type_Root() {
            return livestock_Type_Root;
        }

        public void setLivestock_Type_Root(int livestock_Type_Root) {
            this.livestock_Type_Root = livestock_Type_Root;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        public int getLivestock_Type_ID() {
            return livestock_Type_ID;
        }

        public void setLivestock_Type_ID(int livestock_Type_ID) {
            this.livestock_Type_ID = livestock_Type_ID;
        }

        public String getCreateBy() {
            return createBy;
        }

        public void setCreateBy(String createBy) {
            this.createBy = createBy;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getLivestock_Type_Code() {
            return livestock_Type_Code;
        }

        public void setLivestock_Type_Code(String livestock_Type_Code) {
            this.livestock_Type_Code = livestock_Type_Code;
        }

        public String getLivestock_Type_Flgtail() {
            return livestock_Type_Flgtail;
        }

        public void setLivestock_Type_Flgtail(String livestock_Type_Flgtail) {
            this.livestock_Type_Flgtail = livestock_Type_Flgtail;
        }

        public String getLivestock_Type_FullName() {
            return livestock_Type_FullName;
        }

        public void setLivestock_Type_FullName(String livestock_Type_FullName) {
            this.livestock_Type_FullName = livestock_Type_FullName;
        }

        public String getLivestock_Type_Name() {
            return livestock_Type_Name;
        }

        public void setLivestock_Type_Name(String livestock_Type_Name) {
            this.livestock_Type_Name = livestock_Type_Name;
        }

        public String getLivestock_Type_Old_ID() {
            return livestock_Type_Old_ID;
        }

        public void setLivestock_Type_Old_ID(String livestock_Type_Old_ID) {
            this.livestock_Type_Old_ID = livestock_Type_Old_ID;
        }

        public int getLivestock_Type_Parent() {
            return livestock_Type_Parent;
        }

        public void setLivestock_Type_Parent(int livestock_Type_Parent) {
            this.livestock_Type_Parent = livestock_Type_Parent;
        }

        public BigDecimal getLivestock_Type_Price() {
            return livestock_Type_Price;
        }

        public void setLivestock_Type_Price(BigDecimal livestock_Type_Price) {
            this.livestock_Type_Price = livestock_Type_Price;
        }

        public String getLivestock_Type_Unit() {
            return livestock_Type_Unit;
        }

        public void setLivestock_Type_Unit(String livestock_Type_Unit) {
            this.livestock_Type_Unit = livestock_Type_Unit;
        }

        public int getLivestock_Type_UnitLimit() {
            return livestock_Type_UnitLimit;
        }

        public void setLivestock_Type_UnitLimit(int livestock_Type_UnitLimit) {
            this.livestock_Type_UnitLimit = livestock_Type_UnitLimit;
        }

        public String getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(String updateBy) {
            this.updateBy = updateBy;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public int getLivestock_Amount() {
            return livestock_Amount;
        }

        public void setLivestock_Amount(int livestock_Amount) {
            this.livestock_Amount = livestock_Amount;
        }

        public Double getLivestock_Values() {
            return livestock_Values;
        }

        public void setLivestock_Values(Double livestock_Values) {
            this.livestock_Values = livestock_Values;
        }

        @Override
        public String toString() {
            return livestock_Type_Name;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String LIVESTOCK_TYPE_ID = "livestock_Type_ID";
        public static final String CREATE_BY = "createBy";
        public static final String CREATE_DATE = "createDate";
        public static final String LIVESTOCK_TYPE_CODE = "livestock_Type_Code";
        public static final String LIVESTOCK_TYPE_FLGTAIL = "livestock_Type_Flgtail";
        public static final String LIVESTOCK_TYPE_FULLNAME = "livestock_Type_Fullname";
        public static final String LIVESTOCK_TYPE_NAME = "livestock_Type_Name";
        public static final String LIVESTOCK_TYPE_OLD_ID = "livestock_Type_Old_ID";
        public static final String LIVESTOCK_TYPE_PARENT = "livestock_Type_Parent";
        public static final String LIVESTOCK_TYPE_PRICE = "livestock_Type_Price";
        public static final String LIVESTOCK_TYPE_UNIT = "livestock_Type_Unit";
        public static final String LIVESTOCK_TYPE_UNITLIMIT = "livestock_Type_UnitLimit";
        public static final String UPDATE_BY = "updateBy";
        public static final String UPDATE_DATE = "updateDate";
        public static final String STATUS_ID = "status_ID";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String LIVESTOCK_TYPE_ROOT = "livestock_Type_Root";
    }
}
