package com.zealtech.dld.dldregist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.zealtech.dld.dldregist.MainApplication;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.fragment.MainFragment;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.FarmerModel.FarmerEntity;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;

import org.parceler.Parcels;

import butterknife.ButterKnife;
import io.realm.Realm;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String CLASS_NAME = "MainAvtivity";
    private static final String TAG_HOME_FRAGMENT = "tag_home_fragment";

    final private static String FARMERINFO = "farmerInfo";
    final private static String UUID = "uuid";

    DatabaseRealm databaseRealm;
    DrawerLayout drawer;
    FarmerEntity farmer;
    Farmer farmerItem;
    ImageView btn_back;
    int type,from;
    int current_FarmID;
    RelativeLayout relative_select_farm;
    ViewPager viewPager;
    public FarmerEntity getFarmer() {
        return farmer;
    }

    public Farmer getFarmerItem() {
        return farmerItem;
    }

    public String getUUID() {
        return uuid;
    }

    public int getRegisType(){
        return type;
    }

    public int getFrom(){
        return from;
    }

    String uuid;

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ViewPager getViewPager() {
        return viewPager;
    }

    public void setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
    }

    public void setFarmerItem(Farmer farmerItem) {
        this.farmerItem = farmerItem;
    }

    Realm realm;
    Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CoordinatorLayout appBar = (CoordinatorLayout) findViewById(R.id.app_bar_main);
        btn_back= (ImageView) findViewById(R.id.btn_back);
//        imgProfile = (CircleShape.RoundedImageView) appBar.findViewById(R.id.imgProfileBar);
        MyNoHBTextView txtNav = (MyNoHBTextView) appBar.findViewById(R.id.txtNav);
//        relative_select_farm= (RelativeLayout) appBar.findViewById(R.id.relative_select_farm);
//        imgProfile.setImageResource(R.drawable.test);
//        relative_select_farm.setVisibility(View.GONE);
        farmer = Parcels.unwrap(getIntent().getParcelableExtra(FARMERINFO));
        MainApplication application = (MainApplication) getApplication();
        mTracker = application.getDefaultTracker();

        current_FarmID=getIntent().getIntExtra("farmId",0);
        uuid = getIntent().getStringExtra(UUID);
        type = getIntent().getIntExtra(AppUtils.TYPE,0);
        from= getIntent().getIntExtra(AppUtils.SENDFROM,0);//0 offline, 1 findfarmer(register),5 farmerlist/listfarm ,3 register, 4 hamburger >> 5,4 > update, 0>>add/update
        databaseRealm=new DatabaseRealm(this);
        realm = Realm.getDefaultInstance();
        if (databaseRealm.getDataFarmer(uuid)!=null) {
            farmerItem = realm.copyFromRealm(databaseRealm.getDataFarmer(uuid));
        }else {
            farmerItem=new Farmer();
        }

        ButterKnife.bind(this);

        initInstance();

        checkAppBar();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contentContainer, MainFragment.newInstance(), TAG_HOME_FRAGMENT)
                    .commit();
        }

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (from==0) {
//                    Intent intent = new Intent(MainActivity.this, OfflineActivity.class);
//                    startActivity(intent);
                    finish();
                }else   {
                    finish();}

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(from==1) {
            mTracker.setScreenName("หน้าลงทะเบียนใหม่");
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }else{
            mTracker.setScreenName("หน้าข้อมูลเกษตรกร");
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
    }

    Toolbar toolbar;
    private void initInstance() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");

        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.open_drawer, R.string.close_drawer);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_updatedata).setVisible(false);
    }

    @Override
    public void onBackPressed() {
        if (AppUtils.isOnline(this)==true){
           DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }else {
//            Intent intent = new Intent(MainActivity.this, OfflineActivity.class);
//            startActivity(intent);
            finish();
//            super.onBackPressed();
        }

    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            Intent intent = new Intent(MainActivity.this, SelectTypeActivity.class);
            startActivity(intent);
            MyPreferenceManager myPreferenceManager = new MyPreferenceManager(MainActivity.this);
            myPreferenceManager.setUserId(0);
            myPreferenceManager.setUserPid(0);
            finishAffinity();
        }/* else if (id == R.id.nav_findpid) {
            Intent intent = new Intent(MainActivity.this, FindFarmerActivity.class);
            startActivity(intent);

        }*/ else if (id == R.id.nav_dash) {
            Intent intent = new Intent(MainActivity.this, FarmActivity.class); //mode DashBoard > list farm
            intent.putExtra(UUID,uuid);
            intent.putExtra("mode", "dashBoard");
            intent.putExtra("farmId", 0);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_farmer) {

        } else if (id == R.id.nav_farm) {
            Intent intent = new Intent(MainActivity.this, FarmActivity.class); //mode DashBoard > list farm
            intent.putExtra(UUID,uuid);
            intent.putExtra("mode", "");
            intent.putExtra("farmId", 0);
            startActivity(intent);
            finish();
        }else if (id == R.id.nav_daily) {
            Intent intent = new Intent(MainActivity.this, DailyActivity.class); //mode DashBoard > list farm
            intent.putExtra(UUID,uuid);
            intent.putExtra("mode", "");
            intent.putExtra("farmId", 0);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_updatedata) {
            Intent intent = new Intent(MainActivity.this, OfflineActivity.class);
            startActivity(intent);
        } /*else if (id == R.id.nav_history) {
            Intent intent = new Intent(MainActivity.this, HistoryActivity.class);
            startActivity(intent);
        }*/ else if (id == R.id.nav_aboutus) {

            Intent intent = new Intent(MainActivity.this, AboutUsActivity.class);
            startActivity(intent);

        } /*else if (id == R.id.nav_update) {

        }*/
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void checkAppBar(){

        if (from==3||from==1){
            btn_back.setVisibility(View.VISIBLE);
            toolbar.setNavigationIcon(null);
        }else {
            btn_back.setVisibility(View.GONE);
        }
    }

}
