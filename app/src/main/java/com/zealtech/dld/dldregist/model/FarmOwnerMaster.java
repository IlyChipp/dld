package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/28/2017 AD.
 */
@Parcel
public class FarmOwnerMaster implements APIResponse{
    //DBHelper
    public static final String TABLE = "farm_owner";

    String responseCode;
    String responseMessage;
    ArrayList<OwnerEntity> data;

    public FarmOwnerMaster() { }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<OwnerEntity> getData() {
        return data;
    }

    public void setData(ArrayList<OwnerEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class OwnerEntity{
        int farm_Owner_Type_ID;
        String farm_Owner_Type_Name;
        int orderIndex;
        int status_ID;

        public OwnerEntity() { }

        public int getFarm_Owner_Type_ID() {
            return farm_Owner_Type_ID;
        }

        public void setFarm_Owner_Type_ID(int farm_Owner_Type_ID) {
            this.farm_Owner_Type_ID = farm_Owner_Type_ID;
        }

        public String getFarm_Owner_Type_Name() {
            return farm_Owner_Type_Name;
        }

        public void setFarm_Owner_Type_Name(String farm_Owner_Type_Name) {
            this.farm_Owner_Type_Name = farm_Owner_Type_Name;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }
    }
    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String FARM_OWNER_TYPE_ID = "farm_Owner_Type_ID";
        public static final String FARM_OWNER_TYPE_NAME = "farm_Owner_Type_Name";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
    }
}
