package com.zealtech.dld.dldregist.util;

import com.zealtech.dld.dldregist.model.newFarmer.ResponseInsertFarmer;

/**
 * Created by Jirat on 10/28/2017.
 */

public interface UpdateListener {
    public void onSuccess(String message,ResponseInsertFarmer responseInsertFarmer);
    public void onError(String message,ResponseInsertFarmer responseInsertFarmer);
}
