package com.zealtech.dld.dldregist.widget;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.util.DataValidator;
import com.zealtech.dld.dldregist.util.ValidatingView;

/**
 * Created by dev on 9/27/17.
 */

public class ValidatingEditText extends android.support.v7.widget.AppCompatEditText
    implements ValidatingView
{
    protected DataValidator<CharSequence> dataValidator;

    public final DataValidator<CharSequence> getDataValidator() {
        return dataValidator;
    }

    public final void setDataValidator(DataValidator<CharSequence> dataValidator) {
        this.dataValidator = dataValidator;
    }

    public ValidatingEditText(Context context) { this(context, null); init();}

    public ValidatingEditText(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.textViewStyle);
        init();
    }

    public ValidatingEditText(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init();
        // default validator always return null (success)
        dataValidator = new DataValidator<CharSequence>() {
            @Override
            public String validate(CharSequence data) {
                return null;
            }
        };
    }

    @Override
    public void onFocusChanged(boolean gainFocus, int direction,
                               Rect previouslyFocusedRect)
    {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
        String error = getDataValidationError();
        if (error == null)
        {
            //setBackground(null);
            setError(null);
        } else if (!gainFocus) {
            //setBackground(ContextCompat.getDrawable(getContext(),
            //        R.drawable.back));
            setError(error);
        }
    }

    @Override
    public boolean isDataValid() {
        return dataValidator.validate(getText()) == null;
    }

    @Override
    public String getDataValidationError() {
        return dataValidator.validate(getText());
    }

    @Override
    public void showError() {
        setError(getDataValidationError());
    }

    public void init() {
//        setTextSize(getResources().getDimension(R.dimen.font_edit_text));
        setGravity(Gravity.CENTER_VERTICAL);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), getResources().getString(R.string.font_en_light));
        setTypeface(tf, 1);
    }


}
