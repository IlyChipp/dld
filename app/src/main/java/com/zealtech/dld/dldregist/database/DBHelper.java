package com.zealtech.dld.dldregist.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.zealtech.dld.dldregist.model.AmphurMaster;
import com.zealtech.dld.dldregist.model.AmphurMaster.AmphurEntity;
import com.zealtech.dld.dldregist.model.CompanyTypeMaster;
import com.zealtech.dld.dldregist.model.CompanyTypeMaster.CompanyEntity;
import com.zealtech.dld.dldregist.model.FarmModel;
import com.zealtech.dld.dldregist.model.FarmModel.Column;
import com.zealtech.dld.dldregist.model.FarmModel.FarmEntity;
import com.zealtech.dld.dldregist.model.FarmOwnerMaster;
import com.zealtech.dld.dldregist.model.FarmOwnerMaster.OwnerEntity;
import com.zealtech.dld.dldregist.model.FarmProblemMaster;
import com.zealtech.dld.dldregist.model.FarmProblemMaster.FarmProblemEntity;
import com.zealtech.dld.dldregist.model.FarmerModel;
import com.zealtech.dld.dldregist.model.FarmerModel.FarmerEntity;
import com.zealtech.dld.dldregist.model.FarmerProblemMaster;
import com.zealtech.dld.dldregist.model.FarmerProblemMaster.FarmerProblemEntity;
import com.zealtech.dld.dldregist.model.FarmerTypeMaster;
import com.zealtech.dld.dldregist.model.FarmerTypeMaster.FarmerTypeEntity;
import com.zealtech.dld.dldregist.model.FeedKindMaster;
import com.zealtech.dld.dldregist.model.FeedKindMaster.FeedKindEntity;
import com.zealtech.dld.dldregist.model.FeedModel;
import com.zealtech.dld.dldregist.model.FeedModel.FeedEntity;
import com.zealtech.dld.dldregist.model.FeedSubTypeFormatMaster;
import com.zealtech.dld.dldregist.model.FeedSubTypeFormatMaster.SubTypeFormatEntity;
import com.zealtech.dld.dldregist.model.FeedSubTypeMaster;
import com.zealtech.dld.dldregist.model.FeedSubTypeMaster.SubTypeEntity;
import com.zealtech.dld.dldregist.model.FeedTypeMaster;
import com.zealtech.dld.dldregist.model.FeedTypeMaster.FeedTypeEntity;
import com.zealtech.dld.dldregist.model.HelpMaster;
import com.zealtech.dld.dldregist.model.HelpMaster.HelpEntity;
import com.zealtech.dld.dldregist.model.HoldingLandMaster;
import com.zealtech.dld.dldregist.model.HoldingLandMaster.LandEntity;
import com.zealtech.dld.dldregist.model.LivestockMaster;
import com.zealtech.dld.dldregist.model.LivestockMaster.LivestockEntity;
import com.zealtech.dld.dldregist.model.LivestockModel;
import com.zealtech.dld.dldregist.model.LivestockTypeMaster;
import com.zealtech.dld.dldregist.model.LivestockTypeMaster.TypeEntity;
import com.zealtech.dld.dldregist.model.MainJobMaster;
import com.zealtech.dld.dldregist.model.MainJobMaster.JobEntity;
import com.zealtech.dld.dldregist.model.PrefixMaster;
import com.zealtech.dld.dldregist.model.PrefixMaster.PrefixEntity;
import com.zealtech.dld.dldregist.model.ProvinceMaster;
import com.zealtech.dld.dldregist.model.ProvinceMaster.ProvinceEntity;
import com.zealtech.dld.dldregist.model.SecondJobMaster;
import com.zealtech.dld.dldregist.model.SecondJobMaster.SecondJobEntity;
import com.zealtech.dld.dldregist.model.StandardMaster;
import com.zealtech.dld.dldregist.model.StandardMaster.StandardEntity;
import com.zealtech.dld.dldregist.model.SupportStandardMaster;
import com.zealtech.dld.dldregist.model.SupportStandardMaster.SupportEntity;
import com.zealtech.dld.dldregist.model.TambolMaster;
import com.zealtech.dld.dldregist.model.TambolMaster.TambolEntity;
import com.zealtech.dld.dldregist.model.VillageMaster;
import com.zealtech.dld.dldregist.model.VillageMaster.VillageEntity;
import com.zealtech.dld.dldregist.util.AppUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fanqfang on 9/26/2017 AD.
 */

public class DBHelper extends SQLiteOpenHelper {

    private final String TAG = getClass().getSimpleName();

    private SQLiteDatabase sqLiteDatabase;

    Context curContext;

    private static final String CREATE_FARMER_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "%s REAL, " + //FARMER_ID
                    "%s INTEGER, " + //AMPHUR_ID
                    "%s TEXT, " + //BIRTHDAY
                    "%s INTEGER, " + //COMPANY_TYPE_ID
                    "%s TEXT, " + //CREATE_BY
                    "%s TEXT, " + //CREATE_DATE
                    "%s REAL, " + //DEBTAMOUNT
                    "%s REAL, " + //DEBTSAMOUNT_IN
                    "%s REAL, " + //DEBTSAMOUNT_OUT
                    "%s TEXT, " + //EMAIL
                    "%s INTEGER, " + //FARMER_GROUP_ID
                    "%s INTEGER, " + //FARMER_PROBLEM_ID
                    "%s TEXT, " + //FARMER_PROBLEM_OTHER
                    "%s INTEGER, " + //FARMER_STATUS_ID
                    "%s INTEGER, " + //FARMER_TYPE_ID
                    "%s TEXT, " + //FIRST_NAME
                    "%s INTEGER, " + //GENDER_ID
                    "%s INTEGER, " + //GOVERNMENT_HELP_ID
                    "%s INTEGER, " + //HOLDING_EQUIPMENT
                    "%s INTEGER, " + //HOLDING_LAND_ID
                    "%s REAL, " + //HOME_ID
                    "%s TEXT, " + //HOME_NO
                    "%s INTEGER, " + //IMAGE_ID
                    "%s INTEGER, " + //IS_AGENCY
                    "%s INTEGER, " + //IS_CANCEL
                    "%s INTEGER, " + //IS_DEAD
                    "%s TEXT, " + //LASTNAME
                    "%s REAL, " + //LATITUDE
                    "%s REAL, " + //LONGITUDE
                    "%s INTEGER, " + //MAIN_JOB_ID
                    "%s INTEGER, " + //MARITAL_STATUS_ID
                    "%s TEXT, " + //MOBILE
                    "%s TEXT, " + //PHONE
                    "%s REAL, " + //PID
                    "%s TEXT, " + //POSTCODE
                    "%s INTEGER, " + //PREFIX_ID
                    "%s INTEGER, " + //PROVINCE_ID
                    "%s REAL, " + //REVENUE
                    "%s TEXT, " + //ROAD
                    "%s INTEGER, " + //SECOND_JOB_ID
                    "%s TEXT, " + //SOI
                    "%s INTEGER, " + //STATUS_ID
                    "%s INTEGER, " + //TAMBOL_ID
                    "%s INTEGER, " + //TAX_ID
                    "%s TEXT, " + //UPDATE_BY
                    "%s TEXT, " + //UPDATE_DATE
                    "%s TEXT, " + //VILLAGE
                    "%s INTEGER, " + //VILLAGE_ID
                    "%s INTEGER) ", //LOCAL_ONLY

            FarmerModel.TABLE,
            FarmerModel.Column.ID,
            FarmerModel.Column.FARMER_ID,
            FarmerModel.Column.AMPHUR_ID,
            FarmerModel.Column.BIRTHDAY,
            FarmerModel.Column.COMPANY_TYPE_ID,
            FarmerModel.Column.CREATE_BY,
            FarmerModel.Column.CREATE_DATE,
            FarmerModel.Column.DEBTAMOUNT,
            FarmerModel.Column.DEBTSAMOUNT_IN,
            FarmerModel.Column.DEBTSAMOUNT_OUT,
            FarmerModel.Column.EMAIL,
            FarmerModel.Column.FARMER_GROUP_ID,
            FarmerModel.Column.FARMER_PROBLEM_ID,
            FarmerModel.Column.FARMER_PROBLEM_OTHER,
            FarmerModel.Column.FARMER_STATUS_ID,
            FarmerModel.Column.FARMER_TYPE_ID,
            FarmerModel.Column.FIRST_NAME,
            FarmerModel.Column.GENDER_ID,
            FarmerModel.Column.GOVERNMENT_HELP_ID,
            FarmerModel.Column.HOLDING_EQUIPMENT,
            FarmerModel.Column.HOLDING_LAND_ID,
            FarmerModel.Column.HOME_ID,
            FarmerModel.Column.HOME_NO,
            FarmerModel.Column.IMAGE_ID,
            FarmerModel.Column.IS_AGENCY,
            FarmerModel.Column.IS_CANCEL,
            FarmerModel.Column.IS_DEAD,
            FarmerModel.Column.LASTNAME,
            FarmerModel.Column.LATITUDE,
            FarmerModel.Column.LONGITUDE,
            FarmerModel.Column.MAIN_JOB_ID,
            FarmerModel.Column.MARITAL_STATUS_ID,
            FarmerModel.Column.MOBILE,
            FarmerModel.Column.PHONE,
            FarmerModel.Column.PID,
            FarmerModel.Column.POSTCODE,
            FarmerModel.Column.PREFIX_ID,
            FarmerModel.Column.PROVINCE_ID,
            FarmerModel.Column.REVENUE,
            FarmerModel.Column.ROAD,
            FarmerModel.Column.SECOND_JOB_ID,
            FarmerModel.Column.SOI,
            FarmerModel.Column.STATUS_ID,
            FarmerModel.Column.TAMBOL_ID,
            FarmerModel.Column.TAX_ID,
            FarmerModel.Column.UPDATE_BY,
            FarmerModel.Column.UPDATE_DATE,
            FarmerModel.Column.VILLAGE,
            FarmerModel.Column.VILLAGE_ID,
            FarmerModel.Column.LOCAL_ONLY);

    private static final String CREATE_FARM_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT" +
                    ", %s REAL" +//FARM_ID
                    ", %s INTEGER" + //AMPHUR_ID
                    ", %s REAL" +//ANIMAL_WORTH
                    ", %s REAL" +//AREA_RAI
                    ", %s REAL" +//AREA_NGAN
                    ", %s TEXT" +//AREA_TYPE_ID
                    ", %s TEXT" +//CANCEL_BY
                    ", %s TEXT" +//CANCEL_DATE
                    ", %s TEXT" +//CREATE_BY
                    ", %s TEXT" +//CREATE_DATE
                    ", %s TEXT" +//EMAIL
                    ", %s TEXT" +//FARM_NAME
                    ", %s INTEGER" +//FARM_OWNER_TYPE_ID
                    ", %s TEXT" +//FARM_PROBLEM_DESC
                    ", %s TEXT," +//FARM_STANDARD_NUMBER
                    "  %s REAL" +//FARMER_ID
                    ", %s TEXT" +//HOME_NO
                    ", %s INTEGER" +//IS_CANCEL
                    ", %s REAL" +//LATITUDE
                    ", %s REAL" +//LONGITUDE
                    ", %s TEXT " + //MOO
                    ", %s TEXT" +//MOBILE
                    ", %s INTEGER" +//ORDER_INDEX
                    ", %s TEXT" +//PHONE
                    ", %s TEXT " +//POSTCODE
                    ", %s INTEGER" +//PROVINCE_ID
                    ", %s REAL" +//REVENUE_OF_LIVESTOCK
                    ", %s TEXT" +//ROAD
                    ", %s TEXT" +//SOI
                    ", %s INTEGER" +//STANDARD_ID
                    ", %s INTEGER" +//STATUS_ID
                    ", %s INTEGER" +//SUPPORT_STANDARD_ID
                    ", %s INTEGER" +//TAMBOL_ID
                    ", %s TEXT," +//UPDATE_BY
                    "  %s TEXT" +//UPDATE_DATE
                    ", %s INTEGER" +//VILLAGE_ID
                    ", %s TEXT" +//VILLAGE_NAME
                    ", %s TEXT" +//WORKER_ID
                    ", %s INTEGER) ", //LOCAL_ONLY
            FarmModel.TABLE,
            FarmModel.Column.ID,
            FarmModel.Column.FARM_ID,
            FarmModel.Column.AMPHUR_ID,
            FarmModel.Column.ANIMAL_WORTH,
            FarmModel.Column.AREA_RAI,
            FarmModel.Column.AREA_NGAN,
            FarmModel.Column.AREA_TYPE_ID,
            FarmModel.Column.CANCEL_BY,
            FarmModel.Column.CANCEL_DATE,
            FarmModel.Column.CREATE_BY,
            FarmModel.Column.CREATE_DATE,
            FarmModel.Column.EMAIL,
            FarmModel.Column.FARM_NAME,
            FarmModel.Column.FARM_OWNER_TYPE_ID,
            FarmModel.Column.FARM_PROBLEM_DESC,
            FarmModel.Column.FARM_STANDARD_NUMBER,
            FarmModel.Column.FARMER_ID,
            FarmModel.Column.HOME_NO,
            FarmModel.Column.IS_CANCEL,
            FarmModel.Column.LATITUDE,
            FarmModel.Column.LONGITUDE,
            FarmModel.Column.MOO,
            FarmModel.Column.MOBILE,
            FarmModel.Column.ORDER_INDEX,
            FarmModel.Column.PHONE,
            FarmModel.Column.POSTCODE,
            FarmModel.Column.PROVINCE_ID,
            FarmModel.Column.REVENUE_OF_LIVESTOCK,
            FarmModel.Column.ROAD,
            FarmModel.Column.SOI,
            FarmModel.Column.STANDARD_ID,
            FarmModel.Column.STATUS_ID,
            FarmModel.Column.SUPPORT_STANDARD_ID,
            FarmModel.Column.TAMBOL_ID,
            FarmModel.Column.UPDATE_BY,
            FarmModel.Column.UPDATE_DATE,
            FarmModel.Column.VILLAGE_ID,
            FarmModel.Column.VILLAGE_NAME,
            FarmModel.Column.WORKER_ID,
            FarmModel.Column.LOCAL_ONLY);

    private static final String CREATE_PREFIX_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s INTEGER, %s TEXT,%s TEXT, %s INTEGER)",
            PrefixMaster.TABLE,
            PrefixMaster.Column.ID,
            PrefixMaster.Column.PREFIX_ID,
            PrefixMaster.Column.ORDER_INDEX,
            PrefixMaster.Column.PREFIX_NAME_TH,
            PrefixMaster.Column.PREFIX_NAME_EN,
            PrefixMaster.Column.STATUS_ID);

    private static final String CREATE_PROVINCE_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "%s INTEGER, " + //PROVINCE_ID
                    "%s INTEGER, " + //COUNTRYRANCH_ID
                    "%s INTEGER, " + //GEO_ID
                    "%s REAL, " + //LATITUDE
                    "%s REAL, " + //LONGITUDE
                    "%s INTEGER, " + //ORDER_INDEX
                    "%s TEXT, " + //PROVINCE_NAME_EN
                    "%s TEXT, " + //PROVINCE_NAME_TH
                    "%s INTEGER, " + //PROVINCE_CODE
                    "%s INTEGER)", //STATUS_ID
            ProvinceMaster.TABLE,
            ProvinceMaster.Column.ID,
            ProvinceMaster.Column.PROVINCE_ID,
            ProvinceMaster.Column.COUNTRYRANCH_ID,
            ProvinceMaster.Column.GEO_ID,
            ProvinceMaster.Column.LATITUDE,
            ProvinceMaster.Column.LONGITUDE,
            ProvinceMaster.Column.ORDER_INDEX,
            ProvinceMaster.Column.PROVINCE_NAME_EN,
            ProvinceMaster.Column.PROVINCE_NAME_TH,
            ProvinceMaster.Column.PROVINCE_CODE,
            ProvinceMaster.Column.STATUS_ID);

    private static final String CREATE_AMPHUR_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "%s INTEGER, " + //AMPHUR_ID
                    "%s INTEGER, " + //AMPHUR_CODE
                    "%s TEXT, " + //AMPHUR_NAME_EN
                    "%s TEXT, " + //AMPHUR_NAME_TH
                    "%s REAL, " + //LATITUDE
                    "%s REAL, " + //LONGITUDE
                    "%s INTEGER, " + //ORDER_INDEX
                    "%s INTEGER, " + //PROVINCE_ID
                    "%s INTEGER," + //STATUS_ID
                    "%s TEXT)", //ZIPCODE
            AmphurMaster.TABLE,
            AmphurMaster.Column.ID,
            AmphurMaster.Column.AMPHUR_ID,
            AmphurMaster.Column.AMPHUR_CODE,
            AmphurMaster.Column.AMPHUR_NAME_EN,
            AmphurMaster.Column.AMPHUR_NAME_TH,
            AmphurMaster.Column.LATITUDE,
            AmphurMaster.Column.LONGITUDE,
            AmphurMaster.Column.ORDER_INDEX,
            AmphurMaster.Column.PROVINCE_ID,
            AmphurMaster.Column.STATUS_ID,
            AmphurMaster.Column.ZIPCODE);

    private static final String CREATE_TAMBOL_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "%s INTEGER, " + //TAMBOL_ID
                    "%s INTEGER, " + //AMPHUR_ID
                    "%s REAL, " + //LATITUDE
                    "%s REAL, " + //LONGITUDE
                    "%s INTEGER, " + //ORDER_INDEX
                    "%s INTEGER, " + //STATUS_ID
                    "%s INTEGER, " + //TAMBOL_CODE
                    "%s TEXT, " + //TAMBOL_NAME_EN
                    "%s TEXT," + //TAMBOL_NAME_TH
                    "%s TEXT)", //ZIPCODE
            TambolMaster.TABLE,
            TambolMaster.Column.ID,
            TambolMaster.Column.TAMBOL_ID,
            TambolMaster.Column.AMPHUR_ID,
            TambolMaster.Column.LATITUDE,
            TambolMaster.Column.LONGITUDE,
            TambolMaster.Column.ORDER_INDEX,
            TambolMaster.Column.STATUS_ID,
            TambolMaster.Column.TAMBOL_CODE,
            TambolMaster.Column.TAMBOL_NAME_EN,
            TambolMaster.Column.TAMBOL_NAME_TH,
            TambolMaster.Column.ZIPCODE);

    private static final String CREATE_VILLAGE_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "%s INTEGER, " + //VILLAGE_ID
//                    "%s INTEGER, " + //AMPHUR_ID
                    "%s REAL, " + //LATITUDE
                    "%s REAL, " + //LONGITUDE
                    "%s INTEGER, " + //MOO
                    "%s INTEGER, " + //ORDER_INDEX
                    "%s INTEGER, " + //STATUS_ID
//                    "%s INTEGER, " + //TAMBOL_CODE
                    "%s INTEGER, " + //TAMBOL_ID
//                    "%s TEXT, " + //TAMBOL_NAME_EN
//                    "%s TEXT, " + //TAMBOL_NAME_TH
                    "%s INTEGER, " +//VILLAGE_CODE
                    "%s TEXT )", //VILLAGE_NAME
//                    "%s TEXT )", //ZIPCODE
            VillageMaster.TABLE,
            VillageMaster.Column.ID,
            VillageMaster.Column.VILLAGE_ID,
//            VillageMaster.Column.AMPHUR_ID,
            VillageMaster.Column.LATITUDE,
            VillageMaster.Column.LONGITUDE,
            VillageMaster.Column.MOO,
            VillageMaster.Column.ORDER_INDEX,
            VillageMaster.Column.STATUS_ID,
//            VillageMaster.Column.TAMBOL_CODE,
            VillageMaster.Column.TAMBOL_ID,
//            VillageMaster.Column.TAMBOL_NAME_EN,
//            VillageMaster.Column.TAMBOL_NAME_TH,
            VillageMaster.Column.VILLAGE_CODE,
            VillageMaster.Column.VILLAGE_NAME);
//            VillageMaster.Column.ZIPCODE);

    private static final String CREATE_FARMER_PROBLEM_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT,%s INTEGER, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER)",
            FarmerProblemMaster.TABLE,
            FarmerProblemMaster.Column.ID,
            FarmerProblemMaster.Column.FARMER_PROBLEM_ID,
            FarmerProblemMaster.Column.FARMER_PROBLEM_DESC,
            FarmerProblemMaster.Column.FARMER_PROBLEM_NAME,
            FarmerProblemMaster.Column.ORDER_INDEX,
            FarmerProblemMaster.Column.STATUS_ID);

    private static final String CREATE_FARMER_TYPE_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s INTEGER, %s INTEGER)",
            FarmerTypeMaster.TABLE,
            FarmerTypeMaster.Column.ID,
            FarmerTypeMaster.Column.FARMER_TYPE_ID,
            FarmerTypeMaster.Column.FARMER_TYPE_NAME,
            FarmerTypeMaster.Column.ORDER_INDEX,
            FarmerTypeMaster.Column.STATUS_ID);

    private static final String CREATE_FARM_OWNER_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s INTEGER, %s INTEGER)",
            FarmOwnerMaster.TABLE,
            FarmOwnerMaster.Column.ID,
            FarmOwnerMaster.Column.FARM_OWNER_TYPE_ID,
            FarmOwnerMaster.Column.FARM_OWNER_TYPE_NAME,
            FarmOwnerMaster.Column.ORDER_INDEX,
            FarmOwnerMaster.Column.STATUS_ID);

    private static final String CREATE_FARM_PROBLEM_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT,%s INTEGER, %s TEXT, %s INTEGER, %s INTEGER)",
            FarmProblemMaster.TABLE,
            FarmProblemMaster.Column.ID,
            FarmProblemMaster.Column.FARM_PROBLEM_ID,
            FarmProblemMaster.Column.FARM_PROBLEM_NAME,
            FarmProblemMaster.Column.ORDER_INDEX,
            FarmProblemMaster.Column.STATUS_ID);

    private static final String CREATE_FEED_KIND_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER)",
            FeedKindMaster.TABLE,
            FeedKindMaster.Column.ID,
            FeedKindMaster.Column.FEED_KIND_ID,
            FeedKindMaster.Column.FEED_KIND_NAME_EN,
            FeedKindMaster.Column.FEED_KIND_NAME_TH,
            FeedKindMaster.Column.ORDER_INDEX,
            FeedKindMaster.Column.STATUS_ID);

    private static final String CREATE_FEED_SUBTYPE_FORMAT_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s INTEGER, %s INTEGER)",
            FeedSubTypeFormatMaster.TABLE,
            FeedSubTypeFormatMaster.Column.ID,
            FeedSubTypeFormatMaster.Column.FEED_SUBTYPE_FORMAT_ID,
            FeedSubTypeFormatMaster.Column.FEED_SUBTYPE_FORMAT_NAME,
            FeedSubTypeFormatMaster.Column.ORDER_INDEX,
            FeedSubTypeFormatMaster.Column.STATUS_ID);

    private static final String CREATE_FEED_SUBTYPE_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s INTEGER, %s INTEGER)",
            FeedSubTypeMaster.TABLE,
            FeedSubTypeMaster.Column.ID,
            FeedSubTypeMaster.Column.FEED_SUBTYPE_ID,
            FeedSubTypeMaster.Column.FEED_SUBTYPE_NAME,
            FeedSubTypeMaster.Column.ORDER_INDEX,
            FeedSubTypeMaster.Column.STATUS_ID);

    private static final String CREATE_FEED_TYPE_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s INTEGER, %s INTEGER)",
            FeedTypeMaster.TABLE,
            FeedTypeMaster.Column.ID,
            FeedTypeMaster.Column.FEED_TYPE_ID,
            FeedTypeMaster.Column.FEED_TYPE_NAME,
            FeedTypeMaster.Column.ORDER_INDEX,
            FeedTypeMaster.Column.STATUS_ID);

    private static final String CREATE_FEED_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "%s TEXT, " + //CREATE_BY
                    "%s TEXT, " + //CREATE_DATE
                    "%s INTEGER, " + //FARM_ID
                    "%s REAL, " + //FEED_AREA_NGAN
                    "%s REAL, " + //FEED_AREA_RAI
                    "%s REAL, " + //FEED_AREA_WA
                    "%s INTEGER, " + //FEED_ID
                    "%s INTEGER, " + //FEED_KIND_ID
                    "%s TEXT, " + //FEED_KIND_NAME
                    "%s TEXT, " + //FEED_KIND_OTHER
                    "%s INTEGER, " + //FEED_SUBTYPE_FORMAT_ID
                    "%s INTEGER, " + //FEED_SUBTYPE_ID
                    "%s TEXT, " + //FEED_SUBTYPE_NAME
                    "%s INTEGER, " + //FEED_TYPE_ID
                    "%s TEXT, " + //FEED_TYPE_NAME
                    "%s TEXT, " + //FEED_TYPE_OTHER
                    "%s INTEGER, " + //FEED_WATER_TYPE_ID
                    "%s TEXT, " + //FEED_WATER_TYPE_NAME
                    "%s TEXT, " + //MS_FEED_SUBTYPE_FORMAT_NAME
                    "%s INTEGER, " + //ORDER_INDEX
                    "%s INTEGER, " + //STATUS_ID
                    "%s TEXT, " + //STATUS_NAME
                    "%s TEXT, " + //UPDATE_BY
                    "%s TEXT," + //UPDATE_DATE
                    "%s INTEGER )",  //LOCAL_ONLY
            FeedModel.TABLE,
            FeedModel.Column.ID,
            FeedModel.Column.CREATE_BY,
            FeedModel.Column.CREATE_DATE,
            FeedModel.Column.FARM_ID,
            FeedModel.Column.FEED_AREA_NGAN,
            FeedModel.Column.FEED_AREA_RAI,
            FeedModel.Column.FEED_AREA_WA,
            FeedModel.Column.FEED_ID,
            FeedModel.Column.FEED_KIND_ID,
            FeedModel.Column.FEED_KIND_NAME,
            FeedModel.Column.FEED_KIND_OTHER,
            FeedModel.Column.FEED_SUBTYPE_FORMAT_ID,
            FeedModel.Column.FEED_SUBTYPE_ID,
            FeedModel.Column.FEED_SUBTYPE_NAME,
            FeedModel.Column.FEED_TYPE_ID,
            FeedModel.Column.FEED_TYPE_NAME,
            FeedModel.Column.FEED_TYPE_OTHER,
            FeedModel.Column.FEED_WATER_TYPE_ID,
            FeedModel.Column.FEED_WATER_TYPE_NAME,
            FeedModel.Column.MS_FEED_SUBTYPE_FORMAT_NAME,
            FeedModel.Column.ORDER_INDEX,
            FeedModel.Column.STATUS_ID,
            FeedModel.Column.STATUS_NAME,
            FeedModel.Column.UPDATE_BY,
            FeedModel.Column.UPDATE_DATE,
            FeedModel.Column.LOCAL_ONLY);

    private static final String CREATE_HELP_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT,%s INTEGER,%s INTEGER)",
            HelpMaster.TABLE,
            HelpMaster.Column.ID,
            HelpMaster.Column.GOVERNMENT_HELP_ID,
            HelpMaster.Column.GOVERNMENT_HELP_NAME,
            HelpMaster.Column.ORDER_INDEX,
            HelpMaster.Column.STATUS_ID);

    private static final String CREATE_HOLDING_LAND_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT,%s INTEGER,%s INTEGER)",
            HoldingLandMaster.TABLE,
            HoldingLandMaster.Column.ID,
            HoldingLandMaster.Column.HOLDING_LAND_ID,
            HoldingLandMaster.Column.HOLDING_LAND_NAME,
            HoldingLandMaster.Column.ORDER_INDEX,
            HoldingLandMaster.Column.STATUS_ID);

    private static final String CREATE_LIST_LIVESTOCK_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s TEXT, %s TEXT, %s TEXT,  %s TEXT, %s TEXT, %s TEXT," +
                    "%s INTEGER, %s INTEGER, %s TEXT, %s INTEGER, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER, %s INTEGER)",
            LivestockMaster.TABLE,
            LivestockMaster.Column.ID,
            LivestockMaster.Column.LIVESTOCK_TYPE_ID,
            LivestockMaster.Column.CREATE_BY,
            LivestockMaster.Column.CREATE_DATE,
            LivestockMaster.Column.LIVESTOCK_TYPE_CODE,
            LivestockMaster.Column.LIVESTOCK_TYPE_FLGTAIL,
            LivestockMaster.Column.LIVESTOCK_TYPE_FULLNAME,
            LivestockMaster.Column.LIVESTOCK_TYPE_NAME,
            LivestockMaster.Column.LIVESTOCK_TYPE_OLD_ID,
            LivestockMaster.Column.LIVESTOCK_TYPE_PARENT,
            LivestockMaster.Column.LIVESTOCK_TYPE_PRICE,
            LivestockMaster.Column.LIVESTOCK_TYPE_UNIT,
            LivestockMaster.Column.LIVESTOCK_TYPE_UNITLIMIT,
            LivestockMaster.Column.UPDATE_BY,
            LivestockMaster.Column.UPDATE_DATE,
            LivestockMaster.Column.STATUS_ID,
            LivestockMaster.Column.ORDER_INDEX,
            LivestockMaster.Column.LIVESTOCK_TYPE_ROOT);

    private static final String CREATE_MAIN_JOB_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT,%s INTEGER,%s INTEGER)",
            MainJobMaster.TABLE,
            MainJobMaster.Column.ID,
            MainJobMaster.Column.MAIN_JOB_ID,
            MainJobMaster.Column.MAIN_JOB_NAME,
            MainJobMaster.Column.ORDER_INDEX,
            MainJobMaster.Column.STATUS_ID);

    private static final String CREATE_SECOND_JOB_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT,%s INTEGER,%s INTEGER)",
            SecondJobMaster.TABLE,
            SecondJobMaster.Column.ID,
            SecondJobMaster.Column.SECOND_JOB_ID,
            SecondJobMaster.Column.SECOND_JOB_NAME,
            SecondJobMaster.Column.ORDER_INDEX,
            SecondJobMaster.Column.STATUS_ID);

    private static final String CREATE_LIVESTOCK_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s TEXT, %s INTEGER,%s INTEGER,%s INTEGER,%s INTEGER,%s TEXT, %s TEXT)",
            LivestockModel.TABLE,
            LivestockModel.Column.ID,
            LivestockModel.Column.LIVESTOCK_ID,
            LivestockModel.Column.CREATE_BY,
            LivestockModel.Column.CREATE_DATE,
            LivestockModel.Column.FARM_ID,
            LivestockModel.Column.LIVESTOCK_AMOUNT,
            LivestockModel.Column.LIVESTOCK_TYPE_ID,
            LivestockModel.Column.LIVESTOCK_VALUES,
            LivestockModel.Column.UPDATE_BY,
            LivestockModel.Column.UPDATE_DATE);

    private static final String CREATE_STANDARD_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT,%s INTEGER,%s INTEGER)",
            StandardMaster.TABLE,
            StandardMaster.Column.ID,
            StandardMaster.Column.STANDARD_ID,
            StandardMaster.Column.STANDARD_NAME,
            StandardMaster.Column.ORDER_INDEX,
            StandardMaster.Column.STATUS_ID);

    private static final String CREATE_SUPPORT_STANDARD_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT,%s INTEGER,%s TEXT,%s INTEGER,%s INTEGER)",
            SupportStandardMaster.TABLE,
            SupportStandardMaster.Column.ID,
            SupportStandardMaster.Column.SUPPORT_STANDARD_ID,
            SupportStandardMaster.Column.SUPPORT_STANDARD_NAME,
            SupportStandardMaster.Column.ORDER_INDEX,
            SupportStandardMaster.Column.STATUS_ID);

    private static final String CREATE_COMPANY_TYPE_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT,%s TEXT,%s INTEGER,%s INTEGER)",
            CompanyTypeMaster.TABLE,
            CompanyTypeMaster.Column._ID,
            CompanyTypeMaster.Column.COMPANY_TYPE_NAME,
            CompanyTypeMaster.Column.ID,
            CompanyTypeMaster.Column.IS_COMPANY);

    private static final String CREATE_LIVESTOCK_TYPE_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT,%s INTEGER,%s TEXT,%s INTEGER,%s INTEGER)",
            LivestockTypeMaster.TABLE,
            LivestockTypeMaster.Column.ID,
            LivestockTypeMaster.Column.FARM_STANDARD_LIVESTOCK_TYPE_ID,
            LivestockTypeMaster.Column.FARM_STANDARD_LIVESTOCK_TYPE_NAME,
            LivestockTypeMaster.Column.ORDER_INDEX,
            LivestockTypeMaster.Column.STATUS_ID);



    public DBHelper(Context context) {

        super(context, PrefixMaster.DATABASE_NAME, null, PrefixMaster.DATABASE_VERSION);

        this.curContext = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_FARMER_TABLE);
        db.execSQL(CREATE_PREFIX_TABLE);
        db.execSQL(CREATE_PROVINCE_TABLE);
        db.execSQL(CREATE_AMPHUR_TABLE);
        db.execSQL(CREATE_TAMBOL_TABLE);
        db.execSQL(CREATE_VILLAGE_TABLE);
        db.execSQL(CREATE_FARMER_PROBLEM_TABLE);
        db.execSQL(CREATE_FARMER_TYPE_TABLE);
        db.execSQL(CREATE_FARM_TABLE);
        db.execSQL(CREATE_FARM_OWNER_TABLE);
        db.execSQL(CREATE_FARM_PROBLEM_TABLE);
        db.execSQL(CREATE_FEED_KIND_TABLE);
        db.execSQL(CREATE_FEED_SUBTYPE_FORMAT_TABLE);
        db.execSQL(CREATE_FEED_SUBTYPE_TABLE);
        db.execSQL(CREATE_FEED_TYPE_TABLE);
        db.execSQL(CREATE_FEED_TABLE);
        db.execSQL(CREATE_HELP_TABLE);
        db.execSQL(CREATE_HOLDING_LAND_TABLE);
        db.execSQL(CREATE_LIST_LIVESTOCK_TABLE);
        db.execSQL(CREATE_MAIN_JOB_TABLE);
        db.execSQL(CREATE_SECOND_JOB_TABLE);
        db.execSQL(CREATE_LIVESTOCK_TABLE);
        db.execSQL(CREATE_STANDARD_TABLE);
        db.execSQL(CREATE_SUPPORT_STANDARD_TABLE);
        db.execSQL(CREATE_COMPANY_TYPE_TABLE);
        db.execSQL(CREATE_LIVESTOCK_TYPE_TABLE);

    }

    /**
     * This reads a file from the given Resource-Id and calls every line of it as a SQL-Statement
     *
     * @param context
     *
     * @param resourceId
     *  e.g. R.raw.food_db
     *
     * @return Number of SQL-Statements run
     * @throws IOException
     */
    public int insertFromFile(Context context, int resourceId) throws IOException {
        // Reseting Counter
        int result = 0;

        SQLiteDatabase db = this.getReadableDatabase();

        // Open the resource
        InputStream insertsStream = context.getResources().openRawResource(resourceId);
        BufferedReader insertReader = new BufferedReader(new InputStreamReader(insertsStream));


        db.beginTransaction();

        // Iterate through lines (assuming each insert has its own line and theres no other stuff)
        while (insertReader.ready()) {
            String insertStmt = insertReader.readLine();
            Log.d("STMT", insertStmt);
            db.execSQL(insertStmt);
            result++;
        }
        db.setTransactionSuccessful();
        db.endTransaction();


        insertReader.close();

        // returning number of inserted rows
        return result;
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public boolean initialized() {
        try {
            SQLiteDatabase db = this.getReadableDatabase();

        } catch (SQLiteException e) {
            // no database created yet
            return false;
        }
        // TODO: check that version in db matches version in web API
        return true;
    }

    public boolean isTablePopulated(String tableName) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            String selectQuery = "SELECT  * FROM " + tableName + ";";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor == null)
                return false;
            if (cursor.getCount() == 0)
                return false;
            return true;
        } finally {
            db.close();
        }
    }

    public void removeAndCreateLiveStockSubtype(String tableName){
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            db.execSQL("DROP TABLE IF EXISTS "+tableName);
            db.execSQL(CREATE_LIVESTOCK_TYPE_TABLE);
        } finally {
            db.close();
        }
    }

    public boolean isFarmerPopulated(BigDecimal id) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            String selectQuery = "SELECT  * FROM " + FarmerModel.TABLE + " WHERE " + FarmerModel.Column.PID + " = " + id;
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor == null)
                return false;
            if (cursor.getCount() == 0)
                return false;
            return true;
        } finally {
            db.close();
        }
    }

    public boolean isFarmPopulated(BigDecimal id, SQLiteDatabase db) {
        String selectQuery = "SELECT * FROM " + FarmModel.TABLE + " WHERE " +
                Column.FARM_ID + " = " + id + ";";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor == null)
            return false;
        if (cursor.getCount() == 0)
            return false;
        return true;
    }

    public boolean isFeedPopulated(int id, SQLiteDatabase db) {
        String selectQuery = "SELECT * FROM " + FeedModel.TABLE + " WHERE " +
                FeedModel.Column.FEED_ID + " = " + id + ";";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor == null)
            return false;
        if (cursor.getCount() == 0)
            return false;
        return true;
    }

//    public boolean isFeedPopulated(){
//
//    }

    static String getStringFromCursor(Cursor c, String col) {
        int idx = c.getColumnIndex(col);
        if (!c.isNull(idx))
            return c.getString(idx);
        return null;
    }

    static BigDecimal getBigDecimalFromCursor(Cursor c, String col) {
        int idx = c.getColumnIndex(col);
        if (!c.isNull(idx))
            return new BigDecimal(c.getDouble(idx));
        return null;
    }

    static Integer getIntFromCursor(Cursor c, String col) {
        int idx = c.getColumnIndex(col);
        if (!c.isNull(idx))
            return c.getInt(idx);
        return 0;
    }


    public ArrayList<PrefixEntity> getPrefixList() {
        ArrayList<PrefixEntity> prefixs = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(PrefixMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        while (!cursor.isAfterLast()) {
            prefixs.add(getPrefixInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return prefixs;
    }

    public PrefixEntity getPrefixInfo(Cursor c) {
        PrefixEntity prefix = new PrefixEntity();

        String prefix_ID = c.getString(c.getColumnIndex(PrefixMaster.Column.PREFIX_ID));
        String prefix_NameTh = c.getString(c.getColumnIndex(PrefixMaster.Column.PREFIX_NAME_TH));

        prefix.setPrefix_ID(Integer.parseInt(prefix_ID));
        prefix.setPrefix_NameTh(prefix_NameTh);

        return prefix;
    }

    public FarmerEntity getInfoFarmer(BigDecimal farmerId) {

        SQLiteDatabase db = this.getReadableDatabase();
        FarmerEntity farmer;
        String selectQuery = "SELECT  * FROM " + FarmerModel.TABLE + " WHERE " + FarmerModel.Column.FARMER_ID + " = " + farmerId;

//        String selectQuery = "SELECT  * FROM " + FarmModel.TABLE + "WHERE" + FarmerModel.Column.PID + "=" + pid;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        farmer = setFarmerInfo(cursor);

        db.close();

        return farmer;

    }

    public FarmerEntity setFarmerInfo(Cursor cursor) {

        FarmerEntity farmer = new FarmerEntity();

        farmer.setFarmer_ID(getBigDecimalFromCursor(cursor, FarmerModel.Column.FARMER_ID));
        farmer.setAmphur_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.AMPHUR_ID)));
        farmer.setBirthDay(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.BIRTHDAY)));
        farmer.setCompany_Type_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.COMPANY_TYPE_ID)));
        farmer.setCreateBy(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.CREATE_BY)));
        farmer.setCreateDate(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.CREATE_DATE)));
        farmer.setDebtAmount(getBigDecimalFromCursor(cursor, FarmerModel.Column.DEBTAMOUNT));
        farmer.setDebtsAmountIn(getBigDecimalFromCursor(cursor, FarmerModel.Column.DEBTSAMOUNT_IN));
        farmer.setDebtsAmountOut(getBigDecimalFromCursor(cursor, FarmerModel.Column.DEBTSAMOUNT_OUT));
        farmer.setEmail(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.EMAIL)));
        farmer.setFarmer_Group_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.FARMER_GROUP_ID)));
        farmer.setFarmer_Problem_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.FARMER_PROBLEM_ID)));
        farmer.setFarmer_Problem_Other(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.FARMER_PROBLEM_OTHER)));
        farmer.setFarmer_Status_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.FARMER_STATUS_ID)));
        farmer.setFarmer_Type_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.FARMER_TYPE_ID)));
        farmer.setFirst_Name(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.FIRST_NAME)));
        farmer.setGender_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.GENDER_ID)));
        farmer.setGovernment_Help_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.GOVERNMENT_HELP_ID)));
        farmer.setHolding_Equipment(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.HOLDING_EQUIPMENT)));
        farmer.setHolding_Land_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.HOLDING_LAND_ID)));
        farmer.setHome_ID(getBigDecimalFromCursor(cursor, FarmerModel.Column.HOME_ID));
        farmer.setHomeNo(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.HOME_NO)));
        farmer.setImage_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.IMAGE_ID)));
        farmer.setIsAgency(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.IS_AGENCY)));
        farmer.setIsCancel(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.IS_CANCEL)));
        farmer.setIsDead(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.IS_DEAD)));
        farmer.setLast_Name(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.LASTNAME)));
        farmer.setLatitude(getBigDecimalFromCursor(cursor, FarmerModel.Column.LATITUDE));
        farmer.setLongitude(getBigDecimalFromCursor(cursor, FarmerModel.Column.LONGITUDE));
        farmer.setMain_Job_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.MAIN_JOB_ID)));
        farmer.setMarital_Status_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.MARITAL_STATUS_ID)));
        farmer.setMobile(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.MOBILE)));
        farmer.setPhone(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.PHONE)));
        farmer.setPid(getBigDecimalFromCursor(cursor, FarmerModel.Column.PID));
        farmer.setPrefix_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.PREFIX_ID)));
        farmer.setProvince_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.PROVINCE_ID)));
        farmer.setRevenue(getBigDecimalFromCursor(cursor, FarmerModel.Column.REVENUE));
        farmer.setRoad(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.ROAD)));
        farmer.setSecond_Job_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.SECOND_JOB_ID)));
        farmer.setSoi(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.SOI)));
        farmer.setStatus_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.STATUS_ID)));
        farmer.setTambol_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.TAMBOL_ID)));
        farmer.setTax_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.TAX_ID)));
        farmer.setUpdateBy(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.UPDATE_BY)));
        farmer.setUpdateDate(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.UPDATE_DATE)));
        farmer.setVillage(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.VILLAGE)));
        farmer.setVillage_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.VILLAGE_ID)));

        return farmer;
    }

    public ArrayList<FarmerEntity> getListFarmer() {

        ArrayList<FarmerEntity> farmers = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + FarmModel.TABLE +  " ;";

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            farmers.add(getFarmerInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return farmers;
    }

    public FarmerEntity getFarmerInfo(Cursor c) {
        FarmerEntity farmer = new FarmerEntity();

        BigDecimal farmer_ID = getBigDecimalFromCursor(c, FarmerModel.Column.FARMER_ID);
        int amphur_ID = getIntFromCursor(c, FarmerModel.Column.AMPHUR_ID);
        String birthDay = getStringFromCursor(c, FarmerModel.Column.BIRTHDAY);
        int company_Type_ID = getIntFromCursor(c, FarmerModel.Column.COMPANY_TYPE_ID);
        String createBy = getStringFromCursor(c, FarmerModel.Column.CREATE_BY);
        String createDate = getStringFromCursor(c, FarmerModel.Column.CREATE_DATE);
        BigDecimal debtAmount = getBigDecimalFromCursor(c, FarmerModel.Column.DEBTAMOUNT);
        BigDecimal debtsAmountIn = getBigDecimalFromCursor(c, FarmerModel.Column.DEBTSAMOUNT_IN);
        BigDecimal debtsAmountOut = getBigDecimalFromCursor(c, FarmerModel.Column.DEBTSAMOUNT_OUT);
        String email = getStringFromCursor(c, FarmerModel.Column.EMAIL);
        int farmer_Group_ID = getIntFromCursor(c, FarmerModel.Column.FARMER_GROUP_ID);
        int farmer_Problem_ID = getIntFromCursor(c, FarmerModel.Column.FARMER_PROBLEM_ID);
        String farmer_Problem_Other = getStringFromCursor(c, FarmerModel.Column.FARMER_PROBLEM_OTHER);
        int farmer_Status_ID = getIntFromCursor(c, FarmerModel.Column.STATUS_ID);
        int farmer_Type_ID = getIntFromCursor(c, FarmerModel.Column.COMPANY_TYPE_ID);
        String first_Name = getStringFromCursor(c, FarmerModel.Column.FIRST_NAME);
        int gender_ID = getIntFromCursor(c, FarmerModel.Column.GENDER_ID);
        int government_Help_ID = getIntFromCursor(c, FarmerModel.Column.GOVERNMENT_HELP_ID);
        int holding_Equipment = getIntFromCursor(c, FarmerModel.Column.HOLDING_EQUIPMENT);
        int holding_Land_ID = getIntFromCursor(c, FarmerModel.Column.HOLDING_LAND_ID);
        BigDecimal home_ID = getBigDecimalFromCursor(c, FarmerModel.Column.HOME_ID);
        String homeNo = getStringFromCursor(c, FarmerModel.Column.HOME_NO);
        int image_ID = getIntFromCursor(c, FarmerModel.Column.IMAGE_ID);
        int isAgency = getIntFromCursor(c, FarmerModel.Column.IS_AGENCY);
        int isCancel = getIntFromCursor(c, FarmerModel.Column.IS_CANCEL);
        int isDead = getIntFromCursor(c, FarmerModel.Column.IS_DEAD);
        String last_Name = getStringFromCursor(c, FarmerModel.Column.LASTNAME);
//        BigDecimal latitude = getBigDecimalFromCursor(c, FarmerModel.Column.LATITUDE);
        BigDecimal latitude = getBigDecimalFromCursor(c, FarmerModel.Column.LATITUDE);
        BigDecimal longitude = getBigDecimalFromCursor(c, FarmerModel.Column.LONGITUDE);
        int main_Job_ID = getIntFromCursor(c, FarmerModel.Column.MAIN_JOB_ID);
        int marital_Status_ID = getIntFromCursor(c, FarmerModel.Column.MARITAL_STATUS_ID);
        String mobile = getStringFromCursor(c, FarmerModel.Column.MOBILE);
        String phone = getStringFromCursor(c, FarmerModel.Column.PHONE);
        BigDecimal pid = getBigDecimalFromCursor(c, FarmerModel.Column.PID);
        String postCode = getStringFromCursor(c, FarmerModel.Column.POSTCODE);
        int prefix_ID = getIntFromCursor(c, FarmerModel.Column.PREFIX_ID);
        int province_ID = getIntFromCursor(c, FarmerModel.Column.PROVINCE_ID);
        BigDecimal revenue = getBigDecimalFromCursor(c, FarmerModel.Column.REVENUE);
        String road = getStringFromCursor(c, FarmerModel.Column.ROAD);
        int second_Job_ID = getIntFromCursor(c, FarmerModel.Column.SECOND_JOB_ID);
        String soi = getStringFromCursor(c, FarmerModel.Column.SOI);
        int status_ID = getIntFromCursor(c, FarmerModel.Column.STATUS_ID);
        int tambol_ID = getIntFromCursor(c, FarmerModel.Column.TAMBOL_ID);
        int tax_ID = getIntFromCursor(c, FarmerModel.Column.TAX_ID);
        String updateBy = getStringFromCursor(c, FarmerModel.Column.UPDATE_BY);
        String updateDate = getStringFromCursor(c, FarmerModel.Column.UPDATE_DATE);
        String village = getStringFromCursor(c, FarmerModel.Column.VILLAGE);
        int village_ID = getIntFromCursor(c, FarmerModel.Column.VILLAGE_ID);


        farmer.setFarmer_ID(farmer_ID);
        farmer.setAmphur_ID(amphur_ID);
        farmer.setBirthDay(birthDay);
        farmer.setCompany_Type_ID(company_Type_ID);
        farmer.setCreateBy(createBy);
        farmer.setCreateDate(createDate);
        farmer.setDebtAmount(debtAmount);
        farmer.setDebtsAmountIn(debtsAmountIn);
        farmer.setDebtsAmountOut(debtsAmountOut);
        farmer.setEmail(email);
        farmer.setFarmer_Group_ID(farmer_Group_ID);
        farmer.setFarmer_Problem_ID(farmer_Problem_ID);
        farmer.setFarmer_Problem_Other(farmer_Problem_Other);
        farmer.setFarmer_Status_ID(farmer_Status_ID);
        farmer.setFarmer_Type_ID(farmer_Type_ID);
        farmer.setFirst_Name(first_Name);
        farmer.setGender_ID(gender_ID);
        farmer.setGovernment_Help_ID(government_Help_ID);
        farmer.setHolding_Equipment(holding_Equipment);
        farmer.setHolding_Land_ID(holding_Land_ID);
        farmer.setHome_ID(home_ID);
        farmer.setHomeNo(homeNo);
        farmer.setImage_ID(image_ID);
        farmer.setIsAgency(isAgency);
        farmer.setIsCancel(isCancel);
        farmer.setIsDead(isDead);
        farmer.setLast_Name(last_Name);
        farmer.setLatitude(latitude);
        farmer.setLongitude(longitude);
        farmer.setMain_Job_ID(main_Job_ID);
        farmer.setMarital_Status_ID(marital_Status_ID);
        farmer.setMobile(mobile);
        farmer.setPhone(phone);
        farmer.setPid(pid);
        farmer.setPostCode(postCode);
        farmer.setPrefix_ID(prefix_ID);
        farmer.setProvince_ID(province_ID);
        farmer.setRevenue(revenue);
        farmer.setRoad(road);
        farmer.setSecond_Job_ID(second_Job_ID);
        farmer.setSoi(soi);
        farmer.setStatus_ID(status_ID);
        farmer.setTambol_ID(tambol_ID);
        farmer.setTax_ID(tax_ID);
        farmer.setUpdateBy(updateBy);
        farmer.setUpdateDate(updateDate);
        farmer.setVillage(village);
        farmer.setVillage_ID(village_ID);

        return farmer;
    }

    public ArrayList<ProvinceEntity> getProvinceList() {

        SQLiteDatabase db = this.getWritableDatabase();

        ArrayList<ProvinceEntity> provinces = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + ProvinceMaster.TABLE+ " WHERE " + ProvinceMaster.Column.STATUS_ID + " = " + 1;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        while (!cursor.isAfterLast()) {
            provinces.add(getProvinceInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return provinces;
    }

    public ProvinceEntity getProvinceInfo(Cursor c) {

        ProvinceEntity province = new ProvinceEntity();

        String province_id = c.getString(c.getColumnIndex(ProvinceMaster.Column.PROVINCE_ID));
        String countryRanch_ID = c.getString(c.getColumnIndex(ProvinceMaster.Column.COUNTRYRANCH_ID));
        String province_NameTh = c.getString(c.getColumnIndex(ProvinceMaster.Column.PROVINCE_NAME_TH));
        String province_Code = c.getString(c.getColumnIndex(ProvinceMaster.Column.PROVINCE_CODE));

        province.setProvince_ID(Integer.parseInt(province_id));
        province.setCountryRanch_ID(Integer.parseInt(countryRanch_ID));
        province.setProvince_NameTh(province_NameTh);
        province.setProvince_Code(Integer.parseInt(province_Code));

        return province;
    }

    public ProvinceEntity getProvinceById(int province) {

        ProvinceEntity provinceEntity=new ProvinceEntity();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + ProvinceMaster.TABLE + " WHERE " + ProvinceMaster.Column.PROVINCE_ID + " = " + province;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        while (!cursor.isAfterLast()) {
            provinceEntity=getProvinceInfo(cursor);
            cursor.moveToNext();
        }
        db.close();

        return provinceEntity;
    }


    public ArrayList<AmphurEntity> getAmphurList() {

        ArrayList<AmphurEntity> amphurs = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + AmphurMaster.TABLE;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        while (!cursor.isAfterLast()) {
            amphurs.add(getAmphurInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return amphurs;
    }

    public ArrayList<AmphurEntity> getAmphur(int province) {

        ArrayList<AmphurEntity> amphurs = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + AmphurMaster.TABLE + " WHERE " + AmphurMaster.Column.PROVINCE_ID + " = " + province
                +" AND "+ AmphurMaster.Column.STATUS_ID + " = " + 1;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        while (!cursor.isAfterLast()) {
            amphurs.add(getAmphurInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return amphurs;
    }

    public AmphurEntity getAmphurInfo(Cursor c) {
        AmphurEntity amphur = new AmphurEntity();

        String amphur_ID = c.getString(c.getColumnIndex(AmphurMaster.Column.AMPHUR_ID));
        String amphur_Code = c.getString(c.getColumnIndex(AmphurMaster.Column.AMPHUR_CODE));
        String amphur_NameTh = c.getString(c.getColumnIndex(AmphurMaster.Column.AMPHUR_NAME_TH));
        String province_ID = c.getString(c.getColumnIndex(AmphurMaster.Column.PROVINCE_ID));

        amphur.setAmphur_ID(Integer.parseInt(amphur_ID));
        amphur.setAmphur_Code(Integer.parseInt(amphur_Code));
        amphur.setAmphur_nameTh(amphur_NameTh);
        amphur.setProvince_ID(Integer.parseInt(province_ID));

        return amphur;
    }

    public AmphurEntity getAmphurById(int id) {

        AmphurEntity amphurEntity=new AmphurEntity();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + AmphurMaster.TABLE + " WHERE " + AmphurMaster.Column.AMPHUR_ID + " = " + id;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        while (!cursor.isAfterLast()) {
            amphurEntity=getAmphurInfo(cursor);
            cursor.moveToNext();
        }
        db.close();

        return amphurEntity;
    }

    public ArrayList<TambolEntity> getTambolList() {

        ArrayList<TambolEntity> tambols = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TambolMaster.TABLE;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            tambols.add(getTabolInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return tambols;
    }

    public TambolEntity getTambolWithID(int tambolID) {

        ArrayList<TambolEntity> tambols = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TambolMaster.TABLE + " WHERE " + TambolMaster.Column.TAMBOL_ID + " = " + tambolID;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            tambols.add(getTabolInfo(cursor));
            cursor.moveToNext();
        }
        db.close();
        if (tambols.size() > 0) {
            return tambols.get(0);
        }

        return null;
    }

    public ArrayList<TambolEntity> getTambol(int amphur) {

        ArrayList<TambolEntity> tambols = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TambolMaster.TABLE + " WHERE " + TambolMaster.Column.AMPHUR_ID + " = " + amphur
                +" AND "+ TambolMaster.Column.STATUS_ID + " = " + 1;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            tambols.add(getTabolInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return tambols;
    }

    public TambolEntity getTabolInfo(Cursor c) {
        TambolEntity tambol = new TambolEntity();

        String tambol_ID = c.getString(c.getColumnIndex(TambolMaster.Column.TAMBOL_ID));
        String amphur_ID = c.getString(c.getColumnIndex(TambolMaster.Column.AMPHUR_ID));
        String latitude = c.getString(c.getColumnIndex(TambolMaster.Column.LATITUDE));
        String longitude = c.getString(c.getColumnIndex(TambolMaster.Column.LONGITUDE));
        String tambol_Code = c.getString(c.getColumnIndex(TambolMaster.Column.TAMBOL_CODE));
        String tambol_NameTh = c.getString(c.getColumnIndex(TambolMaster.Column.TAMBOL_NAME_TH));
        String tambol_NameEn = c.getString(c.getColumnIndex(TambolMaster.Column.TAMBOL_NAME_EN));
        String zipcode = c.getString(c.getColumnIndex(TambolMaster.Column.ZIPCODE));

        tambol.setTambol_ID(Integer.parseInt(tambol_ID));
        tambol.setAmphur_ID(Integer.parseInt(amphur_ID));
        tambol.setLatitude(AppUtils.tryParseDouble(latitude));
        tambol.setLongitude(AppUtils.tryParseDouble(longitude));
        tambol.setTambol_Code(Integer.parseInt(tambol_Code));
        tambol.setTambol_NameTh(tambol_NameTh);
        tambol.setTambol_NameEn(tambol_NameEn);
        tambol.setZipcode(zipcode);

        return tambol;
    }

    public TambolEntity getTambolById(int id) {

        TambolEntity tambolEntity=new TambolEntity();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TambolMaster.TABLE + " WHERE " + TambolMaster.Column.TAMBOL_ID + " = " + id;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        while (!cursor.isAfterLast()) {
            tambolEntity=getTabolInfo(cursor);
            cursor.moveToNext();
        }
        db.close();

        return tambolEntity;
    }

    public ArrayList<VillageEntity> getVillage(int tambol) {
        ArrayList<VillageEntity> villages = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + VillageMaster.TABLE + " WHERE " + VillageMaster.Column.TAMBOL_ID + " = " + tambol
                +" AND "+ VillageMaster.Column.STATUS_ID + " = " + 1;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            villages.add(getVillageInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return villages;
    }

    public VillageEntity getVillageInfo(Cursor c) {
        VillageEntity village = new VillageEntity();

        String village_ID = c.getString(c.getColumnIndex(VillageMaster.Column.VILLAGE_ID));
//        String amphur_ID = c.getString(c.getColumnIndex(VillageMaster.Column.AMPHUR_ID));
        String latitude = c.getString(c.getColumnIndex(VillageMaster.Column.LATITUDE));
        String longitude = c.getString(c.getColumnIndex(VillageMaster.Column.LONGITUDE));
        String moo = c.getString(c.getColumnIndex(VillageMaster.Column.MOO));
        String orderIndex = c.getString(c.getColumnIndex(VillageMaster.Column.ORDER_INDEX));
        String status_ID = c.getString(c.getColumnIndex(VillageMaster.Column.STATUS_ID));
//        String tambol_Code = c.getString(c.getColumnIndex(VillageMaster.Column.TAMBOL_CODE));
        String tambol_ID = c.getString(c.getColumnIndex(VillageMaster.Column.TAMBOL_ID));
//        String tambol_NameEn = c.getString(c.getColumnIndex(VillageMaster.Column.TAMBOL_NAME_EN));
//        String tambol_NameTh = c.getString(c.getColumnIndex(VillageMaster.Column.TAMBOL_NAME_TH));
        String village_Code = c.getString(c.getColumnIndex(VillageMaster.Column.VILLAGE_CODE));
        String village_Name = c.getString(c.getColumnIndex(VillageMaster.Column.VILLAGE_NAME));
//        String zipcode = c.getString(c.getColumnIndex(VillageMaster.Column.ZIPCODE));

        village.setVillage_ID(Integer.parseInt(village_ID));
//        village.setAmphur_ID(Integer.parseInt(amphur_ID));
        village.setLatitude(AppUtils.tryParseDouble(latitude));
        village.setLongitude(AppUtils.tryParseDouble(longitude));
        village.setMoo(Integer.parseInt(moo));
        village.setOrderIndex(Integer.parseInt(orderIndex));
        village.setStatus_ID(Integer.parseInt(status_ID));
//        village.setTambol_Code(Integer.parseInt(tambol_Code));
        village.setTambol_ID(Integer.parseInt(tambol_ID));
//        village.setTambol_NameEn(tambol_NameEn);
//        village.setTambol_NameTh(tambol_NameTh);
        village.setVillage_Code(Integer.parseInt(village_Code));
        village.setVillage_Name(village_Name);
//        village.setZipcode(zipcode);

        return village;
    }

    public ArrayList<JobEntity> getMainJob() {

        ArrayList<JobEntity> mainjobs = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + MainJobMaster.TABLE + " WHERE " + MainJobMaster.Column.STATUS_ID + " = " + 1;
        Cursor cursor = db.rawQuery(selectQuery, null);
//        Cursor cursor = db.query(MainJobMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            mainjobs.add(getMainJobInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return mainjobs;
    }

    public JobEntity getMainJobInfo(Cursor c) {
        JobEntity mainjob = new JobEntity();

        String main_Job_ID = c.getString(c.getColumnIndex(MainJobMaster.Column.MAIN_JOB_ID));
        String main_Job_Name = c.getString(c.getColumnIndex(MainJobMaster.Column.MAIN_JOB_NAME));

        mainjob.setMain_Job_ID(Integer.parseInt(main_Job_ID));
        mainjob.setMain_Job_Name(main_Job_Name);

        return mainjob;
    }

    public ArrayList<SecondJobEntity> getSecondJob() {
        ArrayList<SecondJobEntity> secondJobs = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + SecondJobMaster.TABLE + " WHERE " + SecondJobMaster.Column.STATUS_ID + " = " + 1;
        Cursor cursor = db.rawQuery(selectQuery, null);
//        Cursor cursor = db.query(SecondJobMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            secondJobs.add(getSecondJobInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return secondJobs;
    }

    public SecondJobEntity getSecondJobInfo(Cursor c) {
        SecondJobEntity secondjob = new SecondJobEntity();

        String second_Job_ID = c.getString(c.getColumnIndex(SecondJobMaster.Column.SECOND_JOB_ID));
        String second_Job_Name = c.getString(c.getColumnIndex(SecondJobMaster.Column.SECOND_JOB_NAME));

        secondjob.setSecond_Job_ID(Integer.parseInt(second_Job_ID));
        secondjob.setSecond_Job_Name(second_Job_Name);

        return secondjob;
    }

    public ArrayList<LandEntity> getLand() {
        ArrayList<LandEntity> lands = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + HoldingLandMaster.TABLE + " WHERE " + HoldingLandMaster.Column.STATUS_ID + " = " + 1;
        Cursor cursor = db.rawQuery(selectQuery, null);
//        Cursor cursor = db.query(HoldingLandMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            lands.add(getLandInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return lands;
    }

    public LandEntity getLandInfo(Cursor c) {
        LandEntity land = new LandEntity();

        String holding_Land_ID = c.getString(c.getColumnIndex(HoldingLandMaster.Column.HOLDING_LAND_ID));
        String holding_Land_Name = c.getString(c.getColumnIndex(HoldingLandMaster.Column.HOLDING_LAND_NAME));

        land.setHolding_Land_ID(Integer.parseInt(holding_Land_ID));
        land.setHolding_Land_Name(holding_Land_Name);

        return land;
    }

    public ArrayList<HelpEntity> getHelp() {
        ArrayList<HelpEntity> helps = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + HelpMaster.TABLE + " WHERE " + HelpMaster.Column.STATUS_ID + " = " + 1;
        Cursor cursor = db.rawQuery(selectQuery, null);
//        Cursor cursor = db.query(HelpMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            helps.add(getHelpInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return helps;
    }

    public HelpEntity getHelpInfo(Cursor c) {

        HelpEntity help = new HelpEntity();

        String government_Help_ID = c.getString(c.getColumnIndex(HelpMaster.Column.GOVERNMENT_HELP_ID));
        String government_Help_Name = c.getString(c.getColumnIndex(HelpMaster.Column.GOVERNMENT_HELP_NAME));

        help.setGovernment_Help_ID(Integer.parseInt(government_Help_ID));
        help.setGovernment_Help_Name(government_Help_Name);

        return help;
    }

    public ArrayList<FarmerProblemEntity> getFarmerProblem() {
        ArrayList<FarmerProblemEntity> farmerProblems = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + FarmerProblemMaster.TABLE + " WHERE " + FarmerProblemMaster.Column.STATUS_ID + " = " + 1;
        Cursor cursor = db.rawQuery(selectQuery, null);
//        Cursor cursor = db.query(FarmerProblemMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            farmerProblems.add(getFarmerProblemInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return farmerProblems;
    }

    public FarmerProblemEntity getFarmerProblemInfo(Cursor c) {

        FarmerProblemEntity farmerProblem = new FarmerProblemEntity();

        String farmer_Problem_ID = c.getString(c.getColumnIndex(FarmerProblemMaster.Column.FARMER_PROBLEM_ID));
        String farmer_Problem_Name = c.getString(c.getColumnIndex(FarmerProblemMaster.Column.FARMER_PROBLEM_NAME));
        String farmer_Problem_Desc = c.getString(c.getColumnIndex(FarmerProblemMaster.Column.FARMER_PROBLEM_DESC));

        farmerProblem.setFarmer_Problem_ID(Integer.parseInt(farmer_Problem_ID));
        farmerProblem.setFarmer_Problem_Name(farmer_Problem_Name);
        farmerProblem.setFarmer_Problem_Desc(farmer_Problem_Desc);

        return farmerProblem;
    }

    public ArrayList<StandardEntity> getStandard() {
        ArrayList<StandardEntity> standards = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + StandardMaster.TABLE + " WHERE " + StandardMaster.Column.STATUS_ID + " = " + 1;
        Cursor cursor = db.rawQuery(selectQuery, null);
//        Cursor cursor = db.query(StandardMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            standards.add(getStandardInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return standards;
    }

    public StandardEntity getStandardInfo(Cursor c) {

        StandardEntity standard = new StandardEntity();

        String standard_ID = c.getString(c.getColumnIndex(StandardMaster.Column.STANDARD_ID));
        String standard_Name = c.getString(c.getColumnIndex(StandardMaster.Column.STANDARD_NAME));


        standard.setStandard_ID(Integer.parseInt(standard_ID));
        standard.setStandard_Name(standard_Name);


        return standard;
    }

    public ArrayList<SupportEntity> getSupport() {
        ArrayList<SupportEntity> supports = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + SupportStandardMaster.TABLE + " WHERE " + SupportStandardMaster.Column.STATUS_ID + " = " + 1;
        Cursor cursor = db.rawQuery(selectQuery, null);
//        Cursor cursor = db.query(SupportStandardMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            supports.add(getSupportInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return supports;
    }

    public SupportEntity getSupportInfo(Cursor c) {

        SupportEntity support = new SupportEntity();

        String support_Standard_ID = c.getString(c.getColumnIndex(SupportStandardMaster.Column.SUPPORT_STANDARD_ID));
        String support_Standard_Name = c.getString(c.getColumnIndex(SupportStandardMaster.Column.SUPPORT_STANDARD_NAME));

        support.setSupport_Standard_ID(Integer.parseInt(support_Standard_ID));
        support.setSupport_Standard_Name(support_Standard_Name);

        return support;
    }

    public ArrayList<FarmEntity> getListFarm(BigDecimal id) {
        ArrayList<FarmEntity> farms = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + FarmModel.TABLE + " WHERE " + Column.FARMER_ID + " = " + id;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            farms.add(getFarmInfo(cursor));
            cursor.moveToNext();
        }
        db.close();
        return farms;
    }

    public FarmEntity getFarmInfo(Cursor c) {

        FarmEntity farm = new FarmEntity();

        BigDecimal farm_ID = getBigDecimalFromCursor(c, Column.FARM_ID);
        int amphur_ID = getIntFromCursor(c, Column.AMPHUR_ID);
        BigDecimal animalWorth = getBigDecimalFromCursor(c, Column.ANIMAL_WORTH);
        BigDecimal area_Rai = getBigDecimalFromCursor(c, Column.AREA_RAI);
        BigDecimal area_Ngan = getBigDecimalFromCursor(c, Column.AREA_NGAN);
        String area_Type_ID = getStringFromCursor(c, Column.AREA_TYPE_ID);
        String cancelBy = getStringFromCursor(c, Column.CANCEL_BY);
        String cancelDate = getStringFromCursor(c, Column.CANCEL_DATE);
        String createBy = getStringFromCursor(c, Column.CREATE_BY);
        String createDate = getStringFromCursor(c, Column.CREATE_DATE);
        String email = getStringFromCursor(c, Column.EMAIL);
        String farm_Name = getStringFromCursor(c, Column.FARM_NAME);
        int farm_Owner_Type_ID = getIntFromCursor(c, Column.FARM_OWNER_TYPE_ID);
        String farm_Problem_Desc = getStringFromCursor(c, Column.FARM_PROBLEM_DESC);
        String farm_Stadard_Number = getStringFromCursor(c, Column.FARM_STANDARD_NUMBER);
        BigDecimal farmer_ID = getBigDecimalFromCursor(c, Column.FARMER_ID);
        String homeNo = getStringFromCursor(c, Column.HOME_NO);
        int isCancel = getIntFromCursor(c, Column.IS_CANCEL);
        BigDecimal latitude = getBigDecimalFromCursor(c, Column.LATITUDE);
        BigDecimal longitude = getBigDecimalFromCursor(c, Column.LONGITUDE);
        String mobile = getStringFromCursor(c, Column.MOBILE);
        int orderIndex = getIntFromCursor(c, Column.ORDER_INDEX);
        String phone = getStringFromCursor(c, Column.PHONE);
        String postcode = getStringFromCursor(c, Column.POSTCODE);
        int province_ID = getIntFromCursor(c, Column.PROVINCE_ID);
        BigDecimal revenueOfLivestock = getBigDecimalFromCursor(c, Column.REVENUE_OF_LIVESTOCK);
        String road = getStringFromCursor(c, Column.ROAD);
        String soi = getStringFromCursor(c, Column.SOI);
        int standard_ID = getIntFromCursor(c, Column.STANDARD_ID);
        int status_ID = getIntFromCursor(c, Column.STATUS_ID);
        int support_Standard_ID = getIntFromCursor(c, Column.SUPPORT_STANDARD_ID);
        int tambol_ID = getIntFromCursor(c, Column.TAMBOL_ID);
        String updateBy = getStringFromCursor(c, Column.UPDATE_BY);
        String updateDate = getStringFromCursor(c, Column.UPDATE_DATE);
        String village = getStringFromCursor(c, Column.VILLAGE_NAME);
        int village_ID = getIntFromCursor(c, Column.VILLAGE_ID);
        String worker_ID = getStringFromCursor(c, Column.WORKER_ID);

        farm.setFarm_ID(farm_ID);
        farm.setAmphur_ID(amphur_ID);
        farm.setAnimalWorth(animalWorth);
        farm.setArea_Rai(area_Rai);
        farm.setArea_Ngan(area_Ngan);
        farm.setArea_Type_ID(area_Type_ID);
        farm.setCancelBy(cancelBy);
        farm.setCancelDate(cancelDate);
        farm.setCreateBy(createBy);
        farm.setCancelDate(createDate);
        farm.setEmail(email);
        farm.setFarm_Name(farm_Name);
        farm.setFarm_Owner_Type_ID(farm_Owner_Type_ID);
        farm.setFarm_Problem_Desc(farm_Problem_Desc);
        farm.setFarm_Standard_Number(farm_Stadard_Number);
        farm.setFarmer_ID(farmer_ID);
        farm.setHomeNo(homeNo);
        farm.setIsCancel(isCancel);
        farm.setLatitude(latitude);
        farm.setLongitude(longitude);
        farm.setMobile(mobile);
        farm.setOrderIndex(orderIndex);
        farm.setPhone(phone);
        farm.setPostCode(postcode);
        farm.setProvince_ID(province_ID);
        farm.setRevenueOfLivestock(revenueOfLivestock);
        farm.setRoad(road);
        farm.setSoi(soi);
        farm.setStandard_ID(standard_ID);
        farm.setStatus_ID(status_ID);
        farm.setSupport_Standard_ID(support_Standard_ID);
        farm.setTambol_ID(tambol_ID);
        farm.setUpdateBy(updateBy);
        farm.setUpdateDate(updateDate);
        farm.setVillage_Name(village);
        farm.setVillage_ID(village_ID);
        farm.setWorker_ID(worker_ID);

        return farm;

    }

    public ArrayList<LivestockEntity> getListLivestock() {
        ArrayList<LivestockEntity> livestocks = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + LivestockMaster.TABLE + " WHERE " + LivestockMaster.Column.STATUS_ID + " = " + 1;
        Cursor cursor = db.rawQuery(selectQuery, null);
//        Cursor cursor = db.query(LivestockMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            livestocks.add(getLivestockInfo(cursor));
            cursor.moveToNext();
        }
        db.close();
        return livestocks;
    }

    public LivestockEntity getLivestockInfo(Cursor c) {

        if (c == null)
            return null;

        LivestockEntity livestock = new LivestockEntity();

        int livestock_Type_ID = getIntFromCursor(c, LivestockMaster.Column.LIVESTOCK_TYPE_ID);
        String createBy = getStringFromCursor(c, LivestockMaster.Column.CREATE_BY);
        String createDate = getStringFromCursor(c, LivestockMaster.Column.CREATE_DATE);
        String livestock_Type_Code = getStringFromCursor(c, LivestockMaster.Column.LIVESTOCK_TYPE_CODE);
        String livestock_Type_Flgtail = getStringFromCursor(c, LivestockMaster.Column.LIVESTOCK_TYPE_FLGTAIL);
        String livestock_Type_Fullname = getStringFromCursor(c, LivestockMaster.Column.LIVESTOCK_TYPE_FULLNAME);
        String livestock_Type_Name = getStringFromCursor(c, LivestockMaster.Column.LIVESTOCK_TYPE_NAME);
        String livestock_Type_Old_ID = getStringFromCursor(c, LivestockMaster.Column.LIVESTOCK_TYPE_OLD_ID);
        int livestock_Type_Parent = getIntFromCursor(c, LivestockMaster.Column.LIVESTOCK_TYPE_PARENT);
        BigDecimal livestock_Type_Price = getBigDecimalFromCursor(c, LivestockMaster.Column.LIVESTOCK_TYPE_PRICE);
        String livestock_Type_Unit = getStringFromCursor(c, LivestockMaster.Column.LIVESTOCK_TYPE_UNIT);
        int livestock_Type_UnitLimit = getIntFromCursor(c, LivestockMaster.Column.LIVESTOCK_TYPE_UNITLIMIT);
        String updateBy = getStringFromCursor(c, LivestockMaster.Column.UPDATE_BY);
        String updateDate = getStringFromCursor(c, LivestockMaster.Column.UPDATE_DATE);
        int statusID = getIntFromCursor(c, LivestockMaster.Column.STATUS_ID);
        int orderIndex = getIntFromCursor(c, LivestockMaster.Column.ORDER_INDEX);
        int livestock_Type_Root = getIntFromCursor(c, LivestockMaster.Column.LIVESTOCK_TYPE_ROOT);


        livestock.setLivestock_Type_ID(livestock_Type_ID);
        livestock.setCreateBy(createBy);
        livestock.setCreateDate(createDate);
        livestock.setLivestock_Type_Code(livestock_Type_Code);
        livestock.setLivestock_Type_Flgtail(livestock_Type_Flgtail);
        livestock.setLivestock_Type_FullName(livestock_Type_Fullname);
        livestock.setLivestock_Type_Name(livestock_Type_Name);
        livestock.setLivestock_Type_Old_ID(livestock_Type_Old_ID);
        livestock.setLivestock_Type_Parent(livestock_Type_Parent);
        livestock.setLivestock_Type_Price(livestock_Type_Price);
        livestock.setLivestock_Type_Unit(livestock_Type_Unit);
        livestock.setLivestock_Type_UnitLimit(livestock_Type_UnitLimit);
        livestock.setUpdateBy(updateBy);
        livestock.setUpdateDate(updateDate);
        livestock.setStatus_ID(statusID);
        livestock.setOrderIndex(orderIndex);
        livestock.setLivestock_Type_Root(livestock_Type_Root);

        return livestock;
    }

    public ArrayList<FeedKindEntity> getListFeedKind() {
        ArrayList<FeedKindEntity> feedKinds = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + FeedKindMaster.TABLE + " WHERE " + FeedKindMaster.Column.STATUS_ID + " = " + 1
                +" ORDER BY "+FeedKindMaster.Column.ORDER_INDEX;
        Cursor cursor = db.rawQuery(selectQuery, null);
//        Cursor cursor = db.query(FeedKindMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            feedKinds.add(getFeedKindInfo(cursor));
            cursor.moveToNext();
        }
        db.close();
        return feedKinds;
    }

    public FeedKindEntity getFeedKindInfo(Cursor c) {
        FeedKindEntity feedKind = new FeedKindEntity();

        int feed_Kind_ID = getIntFromCursor(c, FeedKindMaster.Column.FEED_KIND_ID);
        String feed_Kind_NameEn = getStringFromCursor(c, FeedKindMaster.Column.FEED_KIND_NAME_EN);
        String feed_Kind_NameTh = getStringFromCursor(c, FeedKindMaster.Column.FEED_KIND_NAME_TH);
        int orderIndex = getIntFromCursor(c, FeedKindMaster.Column.ORDER_INDEX);
        int status_ID = getIntFromCursor(c, FeedKindMaster.Column.STATUS_ID);

        feedKind.setFeed_Kind_ID(feed_Kind_ID);
        feedKind.setFeed_Kind_NameEn(feed_Kind_NameEn);
        feedKind.setFeed_Kind_NameTh(feed_Kind_NameTh);
        feedKind.setOrderIndex(orderIndex);
        feedKind.setStatus_ID(status_ID);

        return feedKind;
    }

    public ArrayList<FeedEntity> getListFeed(BigDecimal id) {
        ArrayList<FeedEntity> feeds = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + FeedModel.TABLE + " WHERE " + Column.FARM_ID + " = " + id;

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            feeds.add(getFeedInfo(cursor));
            cursor.moveToNext();
        }
        db.close();
        return feeds;
    }

    public FeedEntity getFeedInfo(Cursor c) {
        FeedEntity feed = new FeedEntity();

        String createBy = getStringFromCursor(c, FeedModel.Column.CREATE_BY);
        String createDate = getStringFromCursor(c, FeedModel.Column.CREATE_DATE);
        int farm_ID = getIntFromCursor(c, FeedModel.Column.FARM_ID);
        BigDecimal feed_Area_Ngan = getBigDecimalFromCursor(c, FeedModel.Column.FEED_AREA_NGAN);
        BigDecimal feed_Area_Rai = getBigDecimalFromCursor(c, FeedModel.Column.FEED_AREA_RAI);
        BigDecimal feed_Area_Wa = getBigDecimalFromCursor(c, FeedModel.Column.FEED_AREA_WA);
        int feed_ID = getIntFromCursor(c, FeedModel.Column.FEED_ID);
        int feed_Kind_ID = getIntFromCursor(c, FeedModel.Column.FEED_KIND_ID);
        String feed_Kind_Name = getStringFromCursor(c, FeedModel.Column.FEED_KIND_NAME);
        String feed_Kind_Other = getStringFromCursor(c, FeedModel.Column.FEED_KIND_OTHER);
        int feed_SubType_Format_ID = getIntFromCursor(c, FeedModel.Column.FEED_SUBTYPE_FORMAT_ID);
        int feed_SubType_ID = getIntFromCursor(c, FeedModel.Column.FEED_SUBTYPE_ID);
        String feed_SubType_Name = getStringFromCursor(c, FeedModel.Column.FEED_SUBTYPE_NAME);
        int feed_Type_ID = getIntFromCursor(c, FeedModel.Column.FEED_TYPE_ID);
        String feed_Type_Name = getStringFromCursor(c, FeedModel.Column.FEED_TYPE_NAME);
        String feed_Type_Other = getStringFromCursor(c, FeedModel.Column.FEED_TYPE_OTHER);
        int feed_Water_Type_ID = getIntFromCursor(c, FeedModel.Column.FEED_WATER_TYPE_ID);
        String feed_Water_Type_Name = getStringFromCursor(c, FeedModel.Column.FEED_WATER_TYPE_NAME);
        String MS_Feed_SubType_Format_Name = getStringFromCursor(c, FeedModel.Column.MS_FEED_SUBTYPE_FORMAT_NAME);
        int orderIndex = getIntFromCursor(c, FeedModel.Column.ORDER_INDEX);
        int status_ID = getIntFromCursor(c, FeedModel.Column.STATUS_ID);
        String status_Name = getStringFromCursor(c, FeedModel.Column.STATUS_NAME);
        String updateBy = getStringFromCursor(c, FeedModel.Column.UPDATE_BY);
        String updateDate = getStringFromCursor(c, FeedModel.Column.UPDATE_DATE);

        feed.setCreateBy(createBy);
        feed.setCreateDate(createDate);
        feed.setFarm_ID(farm_ID);
        feed.setFeed_Area_Ngan(feed_Area_Ngan);
        feed.setFeed_Area_Rai(feed_Area_Rai);
        feed.setFeed_Area_Wa(feed_Area_Wa);
        feed.setFeed_Kind_ID(feed_Kind_ID);
        feed.setFeed_ID(feed_ID);
        feed.setFeed_Kind_Name(feed_Kind_Name);
        feed.setFeed_Kind_Other(feed_Kind_Other);
        feed.setFeed_SubType_Format_ID(feed_SubType_Format_ID);
        feed.setFeed_SubType_ID(feed_SubType_ID);
        feed.setFeed_SubType_Name(feed_SubType_Name);
        feed.setFeed_Type_ID(feed_Type_ID);
        feed.setFeed_Type_Name(feed_Type_Name);
        feed.setFeed_Type_Other(feed_Type_Other);
        feed.setFeed_Water_Type_ID(feed_Water_Type_ID);
        feed.setFeed_Water_Type_Name(feed_Water_Type_Name);
        feed.setMS_Feed_SubType_Format_Name(MS_Feed_SubType_Format_Name);
        feed.setOrderIndex(orderIndex);
        feed.setStatus_ID(status_ID);
        feed.setStatus_Name(status_Name);
        feed.setUpdateBy(updateBy);
        feed.setUpdateDate(updateDate);

        return feed;
    }

    public ArrayList<SubTypeFormatEntity> getListFeedSubTypeFormat() {
        ArrayList<SubTypeFormatEntity> subTypeFormats = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + FeedSubTypeFormatMaster.TABLE + " WHERE " + FeedSubTypeFormatMaster.Column.STATUS_ID + " = " + 1;
        Cursor cursor = db.rawQuery(selectQuery, null);
//        Cursor cursor = db.query(FeedSubTypeFormatMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            subTypeFormats.add(getSubTypeFormatInfo(cursor));
            cursor.moveToNext();
        }
        db.close();
        return subTypeFormats;
    }

    public SubTypeFormatEntity getSubTypeFormatInfo(Cursor c) {
        SubTypeFormatEntity subTypeFormat = new SubTypeFormatEntity();

        int MS_Feed_SubType_Format_ID = getIntFromCursor(c, FeedSubTypeFormatMaster.Column.FEED_SUBTYPE_FORMAT_ID);
        String MS_Feed_SubType_Format_Name = getStringFromCursor(c, FeedSubTypeFormatMaster.Column.FEED_SUBTYPE_FORMAT_NAME);
        int orderIndex = getIntFromCursor(c, FeedSubTypeFormatMaster.Column.ORDER_INDEX);
        int status_ID = getIntFromCursor(c, FeedSubTypeFormatMaster.Column.STATUS_ID);

        subTypeFormat.setMS_Feed_SubType_Format_ID(MS_Feed_SubType_Format_ID);
        subTypeFormat.setMS_Feed_SubType_Format_Name(MS_Feed_SubType_Format_Name);
        subTypeFormat.setOrderIndex(orderIndex);
        subTypeFormat.setStatus_ID(status_ID);

        return subTypeFormat;

    }

    public ArrayList<SubTypeEntity> getListFeedSubType() {
        ArrayList<SubTypeEntity> subTypes = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + FeedSubTypeMaster.TABLE + " WHERE " + FeedSubTypeMaster.Column.STATUS_ID + " = " + 1;
        Cursor cursor = db.rawQuery(selectQuery, null);
//        Cursor cursor = db.query(FeedSubTypeMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            subTypes.add(getSubTypeInfo(cursor));
            cursor.moveToNext();
        }
        db.close();
        return subTypes;
    }

    public SubTypeEntity getSubTypeInfo(Cursor c) {
        SubTypeEntity subType = new SubTypeEntity();

        int feed_SubType_ID = getIntFromCursor(c, FeedSubTypeMaster.Column.FEED_SUBTYPE_ID);
        String feed_SubType_Name = getStringFromCursor(c, FeedSubTypeMaster.Column.FEED_SUBTYPE_NAME);
        int orderIndex = getIntFromCursor(c, FeedSubTypeMaster.Column.ORDER_INDEX);
        int status_ID = getIntFromCursor(c, FeedSubTypeMaster.Column.STATUS_ID);

        subType.setFeed_SubType_ID(feed_SubType_ID);
        subType.setFeed_SubType_Name(feed_SubType_Name);
        subType.setOrderIndex(orderIndex);
        subType.setStatus_ID(status_ID);

        return subType;
    }

    public ArrayList<FeedTypeEntity> getListFeedType() {
        ArrayList<FeedTypeEntity> feedTypes = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + FeedTypeMaster.TABLE + " WHERE " + FeedTypeMaster.Column.STATUS_ID + " = " + 1;
        Cursor cursor = db.rawQuery(selectQuery, null);
//        Cursor cursor = db.query(FeedTypeMaster.TABLE, null, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            feedTypes.add(getFeedTypeInfo(cursor));
            cursor.moveToNext();
        }
        db.close();
        return feedTypes;
    }

    public FeedTypeEntity getFeedTypeInfo(Cursor c) {
        FeedTypeEntity feedType = new FeedTypeEntity();

        int feed_Type_ID = getIntFromCursor(c, FeedTypeMaster.Column.FEED_TYPE_ID);
        String feed_Type_Name = getStringFromCursor(c, FeedTypeMaster.Column.FEED_TYPE_NAME);
        int orderIndex = getIntFromCursor(c, FeedTypeMaster.Column.ORDER_INDEX);
        int status_ID = getIntFromCursor(c, FeedTypeMaster.Column.STATUS_ID);

        feedType.setFeed_Type_ID(feed_Type_ID);
        feedType.setFeed_Type_Name(feed_Type_Name);
        feedType.setOrderIndex(orderIndex);
        feedType.setStatus_ID(status_ID);

        return feedType;
    }

    public ArrayList<CompanyEntity> getListCompany() {
        ArrayList<CompanyEntity> companyTypes = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + CompanyTypeMaster.TABLE;
        Cursor cursor = db.rawQuery(selectQuery, null);
//        Cursor cursor = db.query(CompanyTypeMaster.TABLE, null, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            companyTypes.add(getCompanyInfo(cursor));
            cursor.moveToNext();
        }
        db.close();
        return companyTypes;
    }

    public CompanyEntity getCompanyInfo(Cursor c) {
        CompanyEntity companies = new CompanyEntity();

        String companyTypeName = getStringFromCursor(c, CompanyTypeMaster.Column.COMPANY_TYPE_NAME);
        int id = getIntFromCursor(c, CompanyTypeMaster.Column.ID);
        int isCompany = getIntFromCursor(c, CompanyTypeMaster.Column.IS_COMPANY);

        companies.setCompanyTypeName(companyTypeName);
        companies.setId(id);
        companies.setIsCompany(isCompany);

        return companies;
    }

    public ArrayList<TypeEntity> getListTypeName(){
        ArrayList<TypeEntity> types = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + LivestockTypeMaster.TABLE + " WHERE " + LivestockTypeMaster.Column.STATUS_ID + " = " + 1;
        Cursor cursor = db.rawQuery(selectQuery, null);
//        Cursor cursor = db.query(LivestockTypeMaster.TABLE, null,null,null,null,null,null);
        if (cursor != null){
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()){
            types.add(getTypeNameInfo(cursor));
            cursor.moveToNext();
        }
        db.close();
        return types;
    }

    public TypeEntity getTypeNameInfo(Cursor c){
        TypeEntity type = new TypeEntity();

        int type_id = getIntFromCursor(c, LivestockTypeMaster.Column.FARM_STANDARD_LIVESTOCK_TYPE_ID);
        String type_name = getStringFromCursor(c, LivestockTypeMaster.Column.FARM_STANDARD_LIVESTOCK_TYPE_NAME);
        int orderIndex = getIntFromCursor(c, LivestockTypeMaster.Column.ORDER_INDEX);
        int status_ID = getIntFromCursor(c, LivestockTypeMaster.Column.STATUS_ID);

        type.setFarm_Standard_LivestockType_ID(type_id);
        type.setFarm_Standard_LivestockType_Name(type_name);
        type.setOrderIndex(orderIndex);
        type.setStatus_ID(status_ID);

        return type;
    }


    List<LivestockEntity> getAncestorsInner(SQLiteDatabase db, LivestockEntity animal) {
        String selected = "SELECT * FROM " + LivestockMaster.TABLE + " WHERE " +
                LivestockMaster.Column.LIVESTOCK_TYPE_PARENT + " = "
                + animal.getLivestock_Type_Parent();
        Cursor c = db.rawQuery(selected, null);
        ArrayList<LivestockEntity> res = new ArrayList<>();
        LivestockEntity parent = getLivestockInfo(c);
        if (parent == null)
            return res;
        res.add(parent);
        res.addAll(getAncestorsInner(db, parent));
        return res;
    }

    public List<LivestockEntity> getAncestors(LivestockEntity animal) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            return getAncestorsInner(db, animal);
        } finally {
            db.close();
        }
    }


    public void addPrefixs(List<PrefixEntity> prefixs) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (PrefixEntity prefix : prefixs) {
                ContentValues values = new ContentValues();

                values.put(PrefixMaster.Column.PREFIX_ID, prefix.getPrefix_ID());
                values.put(PrefixMaster.Column.ORDER_INDEX, prefix.getOrderIndex());
                values.put(PrefixMaster.Column.PREFIX_NAME_EN, prefix.getPrefix_NameEn());
                values.put(PrefixMaster.Column.PREFIX_NAME_TH, prefix.getPrefix_NameTh());
                values.put(PrefixMaster.Column.STATUS_ID, prefix.getStatus_ID());

                db.insert(PrefixMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addProvinces(List<ProvinceEntity> provinces) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (ProvinceEntity province : provinces) {
                ContentValues values = new ContentValues();

                values.put(ProvinceMaster.Column.PROVINCE_ID, province.getProvince_ID());
                values.put(ProvinceMaster.Column.COUNTRYRANCH_ID, province.getCountryRanch_ID());
                values.put(ProvinceMaster.Column.GEO_ID, province.getGeo_ID());
                values.put(ProvinceMaster.Column.LATITUDE, province.getLatitude());
                values.put(ProvinceMaster.Column.LONGITUDE, province.getLongitude());
                values.put(ProvinceMaster.Column.ORDER_INDEX, province.getOrderIndex());
                values.put(ProvinceMaster.Column.PROVINCE_NAME_EN, province.getProvince_NameEn());
                values.put(ProvinceMaster.Column.PROVINCE_NAME_TH, province.getProvince_NameTh());
                values.put(ProvinceMaster.Column.PROVINCE_CODE, province.getProvince_Code());
                values.put(ProvinceMaster.Column.STATUS_ID, province.getStatus_ID());

                db.insert(ProvinceMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addAmphurs(List<AmphurEntity> amphurs) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (AmphurEntity amphur : amphurs) {
                ContentValues values = new ContentValues();

                values.put(AmphurMaster.Column.AMPHUR_ID, amphur.getAmphur_ID());
                values.put(AmphurMaster.Column.AMPHUR_CODE, amphur.getAmphur_Code());
                values.put(AmphurMaster.Column.AMPHUR_NAME_EN, amphur.getAmphur_NameEn());
                values.put(AmphurMaster.Column.AMPHUR_NAME_TH, amphur.getAmphur_nameTh());
                values.put(AmphurMaster.Column.LATITUDE, amphur.getLatitude());
                values.put(AmphurMaster.Column.LONGITUDE, amphur.getlongitude());
                values.put(AmphurMaster.Column.ORDER_INDEX, amphur.getOrderIndex());
                values.put(AmphurMaster.Column.PROVINCE_ID, amphur.getProvince_ID());
                values.put(AmphurMaster.Column.STATUS_ID, amphur.getStatus_ID());

                db.insert(AmphurMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addTambols(List<TambolEntity> tambols) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (TambolEntity tambol : tambols) {
                ContentValues values = new ContentValues();

                values.put(TambolMaster.Column.TAMBOL_ID, tambol.getTambol_ID());
                values.put(TambolMaster.Column.AMPHUR_ID, tambol.getAmphur_ID());
                values.put(TambolMaster.Column.LATITUDE, tambol.getLatitude());
                values.put(TambolMaster.Column.LONGITUDE, tambol.getLongitude());
                values.put(TambolMaster.Column.ORDER_INDEX, tambol.getOrderIndex());
                values.put(TambolMaster.Column.STATUS_ID, tambol.getStatus_ID());
                values.put(TambolMaster.Column.TAMBOL_CODE, tambol.getTambol_Code());
                values.put(TambolMaster.Column.TAMBOL_NAME_EN, tambol.getTambol_NameEn());
                values.put(TambolMaster.Column.TAMBOL_NAME_TH, tambol.getTambol_NameTh());
                values.put(TambolMaster.Column.ZIPCODE, tambol.getZipcode());

                db.insert(TambolMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addVillages(List<VillageEntity> villages) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (VillageEntity village : villages) {
                ContentValues values = new ContentValues();

                values.put(VillageMaster.Column.VILLAGE_ID, village.getVillage_ID());
//                values.put(VillageMaster.Column.AMPHUR_ID, village.getAmphur_ID());
                values.put(VillageMaster.Column.LATITUDE, village.getLatitude());
                values.put(VillageMaster.Column.LONGITUDE, village.getLongitude());
                values.put(VillageMaster.Column.MOO, village.getMoo());
                values.put(VillageMaster.Column.ORDER_INDEX, village.getOrderIndex());
                values.put(VillageMaster.Column.STATUS_ID, village.getStatus_ID());
//                values.put(VillageMaster.Column.TAMBOL_CODE, village.getTambol_Code());
                values.put(VillageMaster.Column.TAMBOL_ID, village.getTambol_ID());
//                values.put(VillageMaster.Column.TAMBOL_NAME_TH, village.getTambol_NameTh());
//                values.put(VillageMaster.Column.TAMBOL_NAME_EN, village.getTambol_NameEn());
                values.put(VillageMaster.Column.VILLAGE_CODE, village.getVillage_Code());
                values.put(VillageMaster.Column.VILLAGE_NAME, village.getVillage_Name());
//                values.put(VillageMaster.Column.ZIPCODE, village.getZipcode());

                db.insertOrThrow(VillageMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFarmer(FarmerEntity farmer) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.insertOrThrow(FarmerModel.TABLE, null, setInfoFarmer(farmer));

        db.close();
    }

    public void addFarm(FarmEntity farm, BigDecimal id) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.insertOrThrow(FarmModel.TABLE, null, setInfoFarm(farm, id));
    }

    public void addFarmOwner(List<OwnerEntity> owners) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (OwnerEntity owner : owners) {
                ContentValues values = new ContentValues();

                values.put(FarmOwnerMaster.Column.FARM_OWNER_TYPE_ID, owner.getFarm_Owner_Type_ID());
                values.put(FarmOwnerMaster.Column.FARM_OWNER_TYPE_NAME, owner.getFarm_Owner_Type_Name());
                values.put(FarmOwnerMaster.Column.ORDER_INDEX, owner.getOrderIndex());
                values.put(FarmOwnerMaster.Column.STATUS_ID, owner.getStatus_ID());

                db.insert(FarmOwnerMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFarmProblem(List<FarmProblemEntity> farmProblems) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (FarmProblemEntity farmProblem : farmProblems) {
                ContentValues values = new ContentValues();

                values.put(FarmProblemMaster.Column.FARM_PROBLEM_ID, farmProblem.getFarm_Problem_ID());
                values.put(FarmProblemMaster.Column.FARM_PROBLEM_NAME, farmProblem.getFarm_Problem_Name());
                values.put(FarmProblemMaster.Column.ORDER_INDEX, farmProblem.getOrderIndex());
                values.put(FarmProblemMaster.Column.STATUS_ID, farmProblem.getStatus_ID());

                db.insert(FarmProblemMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFarmerProblem(List<FarmerProblemEntity> farmerProblems) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (FarmerProblemEntity farmerProblem : farmerProblems) {
                ContentValues values = new ContentValues();

                values.put(FarmerProblemMaster.Column.FARMER_PROBLEM_ID, farmerProblem.getFarmer_Problem_ID());
                values.put(FarmerProblemMaster.Column.FARMER_PROBLEM_DESC, farmerProblem.getFarmer_Problem_Desc());
                values.put(FarmerProblemMaster.Column.FARMER_PROBLEM_NAME, farmerProblem.getFarmer_Problem_Name());
                values.put(FarmerProblemMaster.Column.ORDER_INDEX, farmerProblem.getOrderIndex());
                values.put(FarmerProblemMaster.Column.STATUS_ID, farmerProblem.getStatus_ID());

                db.insert(FarmerProblemMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFarmerType(List<FarmerTypeEntity> farmerTypes) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (FarmerTypeEntity farmerType : farmerTypes) {
                ContentValues values = new ContentValues();

                values.put(FarmerTypeMaster.Column.FARMER_TYPE_ID, farmerType.getFarmer_Type_ID());
                values.put(FarmerTypeMaster.Column.FARMER_TYPE_NAME, farmerType.getFarmer_Type_Name());
                values.put(FarmerTypeMaster.Column.ORDER_INDEX, farmerType.getOrderIndex());
                values.put(FarmerTypeMaster.Column.STATUS_ID, farmerType.getStatus_ID());

                db.insert(FarmerTypeMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFeeds(List<FeedEntity> feeds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (FeedEntity feed : feeds) {
                if (!isFeedPopulated(feed.getFeed_ID(), db))
                    db.insert(FeedModel.TABLE, null, setInfoFeed(feed));
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFeedKind(List<FeedKindEntity> feedKinds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (FeedKindEntity feedKind : feedKinds) {
                ContentValues values = new ContentValues();

                values.put(FeedKindMaster.Column.FEED_KIND_ID, feedKind.getFeed_Kind_ID());
                values.put(FeedKindMaster.Column.FEED_KIND_NAME_EN, feedKind.getFeed_Kind_NameEn());
                values.put(FeedKindMaster.Column.FEED_KIND_NAME_TH, feedKind.getFeed_Kind_NameTh());
                values.put(FeedKindMaster.Column.ORDER_INDEX, feedKind.getOrderIndex());
                values.put(FeedKindMaster.Column.STATUS_ID, feedKind.getStatus_ID());

                db.insert(FeedKindMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addHelp(List<HelpEntity> helps) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (HelpEntity help : helps) {
                ContentValues values = new ContentValues();

                values.put(HelpMaster.Column.GOVERNMENT_HELP_ID, help.getGovernment_Help_ID());
                values.put(HelpMaster.Column.GOVERNMENT_HELP_NAME, help.getGovernment_Help_Name());
                values.put(HelpMaster.Column.ORDER_INDEX, help.getOrderIndex());
                values.put(HelpMaster.Column.STATUS_ID, help.getStatus_ID());

                db.insert(HelpMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addHoldingLand(List<LandEntity> lands) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (LandEntity land : lands) {
                ContentValues values = new ContentValues();

                values.put(HoldingLandMaster.Column.HOLDING_LAND_ID, land.getHolding_Land_ID());
                values.put(HoldingLandMaster.Column.HOLDING_LAND_NAME, land.getHolding_Land_Name());
                values.put(HoldingLandMaster.Column.ORDER_INDEX, land.getOrderIndex());
                values.put(HoldingLandMaster.Column.STATUS_ID, land.getStatus_ID());

                db.insert(HoldingLandMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addMainJob(List<JobEntity> mainjobs) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (JobEntity mainjob : mainjobs) {
                ContentValues values = new ContentValues();

                values.put(MainJobMaster.Column.MAIN_JOB_ID, mainjob.getMain_Job_ID());
                values.put(MainJobMaster.Column.MAIN_JOB_NAME, mainjob.getMain_Job_Name());
                values.put(MainJobMaster.Column.ORDER_INDEX, mainjob.getOrderIndex());
                values.put(MainJobMaster.Column.STATUS_ID, mainjob.getStatus_ID());

                db.insert(MainJobMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addSecondJob(List<SecondJobEntity> secondjobs) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (SecondJobEntity secondjob : secondjobs) {
                ContentValues values = new ContentValues();

                values.put(SecondJobMaster.Column.SECOND_JOB_ID, secondjob.getSecond_Job_ID());
                values.put(SecondJobMaster.Column.SECOND_JOB_NAME, secondjob.getSecond_Job_Name());
                values.put(SecondJobMaster.Column.ORDER_INDEX, secondjob.getOrderIndex());
                values.put(SecondJobMaster.Column.STATUS_ID, secondjob.getStatus_ID());

                db.insert(SecondJobMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addStandard(List<StandardEntity> standards) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (StandardEntity standard : standards) {
                ContentValues values = new ContentValues();

                values.put(StandardMaster.Column.STANDARD_ID, standard.getStandard_ID());
                values.put(StandardMaster.Column.STANDARD_NAME, standard.getStandard_Name());
                values.put(StandardMaster.Column.ORDER_INDEX, standard.getOrderIndex());
                values.put(StandardMaster.Column.STATUS_ID, standard.getStatus_ID());

                db.insert(StandardMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addSupportStandard(List<SupportEntity> supports) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (SupportEntity support : supports) {
                ContentValues values = new ContentValues();

                values.put(SupportStandardMaster.Column.SUPPORT_STANDARD_ID, support.getSupport_Standard_ID());
                values.put(SupportStandardMaster.Column.SUPPORT_STANDARD_NAME, support.getSupport_Standard_Name());
                values.put(SupportStandardMaster.Column.ORDER_INDEX, support.getOrderIndex());
                values.put(SupportStandardMaster.Column.STATUS_ID, support.getStatus_ID());

                db.insert(SupportStandardMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }


    public void addCompanyType(List<CompanyEntity> companyTypes) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (CompanyEntity companyType : companyTypes) {

                ContentValues values = new ContentValues();

                values.put(CompanyTypeMaster.Column.COMPANY_TYPE_NAME, companyType.getCompanyTypeName());
                values.put(CompanyTypeMaster.Column.ID, companyType.getId());
                values.put(CompanyTypeMaster.Column.IS_COMPANY, companyType.getIsCompany());


                db.insert(CompanyTypeMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addLivestockTypeName(List<TypeEntity> types) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (TypeEntity type : types) {
                ContentValues values = new ContentValues();

                values.put(LivestockTypeMaster.Column.FARM_STANDARD_LIVESTOCK_TYPE_ID, type.getFarm_Standard_LivestockType_ID());
                values.put(LivestockTypeMaster.Column.FARM_STANDARD_LIVESTOCK_TYPE_NAME, type.getFarm_Standard_LivestockType_Name());
                values.put(LivestockTypeMaster.Column.ORDER_INDEX, type.getOrderIndex());
                values.put(LivestockTypeMaster.Column.STATUS_ID, type.getStatus_ID());

                db.insert(LivestockTypeMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addListFarm(List<FarmEntity> farms, BigDecimal id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int i = 0;
        db.beginTransaction();
        try {
            for (FarmEntity farm : farms) {
                if (!isFarmPopulated(farms.get(i).getFarm_ID(), db)) {
                    db.insertOrThrow(FarmModel.TABLE, null, setInfoFarm(farm, id));
                }
                i++;
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();

    }

    public void addListLivestock(List<LivestockEntity> livestocks) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (LivestockEntity livestock : livestocks) {
                db.insertOrThrow(LivestockMaster.TABLE, null, setInfoListLivestock(livestock));
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFeedSubTypeFormat(List<SubTypeFormatEntity> subTypeFormats) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (SubTypeFormatEntity subTypeFormat : subTypeFormats) {
                db.insertOrThrow(FeedSubTypeFormatMaster.TABLE, null, setInfoSubTypeFormat(subTypeFormat));
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFeedSubType(List<SubTypeEntity> subTypes) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (SubTypeEntity subType : subTypes) {
                db.insertOrThrow(FeedSubTypeMaster.TABLE, null, setInfoSubType(subType));
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFeedType(List<FeedTypeEntity> feedTypes) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (FeedTypeEntity feedType : feedTypes) {
                db.insertOrThrow(FeedTypeMaster.TABLE, null, setInfoFeedType(feedType));
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }


    public void updateFarmer(FarmerEntity farmer, BigDecimal id) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.update(FarmerModel.TABLE, setInfoFarmer(farmer), FarmerModel.Column.FARMER_ID + " = " + id, null);

        db.close();
    }

    public void updateFarm(FarmEntity farm, BigDecimal id, BigDecimal farmer) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.update(FarmModel.TABLE, setInfoFarm(farm, farmer), FarmModel.Column.FARM_ID + " = " + id, null);

        db.close();
    }

    public void deleteFeed(){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(FeedModel.TABLE,null,null);

        db.close();
    }


    public ContentValues setInfoFarmer(FarmerEntity farmer) {

        ContentValues values = new ContentValues();

        values.put(FarmerModel.Column.FARMER_ID, AppUtils.checkNullBigdecimal(farmer.getFarmer_ID()));

        values.put(FarmerModel.Column.AMPHUR_ID, farmer.getAmphur_ID());
        values.put(FarmerModel.Column.BIRTHDAY, farmer.getBirthDay());
        values.put(FarmerModel.Column.COMPANY_TYPE_ID, farmer.getCompany_Type_ID());
        values.put(FarmerModel.Column.CREATE_BY, farmer.getCreateBy());
        values.put(FarmerModel.Column.CREATE_DATE, farmer.getCreateDate());
        values.put(FarmerModel.Column.DEBTAMOUNT, AppUtils.checkNullBigdecimal(farmer.getDebtAmount()));
        values.put(FarmerModel.Column.DEBTSAMOUNT_IN, AppUtils.checkNullBigdecimal(farmer.getDebtsAmountIn()));
        values.put(FarmerModel.Column.DEBTSAMOUNT_OUT, AppUtils.checkNullBigdecimal(farmer.getDebtsAmountOut()));
        values.put(FarmerModel.Column.EMAIL, farmer.getEmail());
        values.put(FarmerModel.Column.FARMER_GROUP_ID, farmer.getFarmer_Group_ID());
        values.put(FarmerModel.Column.FARMER_PROBLEM_ID, farmer.getFarmer_Problem_ID());
        values.put(FarmerModel.Column.FARMER_PROBLEM_OTHER, farmer.getFarmer_Problem_Other());
        values.put(FarmerModel.Column.FARMER_STATUS_ID, farmer.getStatus_ID());
        values.put(FarmerModel.Column.FARMER_TYPE_ID, farmer.getFarmer_Type_ID());
        values.put(FarmerModel.Column.FIRST_NAME, farmer.getFirst_Name());
        values.put(FarmerModel.Column.GENDER_ID, farmer.getGender_ID());
        values.put(FarmerModel.Column.GOVERNMENT_HELP_ID, farmer.getGovernment_Help_ID());
        values.put(FarmerModel.Column.HOLDING_EQUIPMENT, farmer.getHolding_Equipment());
        values.put(FarmerModel.Column.HOLDING_LAND_ID, farmer.getHolding_Land_ID());
        values.put(FarmerModel.Column.HOME_ID, AppUtils.checkNullBigdecimal(farmer.getHome_ID()));
        values.put(FarmerModel.Column.HOME_NO, farmer.getHomeNo());
        values.put(FarmerModel.Column.IMAGE_ID, farmer.getImage_ID());
        values.put(FarmerModel.Column.IS_AGENCY, farmer.getIsAgency());
        values.put(FarmerModel.Column.IS_CANCEL, farmer.getIsCancel());
        values.put(FarmerModel.Column.IS_DEAD, farmer.getIsDead());
        values.put(FarmerModel.Column.LASTNAME, farmer.getLast_Name());
        values.put(FarmerModel.Column.LATITUDE, AppUtils.checkNullBigdecimal(farmer.getLatitude()));
        values.put(FarmerModel.Column.LONGITUDE, AppUtils.checkNullBigdecimal(farmer.getLongitude()));
        values.put(FarmerModel.Column.MAIN_JOB_ID, farmer.getMain_Job_ID());
        values.put(FarmerModel.Column.MARITAL_STATUS_ID, farmer.getMarital_Status_ID());
        values.put(FarmerModel.Column.MOBILE, farmer.getMobile());
        values.put(FarmerModel.Column.PHONE, farmer.getPhone());
        values.put(FarmerModel.Column.PID, AppUtils.checkNullBigdecimal(farmer.getPid()));
        values.put(FarmerModel.Column.POSTCODE, farmer.getPostCode());
        values.put(FarmerModel.Column.PREFIX_ID, farmer.getPrefix_ID());
        values.put(FarmerModel.Column.PROVINCE_ID, farmer.getProvince_ID());
        values.put(FarmerModel.Column.REVENUE, AppUtils.checkNullBigdecimal(farmer.getRevenue()));
        values.put(FarmerModel.Column.ROAD, farmer.getRoad());
        values.put(FarmerModel.Column.SECOND_JOB_ID, farmer.getSecond_Job_ID());
        values.put(FarmerModel.Column.SOI, farmer.getSoi());
        values.put(FarmerModel.Column.STATUS_ID, farmer.getStatus_ID());
        values.put(FarmerModel.Column.TAMBOL_ID, farmer.getTambol_ID());
        values.put(FarmerModel.Column.TAX_ID, farmer.getTax_ID());
        values.put(FarmerModel.Column.UPDATE_BY, farmer.getUpdateBy());
        values.put(FarmerModel.Column.UPDATE_DATE, farmer.getUpdateDate());
        values.put(FarmerModel.Column.VILLAGE, farmer.getVillage());
        values.put(FarmerModel.Column.VILLAGE_ID, farmer.getVillage_ID());

        return values;
    }

    public ContentValues setInfoFarm(FarmEntity farm, BigDecimal id) {

        ContentValues values = new ContentValues();

        values.put(FarmModel.Column.FARM_ID, AppUtils.checkNullBigdecimal(farm.getFarm_ID()));
        values.put(FarmModel.Column.AMPHUR_ID, farm.getAmphur_ID());
        values.put(FarmModel.Column.ANIMAL_WORTH, AppUtils.checkNullBigdecimal(farm.getAnimalWorth()));
        values.put(FarmModel.Column.AREA_RAI, AppUtils.checkNullBigdecimal(farm.getArea_Rai()));
        values.put(FarmModel.Column.AREA_NGAN, AppUtils.checkNullBigdecimal(farm.getArea_Ngan()));
        values.put(FarmModel.Column.AREA_TYPE_ID, farm.getArea_Type_ID());
        values.put(FarmModel.Column.CANCEL_BY, farm.getCancelBy());
        values.put(FarmModel.Column.CANCEL_DATE, farm.getCancelDate());
        values.put(FarmModel.Column.CREATE_BY, farm.getCreateBy());
        values.put(FarmModel.Column.CREATE_DATE, farm.getCreateDate());
        values.put(FarmModel.Column.EMAIL, farm.getEmail());
        values.put(FarmModel.Column.FARM_NAME, farm.getFarm_Name());
        values.put(FarmModel.Column.FARM_OWNER_TYPE_ID, farm.getFarm_Owner_Type_ID());
        values.put(FarmModel.Column.FARM_PROBLEM_DESC, farm.getFarm_Problem_Desc());
        values.put(FarmModel.Column.FARM_STANDARD_NUMBER, farm.getFarm_Standard_Number());
        values.put(FarmModel.Column.FARMER_ID, id.toPlainString());
        values.put(FarmModel.Column.HOME_NO, farm.getHomeNo());
        values.put(FarmModel.Column.IS_CANCEL, farm.getIsCancel());
        values.put(FarmModel.Column.LATITUDE, AppUtils.checkNullBigdecimal(farm.getLatitude()));
        values.put(FarmModel.Column.LONGITUDE, AppUtils.checkNullBigdecimal(farm.getLongitude()));
        values.put(Column.MOO, farm.getMoo());
        values.put(FarmModel.Column.MOBILE, farm.getMobile());
        values.put(FarmModel.Column.ORDER_INDEX, farm.getOrderIndex());
        values.put(FarmModel.Column.PHONE, farm.getPhone());
        values.put(FarmModel.Column.POSTCODE, farm.getPostCode());
        values.put(FarmModel.Column.PROVINCE_ID, farm.getProvince_ID());
        values.put(FarmModel.Column.REVENUE_OF_LIVESTOCK, AppUtils.checkNullBigdecimal(farm.getRevenueOfLivestock()));
        values.put(FarmModel.Column.ROAD, farm.getRoad());
        values.put(FarmModel.Column.SOI, farm.getSoi());
        values.put(FarmModel.Column.STANDARD_ID, farm.getStandard_ID());
        values.put(FarmModel.Column.STATUS_ID, farm.getStatus_ID());
        values.put(FarmModel.Column.SUPPORT_STANDARD_ID, farm.getSupport_Standard_ID());
        values.put(FarmModel.Column.TAMBOL_ID, farm.getTambol_ID());
        values.put(FarmModel.Column.UPDATE_BY, farm.getUpdateBy());
        values.put(FarmModel.Column.UPDATE_DATE, farm.getUpdateDate());
        values.put(Column.VILLAGE_NAME, farm.getVillage_Name());
        values.put(FarmModel.Column.VILLAGE_ID, farm.getVillage_ID());
        values.put(FarmModel.Column.WORKER_ID, farm.getWorker_ID());

        return values;

    }

    public ContentValues setInfoListLivestock(LivestockEntity livestock) {
        ContentValues values = new ContentValues();

        values.put(LivestockMaster.Column.LIVESTOCK_TYPE_ID, livestock.getLivestock_Type_ID());
        values.put(LivestockMaster.Column.CREATE_BY, livestock.getCreateBy());
        values.put(LivestockMaster.Column.CREATE_DATE, livestock.getCreateDate());
        values.put(LivestockMaster.Column.LIVESTOCK_TYPE_CODE, livestock.getLivestock_Type_Code());
        values.put(LivestockMaster.Column.LIVESTOCK_TYPE_FLGTAIL, livestock.getLivestock_Type_Flgtail());
        values.put(LivestockMaster.Column.LIVESTOCK_TYPE_FULLNAME, livestock.getLivestock_Type_FullName());
        values.put(LivestockMaster.Column.LIVESTOCK_TYPE_NAME, livestock.getLivestock_Type_Name());
        values.put(LivestockMaster.Column.LIVESTOCK_TYPE_OLD_ID, livestock.getLivestock_Type_Old_ID());
        values.put(LivestockMaster.Column.LIVESTOCK_TYPE_PARENT, livestock.getLivestock_Type_Parent());
        values.put(LivestockMaster.Column.LIVESTOCK_TYPE_PRICE, AppUtils.checkNullBigdecimal(livestock.getLivestock_Type_Price()));
        values.put(LivestockMaster.Column.LIVESTOCK_TYPE_UNIT, livestock.getLivestock_Type_Unit());
        values.put(LivestockMaster.Column.LIVESTOCK_TYPE_UNITLIMIT, livestock.getLivestock_Type_UnitLimit());
        values.put(LivestockMaster.Column.UPDATE_BY, livestock.getUpdateBy());
        values.put(LivestockMaster.Column.UPDATE_DATE, livestock.getUpdateDate());
        values.put(LivestockMaster.Column.STATUS_ID, livestock.getStatus_ID());
        values.put(LivestockMaster.Column.ORDER_INDEX, livestock.getOrderIndex());
        values.put(LivestockMaster.Column.LIVESTOCK_TYPE_ROOT, livestock.getLivestock_Type_Root());

        return values;
    }

    public ContentValues setInfoFeed(FeedEntity feed) {

        ContentValues values = new ContentValues();

        values.put(FeedModel.Column.CREATE_BY, feed.getCreateBy());
        values.put(FeedModel.Column.CREATE_DATE, feed.getCreateDate());
        values.put(FeedModel.Column.FARM_ID, feed.getFarm_ID());
        values.put(FeedModel.Column.FEED_AREA_NGAN, AppUtils.checkNullBigdecimal(feed.getFeed_Area_Ngan()));
        values.put(FeedModel.Column.FEED_AREA_RAI, AppUtils.checkNullBigdecimal(feed.getFeed_Area_Rai()));
        values.put(FeedModel.Column.FEED_AREA_WA, AppUtils.checkNullBigdecimal(feed.getFeed_Area_Wa()));
        values.put(FeedModel.Column.FEED_ID, feed.getFeed_ID());
        values.put(FeedModel.Column.FEED_KIND_ID, feed.getFeed_Kind_ID());
        values.put(FeedModel.Column.FEED_KIND_NAME, feed.getFeed_Kind_Name());
        values.put(FeedModel.Column.FEED_KIND_OTHER, feed.getFeed_Kind_Other());
        values.put(FeedModel.Column.FEED_SUBTYPE_FORMAT_ID, feed.getFeed_SubType_Format_ID());
        values.put(FeedModel.Column.FEED_SUBTYPE_ID, feed.getFeed_SubType_ID());
        values.put(FeedModel.Column.FEED_SUBTYPE_NAME, feed.getFeed_SubType_Name());
        values.put(FeedModel.Column.FEED_TYPE_ID, feed.getFeed_Type_ID());
        values.put(FeedModel.Column.FEED_TYPE_NAME, feed.getFeed_Type_Name());
        values.put(FeedModel.Column.FEED_TYPE_OTHER, feed.getFeed_Type_Other());
        values.put(FeedModel.Column.FEED_WATER_TYPE_ID, feed.getFeed_Water_Type_ID());
        values.put(FeedModel.Column.FEED_WATER_TYPE_NAME, feed.getFeed_Water_Type_Name());
        values.put(FeedModel.Column.ORDER_INDEX, feed.getOrderIndex());
        values.put(FeedModel.Column.STATUS_ID, feed.getStatus_ID());
        values.put(FeedModel.Column.STATUS_NAME, feed.getStatus_Name());
        values.put(FeedModel.Column.UPDATE_BY, feed.getUpdateBy());
        values.put(FeedModel.Column.UPDATE_DATE, feed.getUpdateDate());
        values.put(FeedModel.Column.LOCAL_ONLY, feed.isLocalOnly());

        return values;
    }

    public ContentValues setInfoSubTypeFormat(SubTypeFormatEntity subTypeFormat) {
        ContentValues values = new ContentValues();

        values.put(FeedSubTypeFormatMaster.Column.FEED_SUBTYPE_FORMAT_ID, subTypeFormat.getMS_Feed_SubType_Format_ID());
        values.put(FeedSubTypeFormatMaster.Column.FEED_SUBTYPE_FORMAT_NAME, subTypeFormat.getMS_Feed_SubType_Format_Name());
        values.put(FeedSubTypeFormatMaster.Column.ORDER_INDEX, subTypeFormat.getOrderIndex());
        values.put(FeedSubTypeFormatMaster.Column.STATUS_ID, subTypeFormat.getStatus_ID());

        return values;
    }

    public ContentValues setInfoSubType(SubTypeEntity subType) {
        ContentValues values = new ContentValues();

        values.put(FeedSubTypeMaster.Column.FEED_SUBTYPE_ID, subType.getFeed_SubType_ID());
        values.put(FeedSubTypeMaster.Column.FEED_SUBTYPE_NAME, subType.getFeed_SubType_Name());
        values.put(FeedSubTypeMaster.Column.ORDER_INDEX, subType.getOrderIndex());
        values.put(FeedSubTypeMaster.Column.STATUS_ID, subType.getStatus_ID());

        return values;

    }

    public ContentValues setInfoFeedType(FeedTypeEntity feedType) {
        ContentValues values = new ContentValues();

        values.put(FeedTypeMaster.Column.FEED_TYPE_ID, feedType.getFeed_Type_ID());
        values.put(FeedTypeMaster.Column.FEED_TYPE_NAME, feedType.getFeed_Type_Name());
        values.put(FeedTypeMaster.Column.ORDER_INDEX, feedType.getOrderIndex());
        values.put(FeedTypeMaster.Column.STATUS_ID, feedType.getStatus_ID());

        return values;
    }


}
