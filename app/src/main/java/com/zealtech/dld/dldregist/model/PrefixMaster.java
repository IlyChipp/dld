package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/26/2017 AD.
 */
@Parcel
public class PrefixMaster implements APIResponse{

    //DBHelper
    public static final String DATABASE_NAME = "dldregislive.db";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE = "prefix";


    String responseCode;
    String responseMessage;
    ArrayList<PrefixEntity> data;

    public PrefixMaster(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<PrefixEntity> getData() {
        return data;
    }

    public void setData(ArrayList<PrefixEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class PrefixEntity {
        int prefix_ID;
        int orderIndex;
        String prefix_NameEn;
        String prefix_NameTh;
        int status_ID;

        public PrefixEntity(){ }

        public int getPrefix_ID() {
            return prefix_ID;
        }

        public void setPrefix_ID(int prefix_ID) {
            this.prefix_ID = prefix_ID;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public String getPrefix_NameEn() {
            return prefix_NameEn;
        }

        public void setPrefix_NameEn(String prefix_NameEn) {
            this.prefix_NameEn = prefix_NameEn;
        }

        public String getPrefix_NameTh() {
            return prefix_NameTh;
        }

        public void setPrefix_NameTh(String prefix_NameTh) {
            this.prefix_NameTh = prefix_NameTh;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String PREFIX_ID = "prefix_id";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String PREFIX_NAME_TH = "prefix_NameTh";
        public static final String PREFIX_NAME_EN = "prefix_NameEn";
        public static final String STATUS_ID = "status_ID";
    }

}
