package com.zealtech.dld.dldregist.util;

import com.zealtech.dld.dldregist.model.AmphurMaster.AmphurEntity;
import com.zealtech.dld.dldregist.model.ProvinceMaster.ProvinceEntity;
import com.zealtech.dld.dldregist.model.TambolMaster.TambolEntity;
import com.zealtech.dld.dldregist.model.VillageMaster.VillageEntity;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/27/2017 AD.
 */

public class MasterDataUtils {
    public static ArrayList<ProvinceEntity> provinces;
    public static ArrayList<AmphurEntity> amphurs;
    public static ArrayList<TambolEntity> tambols;
    public static ArrayList<VillageEntity> villages;


//    public static ArrayList<ProvinceEntity> getProvinces(Context context) {
//        if(provinces == null){
//            MyPreferenceManager myPreferenceManager = new MyPreferenceManager(context);
//            Gson gson = new Gson();
//            String json = myPreferenceManager.getProvince();
//            provinces = gson.fromJson(json, new TypeToken<ArrayList<ProvinceEntity>>() {}.getType());
//        }
//        return provinces;
//    }
//
//    public static ArrayList<AmphurEntity> getAmphurs(Context context, int provinceId){
//        if(amphurs == null){
//            MyPreferenceManager myPreferenceManager = new MyPreferenceManager(context);
//            Gson gson = new Gson();
//            String json = myPreferenceManager.getAmphur();
//            amphurs = gson.fromJson(json, new TypeToken<ArrayList<AmphurEntity>>() {}.getType());
//        }
//
//        ArrayList<AmphurEntity> amphurs_ = new ArrayList<AmphurEntity>();
//        for(AmphurEntity amphur : amphurs){
//            if(amphur.getProvince_ID() == provinceId){
//                amphurs_.add(amphur);
//            }
//        }
//        return amphurs_;
//    }
//
//    public static ArrayList<TambolEntity> getTambols(Context context, int amphurId){
//        if(tambols == null){
//            MyPreferenceManager myPreferenceManager = new MyPreferenceManager(context);
//            Gson gson = new Gson();
//            String json = myPreferenceManager.getTambol();
//            tambols = gson.fromJson(json, new TypeToken<ArrayList<TambolEntity>>() {}.getType());
//        }
//
//        ArrayList<TambolEntity> tambols_ = new ArrayList<>();
//        for(TambolEntity tambol : tambols){
//            if(tambol.getAmphur_ID() == amphurId){
//                tambols_.add(tambol);
//            }
//        }
//        return tambols_;
//    }
//
//    public static ArrayList<VillageEntity> getVillages(Context context, int tambolID){
//        if(villages == null){
//            MyPreferenceManager myPreferenceManager = new MyPreferenceManager(context);
//            Gson gson = new Gson();
//            String json = myPreferenceManager.getVillage();
//            villages = gson.fromJson(json, new TypeToken<ArrayList<VillageEntity>>() {}.getType());
//        }
//
//        ArrayList<VillageEntity> villages_ = new ArrayList<>();
//        for(VillageEntity village : villages){
//            if(village.getVillage_ID() == tambolID){
//                villages_.add(village);
//            }
//        }
//        return villages_;
//    }
}
