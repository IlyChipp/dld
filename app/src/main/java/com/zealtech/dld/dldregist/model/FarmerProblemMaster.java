package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/28/2017 AD.
 */
@Parcel
public class FarmerProblemMaster implements APIResponse{

    //DBHelper
    public static final String TABLE = "farmer_problem";

    String responseCode;
    String responseMessage;
    ArrayList<FarmerProblemEntity> data;

    public FarmerProblemMaster(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<FarmerProblemEntity> getData() {
        return data;
    }

    public void setData(ArrayList<FarmerProblemEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class FarmerProblemEntity {
        int farmer_Problem_ID;
        String farmer_Problem_Desc;
        String farmer_Problem_Name;
        int orderIndex;
        int status_ID;

        public FarmerProblemEntity(){ }

        public int getFarmer_Problem_ID() {
            return farmer_Problem_ID;
        }

        public void setFarmer_Problem_ID(int farmer_Problem_ID) {
            this.farmer_Problem_ID = farmer_Problem_ID;
        }

        public String getFarmer_Problem_Desc() {
            return farmer_Problem_Desc;
        }

        public void setFarmer_Problem_Desc(String farmer_Problem_Desc) {
            this.farmer_Problem_Desc = farmer_Problem_Desc;
        }

        public String getFarmer_Problem_Name() {
            return farmer_Problem_Name;
        }

        public void setFarmer_Problem_Name(String farmer_Problem_Name) {
            this.farmer_Problem_Name = farmer_Problem_Name;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String FARMER_PROBLEM_ID = "farmer_Problem_ID";
        public static final String FARMER_PROBLEM_DESC = "farmer_Problem_Desc";
        public static final String FARMER_PROBLEM_NAME = "farmer_Problem_Name";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
    }


}
