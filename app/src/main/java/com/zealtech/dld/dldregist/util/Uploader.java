package com.zealtech.dld.dldregist.util;

import android.content.Context;
import android.util.Log;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

/**
 * Created by fanqfang on 10/9/2017 AD.
 */

class RequestSerializer extends Serializer<Request> {

    public static Object getPrivateMember(Object obj, String fieldName)
            throws NoSuchFieldException
    {
        Class<?> klass = obj.getClass();
        Field field = klass.getDeclaredField(fieldName);
        field.setAccessible(true);
        Object res = null;
        try {
            res = field.get(obj);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return res;
    }

    <T> void writeDefault(Kryo kryo, Output out, T obj)
    {
        Class<?> klass = obj.getClass();
        Serializer serializer = kryo.getDefaultSerializer(klass);
        serializer.write(kryo, out, obj);
    }

    <T> T readDefault(Kryo kryo, Input in, Class<T> klass) {
        Serializer serializer = kryo.getDefaultSerializer(klass);
        return (T)serializer.read(kryo, in, byte[].class);
    }

    @Override
    public void write(Kryo kryo, Output out, Request r) {
        out.writeString(r.url().toString());
        out.writeString(r.method());
        RequestBody body = r.body();
        if (body == null) {
            out.writeBoolean(false);
        } else {
            out.writeBoolean(true);
            MediaType mt = body.contentType();
            out.writeString(mt.toString());
            Buffer buf = new Buffer();
            try {
                r.body().writeTo(buf);
            } catch (IOException e) {
                // should never happen
                throw new RuntimeException(e);
            }
            byte[] data = buf.snapshot().toByteArray();
            writeDefault(kryo, out, data);
        }
        String[] headers = null;
        try {
            headers = (String[])getPrivateMember(r.headers(),
                    "namesAndValues");
        } catch (NoSuchFieldException e) {
            System.out.println(e);
        }
        writeDefault(kryo, out, headers);
        // Tag will not be serialized ...
    }

    @Override
    public Request read(Kryo kryo, Input in, Class<Request> type) {
        Request.Builder b = new Request.Builder();
        String url = in.readString();
        b.url(url);
        String method = in.readString();
        // got body
        if (in.readBoolean()) {
            MediaType mt = MediaType.parse(in.readString());
            byte[] data = readDefault(kryo, in, byte[].class);
            RequestBody body = RequestBody.create(mt, data);
            b.method(method, body);
        } else {
            b.method(method, null);
        }
        String[] headers = readDefault(kryo, in, String[].class);
        Headers.Builder hb = new Headers.Builder();
        for (int i = 0; i < headers.length - 1; i += 2) {
            hb.add(headers[i], headers[i + 1]);
        }
        b.headers(hb.build());
        // b.headers(new Headers(headers));
        return b.build();
    }
}

// TODO: maybe create a thread safe custom queue that automatically runs caching function on change.
// TODO: current implementation is bug-prone and ugly (with use of synchronized)

public class Uploader {

    static final String TAG = "Uploader";
    static final String CACHE_FILE = "upload-queue.kryo";
    static Context context = null;
    static Thread uploaderThread = null;
    static Semaphore semaphore = new Semaphore(0);
    static Kryo kryo = null;

    // Queue access must be synchronized.
    // Any modifications must be atomically associated with cache dumps so that cache is always
    // in-sync with queue.
    static LinkedList<Request> queue;

    private Uploader() {}

    synchronized public static void init(Context context) {
        if (Uploader.context != null) {
            Log.w(TAG, "Tried to run init again, ignored.");
            return;
        }
        Uploader.context = context;
        queue = new LinkedList<>();
        kryo = new Kryo();
        kryo.register(Request.class, new RequestSerializer());
        File f = new File(context.getCacheDir(), CACHE_FILE);
        Input in = null;
        try {
            in = new Input(new FileInputStream(f));
        } catch (FileNotFoundException e) {
            Log.d(TAG, "No cached uploads.");
            return;
        }
        try {
            queue = kryo.readObject(in, LinkedList.class);
        } catch (Exception e) {
            Log.d(TAG, "Upload cache corrupted: " + e);
        }
        in.close();
        if (!queue.isEmpty())
            Log.d(TAG, queue.size() + " pending uploads restored from cache.");
        // starts uploader thread also...
        pingUploaderThread();
    }

    public static void sleep(int ms) {
        // TODO: on interrupt sleep only the remaining time, not the full time again.
        while (true) {
            try {
                Thread.sleep(ms);
                break;
            } catch (InterruptedException e) {
                continue;
            }
        }
    }

    // We dont want race condition that could allow many simultaneous uploaderThreads.
    synchronized private static void ensureUploaderThread() {
        if (uploaderThread != null && uploaderThread.isAlive())
            return;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "uploader-thread started.");
                while (true) {
                    //Log.d(TAG, "uploader-thread waiting for signal...");
                    try {
                        semaphore.tryAcquire(30, TimeUnit.MINUTES);
                    } catch (InterruptedException e) { }
                    while (!flush()) {
                        Log.d(TAG, "flush() failed, retrying later...");
                        sleep(60 * 1000);
                    }
                }
            }
        });
        t.setName("uploader-thread");
        t.start();
        uploaderThread = t;
    }

    private static void pingUploaderThread() {
        ensureUploaderThread();
        semaphore.release();
    }

    synchronized private static void dumpToCache() {
        Log.d(TAG, "dumping " + queue.size() + " entries to cache...");
        File f = new File(context.getCacheDir(), CACHE_FILE);
        Output out = null;
        try {
            out = new Output(new FileOutputStream(f));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        kryo.writeObject(out, queue);
        out.close();
    }

    public static void enqueue(Request req) {
        synchronized (Uploader.class) {
            queue.add(req);
            dumpToCache();
        }
        pingUploaderThread();
    }

    // returns false if there is a problem with the connection, otherwise true
    private static boolean sendRequest(Request req) {
        OkHttpClient client = new OkHttpClient();
        try {
            Response res = client.newCall(req).execute();
            if (!res.isSuccessful()) {
                Log.d(TAG, "Problematic status on request " + req);
                Log.d(TAG, "Status: " + res.code());
            }
        } catch (IOException e) {
            Log.d(TAG, "Exception executing request " + req + ":");
            Log.d(TAG, e.toString());
            return false;
        }
        return true;
    }

    private static boolean flush() {
        synchronized (Uploader.class) {
            Log.d(TAG, "flushing " + queue.size() + " entries...");
        }
        Request req;
        while (true) {
            synchronized (Uploader.class) {
                if (queue.isEmpty())
                    break;
                req = queue.peek();
            }
            if (sendRequest(req)) {
                synchronized (Uploader.class) {
                    queue.poll();
                    dumpToCache();
                }
            } else {
                return false;
            }
        }
        return true;
    }

}
