package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/28/2017 AD.
 */
@Parcel
public class FeedSubTypeFormatMaster implements APIResponse{
    //DBHelper
    public static final String TABLE = "feed_subtype_format";

    String responseCode;
    String responseMessage;
    ArrayList<SubTypeFormatEntity> data;

    public FeedSubTypeFormatMaster(){ }

    public static String getTABLE() {
        return TABLE;
    }

    @Override
    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<SubTypeFormatEntity> getData() {
        return data;
    }

    public void setData(ArrayList<SubTypeFormatEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class SubTypeFormatEntity{
        int MS_Feed_SubType_Format_ID;
        String MS_Feed_SubType_Format_Name;
        int orderIndex;
        int status_ID;

        public SubTypeFormatEntity(){ }

        public int getMS_Feed_SubType_Format_ID() {
            return MS_Feed_SubType_Format_ID;
        }

        public void setMS_Feed_SubType_Format_ID(int MS_Feed_SubType_Format_ID) {
            this.MS_Feed_SubType_Format_ID = MS_Feed_SubType_Format_ID;
        }

        public String getMS_Feed_SubType_Format_Name() {
            return MS_Feed_SubType_Format_Name;
        }

        public void setMS_Feed_SubType_Format_Name(String MS_Feed_SubType_Format_Name) {
            this.MS_Feed_SubType_Format_Name = MS_Feed_SubType_Format_Name;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        @Override
        public String toString() {
            return MS_Feed_SubType_Format_Name;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String FEED_SUBTYPE_FORMAT_ID = "MS_Feed_SubType_Format_ID";
        public static final String FEED_SUBTYPE_FORMAT_NAME = "MS_Feed_SubType_Format_Name";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
    }
}
