package com.zealtech.dld.dldregist.fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.camera.CameraModule;
import com.esafirm.imagepicker.features.camera.ImmediateCameraModule;
import com.esafirm.imagepicker.features.camera.OnImageReadyListener;
import com.esafirm.imagepicker.model.Image;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.squareup.picasso.Picasso;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.MainActivity;
import com.zealtech.dld.dldregist.activity.MapActivity;
import com.zealtech.dld.dldregist.activity.PIDScanActivity;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.listener.PIDScannerListener;
import com.zealtech.dld.dldregist.manager.Contextor;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.AmphurMaster.AmphurEntity;
import com.zealtech.dld.dldregist.model.CompanyTypeMaster;
import com.zealtech.dld.dldregist.model.FarmerModel.FarmerEntity;
import com.zealtech.dld.dldregist.model.PrefixMaster.PrefixEntity;
import com.zealtech.dld.dldregist.model.ProvinceMaster.ProvinceEntity;
import com.zealtech.dld.dldregist.model.TambolMaster.TambolEntity;
import com.zealtech.dld.dldregist.model.VillageMaster.VillageEntity;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseBase;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseImage;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseInsertFarmer;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.util.DataValidator;
import com.zealtech.dld.dldregist.util.UpdateListener;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;
import com.zealtech.dld.dldregist.widget.MySpinnerAdapter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.media.MediaRecorder.VideoSource.CAMERA;
import static com.zealtech.dld.dldregist.util.AppUtils.REQUEST_LOCATION;
import static com.zealtech.dld.dldregist.util.AppUtils.RequestPermissionCodeGallery;
import static com.zealtech.dld.dldregist.util.AppUtils.RequestPermissionCodeLocation;


public class FarmerFragment extends Fragment implements PIDScannerListener {

    private static final int REQUEST_BARCODE = 3;
    private static final String CLASS_NAME = "FarmerFragment";

    ArrayList<String> listProvince = new ArrayList<>();
    ArrayList<String> listPrefix = new ArrayList<>();
    ArrayList<String> listVillage = new ArrayList<>();
    ArrayList<String> listTambol = new ArrayList<>();
    ArrayList<String> listAmphur = new ArrayList<>();
    private DatabaseRealm databaseRealm;
    ArrayList<ProvinceEntity> province = new ArrayList<>();
    ArrayList<AmphurEntity> amphur = new ArrayList<>();
    ArrayList<TambolEntity> tambol = new ArrayList<>();
    ArrayList<VillageEntity> village = new ArrayList<>();
    ArrayList<PrefixEntity> prefix = new ArrayList<>();
    ArrayList<String> listCompanies = new ArrayList<>();
    DBHelper dbHelper;
    Boolean isPopulate = true;
    final static String PROVINCE = "provinces";
    final static String AMPHUR = "amphurs";
    final static String TAMBOL = "tambols";
    final static String VILLAGE = "villages";
    HashMap<String,String> result_;
    private FarmerEntity mFarmer;
    private Farmer mFarmerItem;
    private MyEditNoHLTextView txtPID;
    private MyEditNoHLTextView txtNameTH;
    private MyEditNoHLTextView txtLastnameTH;
    private MyEditNoHLTextView txtHomeID;
    private MyEditNoHLTextView txtHomeNo;
    private MyEditNoHLTextView txtPost;
    private MyEditNoHLTextView txtMobile;
    private MyEditNoHLTextView txtPhone, txtTaxId, txtCompanyName;
    private MyEditNoHLTextView txtLat;
    private MyEditNoHLTextView txtLong;
    private MyEditNoHLTextView txtEmail;
    private LinearLayout addFarm;
    private LinearLayout llFarmerAddress, linearLayout_type;
    private Spinner spnAmphur;
    private Spinner spnPrefix;
    private Spinner spnTambol;
    private Spinner spnProvince;
    private Spinner spnVillage;
    private MyEditNoHLTextView txtBD;
    private MyNoHBTextView btnSubmit, title_type, title_info, btnLatLong,btnRead;
//    private MyEditNoHLTextView txtMoo;
    private MyPreferenceManager myPreferenceManager;
    private ImageView imgProfile;
    private FrameLayoutDisable progressBar;
    private Spinner spnCompany;
    private int type, from;
    ArrayList<CompanyTypeMaster.CompanyEntity> companies = new ArrayList<>();
    private MyNoHLTextView txtTopicType;

    private static final int RC_CODE_PICKER = 2000;
    private static final int RC_CAMERA = 3000;
    private static final int WRITE_STORAGE = 4000;
    private CameraModule cameraModule;

    private ArrayList<Image> images = new ArrayList<>();
    public FarmerFragment() {
        super();
    }

    public static FarmerFragment newInstance() {
        FarmerFragment fragment = new FarmerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;

    }

    Boolean isOnline;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_farmer, container, false);
        View v = inflater.inflate(R.layout.item_farmer_address, container, false);
        ButterKnife.bind(this, rootView);
        ButterKnife.bind(this, v);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        initInstances(rootView, v);
        myPreferenceManager = new MyPreferenceManager(getContext());
        llFarmerAddress.addView(v);

        isOnline = AppUtils.isOnline(getActivity());
        from = ((MainActivity) getActivity()).getFrom();
        type = ((MainActivity) getActivity()).getRegisType();
        dbHelper = new DBHelper(getContext());
        mFarmerItem = ((MainActivity) getActivity()).getFarmerItem();
        if (mFarmerItem.getPid() == 0) {
//            mFarmerItem = new Farmer();
            isPopulate = false;
        }
       /* if (mFarmerItem == null) {
            mFarmerItem = new Farmer();
            isPopulate = false;
        }*/

        if (from != 2) {
            if (type == 0) {
                linearLayout_type.setVisibility(View.GONE);
                title_info.setText(getString(R.string.farmer));
            } else if (type == 1) {
                linearLayout_type.setVisibility(View.VISIBLE);
                title_type.setText("ข้อมูลนิติบุคคล");
                title_info.setText("ข้อมูลผู้ประสานงาน");
                txtTopicType.setText("ชื่อบริษัท");
            } else {
                linearLayout_type.setVisibility(View.VISIBLE);
                title_info.setText("ข้อมูลผู้ประสานงาน");
                title_type.setText("ข้อมูลหน่วยงาน");
                txtTopicType.setText("ชื่อหน่วยงาน");
            }
        } else {
            if (mFarmerItem.getCompanyTypeID() != null) {
                if (mFarmerItem.getCompanyTypeID() == 0) {
                    linearLayout_type.setVisibility(View.GONE);
                    title_info.setText(getString(R.string.farmer));
                } else if (type == 1) {
                    linearLayout_type.setVisibility(View.VISIBLE);
                    title_type.setText("ข้อมูลนิติบุคคล");
                    title_info.setText("ข้อมูลผู้ประสานงาน");
                }
            } else {
                linearLayout_type.setVisibility(View.GONE);
                title_info.setText(getString(R.string.farmer));
            }
        }
        if (from == 1) {
            mFarmerItem.setStatusID(1);
        }

     if (mFarmerItem.getPid()!=0&&mFarmerItem.getPid()!=null){
         getImage(String.valueOf(mFarmerItem.getPid()));
     }
        setInfoFarmer();
//        setEnabled();

//        if (!dbHelper.isTablePopulated(FarmerModel.TABLE)) {
//            mFarmer = dbHelper.getInfoFarmer();
//            setInfoFarmer();
//        } else {
//            mFarmer = new FarmerEntity();
//            txtPID.setEnabled(true);
//        }
//        addTotalSpinner();
        getListPrefix();
        getListProvince();
        if (type!=0){ //not farmer
        getListCompany();}

        txtBD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate();
            }
        });

        if(!(from==1||from==3)){
            txtPID.setEnabled(false);
            txtPID.setBackground(getResources().getDrawable(R.drawable.shape_edit_text_disable));
            txtTaxId.setEnabled(false);
            txtTaxId.setBackground(getResources().getDrawable(R.drawable.shape_edit_text_disable));
            txtCompanyName.setEnabled(false);
            txtCompanyName.setBackground(getResources().getDrawable(R.drawable.shape_edit_text_disable));
            txtBD.setEnabled(false);
            txtBD.setBackground(getResources().getDrawable(R.drawable.shape_edit_text_disable));
            spnPrefix.setEnabled(false);
            spnPrefix.setBackground(getResources().getDrawable(R.drawable.shape_edit_text_disable));
            txtNameTH.setEnabled(false);
            txtNameTH.setBackground(getResources().getDrawable(R.drawable.shape_edit_text_disable));
            txtLastnameTH.setEnabled(false);
            txtLastnameTH.setBackground(getResources().getDrawable(R.drawable.shape_edit_text_disable));
            txtHomeNo.setEnabled(false);
            txtHomeNo.setBackground(getResources().getDrawable(R.drawable.shape_edit_text_disable));
            spnProvince.setEnabled(false);
            spnProvince.setBackground(getResources().getDrawable(R.drawable.shape_edit_text_disable));
            txtPost.setEnabled(false);
            txtPost.setBackground(getResources().getDrawable(R.drawable.shape_edit_text_disable));
        }

        return rootView;
    }

    @Optional
    @OnClick(R.id.btnLatLong)
    public void clickbtnLatLong() {
        if (!AppUtils.checkPermissionLocation(getActivity())) {
            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    RequestPermissionCodeLocation);
        } else {
            startActivityForResult(new Intent(getActivity(), MapActivity.class), AppUtils.REQUEST_LOCATION);
        }
    }

    private void initInstances(View rootView, View view) {
        // Init 'View' instance(s) with rootView.findViewById here
        btnLatLong = (MyNoHBTextView) view.findViewById(R.id.btnLatLong);
        llFarmerAddress = (LinearLayout) rootView.findViewById(R.id.llFarmerAddress);
        txtPID = (MyEditNoHLTextView) view.findViewById(R.id.txtPID);
        txtNameTH = (MyEditNoHLTextView) view.findViewById(R.id.txtNameTH);
        txtLastnameTH = (MyEditNoHLTextView) view.findViewById(R.id.txtLastnameTH);
        txtHomeID = (MyEditNoHLTextView) view.findViewById(R.id.txtHomeID);
        txtHomeNo = (MyEditNoHLTextView) view.findViewById(R.id.txtHomeNo);
//        txtMoo = (MyEditNoHLTextView) view.findViewById(R.id.txtMoo);
        txtPost = (MyEditNoHLTextView) view.findViewById(R.id.txtPost);
        txtMobile = (MyEditNoHLTextView) view.findViewById(R.id.txtPhone);
        txtPhone = (MyEditNoHLTextView) view.findViewById(R.id.txtTel);
        txtLat = (MyEditNoHLTextView) view.findViewById(R.id.txtLat);
        txtLong = (MyEditNoHLTextView) view.findViewById(R.id.txtLong);
        txtEmail = (MyEditNoHLTextView) view.findViewById(R.id.txtEmail);
        spnAmphur = (Spinner) view.findViewById(R.id.spnAmphur);
        spnPrefix = (Spinner) view.findViewById(R.id.spnPrefix);
        spnTambol = (Spinner) view.findViewById(R.id.spnTambol);
        spnProvince = (Spinner) view.findViewById(R.id.spnProvince);
        spnVillage = (Spinner) view.findViewById(R.id.spnVillage);
        btnSubmit = (MyNoHBTextView) rootView.findViewById(R.id.btnSubmit);
        txtBD = (MyEditNoHLTextView) view.findViewById(R.id.txtBD);
        imgProfile = (ImageView) view.findViewById(R.id.imgProfile);
        progressBar = (FrameLayoutDisable) rootView.findViewById(R.id.progressBar);
        spnCompany = (Spinner) view.findViewById(R.id.spnCompany);
        linearLayout_type = (LinearLayout) view.findViewById(R.id.linearLayout_type);
        txtTaxId = (MyEditNoHLTextView) view.findViewById(R.id.txtTaxId);
        txtCompanyName = (MyEditNoHLTextView) view.findViewById(R.id.txtCompanyName);
        title_type = (MyNoHBTextView) view.findViewById(R.id.title_type);
        title_info = (MyNoHBTextView) view.findViewById(R.id.title_info);
        txtTopicType = (MyNoHLTextView) view.findViewById(R.id.txtTopicType);
        btnRead= (MyNoHBTextView) view.findViewById(R.id.btnRead);

        spnAmphur.setEnabled(false);
        spnTambol.setEnabled(false);
        spnVillage.setEnabled(false);

        btnRead.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                PIDScanActivity pidScanActivity = new PIDScanActivity(getActivity()
                        ,FarmerFragment.this,imgProfile);
                pidScanActivity.onCreateActivity();
            }
        });
    }

    private void setInfoFarmer() {

        if ( mFarmerItem.getUuid() != null) {

            if (mFarmerItem.getPid() != null || mFarmerItem.getPid() != 0) {
                txtPID.setText("" + mFarmerItem.getPid());
            }

            txtNameTH.setText(mFarmerItem.getFirstName());
            txtLastnameTH.setText(mFarmerItem.getLastName());

            txtHomeNo.setText("" + mFarmerItem.getHomeNo());
//            if (mFarmerItem.getVillageID() != null && mFarmerItem.getVillageID() != 0) {
//                txtMoo.setText("" + mFarmerItem.getVillageID());
//            }

            txtPost.setText("" + mFarmerItem.getPostCode());
            if (mFarmerItem.getMobile() != null ) {
                txtMobile.setText("" + mFarmerItem.getMobile());
            }


            if (mFarmerItem.getPhone() != null ) {
                txtPhone.setText("" + mFarmerItem.getPhone());
            }

            if  (((mFarmerItem.getLatitude() != null) && (mFarmerItem.getLongitude() != null)) && ((!mFarmerItem.getLatitude().equalsIgnoreCase("")) && (!mFarmerItem.getLongitude().equalsIgnoreCase("")))) {
                double lat = Double.parseDouble(mFarmerItem.getLatitude());
                double lng = Double.parseDouble(mFarmerItem.getLongitude());
                if ((lat == 0.0) || (lng == 0.0)) {
                    TambolEntity tambol =  dbHelper.getTambolWithID(mFarmerItem.getTambolID());
                    txtLat.setText(""+tambol.getLatitude());
                    txtLong.setText(""+tambol.getLongitude());
                } else {
                    txtLat.setText(mFarmerItem.getLatitude());
                    txtLong.setText(mFarmerItem.getLongitude());
                }
            } else {
                TambolEntity tambol =  dbHelper.getTambolWithID(mFarmerItem.getTambolID());
                txtLat.setText(""+tambol.getLatitude());
                txtLong.setText(""+tambol.getLongitude());
            }



            if (mFarmerItem.getTaxID() != null && mFarmerItem.getTaxID() != 0) {
                txtTaxId.setText(mFarmerItem.getTaxID() + "");
            }
//           txtCompanyName.setText(mFarmerItem.getCompanyTypeID());
            txtCompanyName.setText(mFarmerItem.getCompany_Name());

            if (mFarmerItem.getFarmerImage()!=null&&!mFarmerItem.getFarmerImage().equals("")) {
                byte[] decodedString = Base64.decode(mFarmerItem.getFarmerImage(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                imgProfile.setImageBitmap(decodedByte);
            }

            if (isOnline == false) {

                try {
                    txtBD.setText(AppUtils.convertDateThai(AppUtils.convertDate(mFarmerItem.getBirthDay())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }


            } else {
                try {
                    if (mFarmerItem.getBirthDay() != null && !mFarmerItem.getBirthDay().equals("") && !mFarmerItem.getBirthDay().equals("null")) {
                        txtBD.setText(AppUtils.convertDateThai(AppUtils.convertDate(mFarmerItem.getBirthDay())));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
//           if (isOnline==true){
//               String latitude = mFarmerItem.getLatitude();
//               Double lat = null,lng = null;
//               if (latitude != null||latitude.length()>0){
//                   String[] parts = latitude.split(",");
//                   lat = Double.valueOf(parts[0]); // 004
//                   lng = Double.valueOf(parts[1]);
//               }
//
//               txtLat.setText(String.valueOf(lat));
//               txtLong.setText(String.valueOf(lng));
//           }else {
//               txtLat.setText(mFarmerItem.getLatitude());
//               txtLong.setText(mFarmerItem.getLongitude());
//           }
            txtEmail.setText(mFarmerItem.getEmail());
        }

    }


    //    public void setValidate() {
    DataValidator<CharSequence> nameValidator = new DataValidator<CharSequence>() {
        @Override
        public String validate(CharSequence data) {
            String s = data.toString();
            if (!s.matches("\\p{Alpha}")) {
                return "Not properly formatted name.";
            }
            return null;
        }
    };
    DataValidator<CharSequence> phoneValidator = new DataValidator<CharSequence>() {
        @Override
        public String validate(CharSequence data) {
            String s = data.toString();
            if (!s.matches("\\+?\\d{9,12}"))
                return "Not a valid phone number.";
            return null;
        }
    };

    DataValidator<CharSequence> mobileValidator = new DataValidator<CharSequence>() {
        @Override
        public String validate(CharSequence data) {
            String s = data.toString();
            if (!s.matches("\\+?\\d{9,12}"))
                return "Not a valid number.";
            return null;
        }
    };
    DataValidator<CharSequence> pidValidator = new DataValidator<CharSequence>() {
        @Override
        public String validate(CharSequence data) {
            String s = data.toString();
            if (!s.matches("\\+?\\d{13}"))
                return "Not a valid number.";
            return null;
        }
    };

    DataValidator<CharSequence> eamilValidator = new DataValidator<CharSequence>() {
        @Override
        public String validate(CharSequence data) {
            String s = data.toString();
            String rexp = "[a-zA-Z0-9_.+-]{1,64}@[a-zA-Z-]+\\.\\p{Alpha}+";
            if (!s.matches(rexp))
                return "Not a valid email address.";
            return null;
        }
    };
//        txtPID.setDataValidator(pidValidator);
//        txtNameTH.setDataValidator(nameValidator);
//        txtLastnameTH.setDataValidator(nameValidator);
//        txtHomeID.setDataValidator(phoneValidator);
//        txtHomeNo.setDataValidator(phoneValidator);
//        txtMoo.setDataValidator(phoneValidator);
//        txtPost.setDataValidator(phoneValidator);
//        txtMobile.setDataValidator(phoneValidator);
//        txtPhone.setDataValidator(phoneValidator);


//    }

//    @OnClick(R.id.btnSubmit)
//    public void clickBtnSubmit() {
//        FarmerEntity old_d = AppUtils.getUserProfile(getContext());
//        FarmerEntity d = new FarmerModel.FarmerEntity();
//        d.setPid(old_d.getPid());
//
//        ProvinceEntity pEnt = provinceEntities.get(spnProvince.getSelectedItemPosition());
//        d.setProvince_ID(pEnt.getProvince_Code());
//
//
//        Gson gson = new GsonBuilder().create();
//        String json = gson.toJson(d);
//        // TODO: save to file, and start upload service...
//    }

    public void setInfoToDatabase() {


//        mFarmerItem=new Farmer();
            mFarmerItem.setBirthDay(AppUtils.convertDateEng(txtBD.getText().toString()));

        mFarmerItem.setProvinceID(provinceID);
        mFarmerItem.setAmphurID(amphurID);
        mFarmerItem.setTambolID(tambolID);
        mFarmerItem.setPrefixID(PrefixID);
        String pid = txtPID.getText().toString();
        mFarmerItem.setPid(Long.parseLong(pid));
        mFarmerItem.setFirstName(txtNameTH.getText().toString());
        mFarmerItem.setLastName(txtLastnameTH.getText().toString());
//        mFarmerItem.setHomeID(Long.parseLong(txtHomeID.getText().toString()));
        mFarmerItem.setHomeNo(txtHomeNo.getText().toString());
        //TODO : Check villages agian
        if (villageID != 0) {
            mFarmerItem.setVillageID(villageID);
        }

//        mFarmerItem.setVillageID(Integer.valueOf(txtMoo.getText().toString()));
        mFarmerItem.setPostCode(txtPost.getText().toString());
        mFarmerItem.setMobile(txtMobile.getText().toString());
        mFarmerItem.setPhone(txtPhone.getText().toString());
        mFarmerItem.setLatitude(txtLat.getText().toString());
        mFarmerItem.setLongitude(txtLong.getText().toString());
        mFarmerItem.setEmail(txtEmail.getText().toString());
        if (txtTaxId.getText().toString().length() > 0) {
            mFarmerItem.setTaxID(Long.valueOf(txtTaxId.getText().toString()));
        }

        mFarmerItem.setCompany_Name(txtCompanyName.getText().toString());

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
        //outState.putParcelable(CLASS_NAME, Parcels.wrap(loginPIDEntity));

    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
            //loginPIDEntity = Parcels.unwrap(savedInstanceState.getParcelable(CLASS_NAME));
        }
    }

    public void addTotalSpinner(){
        PrefixEntity prefixEntity = new PrefixEntity();
        prefixEntity.setPrefix_NameTh("กรุณาเลือกคำนำหน้าชื่อ");
        prefixEntity.setPrefix_ID(-99);

        prefix.add(prefixEntity);

        ProvinceEntity provinceEntity = new ProvinceEntity();
        provinceEntity.setProvince_NameTh("กรุณาเลือกจังหวัด");
        provinceEntity.setProvince_ID(-99);

        province.add(provinceEntity);

        AmphurEntity amphurEntity = new AmphurEntity();
        amphurEntity.setAmphur_nameTh("กรุณาเลือกอำเภอ");
        amphurEntity.setAmphur_ID(-99);

        amphur.add(amphurEntity);

        TambolEntity tambolEntity = new TambolEntity();
        tambolEntity.setTambol_NameTh("กรุณาเลือกตำบล");
        tambolEntity.setTambol_ID(-99);

        tambol.add(tambolEntity);

        VillageEntity villageEntity = new VillageEntity();
        villageEntity.setVillage_Name("กรุณาเลือกหมู่บ้าน");
        villageEntity.setVillage_ID(-99);

        village.add(villageEntity);
    }

    int PrefixID;

    public void getListPrefix() {
        listPrefix.clear();
        prefix.clear();
        int selectIndex = -1;
        PrefixEntity prefixEntity = new PrefixEntity();
        prefixEntity.setPrefix_NameTh("กรุณาเลือกคำนำหน้าชื่อ");
        prefixEntity.setPrefix_ID(-99);
        prefix.add(prefixEntity);

        prefix .addAll( dbHelper.getPrefixList());

        for (int i = 0; i < prefix.size(); i++) {
            listPrefix.add(prefix.get(i).getPrefix_NameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listPrefix);

        spnPrefix.setAdapter(adapter);

        spnPrefix.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (prefix != null) {
//                mFarmerItem.setPrefixID(prefix.get(position).getPrefix_ID());
                    PrefixID = prefix.get(position).getPrefix_ID();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < prefix.size(); i++) {
            if (mFarmerItem.getPrefixID() != null) {
                if (prefix.get(i).getPrefix_ID() == mFarmerItem.getPrefixID()) {
                    selectIndex = i;
                    spnPrefix.setSelection(selectIndex);
                    break;
                }
            }
        }

    }

    int provinceID;
    int amphurID;
    int tambolID;
    int villageID;
    String zipcode;

    public void getListProvince() {
        province.clear();
        ProvinceEntity provinceEntity =new ProvinceEntity();
        provinceEntity.setProvince_NameTh("กรุณาเลือกจังหวัด");
        provinceEntity.setProvince_ID(-99);
        province.add(provinceEntity);
        int selectIndex = -99;

        province.addAll(dbHelper.getProvinceList());

        for (int i = 0; i < province.size(); i++) {
            listProvince.add(province.get(i).getProvince_NameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listProvince);

        spnProvince.setAdapter(adapter);

        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                getListAmphur(province.get(position).getProvince_ID());
//                provinceID = province.get(position).getProvince_ID();
//                spnAmphur.setEnabled(true);
//                spnTambol.setEnabled(false);
//                spnVillage.setEnabled(false);

                provinceID=province.get(position).getProvince_ID();
                if(province.get(position).getProvince_ID()!=-99) {
//                    spnTambol.setEnabled(true);
                    spnAmphur.setEnabled(true);
                    getListAmphur(province.get(position).getProvince_ID());
                }else{
                    spnAmphur.setEnabled(false);
                    amphurID=-99;
                    spnTambol.setEnabled(false);
                    tambolID=-99;
                    spnVillage.setEnabled(false);
                    villageID=-99;
                }
                zipcode="";
                txtPost.setText(zipcode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < province.size(); i++) {
            if (mFarmerItem.getProvinceID() != null) {
                if (province.get(i).getProvince_ID() == mFarmerItem.getProvinceID()) {
                    selectIndex = i;
                    spnProvince.setSelection(selectIndex);
                    break;
                }
            }
        }

    }

    public void getListAmphur(int position) {
        listAmphur.clear();
        amphur.clear();
        AmphurEntity amphurEntity =new AmphurEntity();
        amphurEntity.setAmphur_nameTh("กรุณาเลือกอำเภอ");
        amphurEntity.setAmphur_ID(-99);
        amphur.add(amphurEntity);

        int selectIndex = -99;

        amphur.addAll(dbHelper.getAmphur(position));

        for (int i = 0; i < amphur.size(); i++) {
            listAmphur.add(amphur.get(i).getAmphur_nameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listAmphur);
        spnAmphur.setAdapter(adapter);

        spnAmphur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                getListTambol(amphur.get(position).getAmphur_ID());
//
//                amphurID = amphur.get(position).getAmphur_ID();
//                spnTambol.setEnabled(true);
//                spnVillage.setEnabled(false);
                amphurID = amphur.get(position).getAmphur_ID();
                if (amphur.get(position).getAmphur_ID() != -99) {
                    spnTambol.setEnabled(true);
                    getListTambol(amphur.get(position).getAmphur_ID());
                } else {
                    spnTambol.setEnabled(false);
                    tambolID = -99;
                    spnVillage.setEnabled(false);
                    villageID=-99;
                }
                zipcode="";
                txtPost.setText(zipcode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < amphur.size(); i++) {
            if (mFarmerItem.getAmphurID() != null) {
                if (amphur.get(i).getAmphur_ID() == mFarmerItem.getAmphurID()) {
                    selectIndex = i;
                    spnAmphur.setSelection(selectIndex);
                    break;
                }
            }
        }

        if(result_!=null) {
            if (result_.get("amphur")!=null || ! result_.get("amphur").equals("")) {
                for (int i = 0; i < amphur.size(); i++) {
                    if (amphur.get(i).getAmphur_NameTh().equals(result_.get("amphur"))) {
                        spnAmphur.setSelection(i);
//                        amphurID = amphur.get(i).getAmphur_ID();
//                        spnTambol.setEnabled(true);
//                        getListTambol(amphur.get(i).getAmphur_ID());
                        break;
                    }
                }
            }
        }
    }

    public void getListTambol(int position) {
        listTambol.clear();
        tambol.clear();
        TambolEntity tambolEntity =new TambolEntity();
        tambolEntity.setTambol_NameTh("กรุณาเลือกตำบล");
        tambolEntity.setTambol_ID(-99);
        tambol.add(tambolEntity);

        int selectIndex = -99;

        tambol.addAll(dbHelper.getTambol(position));

        for (int i = 0; i < tambol.size(); i++) {
            listTambol.add(tambol.get(i).getTambol_NameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listTambol);
        spnTambol.setAdapter(adapter);

        spnTambol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                getListVillage(tambol.get(position).getTambol_ID());
//
//                tambolID = tambol.get(position).getTambol_ID();
//
//                txtPost.setText(zipcode);

                tambolID=tambol.get(position).getTambol_ID();
                if(tambol.get(position).getTambol_ID()!=-99) {
//                    spnTambol.setEnabled(true);
                    spnVillage.setEnabled(true);
                    zipcode = tambol.get(position).getZipcode();
                    txtPost.setText(zipcode);
                    getListVillage(tambol.get(position).getTambol_ID());
                }else{
                    spnVillage.setEnabled(false);
                    zipcode="";
                    villageID=-99;
                }


//                spnVillage.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < tambol.size(); i++) {
            if (mFarmerItem.getTambolID() != null) {
                if (tambol.get(i).getTambol_ID() == mFarmerItem.getTambolID()) {
                    selectIndex = i;
                    spnTambol.setSelection(selectIndex);
                    break;
                }
            }
        }

        if(result_!=null) {
            if (result_.get("tambol") != null ||! result_.get("tambol").equals("")) {
                for (int i = 0; i < tambol.size(); i++) {
                    if (tambol.get(i).getTambol_NameTh().equals(result_.get("tambol"))) {
                        spnTambol.setSelection(i);
                        break;
                    }
                }
            }
        }
    }


//    public void getListProvince() {
//        ProvinceEntity provinceEntity =new ProvinceEntity();
//        provinceEntity.setProvince_NameTh("ทั้งหมด");
//        provinceEntity.setProvince_ID(-99);
//        province.add(provinceEntity);
//
//        province.addAll(dbHelper.getProvinceList());
//
//        for (int i = 0; i < province.size(); i++) {
//            listProvince.add(province.get(i).getProvince_NameTh());
//        }
//        MySpinnerAdapter adapter = new MySpinnerAdapter(
//                RegisterActivity.this,
//                android.R.layout.simple_dropdown_item_1line,
//                listProvince);
//
//        spnProvince.setAdapter(adapter);
//
//        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                provinceID=province.get(position).getProvince_ID();
//                if(province.get(position).getProvince_ID()!=-99) {
//                    spnTambol.setEnabled(true);
//                    spnAmphur.setEnabled(true);
//                    getListAmphur(province.get(position).getProvince_ID());
//                }else{
//                    spnAmphur.setEnabled(false);
//                    amphurID=-99;
//                    spnTambol.setEnabled(false);
//                    tambolID=-99;
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }
//
//    public void getListAmphur(int position) {
//        listAmphur.clear();
//        amphur.clear();
//        AmphurEntity amphurEntity =new AmphurEntity();
//        amphurEntity.setAmphur_nameTh("ทั้งหมด");
//        amphurEntity.setAmphur_ID(-99);
//        amphur.add(amphurEntity);
//
//        amphur.addAll(dbHelper.getAmphur(position));
//
//        for (int i = 0; i < amphur.size(); i++) {
//            listAmphur.add(amphur.get(i).getAmphur_nameTh());
//        }
//
//        MySpinnerAdapter adapter = new MySpinnerAdapter(
//                RegisterActivity.this,
//                android.R.layout.simple_dropdown_item_1line,
//                listAmphur);
//        spnAmphur.setAdapter(adapter);
//
//        spnAmphur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                amphurID=amphur.get(position).getAmphur_ID();
//                if(amphur.get(position).getProvince_ID()!=-99) {
//                    spnTambol.setEnabled(true);
//                    getListTambol(amphur.get(position).getAmphur_ID());
//                }else{
//                    spnTambol.setEnabled(false);
//                    tambolID=-99;
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }
//
//    public void getListTambol(int position) {
//        listTambol.clear();
//        tambol.clear();
//        TambolEntity tambolEntity =new TambolEntity();
//        tambolEntity.setTambol_NameTh("ทั้งหมด");
//        tambolEntity.setTambol_ID(-99);
//        tambol.add(tambolEntity);
//
//        tambol.addAll( dbHelper.getTambol(position));
//
//        for (int i = 0; i < tambol.size(); i++) {
//            listTambol.add(tambol.get(i).getTambol_NameTh());
//        }
//
//        MySpinnerAdapter adapter = new MySpinnerAdapter(
//                RegisterActivity.this,
//                android.R.layout.simple_dropdown_item_1line,
//                listTambol);
//        spnTambol.setAdapter(adapter);
//
//        spnTambol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                tambolID=tambol.get(position).getTambol_ID();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }

    public void getListVillage(int position) {

        listVillage.clear();
        village.clear();
        VillageEntity villageEntity =new VillageEntity();
        villageEntity.setVillage_Name("กรุณาเลือกหมู่บ้าน");
        villageEntity.setVillage_ID(-99);
        village.add(villageEntity);

        int selectIndex = -99;

        village.addAll(dbHelper.getVillage(position));

        for (int i = 0; i < village.size(); i++) {
            listVillage.add(village.get(i).getVillage_Name());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listVillage);
        spnVillage.setAdapter(adapter);

        spnVillage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                villageID=village.get(position).getVillage_ID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < village.size(); i++) {
            if (mFarmerItem.getVillageID() != null) {
                if (village.get(i).getVillage_ID() == mFarmerItem.getVillageID()) {
                    selectIndex = i;
                    spnVillage.setSelection(selectIndex);
                    break;
                }
            }
        }

        if(result_!=null) {
            if (result_.get("moo") != null ||! result_.get("moo").equals("")) {
                for (int i = 0; i < village.size(); i++) {
                    if (village.get(i).getVillage_Name().contains(result_.get("moo"))) {
                        spnVillage.setSelection(i);
                        break;
                    }
                }
            }
        }

        if (result_!=null){
            result_=null;
        }
    }



    @Optional
    @OnClick(R.id.imgProfile)
    public void clickImgProfile() {
        start();
    }

    @Optional
    @OnClick(R.id.btnSubmit)
    public void clickBtnSubmit() {
        if (txtPID.getText().toString().length() == 13 ) {
            if (isOnline == true) {

                if (checkValidPersonnalId(txtPID.getText().toString())) {
                    if((from==1||from==3||from==0)) { //case add new
                        if (type == 0) { //personID
                            checkDuplicate(txtPID.getText().toString(), type + 1);
                        } else { //VATID
                            checkDuplicate(txtTaxId.getText().toString(), type + 1);
                        }
                    }else{ //case update no need to check
                        setDataOnline();
                    }
                } else {
                    Toast.makeText(getContext(), "เลขบัตรประชาชนไม่ถูกต้อง", Toast.LENGTH_SHORT).show();
                }

            }else{

                setInfoToDatabase();

                if (!isPopulate) {
                    databaseRealm = new DatabaseRealm(getContext());
                    mFarmerItem.setOnline(false);
                    mFarmerItem.setStatusID(2); //case offline
                    Farmer addDataFarmer = databaseRealm.addDataFarmerReturn(mFarmerItem);

                    if (addDataFarmer != null) {
                        mFarmerItem= addDataFarmer;
                        Toast.makeText(getActivity(), "อัพเดทข้อมูลสำเร็จ", Toast.LENGTH_SHORT).show();
                        /*Intent intent = new Intent(getActivity(), OfflineActivity.class);
                        startActivity(intent);
                        getActivity().finish();*/
                        ((MainActivity)getActivity()). getViewPager().setCurrentItem(2);
                    } else {
                        Toast.makeText(getActivity(), "อัพเดตข้อมูลไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    databaseRealm = new DatabaseRealm(getActivity());
                    boolean upDateDataFarmer = databaseRealm.upDateDataFarmer(mFarmerItem);


                    if (upDateDataFarmer == true) {
                        Toast.makeText(getActivity(), "อัพเดทข้อมูลสำเร็จ", Toast.LENGTH_SHORT).show();
                    /*    Intent intent = new Intent(getActivity(), OfflineActivity.class);
                        startActivity(intent);
                        getActivity().finish();*/
//                            ((MainActivity)getActivity()). getViewPager().setCurrentItem(2);

                    } else {
                        Toast.makeText(getActivity(), "อัพเดตข้อมูลไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } else {
            Toast.makeText(getActivity(), getString(R.string.alert_data_incomplete), Toast.LENGTH_SHORT).show();
        }


    }

    private boolean checkValidPersonnalId(String userpId){

        if(userpId.length() != 13) return false;

        int sum=0;

        for(int i=0; i < 12; i++) {
            sum += Float.parseFloat(userpId.charAt(i)+"")*(13-i);
        }

        if((11-sum%11)%10!=Float.parseFloat(userpId.charAt(12)+"")) {
            return false;
        }

        return true;
    }


    private void checkDuplicate(String id,int userType) {
        Api api = AppUtils.getApiService();

        progressBar.setVisibility(View.VISIBLE);
        Call<ResponseBase> call = api.checkDuplicateID(id, userType);
        call.enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {

                if (response.body() != null) {

                    if (response.body().getResponseCode().equals(getString(R.string.success))) {
                        setDataOnline();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getContext(), response.body().getResponseMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    try {
                        Toast.makeText(getContext(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void setDataOnline(){
        setInfoToDatabase();
            try {
                mFarmerItem.setCreateDate(AppUtils.convertDate(mFarmerItem.getCreateDate()));
                mFarmerItem.setUpdateDate(AppUtils.convertDate(mFarmerItem.getUpdateDate()));
                mFarmerItem.setUpdateBy(String.valueOf(AppUtils.getUserId(getActivity())));
                    mFarmerItem.setBirthDay(AppUtils.convertDate(mFarmerItem.getBirthDay()));

            } catch (ParseException e) {
                e.printStackTrace();
            }
            mFarmerItem.setUpdateDate(AppUtils.getDate());
            mFarmerItem.setStatusID(2); //case online ไม่อ่านบัตร
            ArrayList<Farmer> farmer = new ArrayList<>();
            farmer.add(mFarmerItem);
            insertData(farmer);
    }

    private void insertData2(final Farmer farmer) {
        progressBar.setVisibility(View.VISIBLE);
        AppUtils.updateData(farmer, new UpdateListener() {
            @Override
            public void onSuccess(String message, ResponseInsertFarmer responseInsertFarmer) {
                progressBar.setVisibility(View.GONE);

                databaseRealm = new DatabaseRealm(getActivity());

                boolean upDateDataFarmer = databaseRealm.upDateDataFarmer(farmer);

                if (upDateDataFarmer == true) {
                    Toast.makeText(getActivity(), "อัพเดทข้อมูลสำเร็จ", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "อัพเดตข้อมูลไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String message, ResponseInsertFarmer responseInsertFarmer) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "อัพเดตข้อมูลไม่สำเร็จ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void insertData(final ArrayList<Farmer> farmer) {
        progressBar.setVisibility(View.VISIBLE);
        AppUtils.updateDataArray(farmer, new UpdateListener() {
            @Override
            public void onSuccess(String message, ResponseInsertFarmer responseInsertFarmer) {
                progressBar.setVisibility(View.GONE);

                mFarmerItem = responseInsertFarmer.getFarmerList().get(0).getFarmer();
                mFarmerItem.setUuid(((MainActivity) getActivity()).getUUID());
                ((MainActivity) getActivity()).setFarmerItem(mFarmerItem);
//                mFarmerItem.setFarmerImage();
                databaseRealm = new DatabaseRealm(getContext());

                if(databaseRealm.getDataFarmer(((MainActivity)getActivity()).getUUID())!=null){
                    databaseRealm.upDateDataFarmer(mFarmerItem);
                    ((MainActivity) getActivity()).setUuid(mFarmerItem.getUuid());
                }else {
                    String uu = databaseRealm.addDataFarmerUUID(mFarmerItem);
                    ((MainActivity) getActivity()).setUuid(uu);
                }

                Toast.makeText(getActivity(), "อัพเดทข้อมูลสำเร็จ", Toast.LENGTH_SHORT).show();

                if (!isPopulate) {
                    ((MainActivity)getActivity()). getViewPager().setCurrentItem(2);
                }

            }

            @Override
            public void onError(String message, ResponseInsertFarmer responseInsertFarmer) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "อัพเดตข้อมูลไม่สำเร็จ", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void setDate() {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        int month = i1 + 1;

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(i, i1, i2);

                        txtBD.setText(AppUtils.convertDateThai(i2 + "/" + month + "/" + i));

//                        date = calendar.getTimeInMillis();

//                        editText_date.setText(date2+"");
                    }


                }, year, month, day);
        datePickerDialog.show();
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (!AppUtils.checkPermissionGallery(getActivity())) {
                                    requestPermissions(
                                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                            AppUtils.RequestPermissionCodeGallery);
                                } else {
                                    choosePhotoFromGallary();
                                }

                                break;
                            case 1:
                                if (!AppUtils.checkPermissionCamera(getActivity())) {
                                    requestPermissions(
                                            new String[]{Manifest.permission.CAMERA},
                                            AppUtils.RequestPermissionCodeCamera);
                                } else {
                                    takePhotoFromCamera();
                                }

                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, AppUtils.GALLERY);
    }

    private void takePhotoFromCamera() {

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;

        if (currentapiVersion == android.os.Build.VERSION_CODES.LOLLIPOP) {
            TakePictureIntent();
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, AppUtils.CAMERA);
        }

    }

    private void TakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                getActivity().startActivityForResult(takePictureIntent, CAMERA);
            }
        }
    }

    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

     /*   super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getActivity().getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
*/


        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }

        if(data!=null) {
            IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (scanResult != null) {
                String re = scanResult.getContents();
                txtPID.setText(re);
               /* if (re.length() != 0) {
                    loginPID(re);
                } else {
                    Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.field_pid), Toast.LENGTH_LONG).show();
                }*/

            }
        }

         if (requestCode == RC_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            images = (ArrayList<Image>) ImagePicker.getImages(data);

            if (images != null && !images.isEmpty()) {
                setValueImage(images.get(0));
            }
            return;
        }

        else if (requestCode == RC_CAMERA && resultCode == RESULT_OK) {
            getCameraModule().getImage(getActivity(), data, new OnImageReadyListener() {
                @Override
                public void onImageReady(List<Image> resultImages) {
                    images = (ArrayList<Image>) resultImages;

                    if (images != null && !images.isEmpty()) {
                        setValueImage(images.get(0));
                    }


                    Log.d("IMAGE CAMERA", "DONE");
                }
            });
        }else if (requestCode == REQUEST_LOCATION) {
            String latitude = "", longitude = "";
            if (data != null) {

                latitude = data.getStringExtra("lat") != null ? data.getStringExtra("lat") : "";
                longitude = data.getStringExtra("lng") != null ? data.getStringExtra("lng") : "";
//                address = data.getStringExtra("address");
            }
            txtLat.setText(latitude);
            txtLong.setText(longitude);
            mFarmerItem.setLatitude(latitude);
            mFarmerItem.setLongitude(longitude);

        }

    }
    public void start() {
        ImagePicker imagePicker = ImagePicker.create(this)
                .single()
                .imageTitle("Tap to select"); // image selection title

//        if (useCustomImageLoader) {
//            imagePicker.imageLoader(new GrayscaleImageLoader());
//        }

//        if (isSingleMode) {
//            imagePicker.single();
//        } else {
//            imagePicker.multi(); // multi mode (default mode)
//        }

        imagePicker.limit(1) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
                .imageFullDirectory(Environment.getExternalStorageDirectory().getPath()) // can be full path
                .origin(images) // original selected images, used in multi mode
                .start(RC_CODE_PICKER); // start image picker activity with request code
    }
    private void captureImage() {
        startActivityForResult(
                getCameraModule().getCameraIntent(getActivity()), RC_CAMERA);
    }

    private ImmediateCameraModule getCameraModule() {
        if (cameraModule == null) {
            cameraModule = new ImmediateCameraModule();
        }
        return (ImmediateCameraModule) cameraModule;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case RequestPermissionCodeGallery:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    if (StoragePermission) {
                        choosePhotoFromGallary();
                    } else {
                        Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case RC_CAMERA :
                if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    captureImage();
                }

                break;
            case WRITE_STORAGE :
                if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }

        }
    }

    public void setValueImage(Image img) {
        Bitmap bitmap = null;

        bitmap = BitmapFactory.decodeFile(img.getPath()); // load

        Matrix matrix = new Matrix();
        matrix.postRotate(AppUtils.getImageOrientation(img.getPath()));
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                bitmap.getHeight(), matrix, true);

        String imgString;
        if (rotatedBitmap != null) {
            Bitmap bitmapShow = AppUtils.scaleBitmap(rotatedBitmap, 200, 200);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            imgProfile.setImageBitmap(bitmapShow);

            bitmapShow.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            byte[] profileImage = outputStream.toByteArray();

            imgString = Base64.encodeToString(profileImage,
                    Base64.NO_WRAP);
        } else {
            imgString = "";
        }
        mFarmerItem.setFarmerImage(imgString);
    }

 /*   public void setValueImage(Intent data, int type, String mCurrentPhotoPath) {
        Bitmap bitmap = null;
        String imageName = null;
        String imagePath = null;

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion == android.os.Build.VERSION_CODES.LOLLIPOP && type == AppUtils.GALLERY) {

            File f1 = new File(mCurrentPhotoPath);
            imageName = f1.getName();
            imagePath = f1.getPath();

//            takeImagePath= String.valueOf(imageBitmap);
            bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath); // load


        } else {

            Uri selectedImage = data.getData();

            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor c = getActivity().getContentResolver().query(selectedImage, filePath,
                    null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(filePath[0]);
            String selectedImagePath = c.getString(columnIndex);
            c.close();

            File f = new File(selectedImagePath);
            imageName = f.getName();
            imagePath = f.getPath();
            bitmap = BitmapFactory.decodeFile(selectedImagePath); // load

        }


        imgProfile.setImageBitmap(bitmap);
        String imgString;
        if (bitmap != null) {
            Bitmap bitmapShow = Bitmap.createScaledBitmap(bitmap, 200, 200, false);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            byte[] profileImage = outputStream.toByteArray();

            imgString = Base64.encodeToString(profileImage,
                    Base64.NO_WRAP);
        } else {
            imgString = "";
        }
        mFarmerItem.setFarmerImage(imgString);
    }*/

    public void getListCompany() {
        int selectIndex = -1;
        companies = dbHelper.getListCompany();
        int filter = 0;
        if (type==1){
            filter=1;
        }else if (type==2){
            filter=0;
        }

        for (int i = 0; i < companies.size(); i++) {

            if (companies.get(i).getIsCompany()==filter) {
                listCompanies.add(companies.get(i).getCompanyTypeName());
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listCompanies);

        spnCompany.setAdapter(adapter);

        spnCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mFarmerItem != null) {
                    int id1 = companies.get(position).getId();
                    mFarmerItem.setCompanyTypeID(id1);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < companies.size(); i++) {
            if (mFarmerItem != null && mFarmerItem.getCompanyTypeID() != null)
                if (companies.get(i).getId() == mFarmerItem.getCompanyTypeID()) {
                    selectIndex = i;
                }
        }
        spnCompany.setSelection(selectIndex);
    }

    private void getImage(String pid) {
        progressBar.setVisibility(View.VISIBLE);
        Api api = AppUtils.getApiService();

        Call<ResponseImage> call = api.getImageFarmer(pid);
        call.enqueue(new Callback<ResponseImage>() {
            @Override
            public void onResponse(Call<ResponseImage> call, Response<ResponseImage> response) {
//                progressBar.setVisibility(View.GONE);
//                if (file.exists ()) {file.delete ();}
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {

                    if (response.body().getResponseCode().equals("200")) {

                        Picasso.with(getActivity())
                                .load(response.body().getData().getImageUrl())
                                .error(R.drawable.default_image)
                                .into(imgProfile);

                    } else {

                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<ResponseImage> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }

        });
    }

    @Optional
    @OnClick(R.id.btnScan)
    public void clickBtnScan() {
        if (AppUtils.hasPermission(getActivity())) {
            AppUtils.hideKeyboard(getActivity());

            IntentIntegrator integrator = IntentIntegrator.forSupportFragment(this);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
            integrator.initiateScan();
        } else {
            AppUtils.askPermission(getActivity(), REQUEST_BARCODE);
        }
//        Intent intent = new Intent(getContext(), MainActivity.class);
//        startActivity(intent);
    }

    @Override
    public void onCompleteScan(HashMap<String, String> result) {

        result_=result;

        String bd=result.get("bd");
        int year=Integer.parseInt(bd.substring(0,4))-543;
        String month=bd.substring(4,6);
        String day=bd.substring(6,8);

        txtBD.setText( AppUtils.convertDateThai(day+"/"+month+"/"+year));

        for(int i=0;i<listPrefix.size();i++){
            if(listPrefix.get(i).equals(result.get("prefix"))){
                spnPrefix.setSelection(i);
            }
        }

//        for(int i=0;i<tambol.size();i++){
//            if(tambol.get(i).getTambol_NameTh().equals(result.get("tambol"))){
//                tambolID=tambol.get(i).getTambol_ID();
//            }
//        }
//
//        for(int i=0;i<amphur.size();i++){
//            if(amphur.get(i).getAmphur_NameTh().equals(result.get("amphur"))){
//                amphurID=amphur.get(i).getAmphur_ID();
//            }
//        }
        for(int i=0;i<province.size();i++){
            if(province.get(i).getProvince_NameTh().equals(result.get("province"))){
                provinceID=province.get(i).getProvince_ID();
                spnProvince.setSelection(i);
                break;
            }
        }

        txtPID.setText(result.get("pid"));
        txtHomeNo.setText(result.get("soi"));
        txtNameTH.setText(result.get("firstname"));
        txtLastnameTH.setText(result.get("lastname"));
    }
}
