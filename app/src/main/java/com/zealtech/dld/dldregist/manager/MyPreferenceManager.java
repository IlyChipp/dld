package com.zealtech.dld.dldregist.manager;

import android.content.Context;
import android.content.SharedPreferences;

public class MyPreferenceManager {

    public static final String MyPREFERENCES = "dldregislive";
    private SharedPreferences sharedpreferences;

    private final String userProfile = "userProfile";
    private final String PROVINCE = "PROVINCE";
    private final String AMPHUR = "AMPHUR";
    private final String TAMBOL = "TAMBOL";
    private final String VILLAGE = "VILLAGE";
    private final String isOnline = "isOnline";
    private final String USERID = "userId";
    private final String USERTYPE = "userType";
    private final String LOGINTYPE = "loginType";
    private final String PID = "pid";
    private final String PROID = "proID";
    private final String AMID = "amID";
    private final String TAMID = "tamID";
    private final String removeLiveFirstTime = "removeLiveFirstTime";

    private final String API = "API";

    public MyPreferenceManager(Context context) {
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
    }


    public static String getMyPREFERENCES() {
        return MyPREFERENCES;
    }

    public SharedPreferences getSharedpreferences() {
        return sharedpreferences;
    }

    public void setSharedpreferences(SharedPreferences sharedpreferences) {
        this.sharedpreferences = sharedpreferences;
    }

    public void setPROID(int proid){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // editor.remove(userProfile);
        editor.putInt(PROID, proid);
        editor.commit();
    }

    public Integer getPROID(){
        if (sharedpreferences.contains(PROID)) {
            return sharedpreferences.getInt(PROID, -99);
        }else {
            return -99;
        }
    }

    public void setRemoveLive(int proid){
        //1=remove , -99=not remove yet
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // editor.remove(userProfile);
        editor.putInt(removeLiveFirstTime, proid);
        editor.commit();
    }

    public Integer getRemoveLive(){
        if (sharedpreferences.contains(removeLiveFirstTime)) {
            return sharedpreferences.getInt(removeLiveFirstTime, -99);
        }else {
            return -99;
        }
    }


    public void setAMID(int amid){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // editor.remove(userProfile);
        editor.putInt(AMID, amid);
        editor.commit();
    }

    public Integer getAMID(){
        if (sharedpreferences.contains(AMID)) {
            return sharedpreferences.getInt(AMID, -99);
        }else {
            return -99;
        }
    }

    public void setTAMID(int tamid){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // editor.remove(userProfile);
        editor.putInt(TAMID, tamid);
        editor.commit();
    }

    public Integer getTAMID(){
        if (sharedpreferences.contains(TAMID)) {
            return sharedpreferences.getInt(TAMID, -99);
        }else {
            return -99;
        }
    }

    public boolean isOnline(){
        if (sharedpreferences.contains(isOnline)) {
            return sharedpreferences.getBoolean(isOnline, false);
        }
        return false;
    }

    public void setOnline(boolean online){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // editor.remove(userProfile);
        editor.putBoolean(isOnline, online);
        editor.commit();
    }

//    public void setProvince(String jsonObject){
//        SharedPreferences.Editor editor = sharedpreferences.edit();
//        editor.putString(PROVINCE, jsonObject);
//        editor.commit();
//    }
//    public void setAmphur(String jsonObject){
//        SharedPreferences.Editor editor = sharedpreferences.edit();
//        editor.putString(AMPHUR, jsonObject);
//        editor.commit();
//    }
//    public void setTambol(String jsonObject){
//        SharedPreferences.Editor editor = sharedpreferences.edit();
//        editor.putString(TAMBOL, jsonObject);
//        editor.commit();
//    }
//    public void setVillage(String jsonObject){
//        SharedPreferences.Editor editor = sharedpreferences.edit();
//        editor.putString(VILLAGE, jsonObject);
//        editor.commit();
//    }
//
//    public String getProvince(){
//        if (sharedpreferences.contains(PROVINCE)){
//            return sharedpreferences.getString(PROVINCE, "0");
//        }
//        return "0";
//    }
//
//    public String getAmphur(){
//        if (sharedpreferences.contains(AMPHUR)){
//            return sharedpreferences.getString(AMPHUR, "0");
//        }
//        return "0";
//    }
//
//    public String getTambol(){
//        if (sharedpreferences.contains(TAMBOL)){
//            return sharedpreferences.getString(TAMBOL, "0");
//        }
//        return "0";
//    }
//    public String getVillage(){
//        if (sharedpreferences.contains(VILLAGE)){
//            return sharedpreferences.getString(VILLAGE, "0");
//        }
//        return "0";
//    }


    public String getUserProfile(){
        if (sharedpreferences.contains(userProfile)) {
            return sharedpreferences.getString(userProfile, "0");
        }
        return "0";
    }

    public void setUserProfile(String jsonObject){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // editor.remove(userProfile);
        editor.putString(userProfile, jsonObject);
        editor.commit();
    }
    public void setUserId(int userId){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // editor.remove(userProfile);
        editor.putInt(USERID, userId);
        editor.commit();
    }


    public int getUserId(){
        if (sharedpreferences.contains(USERID)) {
            return sharedpreferences.getInt(USERID, 0);
        }else {
            return 0;
        }
    }

    public void setLoginType(int loginType){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // editor.remove(userProfile);
        editor.putInt(LOGINTYPE, loginType);
        editor.commit();
    }


    public void setUserType(int usetype){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // editor.remove(userProfile);
        editor.putInt(USERTYPE, usetype);
        editor.commit();
    }

    public void setUserPid(long userPid){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // editor.remove(userProfile);
        editor.putLong(PID, userPid);
        editor.commit();
    }

    public int getUserType(){
        if (sharedpreferences.contains(USERTYPE)) {
            return sharedpreferences.getInt(USERTYPE, 0);
        }else {
            return 0;
        }

    }

    public int getLoginType(){
        if (sharedpreferences.contains(LOGINTYPE)) {
            return sharedpreferences.getInt(LOGINTYPE, 0);
        }else {
            return 0;
        }

    }

    public long getUserPid(){
        if (sharedpreferences.contains(PID)) {
            return sharedpreferences.getLong(PID, 0);
        }else {
            return 0;
        }

    }

}
