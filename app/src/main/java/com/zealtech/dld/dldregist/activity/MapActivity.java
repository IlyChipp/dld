package com.zealtech.dld.dldregist.activity;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zealtech.dld.dldregist.Adapter.PlaceAutocompleteAdapter;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.config.SingleShotLocationProvider;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.zealtech.dld.dldregist.util.AppUtils.REQUEST_LOCATION;


/**
 * Created by User on 20/7/2559.
 */
public class MapActivity extends AppCompatActivity implements  View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {
    private static final String CLASS_NAME = "MapActivity";
    private static Context mContext;

    protected GoogleApiClient mGoogleApiClient;
    private PlaceAutocompleteAdapter mAdapter;
    private AutoCompleteTextView mAutocompleteView;
    private String lat; // 004
    private String lng;
    private String address;
    private LatLng map_latlng;
    SupportMapFragment mapFragment;
    private Boolean flag = false;
    private Boolean isLoading = false;
    private ProgressBar progressBar;
    private ImageView button_ok;
    private View map;

    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();

        setContentView(R.layout.map_screen);
        mContext = this;
        initializeLayout();
        // Retrieve the AutoCompleteTextView that will display Place suggestions.
        mAutocompleteView = (AutoCompleteTextView) findViewById(R.id.autocomplete_places);

        // Register a listener that receives callbacks when a suggestion has been selected
        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);

        // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
        // the entire world.
        mAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, BOUNDS_GREATER_SYDNEY,
                null);
        mAutocompleteView.setAdapter(mAdapter);

        map = findViewById(R.id.map);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        flag = displayGpsStatus();
        if (flag) {
            isLoading = true;
            progressBar.setVisibility(View.VISIBLE);
            map.setVisibility(View.GONE);
            loadLocation(this);





        } else {
            Toast.makeText(this, "Gps Status!!, Your GPS is: OFF", Toast.LENGTH_SHORT).show();
        }


    }



    private void initializeLayout() {
        button_ok= (ImageView) findViewById(R.id.button_ok);
        button_ok.setOnClickListener(this);
        if (lat != null && lng != null) {

            map_latlng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));

        }
    }


   /* @Override
    public void onActionBarButtonClick(String button) {
        if (button.equals("left")) {

            Intent intent = new Intent();
            intent.putExtra("lat", "");
            intent.putExtra("lng", "");
            intent.putExtra("address", "");
            setResult(REQUEST_LOCATION, intent);
            finish();


        }
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_ok:
                Intent intent = new Intent();
                intent.putExtra("lat", lat);
                intent.putExtra("lng", lng);
                intent.putExtra("address", address);
                setResult(REQUEST_LOCATION, intent);
                finish();
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeLogging();
    }

    /**
     * Set up targets to receive log data
     */
    public void initializeLogging() {
        // Using Log, front-end to the logging chain, emulates android.util.log method signatures.
        // Wraps Android's native log framework
      /*  LogWrapper logWrapper = new LogWrapper();
        Log.setLogNode(logWrapper);*/

        Log.i(CLASS_NAME, "Ready");
    }

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(com.google.android.gms.common.api.GoogleApiClient,
     * String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);
            Log.i(CLASS_NAME, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            //  Toast.makeText(getApplicationContext(), "Clicked: " + primaryText,
            //Toast.LENGTH_SHORT).show();
            Log.i(CLASS_NAME, "Called getPlaceById to get Place details for " + placeId);
        }
    };
    private String Name;
    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(CLASS_NAME, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);

            // Format details of the place for display and show it in a TextView.
            mAutocompleteView.setText(place.getName() + " " +
                    place.getAddress());
            String latlng = String.valueOf(place.getLatLng());
            Log.i(CLASS_NAME, "Place : " + place.getName() + " " +
                    place.getAddress() + "  Lat :" + latlng);

            String[] parts = latlng.split("\\(");
            String parts1 = parts[1]; // 004
            String[] part2 = parts1.split("\\)");
            String part3 = part2[0];
            String[] part4 = part3.split(",");


            lat = part4[0];
            lng = part4[1];
            address = String.valueOf(place.getName() + " " + place.getAddress());
            Name = "" + place.getName();

            map_latlng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
            mapFragment.getMapAsync(MapActivity.this);


            Log.i(CLASS_NAME, "lat : " + lat + "  lng :" + lng);

            // Display the third party attributions if set.
            final CharSequence thirdPartyAttribution = places.getAttributions();
            if (thirdPartyAttribution == null) {
//                mPlaceDetailsAttribution.setVisibility(View.GONE);
            } else {
//                mPlaceDetailsAttribution.setVisibility(View.VISIBLE);
//                mPlaceDetailsAttribution.setText(Html.fromHtml(thirdPartyAttribution.toString()));
            }

            Log.i(CLASS_NAME, "Place details received: " + place.getName());
            places.release();
        }
    };



    /**
     * Called when the Activity could not connect to Google Play services and the auto manager
     * could resolve the error automatically.
     * In this case the API is not available and notify the user.
     *
     * @param connectionResult can be inspected to determine the cause of the failure
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.e(CLASS_NAME, "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());

        // TODO(Developer): Check error code and notify the user of error state and resolution.
        Toast.makeText(this,
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }

    Marker markerAdd;

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(map_latlng, 16));
        markerAdd = googleMap.addMarker(new MarkerOptions()
                .title(Name)
                .snippet(address)
                .anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
                .position(map_latlng));

        // Do something with Google Map
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(false);

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                if (markerAdd != null) {

                    markerAdd.remove();

                }


                getAddressFromLatLng(point.latitude, point.longitude, googleMap);
                Log.e(CLASS_NAME, "onMapClick "
                        + point.latitude + "---" + point.longitude);

            }
        });

    }

    private void getAddressFromLatLng(double latitude, double longitude, GoogleMap googleMap) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            Name = addresses.get(0).getFeatureName();

            mAutocompleteView.setText(Name + " " + address);

            map_latlng = new LatLng(latitude, longitude);
            mapFragment.getMapAsync(MapActivity.this);

            MarkerOptions marker = new MarkerOptions().position(
                    new LatLng(latitude, longitude)).title(Name);

            // markerAdd = googleMap.addMarker(marker);
            lat = String.valueOf(latitude);
            lng = String.valueOf(longitude);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    /*----Method to Check GPS is enable or disable ----- */
    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = getBaseContext()
                .getContentResolver();
        boolean gpsStatus = Settings.Secure
                .isLocationProviderEnabled(contentResolver,
                        LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;

        } else {
            return false;
        }
    }

    public void loadLocation(Context context) {
        // when you need location
        // if inside activity context = this;

        SingleShotLocationProvider.requestSingleUpdate(context,
                new SingleShotLocationProvider.LocationCallback() {
                    @Override
                    public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                        android.util.Log.d("Location", "my location is " + location.toString());

                        lng = "" + location.longitude;
                        android.util.Log.v(CLASS_NAME, lng);
                        lat = "" + location.latitude;
                        android.util.Log.v(CLASS_NAME, lat);
                        address = location.AddressLine + " "
                                + location.AddressLine2 + " "
                                + location.AddressLine3;

                        progressBar.setVisibility(View.GONE);
                        map.setVisibility(View.VISIBLE);

                        isLoading = false;

                        mapFragment.getMapAsync(MapActivity.this);

                        if (lat != null && lng != null) {

                            map_latlng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));

                        }
                    }

                    @Override
                    public void onNewLocationFail() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }


}
