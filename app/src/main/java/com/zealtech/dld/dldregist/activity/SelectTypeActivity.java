package com.zealtech.dld.dldregist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.LivestockTypeMaster;
import com.zealtech.dld.dldregist.util.AppUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Windows 8.1 on 5/11/2560.
 */

public class SelectTypeActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_type);

        ButterKnife.bind(this);

        MyPreferenceManager myPreferenceManager=new MyPreferenceManager(this);

        if(myPreferenceManager.getRemoveLive()==-99) { ///if not remove firsttime yet
            DBHelper dBHelper = new DBHelper(this);
            if (dBHelper.isTablePopulated(LivestockTypeMaster.TABLE)) {
                try {
                    dBHelper.removeAndCreateLiveStockSubtype(LivestockTypeMaster.TABLE);
                    myPreferenceManager.setRemoveLive(1);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }

    }

    @OnClick(R.id.btn_staff)
    public void clickBtnStaff() {
        Intent intent = new Intent(SelectTypeActivity.this, LoginActivity.class);
        intent.putExtra(AppUtils.LOGINTYPE,1);
        startActivity(intent);
//        finish();

    }

    @OnClick(R.id.btn_farmer)
    public void clickBtnFarmer() {
        Intent intent = new Intent(SelectTypeActivity.this, LoginActivity.class);
        intent.putExtra(AppUtils.LOGINTYPE,2);
        startActivity(intent);
//        finish();

    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

}
