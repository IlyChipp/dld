
package com.zealtech.dld.dldregist.model.farmerList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class DTRowData {

    @Expose
    @SerializedName("farmerId")
    private Integer farmerId;
    @Expose
    @SerializedName("pid")
    private long pid;
    @Expose
    @SerializedName("farmId")
    private Integer farmId;

    public Integer getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(Integer farmerId) {
        this.farmerId = farmerId;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public Integer getFarmId() {
        return farmId;
    }

    public void setFarmId(Integer farmId) {
        this.farmId = farmId;
    }
}
