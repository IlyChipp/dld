package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 10/19/2017 AD.
 */
@Parcel
public class FeedSubTypeMaster implements APIResponse {
    //DBHelper
    public static final String TABLE = "feed_subtype";

    String responseCode;
    String responseMessage;
    ArrayList<SubTypeEntity> data;

    public FeedSubTypeMaster(){ }

    @Override
    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<SubTypeEntity> getData() {
        return data;
    }

    public void setData(ArrayList<SubTypeEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class SubTypeEntity{
        int feed_SubType_ID;
        String feed_SubType_Name;
        int orderIndex;
        int status_ID;

        public SubTypeEntity(){

        }

        public int getFeed_SubType_ID() {
            return feed_SubType_ID;
        }

        public void setFeed_SubType_ID(int feed_SubType_ID) {
            this.feed_SubType_ID = feed_SubType_ID;
        }

        public String getFeed_SubType_Name() {
            return feed_SubType_Name;
        }

        public void setFeed_SubType_Name(String feed_SubType_Name) {
            this.feed_SubType_Name = feed_SubType_Name;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String FEED_SUBTYPE_ID = "feed_SubType_ID";
        public static final String FEED_SUBTYPE_NAME = "feed_SubType_Name";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
    }


}
