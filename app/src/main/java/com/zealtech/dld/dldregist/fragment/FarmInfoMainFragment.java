package com.zealtech.dld.dldregist.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Spinner;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.FarmActivity;
import com.zealtech.dld.dldregist.listener.UpdatableFragment;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FarmInfoMainFragment extends Fragment implements UpdatableFragment {

    final public static String TAG = "FarmInfoMainFragment";

    @BindView(R.id.spn_farm)
    Spinner spn_farm;
    @BindView(R.id.plsAddFarm)
    MyNoHBTextView plsAddFarm;
    @BindView(R.id.contentContainerFarmInfo)
    FrameLayout contentContainerFarmInfo;

    ArrayList<Farm> farms = new ArrayList<>();
    ArrayList<String> farmName = new ArrayList<>();

    FarmInfoFragment farmInfo;

//    int current_farm_pos = 0;

    public FarmInfoMainFragment() {
        super();
    }

    public static FarmInfoMainFragment newInstance() {
        FarmInfoMainFragment fragment = new FarmInfoMainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_farm_info_main, container, false);
        ButterKnife.bind(this, rootView);

//            plsAddFarm.setVisibility(View.GONE);
//            contentContainerFarmInfo.setVisibility(View.VISIBLE);
//
//            for (int i = 0; i < farms.size(); i++) {
//                farmName.add(farms.get(i).getFarmName());
//            }
//
//            MySpinnerAdapter adapter = new MySpinnerAdapter(
//                    getContext(),
//                    android.R.layout.simple_dropdown_item_1line,
//                    farmName);
//
//            spn_farm.setAdapter(adapter);
//
//            spn_farm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    current_farm_pos = position;
//                    filterListFeed(current_farm_pos);
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> parent) {
//
//                }
//            });
//
//            spn_farm.setSelection(0);
//        }else{
//            plsAddFarm.setVisibility(View.VISIBLE);
//            contentContainerFarmInfo.setVisibility(View.GONE);
//        }

        setUpData();


        return rootView;
    }

    public void setUpData() {

        farms.clear();

        farms.addAll(((FarmActivity) (getActivity())).getAllFarm());

        if (farms.size() != 0) {
            plsAddFarm.setVisibility(View.GONE);
            contentContainerFarmInfo.setVisibility(View.VISIBLE);
            filterListFeed(((FarmActivity) getActivity()).getCurrent_FarmID());
        } else {
            plsAddFarm.setVisibility(View.VISIBLE);
            contentContainerFarmInfo.setVisibility(View.GONE);
        }

//        farmInfo.setUpData();
    }

    void filterListFeed(int farmID) {
//        int farmID = farms.get(pos).getFarmID();
        if (farmInfo == null) {
            farmInfo = FarmInfoFragment.newInstance(farmID);
            getFragmentManager().beginTransaction()
                    .replace(R.id.contentContainerFarmInfo, farmInfo)
                    .commit();
        } else {
            farmInfo.farmID = farmID;
            farmInfo.setUpData();
        }

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /*
         * Save Instance State Here
         */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here

    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }


    @Override
    public void update(Double animalWorth) {
        if (farmInfo!= null) {
            farmInfo.updateText(animalWorth);
        }
    }
}
