package com.zealtech.dld.dldregist.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.DailyActivity;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.model.LivestockDaily;
import com.zealtech.dld.dldregist.model.LivestockDaily.LivestockDailyListEntity;
import com.zealtech.dld.dldregist.model.LivestockDailyMaster.LivestockDailyEntity;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.util.ScreenUtils;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DailyFragment extends Fragment {

    @BindView(R.id.tab)
    LinearLayout tab;
    @BindView(R.id.scr_h)
    HorizontalScrollView scr_h;
    @BindView(R.id.progressBar)
    FrameLayoutDisable progressBar;
    @BindView(R.id.plsAddFarm)
    MyNoHBTextView plsAddFarm;
    @BindView(R.id.contentContainerListAnimal)
    FrameLayout contentContainerListAnimal;

    @BindView(R.id.spn_farm)
    Spinner spn_farm;

    String dateFind="";

    private final ArrayList<LivestockDailyEntity> livestockMasters=new ArrayList<>();
    private ArrayList<LivestockDailyEntity> parentTabs=new ArrayList<>();
    private ArrayList<MyNoHLTextView> titles=new ArrayList<>();
    ArrayList<LivestockDailyListEntity> livValues=new ArrayList<>();

    ArrayList<Farm> farms=new ArrayList<>();

//    int current_farm_pos=0;
    int current_tab_pos=0;

    ListDailyLiveStockFragment listLiveStockFragment;

    public DailyFragment() {
        super();
    }

    public static DailyFragment newInstance() {
        DailyFragment fragment = new DailyFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_live_stock, container, false);
        ButterKnife.bind(this, rootView);
        initInstances(rootView);

        setUpData();

        return rootView;
    }

    public void setUpData() {

        farms.clear();
        livestockMasters.clear();
        parentTabs.clear();
        titles.clear();

        farms.addAll(((DailyActivity)(getActivity())).getAllFarm());
//
        if(farms.size()!=0){
            contentContainerListAnimal.setVisibility(View.VISIBLE);
            tab.setVisibility(View.VISIBLE);
            plsAddFarm.setVisibility(View.GONE);

            livestockMasters.addAll(((DailyActivity)getActivity()).getLivestockMasters());

//            getAllParent();

            getLiveStockValue(((DailyActivity)getActivity()).getCurrent_FarmID());

        }else{
            contentContainerListAnimal.setVisibility(View.GONE);
            tab.setVisibility(View.GONE);
            plsAddFarm.setVisibility(View.VISIBLE);
        }
    }

    private void initInstances(View rootView) {

    }

    public void getListLivestock(int rootId,int farmID) {

        ArrayList<LivestockDailyEntity> livestockEntities=getAllRootChild(rootId);

        FragmentManager fragmentManager = getChildFragmentManager();

        String tag=Integer.toString(rootId);

        if (listLiveStockFragment == null) {
            listLiveStockFragment = ListDailyLiveStockFragment
                    .newInstance(rootId,livestockEntities,farmID);
            fragmentManager.beginTransaction()
                    .replace(R.id.contentContainerListAnimal, listLiveStockFragment,tag)
                    .commit();
        } else {
            listLiveStockFragment.livestockEntities = null;
            listLiveStockFragment.setLivestockEntities(livestockEntities);
            listLiveStockFragment.farmID = farmID;
            listLiveStockFragment.setUpData();
        }

    }

    private void getLiveStockValue(final int farmId) {

        livValues.clear();
        progressBar.setVisibility(View.VISIBLE);

        Api api = AppUtils.getApiService();
        dateFind=((DailyActivity)getActivity()).getFindDate();
        Call<LivestockDaily> call = api.getLivestockDaily(Integer.toString(farmId),dateFind);
        call.enqueue(new Callback<LivestockDaily>() {
            @Override
            public void onResponse(Call<LivestockDaily> call, Response<LivestockDaily> response) {
                progressBar.setVisibility(View.GONE);
                if(isAdded()) {
                    if (response.body() != null) {

                        if(response.body().getResponseCode().equals("200")) {
                            livValues.addAll(response.body().getData());
                            if(livValues.size()!=0) {
                                getAllParentWithValue();
                            }else{
                                getAllParent();
                            }
                        }else{

                            Toast.makeText(getActivity(), response.body().getResponseMessage(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        try {
                            Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<LivestockDaily> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }

        });
    }


    private ArrayList<LivestockDailyEntity> getAllRootChild(int root_type) {

        ArrayList<LivestockDailyEntity> livestocks = new ArrayList<>();

        for (int i = 0; i < livestockMasters.size(); i++) {
            int root_id = getRootParentId(livestockMasters.get(i).getLivestock_NoteType_ID());
            if (root_id == root_type && livestockMasters.get(i).getLivestock_NoteType_Parent_ID() != -1) { //remove parent
                LivestockDailyEntity livestockEntity=new LivestockDailyEntity(livestockMasters.get(i));
                livestocks.add(livestockEntity);
            }
        }

        return livestocks;
    }

    private void getAllParent(){
        for (int i = 0; i < livestockMasters.size(); i++) {
            if (livestockMasters.get(i).getLivestock_NoteType_Parent_ID() == -1) {
                parentTabs.add(livestockMasters.get(i));
            }
        }

        if (livestockMasters.size()>0) {
            initTab();
            getListLivestock(parentTabs.get(current_tab_pos).getLivestock_NoteType_ID(),((DailyActivity)getActivity()).getCurrent_FarmID());
        }
    }

    private void getAllParentWithValue() {
//        for (int i = 0; i < livestockMasters.size(); i++) {
//            if (livestockMasters.get(i).getLivestock_NoteType_Parent_ID() == -1) {
//                parentTabs.add(livestockMasters.get(i));
//            }
//        }
       for (int i = 0; i < livestockMasters.size(); i++) {
           for(int j=0;j<livValues.size();j++) {
               int parentId=getRootParentId(livValues.get(j).getLivestock_NoteType_ID());
               if (livestockMasters.get(i).getLivestock_NoteType_ID() == parentId) {
                   if(parentTabs.size()==0) {
                       parentTabs.add(livestockMasters.get(i));
                       break;
                   }else{
                       boolean isInTab=false;
                       for(int k=0;k<parentTabs.size();k++) {
                           if(parentTabs.get(k).getLivestock_NoteType_ID()==parentId) {
                               isInTab=true;
                               break;
                           }
                       }
                       if(!isInTab) {
                           parentTabs.add(livestockMasters.get(i));
                       }
                       break;
                   }
               }
           }
        }

        if (livestockMasters.size()>0) {
            initTab();
            getListLivestock(parentTabs.get(current_tab_pos).getLivestock_NoteType_ID(),((DailyActivity)getActivity()).getCurrent_FarmID());
        }
    }

    private void initTab() {
        tab.removeAllViews();
        current_tab_pos=0;

        for(int i=0;i<parentTabs.size();i++){
            View view = getLayoutInflater().inflate(R.layout.item_tab_livestock, null);
            MyNoHLTextView txt_title = (MyNoHLTextView) view.findViewById(R.id.txt_title);
            final LinearLayout btn_tab = (LinearLayout) view.findViewById(R.id.btn_tab);
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);

            final int i_=i;
            final ScreenUtils screenUtils=new ScreenUtils(getActivity());

            titles.add(txt_title);

            btn_tab.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    int scrollX = (btn_tab.getLeft() - (screenUtils.getScreenWidth()/ 2)) + (btn_tab.getWidth() / 2);
                    scr_h.smoothScrollTo(scrollX, 0);
                    setTextColor(i_);
                    current_tab_pos=i_;
                    getListLivestock(parentTabs.get(current_tab_pos).getLivestock_NoteType_ID(),((DailyActivity)getActivity()).getCurrent_FarmID());
                }
            });

            txt_title.setText(parentTabs.get(i).getLivestock_NoteType_Name());
            setIcon(imageView,parentTabs.get(i).getLivestock_NoteType_ID());
            tab.addView(view);
        }
        setTextColor(0);
        if (parentTabs.size()>0) {
            getListLivestock(parentTabs.get(current_tab_pos).getLivestock_NoteType_ID(),((DailyActivity)getActivity()).getCurrent_FarmID());
        }
    }

    private void setIcon(ImageView imageView,int type_id){
        if(type_id==2){
            imageView.setImageResource(R.drawable.meat);
        }else if(type_id==19){
            imageView.setImageResource(R.drawable.cow_milk);
        }else if(type_id==27){
            imageView.setImageResource(R.drawable.buf);
        }else if(type_id==38){
            imageView.setImageResource(R.drawable.pig);
        }else if(type_id==45){
            imageView.setImageResource(R.drawable.chicken);
        }else if(type_id==51){
            imageView.setImageResource(R.drawable.duck);
        }else if(type_id==57){
            imageView.setImageResource(R.drawable.goat);
        }else if(type_id==67){
            imageView.setImageResource(R.drawable.group);
        }else{
            imageView.setImageResource(R.drawable.group);
        }
    }

    private void setTextColor(int position){
        for(int i=0;i<titles.size();i++){
            if(i!=position){
                titles.get(i).setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
            }else{
                titles.get(i).setTextColor(ContextCompat.getColor(getContext(), R.color.txt_blue2));
            }
        }
    }

    private int getRootParentId(int type_id){
        int id=0;
        for(int i=0;i<livestockMasters.size();i++){
            if(livestockMasters.get(i).getLivestock_NoteType_ID()==type_id){
                if(livestockMasters.get(i).getLivestock_NoteType_Parent_ID()==-1) {
                    return livestockMasters.get(i).getLivestock_NoteType_ID();
                }else{
                    id= getRootParentId(livestockMasters.get(i).getLivestock_NoteType_Parent_ID());
                }
            }
        }
        return id;
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /*
         * Save Instance State Here
         */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }


}
