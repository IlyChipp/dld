package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/26/2017 AD.
 */
@Parcel
public class TambolMaster implements APIResponse{

    //Database
    public static final String TABLE = "tambol";

    String responseCode;
    String responseMessage;
    ArrayList<TambolEntity> data;

    public TambolMaster(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<TambolEntity> getData() {
        return data;
    }

    public void setData(ArrayList<TambolEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class TambolEntity {
        int tambol_ID;
        int amphur_ID;
        Double latitude;
        Double longitude;
        int orderIndex;
        int status_ID;
        int tambol_Code;
        String tambol_NameEn;
        String tambol_NameTh;
        String zipcode;

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public TambolEntity(){ }

        public int getTambol_ID() {
            return tambol_ID;
        }

        public void setTambol_ID(int tambol_ID) {
            this.tambol_ID = tambol_ID;
        }

        public int getAmphur_ID() {
            return amphur_ID;
        }

        public void setAmphur_ID(int amphur_ID) {
            this.amphur_ID = amphur_ID;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        public int getTambol_Code() {
            return tambol_Code;
        }

        public void setTambol_Code(int tambol_Code) {
            this.tambol_Code = tambol_Code;
        }

        public String getTambol_NameEn() {
            return tambol_NameEn;
        }

        public void setTambol_NameEn(String tambol_NameEn) {
            this.tambol_NameEn = tambol_NameEn;
        }

        public String getTambol_NameTh() {
            return tambol_NameTh;
        }

        public void setTambol_NameTh(String tambol_NameTh) {
            this.tambol_NameTh = tambol_NameTh;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String TAMBOL_ID = "tambol_ID";
        public static final String AMPHUR_ID = "amphur_ID";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
        public static final String TAMBOL_CODE = "tambol_Code";
        public static final String TAMBOL_NAME_EN = "tambol_NameEn";
        public static final String TAMBOL_NAME_TH = "tambol_NameTh";
        public static final String ZIPCODE = "zipcode";
    }
}
