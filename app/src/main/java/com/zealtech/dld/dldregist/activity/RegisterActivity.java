package com.zealtech.dld.dldregist.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.camera.CameraModule;
import com.esafirm.imagepicker.features.camera.ImmediateCameraModule;
import com.esafirm.imagepicker.features.camera.OnImageReadyListener;
import com.esafirm.imagepicker.model.Image;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.zealtech.dld.dldregist.MainApplication;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.listener.PIDScannerListener;
import com.zealtech.dld.dldregist.manager.Contextor;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.AmphurMaster;
import com.zealtech.dld.dldregist.model.AmphurMaster.AmphurEntity;
import com.zealtech.dld.dldregist.model.CompanyTypeMaster;
import com.zealtech.dld.dldregist.model.PrefixMaster;
import com.zealtech.dld.dldregist.model.PrefixMaster.PrefixEntity;
import com.zealtech.dld.dldregist.model.ProvinceMaster;
import com.zealtech.dld.dldregist.model.ProvinceMaster.ProvinceEntity;
import com.zealtech.dld.dldregist.model.TambolMaster;
import com.zealtech.dld.dldregist.model.TambolMaster.TambolEntity;
import com.zealtech.dld.dldregist.model.VillageMaster;
import com.zealtech.dld.dldregist.model.VillageMaster.VillageEntity;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.RegisterFarmerItem;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseBase;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseInsertFarmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseRegister;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.util.UpdateListener;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;
import com.zealtech.dld.dldregist.widget.MySpinnerAdapter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.zealtech.dld.dldregist.util.AppUtils.REQUEST_LOCATION;
import static com.zealtech.dld.dldregist.util.AppUtils.RequestPermissionCodeLocation;

public class RegisterActivity extends AppCompatActivity implements PIDScannerListener {

    @BindView(R.id.radioType)
    RadioGroup radio;

    @BindView(R.id.radioNotCancel)
    RadioButton radioNotCancel;

    @BindView(R.id.radioCancel)
    RadioButton radioCancel;

    @BindView(R.id.radioNormal)
    RadioButton radioNormal;

    private LinearLayout llRegister;
    private static final int REQUEST_BARCODE = 3;
    ArrayList<String> listProvince = new ArrayList<>();
    ArrayList<String> listPrefix = new ArrayList<>();
    ArrayList<String> listVillage = new ArrayList<>();
    ArrayList<String> listTambol = new ArrayList<>();
    ArrayList<String> listAmphur = new ArrayList<>();
    ArrayList<ProvinceMaster.ProvinceEntity> province = new ArrayList<>();
    ArrayList<AmphurMaster.AmphurEntity> amphur = new ArrayList<>();
    ArrayList<TambolMaster.TambolEntity> tambol = new ArrayList<>();
    ArrayList<VillageMaster.VillageEntity> village = new ArrayList<>();
    ArrayList<PrefixMaster.PrefixEntity> prefix = new ArrayList<>();
    private RegisterFarmerItem mFarmerItem = new RegisterFarmerItem();
    private Farmer mFarmerItemImage  = new Farmer();

    private MyEditNoHLTextView txtNameTH;
    private MyEditNoHLTextView txtLastnameTH;
    private MyEditNoHLTextView txtHomeNo;
    private MyEditNoHLTextView txtPost;
    private MyEditNoHLTextView txtMobile;
    private MyEditNoHLTextView txtPhone,txtTaxId, txtCompanyName;
    private MyEditNoHLTextView txtLat;
    private MyEditNoHLTextView txtLong;
    private MyEditNoHLTextView txtEmail, txtUsername, txtPassword, txtPassword_re;
    private MyNoHBTextView btnRegist,title_type, title_info;
    private MyNoHLTextView txtTopicType;
    private LinearLayout addFarm;
    private LinearLayout llFarmerAddress,linearLayout_type;
    private Spinner spnAmphur;
    private Spinner spnPrefix;
    private Spinner spnTambol;
    private Spinner spnProvince;
    private Spinner spnVillage;
    private MyEditNoHLTextView txtBD;
    private MyNoHBTextView btnSubmit,btnScan,btnRead,btnLatLong;
    private MyNoHLTextView title_pid;
    private MyEditNoHLTextView txtPID;
    private MyPreferenceManager myPreferenceManager;
    private ImageView imgProfile;
    private FrameLayoutDisable progressBar;
    private Context context = this;

    private Spinner spnCompany;
    ArrayList<String> listCompanies = new ArrayList<>();

    int userType=1;

    private int type, from;
    ArrayList<CompanyTypeMaster.CompanyEntity> companies = new ArrayList<>();


    DBHelper dbHelper;

    boolean isCancel=true;

    HashMap<String,String> result_;

    private static final int RC_CODE_PICKER = 2000;
    private static final int RC_CAMERA = 3000;
    private static final int WRITE_STORAGE = 4000;
    private CameraModule cameraModule;

    private ArrayList<Image> images = new ArrayList<>();

    Tracker mTracker;
//
//    int REQUEST_CODE_PICKER = 103;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        View view = getLayoutInflater().inflate(R.layout.item_farmer_address, null);

        ButterKnife.bind(this);

        MainApplication application = (MainApplication) getApplication();
        mTracker = application.getDefaultTracker();
//        ButterKnife.bind(this,view);
        init(view);
        llRegister.addView(view);
        dbHelper = new DBHelper(context);

        type=1;
        setTypeFaceRadio();
        setRadio();

        isStoragePermissionGranted();
//        addTotalSpinner();
        getListCompany();
        getListProvince();
        getListPrefix();


    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("หน้าลงทะเบียนใหม่");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    private void setTypeFaceRadio() {
        Typeface tf = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font_en_light));
        radioCancel.setTypeface(tf);
        radioNotCancel.setTypeface(tf);
        radioNormal.setTypeface(tf);
    }

    private void setRadio() {
        radioNotCancel.setChecked(true);

        radio.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i==R.id.radioNotCancel){
                    type=1;
                    linearLayout_type.setVisibility(View.GONE);
                    title_info.setText(getString(R.string.farmer));
                }else if (i==R.id.radioCancel){
                    type=2;
                    linearLayout_type.setVisibility(View.VISIBLE);
                    title_info.setText("ข้อมูลผู้ประสานงาน");
                    title_type.setText("ข้อมูลนิติบุคคล");
                    txtTopicType.setText("ชื่อบริษัท");
                }else{
                    type=3;
                    linearLayout_type.setVisibility(View.VISIBLE);
                    title_info.setText("ข้อมูลผู้ประสานงาน");
                    title_type.setText("ข้อมูลหน่วยงาน");
                    txtTopicType.setText("ชื่อหน่วยงาน");
                }
            }
        });

//        if (isCancel) {
//            radioNotCancel.setChecked(true);
//            type=0;
//        } else {
//            radioCancel.setChecked(true);
//            type=1;
//        }
    }


    @Optional
    @OnClick(R.id.btnRegist)
    public void clickBtnRegist() {
        if (txtUsername.getText().toString().length() == 13  && txtPassword.getText().toString().length() > 0) {
            if (checkValidPersonnalId(txtUsername.getText().toString())) {
                if (type == 1) { //personID
                        if (txtPassword.getText().toString().equals(txtPassword_re.getText().toString())) {
                            checkDuplicate(txtUsername.getText().toString(), type);
                        } else {
                            Toast.makeText(context, "ยืนยันรหัสผ่านไม่ถูกต้อง", Toast.LENGTH_SHORT).show();
                        }
                } else { //VATID
                    if (txtPassword.getText().toString().equals(txtPassword_re.getText().toString())) {
                            checkDuplicate(txtTaxId.getText().toString(), type);
                    } else {
                        Toast.makeText(context, "ยืนยันรหัสผ่านไม่ถูกต้อง", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                Toast.makeText(context, "เลขบัตรประชาชนไม่ถูกต้อง", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, getString(R.string.alert_data_incomplete), Toast.LENGTH_SHORT).show();
        }
    }

    private void checkDuplicate(String id,int userType) {
        Api api = AppUtils.getApiService();

        progressBar.setVisibility(View.VISIBLE);
        Call<ResponseBase> call = api.checkDuplicateID(id, userType);
        call.enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {

                if (response.body() != null) {

                    if (response.body().getResponseCode().equals(getString(R.string.success))) {
                        setData();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(context, response.body().getResponseMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    try {
                        Toast.makeText(context, response.errorBody().string(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });
    }

//    @Optional
//    @OnClick(R.id.btnLatLong)
//    public void clickbtnLatLong() {
//        if (!AppUtils.checkPermissionLocation(context)) {
//            requestPermissions(
//                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                    RequestPermissionCodeLocation);
//        } else {
//            startActivityForResult(new Intent(context, MapActivity.class), REQUEST_LOCATION);
//        }
//    }

    @Optional
    @OnClick(R.id.btn_back)
    public void clickBtnBack() {
        finish();
    }

//    @Optional
//    @OnClick(R.id.txtBD)
//    public void clickBtnDate() {
//        setDate();
//    }

//    @Optional
//    @OnClick(R.id.btnScan)
//    public void clickScan() {
//        if (AppUtils.hasPermission(RegisterActivity.this)) {
//            AppUtils.hideKeyboard(RegisterActivity.this);
//
//            IntentIntegrator integrator = new IntentIntegrator(this);
//            integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
//            integrator.initiateScan();
//        } else {
//            AppUtils.askPermission(RegisterActivity.this, REQUEST_BARCODE);
//        }
//    }



//    @Optional
//    @OnClick(R.id.imgProfile)
//    public void clickImgProfile() {
//
//
////        showPictureDialog();
//        start();
//
//    }

    public void setDate() {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        int month = i1 + 1;

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(i, i1, i2);

                        txtBD.setText( AppUtils.convertDateThai(i2+"/"+month+"/"+i));
//                        date = calendar.getTimeInMillis();


//                        editText_date.setText(date2+"");
                    }


                }, year, month, day);
        datePickerDialog.show();

    }

    public void init(View view) {
        linearLayout_type = (LinearLayout) view.findViewById(R.id.linearLayout_type);
        title_pid = (MyNoHLTextView)view. findViewById(R.id.title_pid);
        txtUsername = (MyEditNoHLTextView) findViewById(R.id.txtUsername);
        txtPassword = (MyEditNoHLTextView) findViewById(R.id.txtPassword);
        txtPassword_re = (MyEditNoHLTextView) findViewById(R.id.txtPassword_re);
        llRegister = (LinearLayout) findViewById(R.id.llRegister);
        // Init 'View' instance(s) with rootView.findViewById here
        txtPID = (MyEditNoHLTextView) view.findViewById(R.id.txtPID);
        txtNameTH = (MyEditNoHLTextView) view.findViewById(R.id.txtNameTH);
        txtLastnameTH = (MyEditNoHLTextView) view.findViewById(R.id.txtLastnameTH);
        txtHomeNo = (MyEditNoHLTextView) view.findViewById(R.id.txtHomeNo);
        txtPost = (MyEditNoHLTextView) view.findViewById(R.id.txtPost);
        txtMobile = (MyEditNoHLTextView) view.findViewById(R.id.txtTel);
        txtPhone = (MyEditNoHLTextView) view.findViewById(R.id.txtPhone);
        txtLat = (MyEditNoHLTextView) view.findViewById(R.id.txtLat);
        txtLong = (MyEditNoHLTextView) view.findViewById(R.id.txtLong);
        txtEmail = (MyEditNoHLTextView) view.findViewById(R.id.txtEmail);
        spnAmphur = (Spinner) view.findViewById(R.id.spnAmphur);
        spnPrefix = (Spinner) view.findViewById(R.id.spnPrefix);
        spnTambol = (Spinner) view.findViewById(R.id.spnTambol);
        spnProvince = (Spinner) view.findViewById(R.id.spnProvince);
        spnVillage = (Spinner) view.findViewById(R.id.spnVillage);
        txtBD = (MyEditNoHLTextView) view.findViewById(R.id.txtBD);
        imgProfile = (ImageView) view.findViewById(R.id.imgProfile);
        progressBar = (FrameLayoutDisable) findViewById(R.id.progressBar);

        spnCompany = (Spinner) view.findViewById(R.id.spnCompany);
        txtTaxId = (MyEditNoHLTextView) view.findViewById(R.id.txtTaxId);
        txtCompanyName = (MyEditNoHLTextView) view.findViewById(R.id.txtCompanyName);
        title_type = (MyNoHBTextView) view.findViewById(R.id.title_type);
        title_info = (MyNoHBTextView) view.findViewById(R.id.title_info);
        txtTopicType = (MyNoHLTextView) view.findViewById(R.id.txtTopicType);

        btnScan= (MyNoHBTextView) view.findViewById(R.id.btnScan);
        btnRead= (MyNoHBTextView) view.findViewById(R.id.btnRead);
        btnLatLong= (MyNoHBTextView) view.findViewById(R.id.btnLatLong);

        btnLatLong.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                String provider = Settings.Secure.getString(getContentResolver(),
                        Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

                if(!provider.equals("")) {
                    if (!AppUtils.checkPermissionLocation(context)) {
                        requestPermissions(
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                RequestPermissionCodeLocation);
                    } else {
                        startActivityForResult(new Intent(context, MapActivity.class), REQUEST_LOCATION);
                    }
                }else{

                    AlertDialog.Builder builder= new AlertDialog.Builder(RegisterActivity.this);
                    builder .setTitle("แจ้งเตือน")
                            .setMessage(getString(R.string.access_location))
                            .setPositiveButton("ใช่", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
                                    startActivity(intent);
                                }
                            })
                            .setNegativeButton("ไม่", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            //.setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }
        });

        btnScan.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppUtils.hasPermission(RegisterActivity.this)) {
                    AppUtils.hideKeyboard(RegisterActivity.this);

                    IntentIntegrator integrator = new IntentIntegrator(RegisterActivity.this);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                    integrator.initiateScan();
                } else {
                    AppUtils.askPermission(RegisterActivity.this, REQUEST_BARCODE);
                }
            }
        });

        btnRead.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                PIDScanActivity pidScanActivity = new PIDScanActivity(RegisterActivity.this
                        ,RegisterActivity.this,imgProfile);
                pidScanActivity.onCreateActivity();
            }
        });

        txtBD.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate();
            }
        });

        imgProfile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                start();
            }
        });

        linearLayout_type.setVisibility(View.GONE);
        txtPID.setVisibility(View.GONE);
        title_pid.setVisibility(View.GONE);
    }

    private void setData() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();

        if(type==2||type==3) { //case comp,org
            if (txtTaxId.getText().toString().length() > 0) {
                mFarmerItem.setTaxID(Long.valueOf(txtTaxId.getText().toString()));
            }
            mFarmerItem.setCompany_Name(txtCompanyName.getText().toString());
        }else {//case org

        }

        mFarmerItem.setCreateBy(String.valueOf(""));
        mFarmerItem.setUpdateBy(String.valueOf(""));
        mFarmerItem.setCreateDate(dateFormat.format(date));
        mFarmerItem.setUpdateDate(dateFormat.format(date));
        mFarmerItem.setStatusID(2);

        mFarmerItem.setBirthDay(AppUtils.convertDateEng(txtBD.getText().toString()));
        String pid = txtUsername.getText().toString();
        mFarmerItem.setUsername(pid);
        mFarmerItem.setPassword(txtPassword.getText().toString());
        mFarmerItem.setProvinceID(provinceID);
        mFarmerItem.setAmphurID(amphurID);
        mFarmerItem.setTambolID(tambolID);
        mFarmerItem.setPrefixID(PrefixID);
        mFarmerItem.setPid(Long.parseLong(pid));
        mFarmerItem.setFirstName(txtNameTH.getText().toString());
        mFarmerItem.setLastName(txtLastnameTH.getText().toString());
        mFarmerItem.setHomeNo(txtHomeNo.getText().toString());
        //TODO : Check villages agian
//        mFarmerItem.setVillageID(Integer.valueOf(txtMoo.getText().toString()));
        mFarmerItem.setPostCode(txtPost.getText().toString());
        mFarmerItem.setMobile(txtMobile.getText().toString());
        mFarmerItem.setPhone(txtPhone.getText().toString());
        mFarmerItem.setLatitude(txtLat.getText().toString());
        mFarmerItem.setLongitude(txtLong.getText().toString());
        mFarmerItem.setEmail(txtEmail.getText().toString());


//        mFarmerItemImage.setCreateBy(String.valueOf(""));
        mFarmerItemImage.setUpdateBy(String.valueOf(""));
//        mFarmerItemImage.setCreateDate(dateFormat.format(date));
        mFarmerItemImage.setUpdateDate(dateFormat.format(date));
        mFarmerItemImage.setStatusID(2);
        mFarmerItemImage.setBirthDay(AppUtils.convertDateEng(txtBD.getText().toString()));
        mFarmerItemImage.setProvinceID(provinceID);
        mFarmerItemImage.setAmphurID(amphurID);
        mFarmerItemImage.setTambolID(tambolID);
        mFarmerItemImage.setPrefixID(PrefixID);
        mFarmerItemImage.setPid(Long.parseLong(pid));
        mFarmerItemImage.setFirstName(txtNameTH.getText().toString());
        mFarmerItemImage.setLastName(txtLastnameTH.getText().toString());
        mFarmerItemImage.setHomeNo(txtHomeNo.getText().toString());
        //TODO : Check villages agian
//        mFarmerItemImage.setVillageID(Integer.valueOf(txtMoo.getText().toString()));
        mFarmerItemImage.setPostCode(txtPost.getText().toString());
        mFarmerItemImage.setMobile(txtMobile.getText().toString());
        mFarmerItemImage.setPhone(txtPhone.getText().toString());
        mFarmerItemImage.setLatitude(txtLat.getText().toString());
        mFarmerItemImage.setLongitude(txtLong.getText().toString());
        mFarmerItemImage.setEmail(txtEmail.getText().toString());


        ArrayList<RegisterFarmerItem> farmers = new ArrayList<>();
        farmers.add(mFarmerItem);
        insertData(farmers);

    }

    public void addTotalSpinner(){
        ProvinceMaster.ProvinceEntity provinceEntity = new ProvinceMaster.ProvinceEntity();
        provinceEntity.setProvince_NameTh("ทั้งหมด");
        provinceEntity.setProvince_ID(-99);

        province.add(provinceEntity);

        AmphurMaster.AmphurEntity amphurEntity = new AmphurMaster.AmphurEntity();
        amphurEntity.setAmphur_nameTh("ทั้งหมด");
        amphurEntity.setAmphur_ID(-99);

        amphur.add(amphurEntity);

        TambolMaster.TambolEntity tambolEntity = new TambolMaster.TambolEntity();
        tambolEntity.setTambol_NameTh("ทั้งหมด");
        tambolEntity.setTambol_ID(-99);

        tambol.add(tambolEntity);

        VillageMaster.VillageEntity villageEntity = new VillageMaster.VillageEntity();
        villageEntity.setVillage_Name("ทั้งหมด");
        villageEntity.setVillage_ID(-99);

        village.add(villageEntity);


    }

    int PrefixID;

    public void getListPrefix() {
        listPrefix.clear();
        prefix.clear();
        int selectIndex = -1;
        PrefixEntity prefixEntity = new PrefixEntity();
        prefixEntity.setPrefix_NameTh("กรุณาเลือกคำนำหน้าชื่อ");
        prefixEntity.setPrefix_ID(-99);
        prefix.add(prefixEntity);

        prefix .addAll( dbHelper.getPrefixList());

        for (int i = 0; i < prefix.size(); i++) {
            listPrefix.add(prefix.get(i).getPrefix_NameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                context,
                android.R.layout.simple_dropdown_item_1line,
                listPrefix);

        spnPrefix.setAdapter(adapter);

        spnPrefix.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (prefix != null) {
//                mFarmerItem.setPrefixID(prefix.get(position).getPrefix_ID());
                    PrefixID = prefix.get(position).getPrefix_ID();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < prefix.size(); i++) {
            if (mFarmerItem.getPrefixID() != null) {
                if (prefix.get(i).getPrefix_ID() == mFarmerItem.getPrefixID()) {
                    selectIndex = i;
                    spnPrefix.setSelection(selectIndex);
                    break;
                }
            }
        }
    }

    int provinceID;
    int amphurID;
    int tambolID;
    int villageID;
    String zipcode;

    public void getListProvince() {
        listProvince.clear();
        province.clear();
        ProvinceEntity provinceEntity =new ProvinceEntity();
        provinceEntity.setProvince_NameTh("กรุณาเลือกจังหวัด");
        provinceEntity.setProvince_ID(-99);
        province.add(provinceEntity);
        int selectIndex = -99;

        province.addAll(dbHelper.getProvinceList());

        for (int i = 0; i < province.size(); i++) {
            listProvince.add(province.get(i).getProvince_NameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                context,
                android.R.layout.simple_dropdown_item_1line,
                listProvince);

        spnProvince.setAdapter(adapter);

        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                getListAmphur(province.get(position).getProvince_ID());
//                provinceID = province.get(position).getProvince_ID();
//                spnAmphur.setEnabled(true);
//                spnTambol.setEnabled(false);
//                spnVillage.setEnabled(false);

                provinceID=province.get(position).getProvince_ID();
                if(province.get(position).getProvince_ID()!=-99) {
//                    spnTambol.setEnabled(true);
                    spnAmphur.setEnabled(true);
                    getListAmphur(province.get(position).getProvince_ID());
                }else{
                    spnAmphur.setEnabled(false);
                    amphurID=-99;
                    spnTambol.setEnabled(false);
                    tambolID=-99;
                    spnVillage.setEnabled(false);
                    villageID=-99;
                }
                zipcode="";
                txtPost.setText(zipcode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < province.size(); i++) {
            if (mFarmerItem.getProvinceID() != null) {
                if (province.get(i).getProvince_ID() == mFarmerItem.getProvinceID()) {
                    selectIndex = i;
                    spnProvince.setSelection(selectIndex);
                    break;
                }
            }
        }

    }

    public void getListAmphur(int position) {
        listAmphur.clear();
        amphur.clear();
        AmphurEntity amphurEntity =new AmphurEntity();
        amphurEntity.setAmphur_nameTh("กรุณาเลือกอำเภอ");
        amphurEntity.setAmphur_ID(-99);
        amphur.add(amphurEntity);

        int selectIndex = -99;

        amphur.addAll(dbHelper.getAmphur(position));

        for (int i = 0; i < amphur.size(); i++) {
            listAmphur.add(amphur.get(i).getAmphur_nameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                context,
                android.R.layout.simple_dropdown_item_1line,
                listAmphur);
        spnAmphur.setAdapter(adapter);

        spnAmphur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                getListTambol(amphur.get(position).getAmphur_ID());
//
//                amphurID = amphur.get(position).getAmphur_ID();
//                spnTambol.setEnabled(true);
//                spnVillage.setEnabled(false);
                amphurID = amphur.get(position).getAmphur_ID();
                if (amphur.get(position).getAmphur_ID() != -99) {
                    spnTambol.setEnabled(true);
                    getListTambol(amphur.get(position).getAmphur_ID());
                } else {
                    spnTambol.setEnabled(false);
                    tambolID = -99;
                    spnVillage.setEnabled(false);
                    villageID=-99;
                }
                zipcode="";
                txtPost.setText(zipcode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < amphur.size(); i++) {
            if (mFarmerItem.getAmphurID() != null) {
                if (amphur.get(i).getAmphur_ID() == mFarmerItem.getAmphurID()) {
                    selectIndex = i;
                    spnAmphur.setSelection(selectIndex);
                    break;
                }
            }
        }

        if(result_!=null) {
            if (result_.get("amphur")!=null || ! result_.get("amphur").equals("")) {
                for (int i = 0; i < amphur.size(); i++) {
                    if (amphur.get(i).getAmphur_NameTh().equals(result_.get("amphur"))) {
                        spnAmphur.setSelection(i);
//                        amphurID = amphur.get(i).getAmphur_ID();
//                        spnTambol.setEnabled(true);
//                        getListTambol(amphur.get(i).getAmphur_ID());
                        break;
                    }
                }
            }
        }
    }

    public void getListTambol(int position) {
        listTambol.clear();
        tambol.clear();
        TambolEntity tambolEntity =new TambolEntity();
        tambolEntity.setTambol_NameTh("กรุณาเลือกตำบล");
        tambolEntity.setTambol_ID(-99);
        tambol.add(tambolEntity);

        int selectIndex = -99;

        tambol.addAll(dbHelper.getTambol(position));

        for (int i = 0; i < tambol.size(); i++) {
            listTambol.add(tambol.get(i).getTambol_NameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                context,
                android.R.layout.simple_dropdown_item_1line,
                listTambol);
        spnTambol.setAdapter(adapter);

        spnTambol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                getListVillage(tambol.get(position).getTambol_ID());
//
//                tambolID = tambol.get(position).getTambol_ID();
//
//                txtPost.setText(zipcode);

                tambolID=tambol.get(position).getTambol_ID();
                if(tambol.get(position).getTambol_ID()!=-99) {
//                    spnTambol.setEnabled(true);
                    spnVillage.setEnabled(true);
                    zipcode = tambol.get(position).getZipcode();
                    txtPost.setText(zipcode);
                    getListVillage(tambol.get(position).getTambol_ID());
                }else{
                    spnVillage.setEnabled(false);
                    zipcode="";
                    villageID=-99;
                }


//                spnVillage.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < tambol.size(); i++) {
            if (mFarmerItem.getTambolID() != null) {
                if (tambol.get(i).getTambol_ID() == mFarmerItem.getTambolID()) {
                    selectIndex = i;
                    spnTambol.setSelection(selectIndex);
                    break;
                }
            }
        }

        if(result_!=null) {
            if (result_.get("tambol") != null ||! result_.get("tambol").equals("")) {
                for (int i = 0; i < tambol.size(); i++) {
                    if (tambol.get(i).getTambol_NameTh().equals(result_.get("tambol"))) {
                        spnTambol.setSelection(i);
                        break;
                    }
                }
            }
        }
    }


//    public void getListProvince() {
//        ProvinceEntity provinceEntity =new ProvinceEntity();
//        provinceEntity.setProvince_NameTh("ทั้งหมด");
//        provinceEntity.setProvince_ID(-99);
//        province.add(provinceEntity);
//
//        province.addAll(dbHelper.getProvinceList());
//
//        for (int i = 0; i < province.size(); i++) {
//            listProvince.add(province.get(i).getProvince_NameTh());
//        }
//        MySpinnerAdapter adapter = new MySpinnerAdapter(
//                RegisterActivity.this,
//                android.R.layout.simple_dropdown_item_1line,
//                listProvince);
//
//        spnProvince.setAdapter(adapter);
//
//        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                provinceID=province.get(position).getProvince_ID();
//                if(province.get(position).getProvince_ID()!=-99) {
//                    spnTambol.setEnabled(true);
//                    spnAmphur.setEnabled(true);
//                    getListAmphur(province.get(position).getProvince_ID());
//                }else{
//                    spnAmphur.setEnabled(false);
//                    amphurID=-99;
//                    spnTambol.setEnabled(false);
//                    tambolID=-99;
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }
//
//    public void getListAmphur(int position) {
//        listAmphur.clear();
//        amphur.clear();
//        AmphurEntity amphurEntity =new AmphurEntity();
//        amphurEntity.setAmphur_nameTh("ทั้งหมด");
//        amphurEntity.setAmphur_ID(-99);
//        amphur.add(amphurEntity);
//
//        amphur.addAll(dbHelper.getAmphur(position));
//
//        for (int i = 0; i < amphur.size(); i++) {
//            listAmphur.add(amphur.get(i).getAmphur_nameTh());
//        }
//
//        MySpinnerAdapter adapter = new MySpinnerAdapter(
//                RegisterActivity.this,
//                android.R.layout.simple_dropdown_item_1line,
//                listAmphur);
//        spnAmphur.setAdapter(adapter);
//
//        spnAmphur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                amphurID=amphur.get(position).getAmphur_ID();
//                if(amphur.get(position).getProvince_ID()!=-99) {
//                    spnTambol.setEnabled(true);
//                    getListTambol(amphur.get(position).getAmphur_ID());
//                }else{
//                    spnTambol.setEnabled(false);
//                    tambolID=-99;
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }
//
//    public void getListTambol(int position) {
//        listTambol.clear();
//        tambol.clear();
//        TambolEntity tambolEntity =new TambolEntity();
//        tambolEntity.setTambol_NameTh("ทั้งหมด");
//        tambolEntity.setTambol_ID(-99);
//        tambol.add(tambolEntity);
//
//        tambol.addAll( dbHelper.getTambol(position));
//
//        for (int i = 0; i < tambol.size(); i++) {
//            listTambol.add(tambol.get(i).getTambol_NameTh());
//        }
//
//        MySpinnerAdapter adapter = new MySpinnerAdapter(
//                RegisterActivity.this,
//                android.R.layout.simple_dropdown_item_1line,
//                listTambol);
//        spnTambol.setAdapter(adapter);
//
//        spnTambol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                tambolID=tambol.get(position).getTambol_ID();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }

    public void getListVillage(int position) {

        listVillage.clear();
        village.clear();
        VillageEntity villageEntity =new VillageEntity();
        villageEntity.setVillage_Name("กรุณาเลือกหมู่บ้าน");
        villageEntity.setVillage_ID(-99);
        village.add(villageEntity);

        int selectIndex = -99;

        village.addAll(dbHelper.getVillage(position));

        for (int i = 0; i < village.size(); i++) {
            listVillage.add(village.get(i).getVillage_Name());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                context,
                android.R.layout.simple_dropdown_item_1line,
                listVillage);
        spnVillage.setAdapter(adapter);

        spnVillage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                villageID=village.get(position).getVillage_ID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < village.size(); i++) {
            if (mFarmerItem.getVillageID() != null) {
                if (village.get(i).getVillage_ID() == mFarmerItem.getVillageID()) {
                    selectIndex = i;
                    spnVillage.setSelection(selectIndex);
                    break;
                }
            }
        }

        if(result_!=null) {
            if (result_.get("moo") != null ||! result_.get("moo").equals("")) {
                for (int i = 0; i < village.size(); i++) {
                    if (village.get(i).getVillage_Name().contains(result_.get("moo"))) {
                        spnVillage.setSelection(i);
                        break;
                    }
                }
            }
        }

        if (result_!=null){
            result_=null;
        }
    }

    public void getListCompany() {
        int selectIndex = -1;
        companies = dbHelper.getListCompany();
        int filter = 0;
        if (type==1){
            filter=1;
        }else if (type==2){
            filter=0;
        }

        for (int i = 0; i < companies.size(); i++) {
            if (companies.get(i).getIsCompany()==filter) {
                listCompanies.add(companies.get(i).getCompanyTypeName());
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                RegisterActivity.this,
                android.R.layout.simple_dropdown_item_1line,
                listCompanies);

        spnCompany.setAdapter(adapter);

        spnCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mFarmerItem != null) {
                    int id1 = companies.get(position).getId();
                    mFarmerItem.setCompanyTypeID(id1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < companies.size(); i++) {
            if (mFarmerItem != null && mFarmerItem.getCompanyTypeID() != null)
                if (companies.get(i).getId() == mFarmerItem.getCompanyTypeID()) {
                    selectIndex = i;
                    spnCompany.setSelection(selectIndex);
                    break;
                }
        }

    }

    private boolean checkValidPersonnalId(String userpId){

        if(userpId.length() != 13) return false;

        int sum=0;

        for(int i=0; i < 12; i++) {
            sum += Float.parseFloat(userpId.charAt(i)+"")*(13-i);
        }

        if((11-sum%11)%10!=Float.parseFloat(userpId.charAt(12)+"")) {
            return false;
        }

        return true;
    }

    DatabaseRealm databaseRealm;
    private void insertData(final ArrayList<RegisterFarmerItem> farmer) {
        Api api = AppUtils.getApiService();
        String s = String.valueOf(farmer);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        gsonBuilder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = gsonBuilder.create();

        String json = gson.toJson(farmer);
        progressBar.setVisibility(View.VISIBLE);
        Call<ResponseRegister> call = api.setRegister(json, 0);
        call.enqueue(new Callback<ResponseRegister>() {
            @Override
            public void onResponse(Call<ResponseRegister> call, Response<ResponseRegister> response) {

                if (response.body() != null) {

                    if (response.body().getResponseCode().equals(getString(R.string.success))) {

                        mFarmerItemImage.setFarmerID(response.body().getData().getFarmerID());
                        ArrayList<Farmer> farmers = new ArrayList<>();
                        farmers.add(mFarmerItemImage);
                        insertDataImage(farmers);


                        mFarmerItemImage.setOnline(true);
                        databaseRealm=new DatabaseRealm(RegisterActivity.this);
                        Realm realm = Realm.getDefaultInstance();
                        String uuid=databaseRealm.addDataFarmerUUID(mFarmerItemImage);

                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                        intent.putExtra(AppUtils.UUID,uuid);
                        intent.putExtra(AppUtils.TYPE,0);
                        intent.putExtra(AppUtils.SENDFROM,3); //TODO : Add chooser farmer/comp, Add case ?
                        startActivity(intent);
                        finish();
//                        Toast.makeText(context, response.body().getResponseMessage(), Toast.LENGTH_LONG).show();

                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(context, response.body().getResponseMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    try {
                        Toast.makeText(context, response.errorBody().string(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseRegister> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });

    }
    private void insertDataImage(final ArrayList<Farmer> farmer) {

        AppUtils.updateDataArray(farmer, new UpdateListener() {
            @Override
            public void onSuccess(String message, ResponseInsertFarmer responseInsertFarmer) {

                Toast.makeText(context, "อัพเดทข้อมูลสำเร็จ", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String message, ResponseInsertFarmer responseInsertFarmer) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(context, "อัพเดตข้อมูลไม่สำเร็จ", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void start() {
        ImagePicker imagePicker = ImagePicker.create(this)
                .single()
                .imageTitle("Tap to select"); // image selection title

        imagePicker.limit(1) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
                .imageFullDirectory(Environment.getExternalStorageDirectory().getPath()) // can be full path
                .origin(images) // original selected images, used in multi mode
                .start(RC_CODE_PICKER); // start image picker activity with request code
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }

        if(data!=null) {
            IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (scanResult != null) {
                String re = scanResult.getContents();
                txtUsername.setText(re);
            }
        }

        if (requestCode == REQUEST_LOCATION) {
            String latitude = "", longitude = "";
            if (data != null) {

                latitude = data.getStringExtra("lat") != null ? data.getStringExtra("lat") : "";
                longitude = data.getStringExtra("lng") != null ? data.getStringExtra("lng") : "";
//                address = data.getStringExtra("address");
            }
            txtLat.setText(latitude);
            txtLong.setText(longitude);
            mFarmerItem.setLatitude(latitude );
            mFarmerItem.setLongitude( longitude);

        } else if (requestCode == RC_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            images = (ArrayList<Image>) ImagePicker.getImages(data);

            if (images != null && !images.isEmpty()) {
                setValueImage(images.get(0));
            }
            return;
        }

         else if (requestCode == RC_CAMERA && resultCode == RESULT_OK) {
            getCameraModule().getImage(this, data, new OnImageReadyListener() {
                @Override
                public void onImageReady(List<Image> resultImages) {
                    images = (ArrayList<Image>) resultImages;

                        if (images != null && !images.isEmpty()) {
                            setValueImage(images.get(0));
                        }


                    Log.d("IMAGE CAMERA", "DONE");
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case RequestPermissionCodeLocation:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    if (StoragePermission) {
                        startActivityForResult(new Intent(context, MapActivity.class), AppUtils.REQUEST_LOCATION);
                    } else {
                        Toast.makeText(context, "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;

            case RC_CAMERA :
                if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    captureImage();
                }

                break;
            case WRITE_STORAGE :
                if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
        }
    }

    private void captureImage() {
        startActivityForResult(
                getCameraModule().getCameraIntent(RegisterActivity.this), RC_CAMERA);
    }

    private ImmediateCameraModule getCameraModule() {
        if (cameraModule == null) {
            cameraModule = new ImmediateCameraModule();
        }
        return (ImmediateCameraModule) cameraModule;
    }


    public void setValueImage(Image img) {
        Bitmap bitmap = null;

        bitmap = BitmapFactory.decodeFile(img.getPath()); // load

        Matrix matrix = new Matrix();
        matrix.postRotate(AppUtils.getImageOrientation(img.getPath()));
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                bitmap.getHeight(), matrix, true);

        String imgString;
        if (rotatedBitmap != null) {
            Bitmap bitmapShow = AppUtils.scaleBitmap(rotatedBitmap, 200, 200);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            imgProfile.setImageBitmap(bitmapShow);

            bitmapShow.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            byte[] profileImage = outputStream.toByteArray();

            imgString = Base64.encodeToString(profileImage,
                    Base64.NO_WRAP);
        } else {
            imgString = "";
        }
        mFarmerItemImage.setFarmerImage(imgString);
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("RegisterActivity","Permission is granted");
                return true;
            } else {

                Log.v("RegisterActivity","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_STORAGE);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("RegisterActivity","Permission is granted");
            return true;
        }
    }

    @Override
    public void onCompleteScan(HashMap<String,String> result) {
//        Log.e("moo: ", moo);
//        Log.e("tambol: ", tambol);
//        Log.e("amphur: ", amphur);
//        Log.e("province: ", province);
//        Log.e("prefix: ", prefix);
//        Log.e("bd: ", bd);
        result_=result;

        String bd=result.get("bd");
        int year=Integer.parseInt(bd.substring(0,4))-543;
        String month=bd.substring(4,6);
        String day=bd.substring(6,8);

        txtBD.setText( AppUtils.convertDateThai(day+"/"+month+"/"+year));

        for(int i=0;i<listPrefix.size();i++){
            if(listPrefix.get(i).equals(result.get("prefix"))){
                spnPrefix.setSelection(i);
            }
        }

//        for(int i=0;i<tambol.size();i++){
//            if(tambol.get(i).getTambol_NameTh().equals(result.get("tambol"))){
//                tambolID=tambol.get(i).getTambol_ID();
//            }
//        }
//
//        for(int i=0;i<amphur.size();i++){
//            if(amphur.get(i).getAmphur_NameTh().equals(result.get("amphur"))){
//                amphurID=amphur.get(i).getAmphur_ID();
//            }
//        }
        for(int i=0;i<province.size();i++){
            if(province.get(i).getProvince_NameTh().equals(result.get("province"))){
                provinceID=province.get(i).getProvince_ID();
                spnProvince.setSelection(i);
                break;
            }
        }

        txtUsername.setText(result.get("pid"));
        txtHomeNo.setText(result.get("soi"));
        txtNameTH.setText(result.get("firstname"));
        txtLastnameTH.setText(result.get("lastname"));
    }
}
