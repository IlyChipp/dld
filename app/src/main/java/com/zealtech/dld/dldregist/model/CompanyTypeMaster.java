package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 10/27/2017 AD.
 */
@Parcel
public class CompanyTypeMaster implements APIResponse {
    public static final String TABLE = "company";
    String responseCode;
    String responseMessage;
    ArrayList<CompanyEntity> data;

    public CompanyTypeMaster(){}

    @Override
    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<CompanyEntity> getData() {
        return data;
    }

    public void setData(ArrayList<CompanyEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class CompanyEntity{
        String companyTypeName;
        int id;
        int isCompany;

        public CompanyEntity(){ }

        public String getCompanyTypeName() {
            return companyTypeName;
        }

        public void setCompanyTypeName(String companyTypeName) {
            this.companyTypeName = companyTypeName;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsCompany() {
            return isCompany;
        }

        public void setIsCompany(int isCompany) {
            this.isCompany = isCompany;
        }
    }

    public class Column{
        public static final String _ID = BaseColumns._ID;
        public static final String COMPANY_TYPE_NAME = "companyTypeName";
        public static final String ID = "id";
        public static final String IS_COMPANY = "isCompany";

    }
}
