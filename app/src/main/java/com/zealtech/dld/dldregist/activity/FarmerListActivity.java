package com.zealtech.dld.dldregist.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.zealtech.dld.dldregist.MainApplication;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.manager.Contextor;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.SearchModel;
import com.zealtech.dld.dldregist.model.farmerList.AaData;
import com.zealtech.dld.dldregist.model.farmerList.DTRowData;
import com.zealtech.dld.dldregist.model.farmerList.FarmerSearchList;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseFarmer;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.util.EndlessRecyclerOnScrollListener;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Jirat on 10/25/2017.
 */

public class FarmerListActivity extends AppCompatActivity {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

    @BindView(R.id.txt_search)
    MyEditNoHLTextView txt_search;
    @BindView(R.id.layout_search)
    RelativeLayout layout_search;

    @BindView(R.id.progressBar)
    FrameLayoutDisable progressBar;
    @BindView(R.id.not_found)
    FrameLayout not_found;


    ArrayList<AaData> farmerSearchLists = new ArrayList<>();
    final private static String UUID = "uuid";

    //    ArrayList<Farmer> farmers=new ArrayList<>();
    Farmer farmer;
    Context context;
    DatabaseRealm databaseRealm;
    ListAdapter listFarmAdapter;

    SearchModel searchModel;
    int currentPage = 0;

    int mode;
    String from = "";
    int userType;
    boolean isSearch = false;

    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_list);
        ButterKnife.bind(this);
        context = this;

        MainApplication application = (MainApplication) getApplication();
        mTracker = application.getDefaultTracker();

        databaseRealm = new DatabaseRealm(this);

        farmerSearchLists = Parcels.unwrap(getIntent().getParcelableExtra("farmersList"));
        searchModel = Parcels.unwrap(getIntent().getParcelableExtra("searchModel"));
        mode = getIntent().getIntExtra("mode", 0);
        userType = getIntent().getIntExtra("userType", 0);

        if (userType == 1) {
            if (mode == 0) {
                layout_search.setVisibility(View.VISIBLE);

                txt_search.setOnEditorActionListener(new OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                        if (i == EditorInfo.IME_ACTION_SEARCH) {
                            search();
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(txt_search.getWindowToken(), 0);
                            return true;
                        }
                        return false;
                    }
                });

            } else {
                layout_search.setVisibility(View.GONE);
            }
        } else {
            layout_search.setVisibility(View.GONE);
        }
//        initList();
//        farmers.addAll(databaseRealm.getDataFarmerArray(true));

//        txt_search.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                search();
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
    }

    //    private String getAddress(int position){
//        return farmers.get(position).getAmphurID()+"";
//    }
    private void initList() {
        currentPage = 0;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(FarmerListActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(linearLayoutManager);
        listFarmAdapter = new ListAdapter();
        recyclerview.setAdapter(listFarmAdapter);
        recyclerview.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (current_page > currentPage) {
//                    if (currentPage < totalPage) {
                    currentPage += 20;
                    isSearch = false;
                    callListFarmer();
                }
            }
        });
    }


    @OnClick(R.id.btn_search)
    public void search() {
        isSearch = true;
        currentPage = 0;
        not_found.setVisibility(View.GONE);
        callListFarmer();
    }

    private void callListFarmer() {

        Integer proID = null;
        if (searchModel.getProvinnceId() != -99) {
            proID = searchModel.getProvinnceId();
        }

        Integer amID = null;
        if (searchModel.getAmphurId() != -99) {
            amID = searchModel.getAmphurId();
        }

        Integer tamID = null;
        if (searchModel.getTambolId() != -99) {
            tamID = searchModel.getTambolId();

        }

        if (userType == 1) {
            switch (mode) {
                case 0:
                    getListFarm(currentPage, 20, AppUtils.getStringFromEdt(txt_search.getText()), proID, amID
                            , tamID, null, userType, null, null, null);
                    break;
                case 1:
                    getListFarmer(currentPage, 20, null, null, null, null, null, 1, null, null, null
                            , null, searchModel.getFirstname(), searchModel.getLastname());
                    break;
                case 2:
                    String name = null;
                    if (txt_search.getText().length() != 0) {
                        name = txt_search.getText().toString();
                    }
                    getListFarm(currentPage, 20, name, proID, amID, tamID, null, 1, searchModel.getLat_(), searchModel.getLong_(), "5");
                    break;
                case 3:
                    getListFarmer(currentPage, 20, null, null, null, null, null, 1, null, null, null
                            , Long.toString(searchModel.getPid()), null, null);
                    break;
            }
        } else if (userType == 2) {
            switch (mode) {
                case 0:
                    getListFarmer(currentPage, 20, searchModel.getCompPid(), null, null, null, null, 2, null, null, null
                            ,null,null,null);
                    break;
                case 1:
                    getListFarmer(currentPage, 20, searchModel.getCompName(), null, null, null, null, 2, null, null, null
                            ,null,null,null);
                    break;
            }
        } else if (userType == 3) {
            getListFarmer(currentPage, 20, searchModel.getDeptName(), null, null, null, null, 3, null, null, null
                    ,null,null,null);
        }
    }

    private void getListFarmer(Integer iDisplayStart, Integer iDisplayLength, String sSearch, Integer ProvinceId
            , Integer AmphurId, Integer TambolId, Integer VillageId, Integer userType, String latitude
            , String longitude, String distanceKilometer,String Pid,String FirstName,String LastName) {

        progressBar.setVisibility(View.VISIBLE);
        Api api = AppUtils.getApiService();
        Call<FarmerSearchList> call = api.getListFarmer(iDisplayStart, iDisplayLength, sSearch, ProvinceId
                , AmphurId, TambolId, VillageId, userType, latitude, longitude, distanceKilometer,Pid,FirstName,LastName);
        call.enqueue(new Callback<FarmerSearchList>() {
            @Override
            public void onResponse(Call<FarmerSearchList> call, Response<FarmerSearchList> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
//                    if (response.body().getResponseCode().equals(getString(R.string.success))) {

//                        if(response.body().getAaData().size()!=0){
                    if (isSearch) {
                        View view = getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        if (response.body().getAaData().size() != 0) {
                            recyclerview.setVisibility(View.VISIBLE);
                            farmerSearchLists.clear();
                            farmerSearchLists.addAll(response.body().getAaData());
                            initList();
                        } else {
                            recyclerview.setVisibility(View.GONE);
                            not_found.setVisibility(View.VISIBLE);
                        }
                    } else {
                        if (farmerSearchLists.size() == 0) {
                            farmerSearchLists.addAll(response.body().getAaData());
                            initList();
                        } else {
                            recyclerview.setVisibility(View.VISIBLE);
                            farmerSearchLists.addAll(response.body().getAaData());
                            listFarmAdapter.notifyDataSetChanged();
                        }
                    }
//                        }

//                    } else {
//                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
//                    }
                } else {
                    Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<FarmerSearchList> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getListFarm(Integer iDisplayStart, Integer iDisplayLength, String sSearch, Integer ProvinceId
            , Integer AmphurId, Integer TambolId, Integer VillageId, Integer userType, String latitude
            , String longitude, String distanceKilometer) {

        progressBar.setVisibility(View.VISIBLE);
        Api api = AppUtils.getApiService();
        Call<FarmerSearchList> call = api.getListFarmD(iDisplayStart, iDisplayLength, sSearch, ProvinceId
                , AmphurId, TambolId, VillageId, userType, latitude, longitude, distanceKilometer);
        call.enqueue(new Callback<FarmerSearchList>() {
            @Override
            public void onResponse(Call<FarmerSearchList> call, Response<FarmerSearchList> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
//                    if (response.body().getResponseCode().equals(getString(R.string.success))) {

//                        if(response.body().getAaData().size()!=0){
                    if (isSearch) {
                        View view = getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        if (response.body().getAaData().size() != 0) {
                            recyclerview.setVisibility(View.VISIBLE);
                            farmerSearchLists.clear();
                            farmerSearchLists.addAll(response.body().getAaData());
                            initList();
                        } else {
                            recyclerview.setVisibility(View.GONE);
                            not_found.setVisibility(View.VISIBLE);
                        }
                    } else {
                        if (farmerSearchLists.size() == 0) {
                            farmerSearchLists.addAll(response.body().getAaData());
                            initList();
                        } else {
                            recyclerview.setVisibility(View.VISIBLE);
                            farmerSearchLists.addAll(response.body().getAaData());
                            listFarmAdapter.notifyDataSetChanged();
                        }
                    }
//                        }

//                    } else {
//                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
//                    }
                } else {
                    Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<FarmerSearchList> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getFarmerByPID(long idcard, final int btn, final int farmId) {
        progressBar.setVisibility(View.VISIBLE);
        Api api = AppUtils.getApiService();
        Call<ResponseFarmer> call = api.getFarmerByPID(idcard);
        call.enqueue(new Callback<ResponseFarmer>() {
            @Override
            public void onResponse(Call<ResponseFarmer> call, Response<ResponseFarmer> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getResponseCode().equals(getString(R.string.success))) {

                        databaseRealm.deleteDataFarmerOnline();
                        Farmer data = response.body().getData();
//                        AppUtils.addDefaultToNull(data);
                        data.setOnline(true);
                        String uuid = databaseRealm.addDataFarmerUUID(data);

                        gotoActivityByBtn(btn, uuid, farmId);

                    } else {
                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseFarmer> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void gotoActivityByBtn(int btn, String uuid, int farmId) {
        if (btn == 1) {
            if (userType == 1 && (mode == 1 || mode == 3)) { //case listfarmer
                Intent intent = new Intent(FarmerListActivity.this, FarmActivity.class); //mode DashBoard > list farm
                intent.putExtra(UUID, uuid);
                intent.putExtra("mode", "dashBoard");
                intent.putExtra("farmId", farmId);
                startActivity(intent);
            } else { //case listfarm (area)
                Intent intent = new Intent(FarmerListActivity.this, MainActivity.class);
                intent.putExtra(UUID, uuid);
                intent.putExtra(AppUtils.SENDFROM,5); //case update
                startActivity(intent);
            }
        } else if (btn == 2) {
            Intent intent = new Intent(FarmerListActivity.this, FarmActivity.class); //mode MainFarm To FarmData
            intent.putExtra(UUID, uuid);
            intent.putExtra("mode", "farm");
            intent.putExtra("farmId", farmId);
            startActivity(intent);
        } else if (btn == 3) {
            Intent intent = new Intent(FarmerListActivity.this, FarmActivity.class); //mode MainFarm To Livestock
            intent.putExtra(UUID, uuid);
            intent.putExtra("mode", "live");
            intent.putExtra("farmId", farmId);
            startActivity(intent);
        }
    }

    private void getFarmerData(long pid, int farmerID, int btn, final int farmId) {

//        Farmer farmer=new Farmer();
//        farmer.setBirthDay("Jun 20, 1968 12:00:00 AM");
//        AppUtils.addDefaultToNull(farmer);

        if (databaseRealm.getDataFarmerArray(true).size() != 0) {

            if (databaseRealm.isFarmerExist(farmerID)) { //use old
                String uuid = databaseRealm.getListDataFarmer(true).get(0).getUuid();
                gotoActivityByBtn(btn, uuid, farmId);
            } else { //case choose another farmer > load new
                getFarmerByPID(pid, btn, farmId);
            }
        } else { //case first time > load new
            getFarmerByPID(pid, btn, farmId);
        }

    }

    public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_farmer, parent, false);

            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {

            int index = position + 1;

            holder.txt_order.setText("ลำดับที่ " + index);
            holder.txt_name.setText(farmerSearchLists.get(position).get_1());
            holder.txt_address.setText(farmerSearchLists.get(position).get_2());
            holder.txt_date_edit.setText(farmerSearchLists.get(position).get_3());
            holder.txt_date_exp.setText(farmerSearchLists.get(position).get_4());
            holder.txt_status.setText(farmerSearchLists.get(position).get_5());

            final DTRowData dtRowData = farmerSearchLists.get(position).getdTRowData();

            if (userType == 1 && (mode == 1 || mode == 3)) { //case listfarmer, to dashboard
                holder.btn_livestock.setVisibility(View.GONE);
                holder.btn_farm.setVisibility(View.GONE);
                holder.btn_farmer.setText("ข้อมูลเกษตรกร");
            } else { //case listfarm , to farm
                holder.btn_livestock.setVisibility(View.VISIBLE);
                holder.btn_farm.setVisibility(View.VISIBLE);
                holder.btn_farmer.setText(getString(R.string.farmerData));
            }

            if (dtRowData.getFarmId() != null) {

            }

            holder.btn_farmer.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (dtRowData.getFarmId() != null) {
                        getFarmerData(dtRowData.getPid(), dtRowData.getFarmerId(), 1, dtRowData.getFarmId());
                    } else {
                        getFarmerData(dtRowData.getPid(), dtRowData.getFarmerId(), 1, 0);
                    }
                }
            });

            holder.btn_farm.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFarmerData(dtRowData.getPid(), dtRowData.getFarmerId(), 2, dtRowData.getFarmId());
                }
            });

            holder.btn_livestock.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFarmerData(dtRowData.getPid(), dtRowData.getFarmerId(), 3, dtRowData.getFarmId());
                }
            });

            if(userType == 1 && (mode == 1 || mode == 3)){
                holder.txt_add_title.setText("ที่อยู่ตามทะเบียนบ้าน");
                holder.layout_status.setVisibility(View.GONE);
            }else if (userType == 2||userType==3){
                holder.txt_add_title.setText("ที่อยู่ตามทะเบียนบ้าน");
                holder.layout_status.setVisibility(View.GONE);
            }else{
                holder.txt_add_title.setText(getResources().getString(R.string.farm_lo));
                holder.layout_status.setVisibility(View.VISIBLE);
            }

        }

        @Override
        public int getItemCount() {
            return farmerSearchLists.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private final MyNoHLTextView txt_order;
            private final MyNoHBTextView txt_name;
            private final MyNoHBTextView txt_address;
            private final MyNoHBTextView txt_date_edit;
            private final MyNoHBTextView txt_date_exp;
            private final MyNoHBTextView txt_status;

            private final MyNoHBTextView btn_farm;
            private final MyNoHBTextView btn_farmer;
            private final MyNoHBTextView btn_livestock;
            private final MyNoHBTextView txt_add_title;

            LinearLayout layout_status;

            public ViewHolder(View itemView) {
                super(itemView);

                txt_name = (MyNoHBTextView) itemView.findViewById(R.id.txt_name);
                txt_address = (MyNoHBTextView) itemView.findViewById(R.id.txt_address);
                txt_date_edit = (MyNoHBTextView) itemView.findViewById(R.id.txt_date_edit);
                txt_date_exp = (MyNoHBTextView) itemView.findViewById(R.id.txt_date_exp);
                txt_status = (MyNoHBTextView) itemView.findViewById(R.id.txt_status);
                txt_order = (MyNoHLTextView) itemView.findViewById(R.id.txt_order);

                txt_add_title = (MyNoHBTextView) itemView.findViewById(R.id.txt_add_title);
                layout_status = (LinearLayout) itemView.findViewById(R.id.layout_status);

                btn_farm = (MyNoHBTextView) itemView.findViewById(R.id.btn_farm);
                btn_farmer = (MyNoHBTextView) itemView.findViewById(R.id.btn_farmer);
                btn_livestock = (MyNoHBTextView) itemView.findViewById(R.id.btn_livestock);

            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        currentPage = 0;
        farmerSearchLists.clear();
//        listFarmAdapter= new ListAdapter();
        callListFarmer();

            mTracker.setScreenName("หน้ารายชื่อเกษตรกร");
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

        final int userType = AppUtils.getUserType(FarmerListActivity.this);

        if (userType == 2) {
            new AlertDialog.Builder(FarmerListActivity.this).setTitle(getResources().getString(R.string.alert))
                    .setMessage(getResources().getString(R.string.alert_logout))
                    .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            MyPreferenceManager myPreferenceManager = new MyPreferenceManager(FarmerListActivity.this);
                            myPreferenceManager.setUserId(0);
                            myPreferenceManager.setUserPid(0);
                            Intent intent = new Intent(FarmerListActivity.this, LoginActivity.class);
                            intent.putExtra(AppUtils.LOGINTYPE,myPreferenceManager.getLoginType());
                            startActivity(intent);
                            finish();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .show();
        } else {
            super.onBackPressed();
        }
    }

    @Optional
    @OnClick(R.id.btn_back)
    public void clickBtnBack() {

        final int userType = AppUtils.getUserType(FarmerListActivity.this);

        if (userType == 2) {
            new AlertDialog.Builder(FarmerListActivity.this).setTitle(getResources().getString(R.string.alert))
                    .setMessage(getResources().getString(R.string.alert_logout))
                    .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            MyPreferenceManager myPreferenceManager = new MyPreferenceManager(FarmerListActivity.this);
                            myPreferenceManager.setUserId(0);
                            myPreferenceManager.setUserPid(0);
                            Intent intent = new Intent(FarmerListActivity.this, LoginActivity.class);
                            intent.putExtra(AppUtils.LOGINTYPE,myPreferenceManager.getLoginType());
                            startActivity(intent);
                            finish();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .show();
        } else {
            finish();
        }

    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode ==  AppUtils.RequestFarmList) {
            if (data != null) {

            }

        }
    }*/
}
