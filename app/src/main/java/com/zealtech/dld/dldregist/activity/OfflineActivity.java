package com.zealtech.dld.dldregist.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.zealtech.dld.dldregist.MainApplication;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.newFarmer.FarmImage;
import com.zealtech.dld.dldregist.model.newFarmer.FarmStandard;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.Feed;
import com.zealtech.dld.dldregist.model.newFarmer.Livestock;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseInsertFarmer;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.util.UpdateListener;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class OfflineActivity extends AppCompatActivity {

    final private static String FARMERINFO = "farmerInfo";
    final private static String UUID = "uuid";

    private LinearLayout btnAddFarmer;
    private LinearLayout llListFarmer;
    private RecyclerView listFarm;
    ArrayList<Farmer> farmers = new ArrayList<>();
    DBHelper dbHelper;
    DatabaseRealm databaseRealm;
    FrameLayoutDisable progressBar;
    Context context;
    Realm realm;
    private LinearLayout llTopic;
    private MyNoHBTextView btnSubmit,txtNav;

    Tracker mTracker;
    boolean online;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline);
        View view = getLayoutInflater().inflate(R.layout.item_list_farmer_off, null);
        context=this;
        ButterKnife.bind(this);
        ButterKnife.bind(this, view);
        init(view);

        MainApplication application = (MainApplication) getApplication();
        mTracker = application.getDefaultTracker();

        dbHelper = new DBHelper(this);

         online = AppUtils.isOnline(this);
        getListFarmer();


        if (online==true){
            btnSubmit.setVisibility(View.VISIBLE);
            llTopic.setVisibility(View.GONE);
            txtNav.setText("ส่งข้อมูลทะเบียนเกษตร");


        }else {
            btnSubmit.setVisibility(View.GONE);
            llTopic.setVisibility(View.VISIBLE);
            txtNav.setText(getString(R.string.nav_offline));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("หน้ากรอกข้อมูลออฟไลน์");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void init(View view) {
//        btnManage = (MyNoHLTextView) views.findViewById(R.id.btnManage);
//        btnUpdate = (MyNoHLTextView) views.findViewById(R.id.btnUpdate);
        llTopic= (LinearLayout) findViewById(R.id.llTopic);
        txtNav= (MyNoHBTextView) findViewById(R.id.txtNav);
        btnAddFarmer = (LinearLayout) findViewById(R.id.btnAddFarmer);
        llListFarmer = (LinearLayout) findViewById(R.id.llListFarmer);
        listFarm = (RecyclerView) findViewById(R.id.listFarm);
        progressBar = (FrameLayoutDisable) findViewById(R.id.progressBar);
        btnSubmit= (MyNoHBTextView) findViewById(R.id.btnSubmit);
    }

    @Optional
    @OnClick(R.id.btnManage)
    public void clickBtnManageData() {
        Intent intent = new Intent(OfflineActivity.this, FindFarmerActivity.class);
        startActivity(intent);
        finish();
    }
    @Optional
    @OnClick(R.id.btnSubmit)
    public void clickBtnSubmitAll() {
        int userId = AppUtils.getUserId(OfflineActivity.this);
        if (userId!=0){

            ArrayList<Farmer> dataFarmerArray = databaseRealm.getDataFarmerArray(false);

            for (int i = 0;i<dataFarmerArray.size();i++){
                Farmer farmer = dataFarmerArray.get(i);
                farmer.setCreateBy(String.valueOf(userId));
                farmer.setUpdateBy(String.valueOf(userId));
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date = new Date();
                farmer.setCreateDate(dateFormat.format(date));
                farmer.setUpdateDate(dateFormat.format(date));
                farmer.setStatusID(2);

                for (int j=0;j<farmer.getFarm().size();j++){
                    farmer.getFarm().get(j).setCreateBy(String.valueOf(userId));
                    farmer.getFarm().get(j).setUpdateBy(String.valueOf(userId));
                    farmer.getFarm().get(j).setCreateDate(dateFormat.format(date));
                    farmer.getFarm().get(j).setUpdateDate(dateFormat.format(date));
                    farmer.getFarm().get(j).setStatusID(1);
//                    farmer.getFarm().get(j).setFarmID(0);

                    if ( farmer.getFarm().get(j).getWorker()!=null){
                    farmer.getFarm().get(j).getWorker().setStatus_ID(5);
                    }

                    RealmList<FarmStandard> farm_standard = farmer.getFarm().get(j).getFarmStandard();
                    for (int k=0;k<farm_standard.size();k++){
                        farm_standard.get(k).setStatusID(1);
                    }

                    RealmList<FarmImage> farmImage = farmer.getFarm().get(j).getFarmImage();
                    for (int l=0;l<farmImage.size();l++){
                        farmImage.get(l).setStatus_ID(5);
                    }

                    RealmList<Feed> feed = farmer.getFarm().get(j).getFeed();
                    for (int m=0;m<feed.size();m++){
                        feed.get(m).setStatusID(1);
                    }

                    RealmList<Livestock> livestock = farmer.getFarm().get(j).getLivestock();
                    for (int n=0;n<livestock.size();n++){
                        livestock.get(n).setStatus_ID(1);
                    }
                }

            }

            AppUtils.updateDataArray(dataFarmerArray, new UpdateListener() {
                @Override
                public void onSuccess(String message, ResponseInsertFarmer responseInsertFarmer) {
                    //delete all
                    databaseRealm.deleteDataFarmerOffline();
                    Toast.makeText(OfflineActivity.this, "อัพเดทข้อมูลสำเร็จ", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(String message, ResponseInsertFarmer responseInsertFarmer) {
                    Toast.makeText(OfflineActivity.this, "อัพเดตข้อมูลไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                }
            });

                        /*notifyDataSetChanged();*/
        }else {
            Intent intent = new Intent(context, LoginSendDataActivity.class);
            startActivity(intent);

        }


       /* int userId = AppUtils.getUserId(OfflineActivity.this);
        for (int i=0;i<farmers.size();i++){
            farmers.get(i).setCreateBy(String.valueOf(userId));
            farmers.get(i).setUpdateBy(String.valueOf(userId));
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            farmers.get(i).setCreateDate(dateFormat.format(date));
            farmers.get(i).setUpdateDate(dateFormat.format(date));
            ArrayList<Farmer>newFarmers = new ArrayList<Farmer>();
            newFarmers.add( farmers.get(i));
            insertData(newFarmers,i);


        }

        farmers.removeAll();
        ListAdapter listAdapter = new ListAdapter();
        listFarm.setAdapter(listAdapter);
*/


    }


    @Optional
    @OnClick(R.id.btn_back)
    public void clickBtnBack() {
        finish();
    }


    @Optional
    @OnClick(R.id.btnAddFarmer)
    public void clickBtnAddFarmer() {
        Intent intent = new Intent(OfflineActivity.this, MainActivity.class);
        intent.putExtra(AppUtils.TYPE,0);
        intent.putExtra(AppUtils.SENDFROM,0);
        startActivity(intent);
        finish();
    }

    @Optional
    @OnClick(R.id.btnUpdate)
    public void clickBtnUpdate() {
        Intent intent = new Intent(OfflineActivity.this, LoginSendDataActivity.class);
        startActivity(intent);
        finish();
    }

    public void getListFarmer() {

        progressBar.setVisibility(View.VISIBLE);

        databaseRealm=new DatabaseRealm(this);
        realm = Realm.getDefaultInstance();
        RealmResults<Farmer> dataFarmer = databaseRealm.getListDataFarmer(false);
        farmers.addAll(realm.copyFromRealm(dataFarmer));


        listFarm.setLayoutManager(new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false));
        ListAdapter listAdapter = new ListAdapter();
        listFarm.setAdapter(listAdapter);
        progressBar.setVisibility(View.GONE);
 /*       if (dbHelper.getListFarmer() != null) {
            for (int i = 0; i < dbHelper.getListFarmer().size(); i++) {
                farmers.add(dbHelper.getListFarmer().get(i));
            }
            listFarm.setLayoutManager(new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false));
            ListAdapter listAdapter = new ListAdapter();
            listFarm.setAdapter(listAdapter);

        }*/
    }

    public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_farmer_off, parent, false);

            return new ViewHolder(v, context);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            final Farmer item = farmers.get(position);
            item.setFarmerID(0);
            item.setStatusID(2);

            holder.txtFarmerName.setText(item.getFirstName()+" "+item.getLastName());
            holder.btnManage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(OfflineActivity.this, MainActivity.class);
                    intent.putExtra(UUID,item.getUuid());
                    startActivity(intent);
                }
            });

            holder.btnManageFarm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(OfflineActivity.this, FarmActivity.class); //mode DashBoard > list farm
                    intent.putExtra(UUID,item.getUuid());
                    intent.putExtra("mode", "dashBoard");
                    intent.putExtra("farmId", 0);
                    startActivity(intent);

                }
            });
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    new AlertDialog.Builder(context).setTitle(getResources().getString(R.string.alert))
                            .setMessage(getResources().getString(R.string.alert_delete))
                            .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    databaseRealm = new DatabaseRealm(context);
                                    databaseRealm.deleteDataFarmer(farmers.get(0));

                                    databaseRealm=new DatabaseRealm(context);


                                    farmers.remove(position);
                                    ListAdapter listAdapter = new ListAdapter();
                                    listFarm.setAdapter(listAdapter);

                                    Toast.makeText(context, "ลบข้อมูลสำเร็จ", Toast.LENGTH_LONG).show();
                                }
                            })
                            .setNegativeButton(getResources().getString(R.string.no),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                            .show();
                }
            });


            holder.btnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (AppUtils.getUserId(OfflineActivity.this)!=0){
                        int userId = AppUtils.getUserId(OfflineActivity.this);
                        item.setCreateBy(String.valueOf(userId));
                        item.setUpdateBy(String.valueOf(userId));
                        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        Date date = new Date();
                        item.setCreateDate(dateFormat.format(date));
                        item.setUpdateDate(dateFormat.format(date));
                        item.setStatusID(2);

                        for (int i=0;i<item.getFarm().size();i++){
                            item.getFarm().get(i).setCreateBy(String.valueOf(userId));
                            item.getFarm().get(i).setUpdateBy(String.valueOf(userId));
                            item.getFarm().get(i).setCreateDate(dateFormat.format(date));
                            item.getFarm().get(i).setUpdateDate(dateFormat.format(date));
                            item.getFarm().get(i).setStatusID(2);
//                            item.getFarm().get(i).setFarmID(0);



                            if ( item.getFarm().get(i).getWorker()!=null){
                                item.getFarm().get(i).getWorker().setStatus_ID(5);
                            }

                            RealmList<FarmStandard> farm_standard = item.getFarm().get(i).getFarmStandard();
                            for (int k=0;k<farm_standard.size();k++){
                                farm_standard.get(k).setStatusID(1);
                            }

                            RealmList<FarmImage> farmImage = item.getFarm().get(i).getFarmImage();
                            for (int l=0;l<farmImage.size();l++){
                                farmImage.get(l).setStatus_ID(5);
                            }

                            RealmList<Feed> feed = item.getFarm().get(i).getFeed();
                            for (int m=0;m<feed.size();m++){
                                feed.get(m).setStatusID(1);
                            }

                            RealmList<Livestock> livestock = item.getFarm().get(i).getLivestock();
                            for (int n=0;n<livestock.size();n++){
                                livestock.get(n).setStatus_ID(5);
                            }
                        }
                        ArrayList<Farmer>farmers = new ArrayList<Farmer>();
                        farmers.add(item);
                        insertData(farmers,position);

                        /*notifyDataSetChanged();*/
                    }else {
                        Intent intent = new Intent(context, LoginSendDataActivity.class);
                        startActivity(intent);
                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return farmers.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private MyNoHBTextView txtFarmerName;
            private MyNoHLTextView btnManage,btnManageFarm;
            private MyNoHLTextView btnUpdate,delete;

            public ViewHolder(View itemView,Context context) {
                super(itemView);
                txtFarmerName = (MyNoHBTextView) itemView.findViewById(R.id.txtFarmerName);
                btnManage = (MyNoHLTextView) itemView.findViewById(R.id.btnManage);
                btnUpdate = (MyNoHLTextView) itemView.findViewById(R.id.btnUpdate);
                btnManageFarm= (MyNoHLTextView) itemView.findViewById(R.id.btnManageFarm);
                delete= (MyNoHLTextView) itemView.findViewById(R.id.delete);

                if (online==true){
                    btnManage.setVisibility(View.GONE);
                    btnUpdate.setVisibility(View.VISIBLE);
                    btnManageFarm.setVisibility(View.GONE);
                }else {
                    btnManage.setVisibility(View.VISIBLE);
                    btnUpdate.setVisibility(View.GONE);
                    btnManageFarm.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void insertData(final ArrayList<Farmer> farmer, final int position) {
//        Api api = AppUtils.getApiService();
//        String s = String.valueOf(farmer);
//        GsonBuilder gsonBuilder = new GsonBuilder();
//        gsonBuilder.serializeNulls();
//        gsonBuilder.excludeFieldsWithoutExposeAnnotation();
//        Gson gson = gsonBuilder.create();
//
//        String json = gson.toJson(farmer);
//        progressBar.setVisibility(View.VISIBLE);
//        Call<ResponseInsertFarmer> call = api.setInsertData(json);
//        call.enqueue(new Callback<ResponseInsertFarmer>() {
//            @Override
//            public void onResponse(Call<ResponseInsertFarmer> call, Response<ResponseInsertFarmer> response) {
//                progressBar.setVisibility(View.GONE);
//                if (response.body() != null) {
//
//                    if (response.body().getResponseCode().equals(getString(R.string.success))) {
//                        databaseRealm = new DatabaseRealm(context);
//                        databaseRealm.deleteDataFarmer(farmer.get(0));
//
//                        databaseRealm=new DatabaseRealm(context);
//
//                        /*realm = Realm.getDefaultInstance();
//                        RealmResults<Farmer> dataFarmer = databaseRealm.getListDataFarmer(false);
//                        farmers.addAll(realm.copyFromRealm(dataFarmer));*/
//                        farmers.remove(position);
//                        ListAdapter listAdapter = new ListAdapter();
//                        listFarm.setAdapter(listAdapter);
//
//                        Toast.makeText(context, response.body().getResponseMessage(), Toast.LENGTH_LONG).show();
//
//                    } else {
//                        Toast.makeText(context, response.body().getResponseMessage(), Toast.LENGTH_LONG).show();
//                    }
//
//                } else {
//                    try {
//                        Toast.makeText(context, response.errorBody().string(), Toast.LENGTH_LONG).show();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseInsertFarmer> call, Throwable t) {
//                progressBar.setVisibility(View.GONE);
//                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//
//        });
        AppUtils.updateDataArray(farmer, new UpdateListener() {
            @Override
            public void onSuccess(String message, ResponseInsertFarmer responseInsertFarmer) {
                progressBar.setVisibility(View.GONE);

                        databaseRealm = new DatabaseRealm(context);
                        databaseRealm.deleteDataFarmer(farmer.get(0));

                        databaseRealm=new DatabaseRealm(context);

                        /*realm = Realm.getDefaultInstance();
                        RealmResults<Farmer> dataFarmer = databaseRealm.getListDataFarmer(false);
                        farmers.addAll(realm.copyFromRealm(dataFarmer));*/
                        farmers.remove(position);
                        ListAdapter listAdapter = new ListAdapter();
                        listFarm.setAdapter(listAdapter);

                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(String message, ResponseInsertFarmer responseInsertFarmer) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        MyPreferenceManager myPreferenceManager = new MyPreferenceManager(OfflineActivity.this);
        intent.putExtra(AppUtils.LOGINTYPE,myPreferenceManager.getLoginType());
        startActivity(intent);
    }

}
