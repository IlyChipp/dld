package com.zealtech.dld.dldregist.model.newFarmer;

import java.util.ArrayList;

/**
 * Created by Windows 8.1 on 26/10/2560.
 */

public class ResponseInsertFarmer {
    private String responseCode;
    private String responseMessage;
    private ArrayList<FarmerList> farmerList;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<FarmerList> getFarmerList() {
        return farmerList;
    }

    public void setFarmerList(ArrayList<FarmerList> farmerList) {
        this.farmerList = farmerList;
    }
}
