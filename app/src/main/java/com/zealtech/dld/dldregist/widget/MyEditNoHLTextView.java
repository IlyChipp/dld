package com.zealtech.dld.dldregist.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.WindowManager;

import com.zealtech.dld.dldregist.R;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by fanqfang on 9/8/2017 AD.
 */

public class MyEditNoHLTextView extends android.support.v7.widget.AppCompatEditText{
    public MyEditNoHLTextView(Context context) {
        super(context);
        init();
    }

    public MyEditNoHLTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyEditNoHLTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
//        setTextSize(getResources().getDimension(R.dimen.font_edit_text));
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), getResources().getString(R.string.font_en_light));
        setTypeface(tf, 1);
        setGravity(Gravity.CENTER_VERTICAL);


        WindowManager wm = (WindowManager) getContext().getSystemService(WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);

//        if (AppUtils.isTablet(getContext())){
//
//                setTextSize((int) ((metrics.widthPixels * 23)/1080));
//
//            setPadding((int)getResources().getDimension(R.dimen.txt_padding_l),0
//                    ,(int)getResources().getDimension(R.dimen.txt_padding_l),0);
//        }else {
//            if (metrics.widthPixels==1080){
//                setTextSize((int) ((metrics.widthPixels * 15)/1080));
//            }else if (metrics.widthPixels==768){
//                setTextSize((int) ((metrics.widthPixels * 20)/1080));
//            }else if (metrics.widthPixels==1440){
//                setTextSize((int) ((metrics.widthPixels * 12)/1080));
//
//            }else {
//                setTextSize((int) ((metrics.widthPixels * 30)/1080));
//            }

            final float scale = getContext().getResources().getDisplayMetrics().density;
            int pixels = (int) (getResources().getDimension(R.dimen.txt_padding_s) * scale + 0.5f);

            setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.txt_s));

            setPadding(pixels,0,pixels,0);
//        }

     /*  if (AppUtils.isTablet(getContext())){
           setTextSize(getResources().getDimension(R.dimen.txt_edit));
           setPadding((int)getResources().getDimension(R.dimen.txt_padding_l), 0
                   ,(int)getResources().getDimension(R.dimen.txt_padding_l),0);
        }else {
           setTextSize(getResources().getDimension(R.dimen.txt_sss));
           setPadding((int)getResources().getDimension(R.dimen.txt_padding_l), (int)getResources().getDimension(R.dimen.txt_padding)
                   ,(int)getResources().getDimension(R.dimen.txt_padding_l),(int)getResources().getDimension(R.dimen.txt_padding));
       }*/
    }
}
