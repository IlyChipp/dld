/*
 * Copyright (C) 2011-2013 Advanced Card Systems Ltd. All Rights Reserved.
 * 
 * This software is the confidential and proprietary information of Advanced
 * Card Systems Ltd. ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with the terms
 * of the license agreement you entered into with ACS.
 */

package com.zealtech.dld.dldregist.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.acs.smartcard.Features;
import com.acs.smartcard.Reader;
import com.acs.smartcard.Reader.OnStateChangeListener;
import com.acs.smartcard.ReaderException;
import com.zealtech.dld.dldregist.listener.PIDScannerListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Test program for ACS smart card readers.
 * 
 * @author Godfrey Chung
 * @version 1.1.1, 16 Apr 2013
 */
public class PIDScanActivity {

    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

    private static final String CMD_CARD_ISSUE_EXPIRE = "80b00167020012";
    private static final String CMD_CID = "80b0000402000d";
    private static final String CMD_ADDRESS = "80b01579020064";
    private static final String CMD_PERSON_INFO = "80b000110200E0";
    private static final String CMD_IMG_1 = "80B0017B0200FF";
    private static final String CMD_IMG_2 = "80B0027A0200FF";
    private static final String CMD_IMG_3 = "80B003790200FF";
    private static final String CMD_IMG_4 = "80B004780200FF";
    private static final String CMD_IMG_5 = "80B005770200FF";
    private static final String CMD_IMG_6 = "80B006760200FF";
    private static final String CMD_IMG_7 = "80B007750200FF";
    private static final String CMD_IMG_8 = "80B008740200FF";
    private static final String CMD_IMG_9 = "80B009730200FF";
    private static final String CMD_IMG_10 = "80B00A720200FF";
    private static final String CMD_IMG_11 = "80B00B710200FF";
    private static final String CMD_IMG_12 = "80B00C700200FF";
    private static final String CMD_IMG_13 = "80B00D6F0200FF";
    private static final String CMD_IMG_14 = "80B00E6E0200FF";
    private static final String CMD_IMG_15 = "80B00F6D0200FF";
    private static final String CMD_IMG_16 = "80B0106C0200FF";
    private static final String CMD_IMG_17 = "80B0116B0200FF";
    private static final String CMD_IMG_18 = "80B0126A0200FF";
    private static final String CMD_IMG_19 = "80B013690200FF";
    private static final String CMD_IMG_20 = "80B014680200FF";

    String[] action= {CMD_CID,CMD_ADDRESS,CMD_PERSON_INFO,CMD_IMG_1,CMD_IMG_2,CMD_IMG_3,CMD_IMG_4
            ,CMD_IMG_5,CMD_IMG_6,CMD_IMG_7,CMD_IMG_8,CMD_IMG_9,CMD_IMG_10,CMD_IMG_11,CMD_IMG_12
            ,CMD_IMG_13,CMD_IMG_14,CMD_IMG_15,CMD_IMG_16,CMD_IMG_17,CMD_IMG_18,CMD_IMG_19,CMD_IMG_20};

    private UsbManager mManager;
    private PendingIntent mPermissionIntent;

    private static Reader mReader;
    static byte[] respAPDU = null;
    static byte[] respAPDU2 = null;
    static byte[] byteAPDU2 = null;

    private Features mFeatures = new Features();

    private String deviceName="Please Reconnect Device";
    private int slotNum=0;

    private Activity activity;
    private boolean isOpened=false;
    private boolean isReading=false;
    private boolean isSetInfo=false;

//    String result="";
    String pid="";
    String address="";
    String info="";
//    String imgPic="";

    PIDScannerListener pidScannerListener;
    ProgressDialog progressDialog;

    String soi,moo,tambol,amphur,province,prefix,firstname,lastname,bd;
    ImageView imageView;

    byte[] byteArray;
//    byte[] byteArray2;

//    byte[] img1;
//    byte[] img2;
//    byte[] img3;
//    byte[] img4;
//    byte[] img5;
//    byte[] img6;
//    byte[] img7;
//    byte[] img8;
//    byte[] img9;
//    byte[] img10;
//    byte[] img11;
//    byte[] img12;
//    byte[] img13;
//    byte[] img14;
//    byte[] img15;
//    byte[] img16;
//    byte[] img17;
//    byte[] img18;
//    byte[] img19;
//    byte[] img20;

    public PIDScanActivity() {

    }

    public PIDScanActivity(Activity activity, PIDScannerListener pidScannerListener, ImageView imageView) {
        this.activity = activity;
        this.pidScannerListener=pidScannerListener;
        this.imageView=imageView;
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            if (ACTION_USB_PERMISSION.equals(action)) {

                synchronized (this) {

                    UsbDevice device = (UsbDevice) intent
                            .getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            new OpenTask().execute(device);
                        }
                    }
                }

            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {

                synchronized (this) {

                    // Update reader list
                    for (UsbDevice device : mManager.getDeviceList().values()) {
                        if (mReader.isSupported(device)) {
                            deviceName=device.getDeviceName();
                        }
                    }

                    UsbDevice device = (UsbDevice) intent
                            .getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (device != null && device.equals(mReader.getDevice())) {
                        new CloseTask().execute();
                    }
                }
            }
        }
    };

    private static byte[] atohex(String data) {
        String hexchars = "0123456789abcdef";
        data = data.replaceAll(" ", "").toLowerCase();
        if (data == null) {
            return null;
        }
        byte[] hex = new byte[(data.length() / 2)];
        for (int ii = 0; ii < data.length(); ii += 2) {
            int i1 = hexchars.indexOf(data.charAt(ii));
            hex[ii / 2] = (byte) ((i1 << 4) | hexchars.indexOf(data.charAt(ii + 1)));
        }
        return hex;
    }

    private class OpenTask extends AsyncTask<UsbDevice, Void, Exception> {

        @Override
        protected Exception doInBackground(UsbDevice... params) {
            Exception result = null;
            try {
                mReader.open(params[0]);
            } catch (Exception e) {
                result = e;
            }

            return result;
        }

        @Override
        protected void onPostExecute(Exception result) {
            if(!isOpened) {
                isOpened=true;
//                Toast.makeText(activity, "Open Complete", Toast.LENGTH_LONG).show();
                if (result != null) {

                } else {
                    mFeatures.clear();
//                    readCard();
                    progressDialog=new ProgressDialog(activity);
                    progressDialog.setMessage("กำลังอ่านข้อมูล...");
                    progressDialog.show();
                   new ReadTask().execute();
                }
            }
        }
    }

    private class CloseTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            mReader.close();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }

    }

    private class ReadTask extends AsyncTask<Void, String, String> {


        @Override
        protected String doInBackground(Void... params) {

            return readCard();
        }

        @Override
        protected void onPostExecute(String result) {

            progressDialog.dismiss();

//            Toast.makeText(activity,result,Toast.LENGTH_LONG).show();
            onDestroyActivity();
//            Toast.makeText(activity,"Reader close",Toast.LENGTH_LONG).show();
            setInfo();
        }

    }

    private void setInfo(){
//        result=result.replaceAll("\u0090", "")
//                .replaceAll(" ","");
//        Log.e("Result: ",result);
//        pid="1103700753435��";
//        address="109/12#หมู่ที่2###ซอยกาญจนาภิเษก20##แขวงสะพานสูง#เขตสะพานสูง#กรุงเทพมหานคร��";
//        info="นาย#จิรัฏฐ์##อัครวราวงศ์Mr.#Jirat##Akkaravarawong25350412110444137535/101��";

        if(!isSetInfo) {
            isSetInfo=true;



            //get pid
            if(pid.length()!=0) {

                pid = pid.replaceAll("\u0090", "").replaceAll(" ", "");
                Log.e("pid: ", pid);
                pid = pid.substring(0, 13);

                address = address.replaceAll("\u0090", "").replaceAll(" ", "");
                Log.e("address: ", address);
                //get soi
                soi = address.substring(0, address.indexOf("#"));
                if (address.contains("ซอย")) {
                    soi += " " + address.substring(address.indexOf("ซอย"), address.indexOf("#", address.indexOf("ซอย")));
                }

                //get moo
                if (address.contains("หมู่ที่")) {
                    moo = address.substring(address.indexOf("หมู่ที่"), address.indexOf("#", address.indexOf("หมู่ที่")));
                    moo = moo.replace("หมู่ที่", "");
                    if (Integer.parseInt(moo) < 10) {
                        moo = "0" + moo;
                    }
                } else {
                    moo = "";
                }

                //get tambol
                if (address.contains("ตำบล")) {
                    tambol = address.substring(address.indexOf("ตำบล"), address.indexOf("#", address.indexOf("ตำบล")));
                    tambol = tambol.replace("ตำบล", "");
                }
                if (address.contains("แขวง")) {
                    tambol = address.substring(address.indexOf("แขวง"), address.indexOf("#", address.indexOf("แขวง")));
                    tambol = tambol.replace("แขวง", "");
                }

                //get amphur
                if (address.contains("อำเภอ")) {
                    amphur = address.substring(address.indexOf("อำเภอ"), address.indexOf("#", address.indexOf("อำเภอ")));
                    amphur = amphur.replace("อำเภอ", "");
                }
                if (address.contains("เขต")) {
                    amphur = address.substring(address.indexOf("เขต"), address.indexOf("#", address.indexOf("เขต")));
                }

                //get province
                if (address.contains("กรุงเทพมหานคร")) {
                    province = "กรุงเทพมหานคร";
                }
                if (address.contains("จังหวัด")) {
                    province = address.substring(address.indexOf("จังหวัด"),address.length()-1);
                    province = province.replace("จังหวัด", "");
                }

                info = info.replaceAll("\u0090", "").replaceAll(" ", "");

                //get prefix
                prefix = info.substring(0, info.indexOf("#"));

                //get firstname
                firstname = info.substring(info.indexOf("#") + 1, info.indexOf("##"));

                //getLastname
                info = info.replaceAll("[a-zA-Z.]", "");
//                Log.e("info: ", info);
                lastname = info.substring(info.indexOf("##") + 2, info.indexOf("###"));

                //getBirthday
                bd = info.substring(info.indexOf("###") + 3, info.indexOf("###") + 11);
//
//                Log.e("pid: ", pid);
//                Log.e("soi: ", soi);
//                Log.e("moo: ", moo);
//                Log.e("tambol: ", tambol);
//                Log.e("amphur: ", amphur);
//                Log.e("province: ", province);
//                Log.e("prefix: ", prefix);
//                Log.e("firstname: ", firstname);
//                Log.e("lastname: ", lastname);
//                Log.e("bd: ", bd);

                HashMap<String, String> result = new HashMap<>();
                result.put("pid", pid);
                result.put("soi", soi);
                result.put("moo", moo);
                result.put("tambol", tambol);
                result.put("amphur", amphur);
                result.put("province", province);
                result.put("prefix", prefix);
                result.put("firstname", firstname);
                result.put("lastname", lastname);
                result.put("bd", bd);

                pidScannerListener.onCompleteScan(result);

//                Log.e("btyearray2 L: ", byteArray2.length + "");
//                Log.e("btyearray2 S: ", new String(byteArray2));
//                byte [] decoded =Base64.decode(byteArray,0);
//                Log.e("btyearray String64: ",decoded+"");

//        byte[] decodedString = Base64.decode(imgPic, Base64.DEFAULT)
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap decodedByte = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length,options);
//
//        Log.e("Bitmap: ",decodedByte.toString());
//
////        Bitmap bitmapShow = AppUtils.scaleBitmap(decodedByte, 100, 100);
//
        imageView.setImageBitmap(decodedByte);

//        imageView.setImageBitmap(decodedByte);
            }
        }
    }

    /** Called when the activity is first created. */

    public void onCreateActivity() {

//        setInfo();
        // Get USB manager
        mManager = (UsbManager) activity.getSystemService(Context.USB_SERVICE);

        // Initialize reader
        mReader = new Reader(mManager);
        mReader.setOnStateChangeListener(new OnStateChangeListener() {

            @Override
            public void onStateChange(int slotNum, int prevState, int currState) {

                if (prevState < Reader.CARD_UNKNOWN
                        || prevState > Reader.CARD_SPECIFIC) {
                    prevState = Reader.CARD_UNKNOWN;
                }

                if (currState < Reader.CARD_UNKNOWN
                        || currState > Reader.CARD_SPECIFIC) {
                    currState = Reader.CARD_UNKNOWN;
                }
            }
        });

        // Register receiver for USB permission
        mPermissionIntent = PendingIntent.getBroadcast(activity, 0, new Intent(
                ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        activity.registerReceiver(mReceiver, filter);

        for (UsbDevice device : mManager.getDeviceList().values()) {
            if (mReader.isSupported(device)) {
                deviceName=device.getDeviceName();
            }
        }

        Toast.makeText(activity,deviceName,Toast.LENGTH_SHORT).show();

        // Initialize open button
        if (deviceName != null) {

            // For each device
            for (UsbDevice device : mManager.getDeviceList().values()) {

                // If device name is found
                if (deviceName.equals(device.getDeviceName())) {

                    // Request permission
                    mManager.requestPermission(device,
                            mPermissionIntent);

                    break;
                }
            }
        }

        // Hide input window
        activity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private String readCard(){
//        result = "";
        pid="";
        address="";
        info="";
//        imgPic="";
        if(!isReading) {
            isReading=true;
//        Toast.makeText(activity,"hello",Toast.LENGTH_LONG).show();
            // If slot is selected
            if (slotNum != Spinner.INVALID_POSITION) {

                try {
                    byte[] byteAPDU;
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    mReader.power(slotNum, Reader.CARD_WARM_RESET);
                    mReader.setProtocol(slotNum, Reader.PROTOCOL_T0 | Reader.PROTOCOL_T1);
                    byteAPDU = atohex("00A4040008A000000054480001");
                    respAPDU = transceives(byteAPDU, slotNum);

                    for (int i = 0; i < action.length; i++) {

                        byteAPDU = atohex(action[i]);

                        respAPDU = transceives(byteAPDU, slotNum);

                        if (i <= 2) {
                            if(i==0){
                                pid=new String(respAPDU, "TIS-620");
                            }else if (i==1){
                                address=new String(respAPDU, "TIS-620");
                            }else{
                                info=new String(respAPDU, "TIS-620");
                            }
                        } else {

//                            if(i==3){
//                                img1=respAPDU;
//                            }else if (i==4){
//                                img2=respAPDU;
//                            }else if (i==5){
//                                img3=respAPDU;
//                            }else if (i==6){
//                                img4=respAPDU;
//                            }else if (i==7){
//                                img5=respAPDU;
//                            }else if (i==8){
//                                img6=respAPDU;
//                            }else if (i==9){
//                                img7=respAPDU;
//                            }else if (i==10){
//                                img8=respAPDU;
//                            }else if (i==11){
//                                img9=respAPDU;
//                            }else if (i==12){
//                                img10=respAPDU;
//                            }else if (i==13){
//                                img11=respAPDU;
//                            }else if (i==14){
//                                img12=respAPDU;
//                            }else if (i==15){
//                                img13=respAPDU;
//                            }else if (i==16){
//                                img14=respAPDU;
//                            }else if (i==17){
//                                img15=respAPDU;
//                            }else if (i==18){
//                                img16=respAPDU;
//                            }else if (i==19){
//                                img17=respAPDU;
//                            }else if (i==20){
//                                img18=respAPDU;
//                            }else if (i==21){
//                                img19=respAPDU;
//                            }else if (i==22){
//                                img20=respAPDU;
//                            }
                            respAPDU=Arrays.copyOfRange(respAPDU,0,respAPDU.length-2);
//                            Log.e("btyearray utf: "+i,new String(respAPDU, "UTF-8"));
                            baos.write(respAPDU);
////                            imgPic += new String(respAPDU);
                        }
                    }

//                    byteArray2=concat(img1,img2,img3,img4,img5,img6,img7,img8,img9,img10,img11,img12
//                            ,img13,img14,img15,img16,img17,img18,img19,img20);

//                    baos.flush();
//                    FileOutputStream fos = new FileOutputStream (new File("myFile"));
//                    try {
//                        baos.writeTo(fos);
//                    } catch(IOException ioe) {
//                        ioe.printStackTrace();
//                    } finally {
//                        fos.close();q
//                    }
                    byteArray=baos.toByteArray();
//                    File sdCard = Environment.getExternalStorageDirectory();
//                    File dir = new File (sdCard.getAbsolutePath() + "/testfile");
//                    dir.mkdirs();
//                    File file = new File(dir, "picturefile");
//                    FileUtils.writeByteArrayToFile(file, byteArray);
//                    baos.flush();
//                    baos.close();

                } catch (IllegalArgumentException e) {
//                    Log.e("Error Position : ", i + "");
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
//                    Log.e("Error Position : ", i + "");
                    e.printStackTrace();
                } catch (ReaderException e) {
//                    Log.e("Error Position : ", i + "");
                    e.printStackTrace();
                } catch (IOException e) {
//                    Log.e("Error Position : ", i + "");
                    e.printStackTrace();
                }

//            Toast.makeText(activity, result, Toast.LENGTH_LONG).show();
            }
        }
        return pid;
    }

    byte[] concat(byte[]...arrays)
    {
        // Determine the length of the result array
        int totalLength = 0;
        for (int i = 0; i < arrays.length; i++)
        {
            totalLength += arrays[i].length;
        }

        // create the result array
        byte[] result = new byte[totalLength];

        // copy the source arrays into the result array
        int currentIndex = 0;
        for (int i = 0; i < arrays.length; i++)
        {
            System.arraycopy(arrays[i], 0, result, currentIndex, arrays[i].length);
            currentIndex += arrays[i].length;
        }

        return result;
    }

    public void onDestroyActivity() {

        // Close reader
        mReader.close();

        // Unregister receiver
        activity.unregisterReceiver(mReceiver);
    }

    private static byte[] transceives(byte[] data,int slotNum) {
        byte[] response = new byte[512];
        int responseLength = 0;
        try {
            responseLength = mReader.transmit(slotNum, data, data.length, response, response.length);
            respAPDU2 = Arrays.copyOf(response, responseLength);
            byte[] GetResponse = new byte[]{(byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0};
            if (response[0] == (byte) 108) {
                GetResponse[0] = data[0];
                GetResponse[1] = data[1];
                GetResponse[2] = data[2];
                GetResponse[3] = data[3];
                GetResponse[4] = response[1];
            } else {
                GetResponse[0] = (byte) 0;
                GetResponse[1] = (byte) -64;
                GetResponse[2] = (byte) 0;
                GetResponse[3] = (byte) 0;
                GetResponse[4] = response[1];
            }
            byteAPDU2 = GetResponse;
            responseLength = mReader.transmit(slotNum, GetResponse, GetResponse.length, response, response.length);
        } catch (Exception e) {
            //print(e.toString());
            e.printStackTrace();
        }
        return Arrays.copyOf(response, responseLength);
    }

}
