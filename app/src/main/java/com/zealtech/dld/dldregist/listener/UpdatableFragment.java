package com.zealtech.dld.dldregist.listener;

/**
 * Created by Jirat on 1/9/2018.
 */

public interface UpdatableFragment {
    public void update(Double animalWorth);
}
