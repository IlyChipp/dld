package com.zealtech.dld.dldregist.util;

/**
 * Created by dev on 9/27/17.
 */

public interface ValidatingView {

    public boolean isDataValid();
    public String getDataValidationError();
    public void showError();

}
