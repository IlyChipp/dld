
package com.zealtech.dld.dldregist.model.farmerList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class FarmerSearchList {

    @Expose
    @SerializedName("iTotalRecords")
    private Integer iTotalRecords;
    @Expose
    @SerializedName("aaData")
    private List<AaData> aaData = null;
    @Expose
    @SerializedName("iTotalDisplayRecords")
    private Integer iTotalDisplayRecords;
    @Expose
    @SerializedName("sEcho")
    private Integer sEcho;

    public Integer getiTotalRecords() {
        return iTotalRecords;
    }

    public void setiTotalRecords(Integer iTotalRecords) {
        this.iTotalRecords = iTotalRecords;
    }

    public List<AaData> getAaData() {
        return aaData;
    }

    public void setAaData(List<AaData> aaData) {
        this.aaData = aaData;
    }

    public Integer getiTotalDisplayRecords() {
        return iTotalDisplayRecords;
    }

    public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
        this.iTotalDisplayRecords = iTotalDisplayRecords;
    }

    public Integer getsEcho() {
        return sEcho;
    }

    public void setsEcho(Integer sEcho) {
        this.sEcho = sEcho;
    }
}
