package com.zealtech.dld.dldregist.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.FarmActivity;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.model.AmphurMaster.AmphurEntity;
import com.zealtech.dld.dldregist.model.ProvinceMaster.ProvinceEntity;
import com.zealtech.dld.dldregist.model.StandardMaster.StandardEntity;
import com.zealtech.dld.dldregist.model.SupportStandardMaster.SupportEntity;
import com.zealtech.dld.dldregist.model.TambolMaster.TambolEntity;
import com.zealtech.dld.dldregist.model.VillageMaster.VillageEntity;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MySpinnerAdapter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class FarmFragment extends Fragment {

    private static final int RESULT_CANCELED = 0;
    private static final String IMAGE_DIRECTORY = "/demonuts";
    private int GALLERY = 1, CAMERA = 2;

    ArrayList<Bitmap> bitmaps = new ArrayList<>();
    ArrayList<String> listVillage = new ArrayList<>();
    ArrayList<String> listTambol = new ArrayList<>();
    ArrayList<String> listAmphur = new ArrayList<>();
    ArrayList<String> listProvince = new ArrayList<>();
    ArrayList<String> listStandard = new ArrayList<>();
    ArrayList<String> listSupport = new ArrayList<>();

    ArrayList<VillageEntity> villages = new ArrayList<>();
    ArrayList<TambolEntity> tambols = new ArrayList<>();
    ArrayList<AmphurEntity> amphurs = new ArrayList<>();
    ArrayList<ProvinceEntity> provinces = new ArrayList<>();
    ArrayList<StandardEntity> standards = new ArrayList<>();
    ArrayList<SupportEntity> supports = new ArrayList<>();

    ImageView imageView;
    int click = 0;

    private ImageView btnAddPhoto;
    private LinearLayout llAddphoto;
    private LinearLayout llFarmAddress;
    private MyNoHBTextView txtTopicAddress;
    private View view;
    private Spinner spnSupport;
    private Spinner spnStandard;
    private Spinner spnProvince;
    private Spinner spnAmphur;
    private Spinner spnTambol;
    private Spinner spnVillage;
    private MyEditNoHLTextView txtFarmName;
    private MyEditNoHLTextView txtPhone;
    private MyEditNoHLTextView txtSalary;
    private MyEditNoHLTextView txtProblem;
    private MyEditNoHLTextView txtMobile;
    private MyEditNoHLTextView txtNgan;
    private MyEditNoHLTextView txtRai;
    private MyEditNoHLTextView txtLong;
    private MyEditNoHLTextView txtLat;
    private MyEditNoHLTextView txtPostCode;
    private MyEditNoHLTextView txtStreet;
    private MyEditNoHLTextView txtSoi;
    private MyEditNoHLTextView txtHomeNo;
    private MyEditNoHLTextView txtVillage;
    private MyEditNoHLTextView txtEmail;
    private Farm mFarmItem;
    DBHelper dbHelper;
    Boolean isEmpty = false;
    String zipcode = "";


    public FarmFragment() {
        super();
    }


    public static FarmFragment newInstance() {
        FarmFragment fragment = new FarmFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_farm, container, false);
        View v = inflater.inflate(R.layout.item_farm_address, container, false);
        view = inflater.inflate(R.layout.item_pic, container, false);

        ButterKnife.bind(this, rootView);
        initInstances(rootView, v);
        dbHelper = new DBHelper(getContext());

        mFarmItem = ((FarmActivity) getActivity()).getAllFarm().get(0);
        if (mFarmItem != null) {
            setInfoFarm();
            txtFarmName.setEnabled(false);
        } else {
            txtFarmName.setEnabled(true);
            mFarmItem = new Farm();
        }

        llFarmAddress.addView(v);
        addTotalSpinner();
        getListProvince();
//        getStandard();
        getSupport();


        return rootView;
    }

    private void initInstances(View rootView, View v) {
        // Init 'View' instance(s) with rootView.findViewById here

        llAddphoto = (LinearLayout) rootView.findViewById(R.id.llAddphoto);
        btnAddPhoto = (ImageView) rootView.findViewById(R.id.btnAddPhoto);
        llFarmAddress = (LinearLayout) rootView.findViewById(R.id.llFarmAddress);
        txtTopicAddress = (MyNoHBTextView) rootView.findViewById(R.id.txtTopicAddress);
        txtFarmName = (MyEditNoHLTextView) v.findViewById(R.id.txtFarmName);
        txtHomeNo = (MyEditNoHLTextView) v.findViewById(R.id.txtFarmNo);
        txtSoi = (MyEditNoHLTextView) v.findViewById(R.id.txtSoi);
        txtVillage = (MyEditNoHLTextView) v.findViewById(R.id.txtVillage);
        txtStreet = (MyEditNoHLTextView) v.findViewById(R.id.txtStreet);
        txtPostCode = (MyEditNoHLTextView) v.findViewById(R.id.txtPostCode);
        txtLat = (MyEditNoHLTextView) v.findViewById(R.id.txtLatitude);
        txtLong = (MyEditNoHLTextView) v.findViewById(R.id.txtLongitude);
        txtRai = (MyEditNoHLTextView) v.findViewById(R.id.txtRai);
        txtNgan = (MyEditNoHLTextView) v.findViewById(R.id.txtNgan);
        txtMobile = (MyEditNoHLTextView) v.findViewById(R.id.txtMobile);
        txtPhone = (MyEditNoHLTextView) v.findViewById(R.id.txtPhone);
        txtProblem = (MyEditNoHLTextView) rootView.findViewById(R.id.txtProblem);
        txtSalary = (MyEditNoHLTextView) rootView.findViewById(R.id.txtRevenue);
        txtEmail = (MyEditNoHLTextView) v.findViewById(R.id.txtEmail);
        spnVillage = (Spinner) v.findViewById(R.id.spnVillage);
        spnTambol = (Spinner) v.findViewById(R.id.spnTambol);
        spnAmphur = (Spinner) v.findViewById(R.id.spnAmphur);
        spnProvince = (Spinner) v.findViewById(R.id.spnProvince);
        spnStandard = (Spinner) rootView.findViewById(R.id.spnStandard);
//        spnSupport = (Spinner) rootView.findViewById(R.id.spnStandard_sub);

        spnAmphur.setEnabled(false);
        spnTambol.setEnabled(false);
        spnVillage.setEnabled(false);

    }

    @OnClick(R.id.btnAddPhoto)
    public void addPhoto() {
        imageView = new ImageView(getContext());
        showPictureDialog();
    }

    public void setInfoFarm() {

        txtFarmName.setText(mFarmItem.getFarmName());
        txtHomeNo.setText(mFarmItem.getHomeNo());
        txtVillage.setText(mFarmItem.getVillageID());
        txtSoi.setText(mFarmItem.getSoi());
        txtPostCode.setText(mFarmItem.getPostCode());
//        txtLong.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(6)});
//        txtLat.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(6)});
        txtLat.setText(mFarmItem.getLatitude());
        txtLong.setText(mFarmItem.getLongitude());
        txtRai.setText(mFarmItem.getAreaRai().toString());
        txtNgan.setText(mFarmItem.getAreaNgan().toString());
        txtMobile.setText(mFarmItem.getMobile());
        txtPhone.setText(mFarmItem.getPhone());
        txtEmail.setText(mFarmItem.getEmail());
        txtProblem.setText(mFarmItem.getFarmProblemDesc());
        txtSalary.setText(mFarmItem.getRevenueOfLivestock().toString());

    }

    @OnClick(R.id.btnSubmit)
    public void clickBtnSubmit() {

//        if (!dbHelper.isTablePopulated(FarmModel.TABLE)) {
//            setInfoToDatabase(mFarmItem);
//            dbHelper.addFarm(mFarmItem, mFarmItem.getFarmer_ID());
//        } else {
//            setInfoToDatabase(mFarmItem);
//            dbHelper.updateFarm(mFarmItem, mFarmItem.getFarm_ID(), mFarmItem.getFarmer_ID());
//        }

        setInfoToDatabase();
//        dbHelper.updateFarm(mFarmItem,mFarmItem.getFarm_ID());

    }

    public void setInfoToDatabase() {
        mFarmItem = new Farm();
        mFarmItem.setFarmName(txtFarmName.getText().toString());
        mFarmItem.setHomeNo(txtHomeNo.getText().toString());
        mFarmItem.setSoi(txtSoi.getText().toString());
        mFarmItem.setPostCode(txtPostCode.getText().toString());
        mFarmItem.setLatitude(txtLat.getText().toString());
        mFarmItem.setLongitude(txtLong.getText().toString());
        mFarmItem.setAreaRai(AppUtils.tryParseDouble(txtRai.getText().toString()));
        mFarmItem.setAreaNgan(AppUtils.tryParseDouble(txtNgan.getText().toString()));
        mFarmItem.setMobile(txtMobile.getText().toString());
        mFarmItem.setPhone(txtPhone.getText().toString());
        mFarmItem.setEmail(txtEmail.getText().toString());
        mFarmItem.setFarmProblemDesc(txtProblem.getText().toString());
        mFarmItem.setRevenueOfLivestock(AppUtils.tryParseDouble(txtSalary.getText().toString()));

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }


    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), contentURI);
                    bitmaps.add(bitmap);
//                    String path = saveImage(bitmap);
                    Toast.makeText(getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
                    imageView.setLayoutParams(new ViewGroup.LayoutParams((int) getResources().getDimension(R.dimen.picture_width)
                            , (int) getResources().getDimension(R.dimen.picture_width)));

                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    imageView.setImageBitmap(bitmaps.get(click));
                    llAddphoto.addView(imageView);
                    click++;

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            bitmaps.add(thumbnail);
            Toast.makeText(getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();

            imageView.setLayoutParams(new ViewGroup.LayoutParams((int) getResources().getDimension(R.dimen.picture_width)
                    , (int) getResources().getDimension(R.dimen.picture_width)));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setImageBitmap(bitmaps.get(click));

            llAddphoto.addView(imageView);
            click++;
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getContext(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    public void addTotalSpinner() {
        ProvinceEntity provinceEntity = new ProvinceEntity();
        provinceEntity.setProvince_NameTh("กรุณาเลือกจังหวัด");
        provinceEntity.setProvince_ID(-99);

        provinces.add(provinceEntity);

        AmphurEntity amphurEntity = new AmphurEntity();
        amphurEntity.setAmphur_nameTh("กรุณาเลือกอำเภอ");
        amphurEntity.setAmphur_ID(-99);

        amphurs.add(amphurEntity);

        TambolEntity tambolEntity = new TambolEntity();
        tambolEntity.setTambol_NameTh("กรุณาเลือกตำบล");
        tambolEntity.setTambol_ID(-99);

        tambols.add(tambolEntity);

        VillageEntity villageEntity = new VillageEntity();
        villageEntity.setVillage_Name("กรุณาเลือกหมู่บ้าน");
        villageEntity.setVillage_ID(-99);

        villages.add(villageEntity);
    }


    public void getListProvince() {
        int selectIndex = -99;

        provinces.addAll(dbHelper.getProvinceList());

        for (int i = 0; i < provinces.size(); i++) {
            listProvince.add(provinces.get(i).getProvince_NameTh());
            if (mFarmItem.getProvinceID() != null) {
                if (provinces.get(i).getProvince_Code() == mFarmItem.getProvinceID()) {
                    selectIndex = i;
                }
            }
        }
        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listProvince);

        spnProvince.setAdapter(adapter);

        spnProvince.setSelection(selectIndex);

        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                getListAmphur(provinces.get(position).getProvince_ID());

                if (position > 0) {
                    spnAmphur.setEnabled(true);
                }
                mFarmItem.setProvinceID(provinces.get(position).getProvince_ID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void getListAmphur(int position) {

        listAmphur.clear();
        int selectIndex = -99;

        amphurs.addAll(dbHelper.getAmphur(position));

        for (int i = 0; i < amphurs.size(); i++) {
            listAmphur.add(amphurs.get(i).getAmphur_nameTh());
            if (mFarmItem != null) {
                if (amphurs.get(i).getAmphur_Code() == mFarmItem.getAmphurID()) {
                    selectIndex = i;
                }
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listAmphur);
        spnAmphur.setAdapter(adapter);

        spnAmphur.setSelection(selectIndex);

        spnAmphur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getListTambol(amphurs.get(position).getAmphur_ID());
                if (position > 0) {
                    spnTambol.setEnabled(true);
                }
                mFarmItem.setAmphurID(amphurs.get(position).getAmphur_ID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListTambol(int position) {

        listTambol.clear();
        int selectIndex = -99;

        tambols.addAll(dbHelper.getTambol(position));
        for (int i = 0; i < tambols.size(); i++) {
            listTambol.add(tambols.get(i).getTambol_NameTh());
            if (mFarmItem != null) {
                if (tambols.get(i).getTambol_Code() == mFarmItem.getTambolID()) {
                    selectIndex = i;
                }
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listTambol);
        spnTambol.setAdapter(adapter);

        spnTambol.setSelection(selectIndex);

        spnTambol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getListVillage(tambols.get(position).getTambol_ID());

                if (position > 0) {
                    spnVillage.setEnabled(true);
                }
                mFarmItem.setTambolID(tambols.get(position).getTambol_ID());
                zipcode = tambols.get(position).getZipcode();
                txtPostCode.setText(zipcode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListVillage(int position) {

        listVillage.clear();
        int selectIndex = -99;

        villages.addAll(dbHelper.getVillage(position));

        for (int i = 0; i < villages.size(); i++) {
            listVillage.add(villages.get(i).getVillage_Name());
            if (mFarmItem != null) {
                if (villages.get(i).getVillage_ID() == mFarmItem.getVillageID()) {
                    selectIndex = i;
                }
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listVillage);
        spnVillage.setAdapter(adapter);

        spnVillage.setSelection(selectIndex);

        spnVillage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mFarmItem.setVillageID(villages.get(position).getVillage_ID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

//    public void getStandard() {
//
//        int selectIndex = -1;
//        standards = dbHelper.getStandard();
//
//
//        for (int i = 0; i < standards.size(); i++) {
//            listStandard.add(standards.get(i).getStandard_Name());
//
//            if (mFarmItem != null) {
//                if (standards.get(i).getStandard_ID() == mFarmItem.getStandardID()){
//                    selectIndex = i;
//                }
//            }
//        }
//
//        MySpinnerAdapter adapter = new MySpinnerAdapter(
//                getContext(),
//                android.R.layout.simple_dropdown_item_1line,
//                listStandard);
//        spnStandard.setAdapter(adapter);
//
//        spnStandard.setSelection(selectIndex);
//
//        spnStandard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                mFarmItem.setStandardID(standards.get(position).getStandard_ID());
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }

    public void getSupport() {
        int selectIndex = -1;
        supports = dbHelper.getSupport();

        for (int i = 0; i < supports.size(); i++) {
            listSupport.add(supports.get(i).getSupport_Standard_Name());
            Log.d("STANDARD LIST -->> ", String.valueOf(supports.get(i).getSupport_Standard_Name()));
            if (supports.get(i).getSupport_Standard_ID() == mFarmItem.getSupportStandardID()) {
                selectIndex = i;
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listSupport);
        spnSupport.setAdapter(adapter);

        spnSupport.setSelection(selectIndex);

        spnSupport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mFarmItem.setSupportStandardID(supports.get(position).getSupport_Standard_ID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


}
