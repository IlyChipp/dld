package com.zealtech.dld.dldregist.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.zealtech.dld.dldregist.MainApplication;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.AddFarmActivity;
import com.zealtech.dld.dldregist.activity.FarmActivity;
import com.zealtech.dld.dldregist.activity.HistoryActivity;
import com.zealtech.dld.dldregist.activity.MainActivity;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.model.FarmModel.FarmEntity;
import com.zealtech.dld.dldregist.model.FeedModel.FeedEntity;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseInsertFarmer;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.util.UpdateListener;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.zealtech.dld.dldregist.util.AppUtils.UUID;

public class ListFarmFragment extends Fragment {

    @BindView(R.id.plsAddFarm)
    MyNoHBTextView plsAddFarm;

    @BindView(R.id.progressBar)
    FrameLayoutDisable progressBar;

    DBHelper dbHelper;
    private static final String CLASS_NAME = "ListFarmFragment";
    final private static String FARMINFO = "farmInfo";
    private ArrayList<Farm> farmEntities = new ArrayList<>();
    private ArrayList<FeedEntity> feeds = new ArrayList<>();
    private FarmEntity farm = new FarmEntity();
    private RecyclerView listFarm;
    private Farmer mFarmerItem;
    DatabaseRealm databaseRealm;
    private int loginType;
    String mode = "";
    Tracker mTracker;

    public ListFarmFragment() {
        super();
    }

    public static ListFarmFragment newInstance() {
        ListFarmFragment fragment = new ListFarmFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_farm, container, false);
        ButterKnife.bind(this, rootView);
        initInstances(rootView);
        databaseRealm = new DatabaseRealm(getContext());

        MainApplication application = (MainApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        loginType = getActivity().getIntent().getIntExtra("loginType", 0);
        Bundle args = getArguments();
        mode = args.getString("mode");
        dbHelper = new DBHelper(getActivity());

//        dbHelper = new DBHelper(getContext());
//        mFarmer = ((MainActivity) getActivity()).getFarmer();
//        getListFarm();
        initList();

        return rootView;
    }



    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        listFarm = (RecyclerView) rootView.findViewById(R.id.listFarm);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here

        }
    }

    @OnClick(R.id.addFarm)
    public void gotoFarm() {
        //TODO: CHANGE FLOW TO ADD FARM
        Intent intent = new Intent(getActivity(), AddFarmActivity.class);
        intent.putExtra(UUID, mFarmerItem.getUuid());
        intent.putExtra("position", -1);
        getActivity().startActivity(intent);
    }

//    public void getListFarm() {
//        if (mFarmer != null) {
//            Api api = AppUtils.getApiService();
//            Call<ResponseFarm> call = api.getListFarm("" + mFarmer.getFarmer_ID());
//            call.enqueue(new Callback<ResponseFarm>() {
//                @Override
//                public void onResponse(Call<ResponseFarm> call, Response<ResponseFarm> response) {
//                    if (response.body() != null && response.body().getResponseCode().equals(getString(R.string.success))) {
//
//                        dbHelper.addListFarm(response.body().getData(), mFarmer.getFarmer_ID());
//
//                        farmEntities.addAll(dbHelper.getListFarm(mFarmer.getFarmer_ID()));
//
//                        listFarm.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//                        ListAdapter listFarmAdapter = new ListAdapter();
//                        listFarm.setAdapter(listFarmAdapter);
//
//                    } else {
//                        Toast.makeText(getContext(), getString(R.string.content_fail), Toast.LENGTH_LONG).show();
//
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseFarm> call, Throwable t) {
//                    try {
//                        Log.d(CLASS_NAME, t.getMessage());
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//        }
//    }

    private void deleteFarm(final int position) {
        progressBar.setVisibility(View.VISIBLE);
        final Farmer farmer = ((FarmActivity) getActivity()).getFarmer();
        farmer.getFarm().get(position).setStatusID(-9); //remove

        if (AppUtils.isOnline(getActivity())) {
//            ArrayList<Farmer> farmers=new ArrayList<>();
//            farmers.add(farmer);

            AppUtils.updateData(farmer, new UpdateListener() {
                @Override
                public void onSuccess(String message, ResponseInsertFarmer responseInsertFarmer) {
                    progressBar.setVisibility(View.GONE);
                    farmer.getFarm().remove(position);
                    databaseRealm.upDateDataFarmer(farmer);
                    farmEntities = new ArrayList<Farm>();
                    farmEntities.addAll((farmer.getFarm()));
                    ListAdapter listFarmAdapter = new ListAdapter();
                    listFarm.setAdapter(listFarmAdapter);
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

                }

                @Override
                public void onError(String message, ResponseInsertFarmer responseInsertFarmer) {
                    progressBar.setVisibility(View.GONE);
                    farmer.getFarm().get(position).setStatusID(1); //reset
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            progressBar.setVisibility(View.GONE);
            ; //ref to activity
            farmer.getFarm().remove(position);
            databaseRealm.upDateDataFarmer(farmer);
            farmEntities = new ArrayList<Farm>();
            farmEntities.addAll((farmer.getFarm()));
            ListAdapter listFarmAdapter = new ListAdapter();
            listFarm.setAdapter(listFarmAdapter);
            Toast.makeText(getContext(), getResources().getString(R.string.update_success), Toast.LENGTH_LONG).show();
        }
    }

    private void initList() {

        mFarmerItem = ((FarmActivity) getActivity()).getFarmer();
        farmEntities.clear();
        farmEntities.addAll(((FarmActivity) getActivity()).getAllFarm());

        if (farmEntities == null || farmEntities.size() == 0) {
            listFarm.setVisibility(View.GONE);
            plsAddFarm.setVisibility(View.VISIBLE);
        } else {
            listFarm.setVisibility(View.VISIBLE);
            plsAddFarm.setVisibility(View.GONE);

            listFarm.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            ListAdapter listFarmAdapter = new ListAdapter();
            listFarm.setAdapter(listFarmAdapter);
        }

    }

    private String getAddress(Farm farm_a){
        String address="";
        String home="";
        String soi="";
        String road="";
        String post="";

        String province= dbHelper.getProvinceById(farm_a.getProvinceID()).getProvince_NameTh();
        String amphur= dbHelper.getAmphurById(farm_a.getAmphurID()).getAmphur_nameTh();
        String tambol= dbHelper.getTambolById(farm_a.getTambolID()).getTambol_NameTh();

        if(farm_a.getSoi()!=null&&!farm_a.getSoi().equals("")){
            soi=" ซอย "+farm_a.getSoi();
        }

        if(farm_a.getRoad()!=null&&!farm_a.getRoad().equals("")){
            road=" ถนน "+farm_a.getRoad();
        }

        if(farm_a.getPostCode()!=null&&!farm_a.getPostCode().equals("")){
            post=""+farm_a.getPostCode();
        }

        if(farm_a.getHomeNo()!=null&&!farm_a.getHomeNo().equals("")){
            home=""+farm_a.getHomeNo();
        }

        address=home+soi+road
                + " " +tambol
                + " " +amphur
                + " " +province
                +" "+post;

        return address;
    }

    public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_farm, parent, false);

            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {

            final Farm item = farmEntities.get(position);

            holder.mtxtFarmName.setText(getAddress(item));

            String update = getResources().getString(R.string.fl_edit) + item.getUpdateDate();
            holder.txt_date_update.setText(update);

            String exp = getResources().getString(R.string.fl_exp) + item.getFarmExpire();
            holder.txt_date_exp.setText(exp);

            holder.btn_his.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), HistoryActivity.class);
                    getActivity().startActivity(intent);
                }
            });

            holder.btn_single.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
                        String main = "http://122.155.204.29/singleForm.rpt?init=pdf";
                        String pompt = "&prompt0=" + item.getFarmID();
                        Uri uri = Uri.parse(main + pompt);
                        pdfIntent.setDataAndType(uri, "application/pdf");
                        startActivity(pdfIntent);
                    } catch (Exception e) {
                        new android.support.v7.app.AlertDialog.Builder(getActivity())
                                .setTitle(getResources().getString(R.string.error))
                                .setMessage("ไม่สามารถอ่านไฟล์ pdf ออนไลน์ได้ " +
                                        "กรุณาดาวน์โหลด google pdf viewer")
                                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        final String appPackageName = "com.google.android.apps.pdfviewer"; // getPackageName() from Context or Activity object
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        } catch (android.content.ActivityNotFoundException anfe) {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                        }
                                    }
                                })
                                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                }
            });

            holder.mshowFarm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Intent intent = new Intent(getActivity(), FarmActivity.class);
//                    intent.putExtra(FARMINFO, Parcels.wrap(item));
//                    getActivity().startActivity(intent);
                }
            });

            holder.list_farm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (AppUtils.isOnline(getActivity())) {
                        ((FarmActivity) getActivity()).getMainFarm(farmEntities.get(position).getFarmID(), mode);
                    } else {
                        ((FarmActivity) getActivity()).setCurrent_FarmPosition(position); //offline can't ref to farmid
                        ((FarmActivity) getActivity()).getMainFarm(farmEntities.get(position).getFarmID(), mode);
                    }
//                    Intent intent = new Intent(getActivity(), AddFarmActivity.class);
//                    intent.putExtra(AppUtils.UUID, mFarmerItem.getUuid());
//                    intent.putExtra("position", position);
//                    getActivity().startActivity(intent);
                }
            });

//            holder.mtxtFarmName.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (AppUtils.isOnline(getActivity())) {
//                        ((FarmActivity) getActivity()).getMainFarm(farmEntities.get(position).getFarmID(), mode);
//                    } else {
//                        ((FarmActivity) getActivity()).setCurrent_FarmPosition(position); //offline can't ref to farmid
//                        ((FarmActivity) getActivity()).getMainFarm(farmEntities.get(position).getFarmID(), mode);
//                    }
////                    Intent intent = new Intent(getActivity(), AddFarmActivity.class);
////                    intent.putExtra(AppUtils.UUID, mFarmerItem.getUuid());
////                    intent.putExtra("position", position);
////                    getActivity().startActivity(intent);
//                }
//            });

            holder.delete.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(getContext()).setTitle(getResources().getString(R.string.alert))
                            .setMessage(getResources().getString(R.string.alert_delete))
                            .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    deleteFarm(position);
                                }
                            })
                            .setNegativeButton(getResources().getString(R.string.no),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                            .show();
                }
            });

            holder.btn_farmer.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra(UUID, mFarmerItem.getUuid());
                    intent.putExtra(AppUtils.SENDFROM,5);  //case update
                    startActivity(intent);
                    getActivity().finish();
                }
            });

            holder.btn_farm.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Intent intent = new Intent(getActivity(), FarmActivity.class); //mode MainFarm To FarmData
//                    intent.putExtra(UUID, mFarmerItem.getUuid());
//                    intent.putExtra("mode", "farm");
//                    intent.putExtra("farmId", farmEntities.get(position).getFarmID());
//                    startActivity(intent);
//                    getActivity().finish();
                    ((FarmActivity) getActivity()).setMode("farm");
                    if (AppUtils.isOnline(getActivity())) {
                        ((FarmActivity) getActivity()).getMainFarm(farmEntities.get(position).getFarmID(), mode);
                    } else {
                        ((FarmActivity) getActivity()).setCurrent_FarmPosition(position); //offline can't ref to farmid
                        ((FarmActivity) getActivity()).getMainFarm(farmEntities.get(position).getFarmID(), mode);
                    }
                }
            });

            holder.btn_livestock.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Intent intent = new Intent(getActivity(), FarmActivity.class); //mode MainFarm To Livestock
//                    intent.putExtra(UUID, mFarmerItem.getUuid());
//                    intent.putExtra("mode", "live");
//                    intent.putExtra("farmId", farmEntities.get(position).getFarmID());
//                    startActivity(intent);
//                    getActivity().finish();
                    ((FarmActivity) getActivity()).setMode("live");
                    if (AppUtils.isOnline(getActivity())) {
                        ((FarmActivity) getActivity()).getMainFarm(farmEntities.get(position).getFarmID(), mode);
                    } else {
                        ((FarmActivity) getActivity()).setCurrent_FarmPosition(position); //offline can't ref to farmid
                        ((FarmActivity) getActivity()).getMainFarm(farmEntities.get(position).getFarmID(), mode);
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return farmEntities.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private final MyNoHBTextView mtxtFarmName;
            private final MyNoHLTextView txt_date_update;
            private final MyNoHLTextView txt_date_exp;
            private final MyNoHLTextView delete;

            private final MyNoHBTextView btn_his;
            private final MyNoHBTextView btn_single;
            private final LinearLayout list_farm;
            private final LinearLayout mshowFarm;

            private final MyNoHBTextView btn_farm;
            private final MyNoHBTextView btn_farmer;
            private final MyNoHBTextView btn_livestock;

            public ViewHolder(View itemView) {
                super(itemView);

                mtxtFarmName = (MyNoHBTextView) itemView.findViewById(R.id.txtFarmName);
                txt_date_update = (MyNoHLTextView) itemView.findViewById(R.id.txt_date_update);
                txt_date_exp = (MyNoHLTextView) itemView.findViewById(R.id.txt_date_exp);
                list_farm = (LinearLayout) itemView.findViewById(R.id.list_farm);
                btn_his = (MyNoHBTextView) itemView.findViewById(R.id.btn_his);
                btn_single = (MyNoHBTextView) itemView.findViewById(R.id.btn_single);
                delete = (MyNoHLTextView) itemView.findViewById(R.id.delete);

                mshowFarm = (LinearLayout) itemView.findViewById(R.id.showFarm);

                btn_farm = (MyNoHBTextView) itemView.findViewById(R.id.btn_farm);
                btn_farmer = (MyNoHBTextView) itemView.findViewById(R.id.btn_farmer);
                btn_livestock = (MyNoHBTextView) itemView.findViewById(R.id.btn_livestock);

                if (loginType == 1) {
                    btn_single.setVisibility(View.VISIBLE);
                } else {
                    btn_single.setVisibility(View.GONE);
                }
            }
        }

    }


    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName("หน้ารายชื่อฟาร์ม");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        initList();
    }


}
