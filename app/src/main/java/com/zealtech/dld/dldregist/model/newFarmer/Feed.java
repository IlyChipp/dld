
package com.zealtech.dld.dldregist.model.newFarmer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Field;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Feed extends RealmObject {

    @PrimaryKey
    private String uuid;

    @Expose
    @SerializedName("feed_ID")
    private Integer feedID=0;

    @Expose
    @SerializedName("createBy")
    private String createBy="";

    @Expose
    @SerializedName("createDate")
    private String createDate="";

    @Expose
    @SerializedName("feed_Area_Ngan")
    private Double feedAreaNgan=0.0;

    @Expose
    @SerializedName("feed_Area_Rai")
    private Double feedAreaRai=0.0;

    @Expose
    @SerializedName("feed_Area_Wa")
    private Double feedAreaWa=0.0;

    @Expose
    @SerializedName("feed_Kind_ID")
    private Integer feedKindID=1;

    @Expose
    @SerializedName("feed_Kind_Other")
    private String feedKindOther="";

    @Expose
    @SerializedName("feed_PerRai")
    private Integer feedPerRai=0;

    @Expose
    @SerializedName("feed_SubType_Format_ID")
    private Integer feedSubTypeFormatID=1;

    @Expose
    @SerializedName("feed_SubType_ID")
    private Integer feedSubTypeID=0;

    @Expose
    @SerializedName("feed_Type_ID")
    private Integer feedTypeID=0;

    @Expose
    @SerializedName("feed_Type_Other")
    private String feedTypeOther="";

    @Expose
    @SerializedName("feed_Water_Type_ID")
    private Integer feedWaterTypeID=0;

    @Expose
    @SerializedName("orderIndex")
    private Integer orderIndex=0;

    @Expose
    @SerializedName("status_ID")
    private Integer statusID=0;

    @Expose
    @SerializedName("updateBy")
    private String updateBy="";

    @Expose
    @SerializedName("updateDate")
    private String updateDate="";

    // Local only data members:
    private boolean localOnly;

    public Feed() { }

    public Feed(Feed other) {
        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                field.set(this, field.get(other));
            } catch (IllegalAccessException ex) { throw new AssertionError(ex); }
        }
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getFeedID() {
        return feedID;
    }

    public void setFeedID(Integer feedID) {
        this.feedID = feedID;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Double getFeedAreaNgan() {
        return feedAreaNgan;
    }

    public void setFeedAreaNgan(Double feedAreaNgan) {
        this.feedAreaNgan = feedAreaNgan;
    }

    public Double getFeedAreaRai() {
        return feedAreaRai;
    }

    public void setFeedAreaRai(Double feedAreaRai) {
        this.feedAreaRai = feedAreaRai;
    }

    public Double getFeedAreaWa() {
        return feedAreaWa;
    }

    public void setFeedAreaWa(Double feedAreaWa) {
        this.feedAreaWa = feedAreaWa;
    }

    public Integer getFeedKindID() {
        return feedKindID;
    }

    public void setFeedKindID(Integer feedKindID) {
        this.feedKindID = feedKindID;
    }

    public String getFeedKindOther() {
        return feedKindOther;
    }

    public void setFeedKindOther(String feedKindOther) {
        this.feedKindOther = feedKindOther;
    }

    public Integer getFeedSubTypeFormatID() {
        return feedSubTypeFormatID;
    }

    public void setFeedSubTypeFormatID(Integer feedSubTypeFormatID) {
        this.feedSubTypeFormatID = feedSubTypeFormatID;
    }

    public Integer getFeedSubTypeID() {
        return feedSubTypeID;
    }

    public void setFeedSubTypeID(Integer feedSubTypeID) {
        this.feedSubTypeID = feedSubTypeID;
    }

    public Integer getFeedTypeID() {
        return feedTypeID;
    }

    public void setFeedTypeID(Integer feedTypeID) {
        this.feedTypeID = feedTypeID;
    }

    public String getFeedTypeOther() {
        return feedTypeOther;
    }

    public void setFeedTypeOther(String feedTypeOther) {
        this.feedTypeOther = feedTypeOther;
    }

    public Integer getFeedWaterTypeID() {
        return feedWaterTypeID;
    }

    public void setFeedWaterTypeID(Integer feedWaterTypeID) {
        this.feedWaterTypeID = feedWaterTypeID;
    }

    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    public Integer getStatusID() {
        return statusID;
    }

    public void setStatusID(Integer statusID) {
        this.statusID = statusID;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public boolean isLocalOnly() {
        return localOnly;
    }

    public void setLocalOnly(boolean localOnly) {
        this.localOnly = localOnly;
    }

    public Integer getFeedPerRai() {
        return feedPerRai;
    }

    public void setFeedPerRai(Integer feedPerRai) {
        this.feedPerRai = feedPerRai;
    }

    @Override
    public String toString() {
        return String.format("Feed{id: %d, typeId: %d, subtypeId: %d}",
                feedID, feedTypeID, feedSubTypeID);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof Feed))
            return false;
        Feed other = (Feed)obj;
        if (this.uuid.equals(other.uuid))
            return true;
        return false;
    }

}
