package com.zealtech.dld.dldregist.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.FarmActivity;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.model.AmphurMaster.AmphurEntity;
import com.zealtech.dld.dldregist.model.HelpMaster.HelpEntity;
import com.zealtech.dld.dldregist.model.LivestockMaster.LivestockEntity;
import com.zealtech.dld.dldregist.model.LivestockTypeMaster.TypeEntity;
import com.zealtech.dld.dldregist.model.PrefixMaster.PrefixEntity;
import com.zealtech.dld.dldregist.model.ProvinceMaster.ProvinceEntity;
import com.zealtech.dld.dldregist.model.StandardMaster.StandardEntity;
import com.zealtech.dld.dldregist.model.TambolMaster.TambolEntity;
import com.zealtech.dld.dldregist.model.VillageMaster.VillageEntity;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.model.newFarmer.FarmList;
import com.zealtech.dld.dldregist.model.newFarmer.FarmStandard;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseFarmStandard;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseInsertFarmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseWorker;
import com.zealtech.dld.dldregist.model.newFarmer.Worker;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.util.UpdateListener;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;
import com.zealtech.dld.dldregist.widget.MySpinnerAdapter;
import com.zealtech.dld.dldregist.widget.TypeFacedArrayAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.RealmList;
import io.realm.RealmObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FarmInfoFragment extends Fragment  {
    @BindView(R.id.llAddressInfo)
    LinearLayout llAddressInfo;
    @BindView(R.id.txtPID)
    MyEditNoHLTextView txtPID;
    @BindView(R.id.txtBD)
    MyEditNoHLTextView txtBD;
    @BindView(R.id.txtNameTH)
    MyEditNoHLTextView txtNameTH;
    @BindView(R.id.txtLastnameTH)
    MyEditNoHLTextView txtLastnameTH;
    @BindView(R.id.txtNameEN)
    MyEditNoHLTextView txtNameEN;
    @BindView(R.id.txtLastnameEN)
    MyEditNoHLTextView txtLastnameEN;
    @BindView(R.id.txtHomeNo)
    MyEditNoHLTextView txtHomeNo;

//    @BindView(R.id.txtMoo)
//    MyEditNoHLTextView txtMoo;

    @BindView(R.id.spnAmphur)
    Spinner spnAmphur;
    @BindView(R.id.spnTambol)
    Spinner spnTambol;
    @BindView(R.id.spnProvince)
    Spinner spnProvince;
    @BindView(R.id.spnVillage)
    Spinner spnVillage;

    @BindView(R.id.txtPost)
    MyEditNoHLTextView txtPost;
    @BindView(R.id.txtTel)
    MyEditNoHLTextView txtTel;
    @BindView(R.id.txtPhone)
    MyEditNoHLTextView txtPhone;
    @BindView(R.id.txtLat)
    MyEditNoHLTextView txtLat;
    @BindView(R.id.txtLong)
    MyEditNoHLTextView txtLong;
    @BindView(R.id.txtRevenue)
    MyEditNoHLTextView txtRevenue;
    @BindView(R.id.txtAnimalWorth)
    MyEditNoHLTextView txtAnimalWorth;
    @BindView(R.id.txtDebt)
    MyEditNoHLTextView txtDebt;
    @BindView(R.id.txtRai)
    MyEditNoHLTextView txtRai;
    @BindView(R.id.txtNgan)
    MyEditNoHLTextView txtNgan;
    @BindView(R.id.txtWaa)
    MyEditNoHLTextView txtWaa;

    @BindView(R.id.txtMachine)
    MyEditNoHLTextView txtMachine;
    @BindView(R.id.txtProblem)
    MyEditNoHLTextView txtProblem;
//    @BindView(R.id.txtHelp)
//    MyEditNoHLTextView txtHelp;

    @BindView(R.id.spnPrefix)
    Spinner spnPrefix;
    @BindView(R.id.spnPrefixEN)
    Spinner spnPrefixEN;
    @BindView(R.id.spnHelp)
    Spinner spnHelp;

    @BindView(R.id.radio_farm)
    RadioButton radio_farm;
    @BindView(R.id.radio_not_farm)
    RadioButton radio_not_farm;
    @BindView(R.id.listFarmStds)
    RecyclerView listFarmStds;

    @BindView(R.id.progressBar)
    FrameLayoutDisable progressBar;

    private RadioGroup radioGroup;

    ArrayList<String> listProvince = new ArrayList<>();
    ArrayList<String> listPrefix = new ArrayList<>();
    ArrayList<String> listVillage = new ArrayList<>();
    ArrayList<String> listTambol = new ArrayList<>();
    ArrayList<String> listAmphur = new ArrayList<>();
    private DatabaseRealm databaseRealm;
    ArrayList<ProvinceEntity> province = new ArrayList<>();
    ArrayList<AmphurEntity> amphur = new ArrayList<>();
    ArrayList<TambolEntity> tambol = new ArrayList<>();
    ArrayList<VillageEntity> village = new ArrayList<>();
    ArrayList<PrefixEntity> prefix = new ArrayList<>();
    RealmList<FarmStandard> farmStds = new RealmList<>();
    HashSet<FarmStandard> oldFarmStds = new HashSet<>();

    ArrayList<StandardEntity> listStandard = new ArrayList<>();
    ArrayList<LivestockEntity> livestockMasters = new ArrayList<>();
    ArrayList<TypeEntity> listTypes = new ArrayList<>();

    ArrayList<HelpEntity> helps = new ArrayList<>();
    ArrayList<String> listHelps = new ArrayList<>();

    private Farm mFarmItem;
    private Farmer mFarmerItem;
    private Worker mWorkerItem;
    DBHelper dbHelper;
    private RadioButton radisoOwner;
    private RadioButton radioNotOwner;
    boolean isWorker = false;
    boolean isFarmer = false;
    boolean isOldWorker = false;
    String zipcode = "";

    int farmID;
    String userID = "";

    LinearLayout llAddStandard;
    LinearLayout btnAddView;

    public void setFarmID(int farmID) {
        this.farmID = farmID;
    }

    public FarmInfoFragment() {
        super();
    }

    public static FarmInfoFragment newInstance(int farmID) {
        FarmInfoFragment fragment = new FarmInfoFragment();
        Bundle args = new Bundle();
        fragment.setFarmID(farmID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_farm_info, container, false);
        ButterKnife.bind(this, rootView);

        initInstances(rootView);
        setTypeFaceRadio();

        dbHelper = new DBHelper(getContext());
        databaseRealm = new DatabaseRealm(getContext());

        setUpData();

        return rootView;
    }

    public void setUpData() {
        farmID= ((FarmActivity) getActivity()).getCurrent_FarmID();
        livestockMasters.clear();
        mFarmItem = ((FarmActivity) getActivity()).getFarmById(farmID);
        mFarmerItem = ((FarmActivity) getActivity()).getFarmer();
        livestockMasters.addAll(((FarmActivity) getActivity()).getLivestockMasters());
        userID = Integer.toString(AppUtils.getUserId(getActivity()));


        addTotalSpinner();
        getListStandard();
        getListHelp();
        setRadioGroup();

        setRadio();

        if (AppUtils.isOnline(getActivity())) {
            fetchFarmStandard();
        } else {
            farmStds.clear();
            farmStds.addAll(((FarmActivity) getActivity()).getFarmById(farmID).getFarmStandard());

            oldFarmStds.clear();
            oldFarmStds.addAll(((FarmActivity) getActivity()).getFarmById(farmID).getFarmStandard());

            mWorkerItem = ((FarmActivity) getActivity()).getFarmById(farmID).getWorker();

            initList();
        }

    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        radioGroup = (RadioGroup) rootView.findViewById(R.id.radio);
        radisoOwner = (RadioButton) rootView.findViewById(R.id.radioOwner);
        radioNotOwner = (RadioButton) rootView.findViewById(R.id.radioNotOwner);
        llAddStandard = (LinearLayout) rootView.findViewById(R.id.llAddStandard);
        btnAddView = (LinearLayout) rootView.findViewById(R.id.btnAdd);

    }

    private void fetchFarmStandard() {

        progressBar.setVisibility(View.VISIBLE);

        Api api = AppUtils.getApiService();
        Call<ResponseFarmStandard> call = api.getFarmStandard(Integer.toString(farmID));
//        Call<ResponseFarmStandard> call = api.getFarmStandard("2674050");
        call.enqueue(new Callback<ResponseFarmStandard>() {
            @Override
            public void onResponse(Call<ResponseFarmStandard> call, Response<ResponseFarmStandard> response) {
                progressBar.setVisibility(View.GONE);
                if (isAdded()) {
                    if (response.body() != null) {
                        farmStds.clear();
                        farmStds.addAll(response.body().getData());

                        oldFarmStds.clear();
                        oldFarmStds.addAll(response.body().getData());

                        initList();

                    } else {
                        try {
                            Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseFarmStandard> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void initList() {
        listFarmStds.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//        listFarmStds.setNestedScrollingEnabled(false);
        ListAdapter listFarmStdsAdapter = new ListAdapter();
        listFarmStds.setAdapter(listFarmStdsAdapter);

    }


    private void setTypeFaceRadio() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), getResources().getString(R.string.font_en_light));
        radisoOwner.setTypeface(tf);
        radioNotOwner.setTypeface(tf);


            radio_farm.setTypeface(tf);
            radio_not_farm.setTypeface(tf);

    }

    private void setFarmerInfo() {

//        txtBD.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View views) {
//                setDate();
//            }
//        });
//        try {
//            txtBD.setText(AppUtils.convertDate(mFarmerItem.getBirthDay()));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        txtNameEN.setText("");
//        txtLastnameEN.setText("");


        txtPID.setText(AppUtils.checkNullLong(mFarmerItem.getPid()));
        txtNameTH.setText(mFarmerItem.getFirstName());
        txtLastnameTH.setText(mFarmerItem.getLastName());
        txtHomeNo.setText(mFarmerItem.getHomeNo());
//        txtMoo.setText(mFarmerItem.getMoo());
        txtPost.setText(mFarmItem.getPostCode());
        txtTel.setText(mFarmerItem.getPhone());
        txtPhone.setText(mFarmerItem.getMobile());
        txtLat.setText(mFarmerItem.getLatitude());
        txtLong.setText(mFarmerItem.getLongitude());
        if (mFarmerItem.getRevenue() != null && mFarmerItem.getRevenue() != 0) {
            txtRevenue.setText(mFarmerItem.getRevenue()+"");
        }
        txtDebt.setText(AppUtils.checkNullInteger(mFarmerItem.getDebtAmount()));



    }

    private void setWorkerInfo() {
        // try {
//            txtBD.setText(AppUtils.convertDate(mWorkerItem));
//        }
//        txtBD.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setDate();
//            }
//        });
        if (mWorkerItem == null) {
            txtPID.setText("");
            txtNameTH.setText("");
            txtLastnameTH.setText("");
//        txtMoo.setText(mWorkerItem.getMoo());
            txtHomeNo.setText("");
            txtPost.setText("");
            txtTel.setText("");
            txtPhone.setText("");
        } else {
            txtPID.setText(AppUtils.checkNullLong(mWorkerItem.getPid()));
            txtNameTH.setText(mWorkerItem.getFirst_Name());
            txtLastnameTH.setText(mWorkerItem.getLast_Name());
//        txtMoo.setText(mWorkerItem.getMoo());
            txtHomeNo.setText(mWorkerItem.getHomeNo());
            txtPost.setText(AppUtils.checkNullInteger(mWorkerItem.getPostCode()));
            txtTel.setText(mWorkerItem.getMobile());
            txtPhone.setText(mWorkerItem.getPhone());
        }


        getListPrefix();
        getListProvince();
    }

    private void setFarmInfo() {

        txtRevenue.setText(AppUtils.checkNullDouble(mFarmItem.getRevenueOfLivestock()));
        txtAnimalWorth.setText(AppUtils.checkNullDouble(mFarmItem.getAnimalWorth()));
        txtRai.setText(AppUtils.checkNullDouble(mFarmItem.getAreaRai()));
        txtNgan.setText(AppUtils.checkNullDouble(mFarmItem.getAreaNgan()));
        txtDebt.setText(AppUtils.checkNullDouble(mFarmItem.getDebtAmount()));
        txtWaa.setText(AppUtils.checkNullDouble(mFarmItem.getArea_Wa()));
        txtMachine.setText(mFarmItem.getHolding_Equipment());
        txtProblem.setText(mFarmItem.getFarmProblemDesc());

//        if (mFarmItem.getGovernment_Help_ID()!= null &&mFarmItem.getGovernment_Help_ID() != 0) {
//            txtHelp.setText(AppUtils.checkNullInteger(mFarmItem.getGovernment_Help_ID()));
//        }

    }

    private void setInfoFarmToUpdate() {
        mFarmItem.setRevenueOfLivestock(AppUtils.getDoubleFromEdt(txtRevenue.getText().toString()));
        mFarmItem.setAnimalWorth(AppUtils.getDoubleFromEdt(txtAnimalWorth.getText().toString()));
        mFarmItem.setAreaRai(AppUtils.getDoubleFromEdt(txtRai.getText().toString()));
        mFarmItem.setAreaNgan(AppUtils.getDoubleFromEdt(txtNgan.getText().toString()));
        mFarmItem.setFarmProblemDesc(AppUtils.getStringFromEdt(txtProblem.getText()));
        mFarmItem.setDebtAmount(AppUtils.getDoubleFromEdt(txtDebt.getText().toString()));
        mFarmItem.setHolding_Equipment(AppUtils.getStringFromEdt(txtMachine.getText()));
//        mFarmItem.setGovernment_Help_ID(AppUtils.getIntegerFromEdt(txtHelp.getText().toString()));

        if (radio_farm.isChecked()==true){
            mFarmItem.setIsFarm(0);
        }else {
            mFarmItem.setIsFarm(1);
        }
    }

    private void setInfoFarmerToUpdate(Farmer mFarmerItem) {
        mFarmerItem.setPid(AppUtils.tryParseLong(txtPID.getText().toString()));
        mFarmerItem.setFirstName(AppUtils.getStringFromEdt(txtNameTH.getText()));
        mFarmerItem.setLastName(AppUtils.getStringFromEdt(txtLastnameTH.getText()));
//        mFarmerItem.setMoo(AppUtils.getStringFromEdt(txtMoo.getText()));
        mFarmerItem.setHomeNo(AppUtils.getStringFromEdt(txtHomeNo.getText()));
        mFarmerItem.setPostCode(AppUtils.getStringFromEdt(txtPost.getText()));
        mFarmerItem.setMobile(AppUtils.getStringFromEdt(txtTel.getText()));
        mFarmerItem.setPhone(AppUtils.getStringFromEdt(txtPhone.getText()));
    }

    private void setInfoWorkerToUpdate( Farm farm ) {
        if (!isOldWorker)
            mWorkerItem.setWorker_ID("0");
        else
            mWorkerItem.setWorker_ID(farm.getWorkerID());

        mWorkerItem.setStatus_ID(1);
        mWorkerItem.setPid(AppUtils.tryParseLong(txtPID.getText().toString()));
        mWorkerItem.setFirst_Name(AppUtils.getStringFromEdt(txtNameTH.getText()));
        mWorkerItem.setLast_Name(AppUtils.getStringFromEdt(txtLastnameTH.getText()));
//        mWorkerItem.setMoo(AppUtils.getStringFromEdt(txtMoo.getText()));
        mWorkerItem.setHomeNo(AppUtils.getStringFromEdt(txtHomeNo.getText()));
        mWorkerItem.setPostCode(AppUtils.getIntegerFromEdt(txtPost.getText().toString()));
        mWorkerItem.setMobile(AppUtils.getStringFromEdt(txtTel.getText()));
        mWorkerItem.setPhone(AppUtils.getStringFromEdt(txtPhone.getText()));
        mWorkerItem.setProvince_ID(provinceID);
        mWorkerItem.setAmphur_ID(amphurID);
        mWorkerItem.setTambol_ID(tambolID);
        mWorkerItem.setVillage_ID(villageID);
    }

    public void addTotalSpinner(){

        prefix.clear();
        province.clear();
        amphur.clear();
        tambol.clear();
        village.clear();

        listPrefix.clear();
        listProvince.clear();
        listAmphur.clear();
        listTambol.clear();
        listVillage.clear();

        PrefixEntity prefixEntity = new PrefixEntity();
        prefixEntity.setPrefix_NameTh("กรุณาเลือกคำนำหน้าชื่อ");
        prefixEntity.setPrefix_ID(-99);

        prefix.add(prefixEntity);

        ProvinceEntity provinceEntity = new ProvinceEntity();
        provinceEntity.setProvince_NameTh("กรุณาเลือกจังหวัด");
        provinceEntity.setProvince_ID(-99);

        province.add(provinceEntity);

        AmphurEntity amphurEntity = new AmphurEntity();
        amphurEntity.setAmphur_nameTh("กรุณาเลือกอำเภอ");
        amphurEntity.setAmphur_ID(-99);

        amphur.add(amphurEntity);

        TambolEntity tambolEntity = new TambolEntity();
        tambolEntity.setTambol_NameTh("กรุณาเลือกตำบล");
        tambolEntity.setTambol_ID(-99);

        tambol.add(tambolEntity);

        VillageEntity villageEntity = new VillageEntity();
        villageEntity.setVillage_Name("กรุณาเลือกหมู่บ้าน");
        villageEntity.setVillage_ID(-99);

        village.add(villageEntity);
    }


    int PrefixID;

    public void getListPrefix() {
        int selectIndex = -99;
        prefix.addAll(dbHelper.getPrefixList());

        for (int i = 0; i < prefix.size(); i++) {
            listPrefix.add(prefix.get(i).getPrefix_NameTh());
            Log.e("PREFIX", prefix.get(i).getPrefix_NameTh());
            if (mWorkerItem != null) {
                if (mWorkerItem.getPrefix_ID() != null) {
                    if (prefix.get(i).getPrefix_ID() == mWorkerItem.getPrefix_ID()) {
                        selectIndex = i;
                    }
                }
            }

        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listPrefix);

        spnPrefix.setAdapter(adapter);

        spnPrefix.setSelection(selectIndex);

        spnPrefix.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (prefix != null) {
//                mFarmerItem.setPrefixID(prefix.get(position).getPrefix_ID());
//                    TextView tv = (TextView) parent.getSelectedView();
//                    tv.setText(prefix.get(position).getPrefix_NameTh());
                    PrefixID = prefix.get(position).getPrefix_ID();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    int provinceID;

    public void getListProvince() {
        int selectIndex = -99;
        province.clear();
        listProvince.clear();
        province.addAll(dbHelper.getProvinceList());

        for (int i = 0; i < province.size(); i++) {
            listProvince.add(province.get(i).getProvince_NameTh());
            if (mWorkerItem != null) {
                if (mWorkerItem.getProvince_ID() != null) {
                    if (province.get(i).getProvince_ID() == mWorkerItem.getProvince_ID()) {
                        selectIndex = i;
                    }
                }

            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listProvince);

        spnProvince.setAdapter(adapter);

        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                TextView tv = (TextView) parent.getSelectedView();
                if(tv!=null) {
                    tv.setText(province.get(position).getProvince_NameTh());
                }

                getListAmphur(province.get(position).getProvince_ID());
                if (position > 0) {
                    spnAmphur.setEnabled(true);
                }
                provinceID = province.get(position).getProvince_ID();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final int finalSelectIndex = selectIndex;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                spnProvince.setSelection(finalSelectIndex);
            }
        }, 100);


    }

    int amphurID;

    public void getListAmphur(int position) {
        int selectIndex = -99;

        amphur.clear();
        listAmphur.clear();
        amphur.addAll(dbHelper.getAmphur(position));

        for (int i = 0; i < amphur.size(); i++) {
            listAmphur.add(amphur.get(i).getAmphur_nameTh());
            if (mWorkerItem != null) {
                if (mWorkerItem.getAmphur_ID() != null) {
                    if (amphur.get(i).getAmphur_ID() == mWorkerItem.getAmphur_ID()) {
                        selectIndex = i;
                    }
                }
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listAmphur);
        spnAmphur.setAdapter(adapter);

        spnAmphur.setSelection(selectIndex);

        spnAmphur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                getListTambol(amphur.get(position).getAmphur_ID());
                if (position > 0) {
                    spnTambol.setEnabled(true);
                }
//                mFarmerItem.setAmphurID(amphur.get(position).getAmphur_ID());

                amphurID = amphur.get(position).getAmphur_ID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        final int finalSelectIndex = selectIndex;
        final int finalSelectIndex = selectIndex;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                spnAmphur.setSelection(finalSelectIndex);
            }
        }, 100);
    }

    int tambolID;

    public void getListTambol(int position) {
        int selectIndex = -99;

        listTambol.clear();
        tambol.clear();
        tambol.addAll(dbHelper.getTambol(position));

        for (int i = 0; i < tambol.size(); i++) {
            listTambol.add(tambol.get(i).getTambol_NameTh());
            if (mWorkerItem != null) {
                if (mWorkerItem.getWorker_ID() != null) {
                    if (tambol.get(i).getTambol_ID() == mWorkerItem.getTambol_ID()) {
                        selectIndex = i;
                    }
                }
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listTambol);
        spnTambol.setAdapter(adapter);

        spnTambol.setSelection(selectIndex);

        spnTambol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getListVillage(tambol.get(position).getTambol_ID());
                tambolID = tambol.get(position).getTambol_ID();
                zipcode = tambol.get(position).getZipcode();
                txtPost.setText(zipcode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final int finalSelectIndex = selectIndex;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                spnTambol.setSelection(finalSelectIndex);
            }
        }, 100);
    }

    int villageID;

    public void getListVillage(int position) {

        int selectIndex = -99;

        listVillage.clear();
        village.clear();
        village.addAll(dbHelper.getVillage(position));

        for (int i = 0; i < village.size(); i++) {
            listVillage.add(village.get(i).getVillage_Name());
            if (mWorkerItem != null) {
                if (mWorkerItem.getWorker_ID() != null && mWorkerItem.getVillage_ID() != null) {
                    if (village.get(i).getVillage_ID() == mWorkerItem.getVillage_ID()) {
                        selectIndex = i;

                    }
                }
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                listVillage);
        spnVillage.setAdapter(adapter);

        spnVillage.setSelection(selectIndex);

        spnVillage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                villageID = village.get(position).getVillage_ID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

//    public void setDate(){
//
//        final Calendar c = Calendar.getInstance();
//        int year = c.get(Calendar.YEAR);
//        int month = c.get(Calendar.MONTH);
//        int day = c.get(Calendar.DAY_OF_MONTH);
//
//        DatePickerDialog datePickerDialog =new DatePickerDialog(getActivity(),
//                new DatePickerDialog.OnDateSetListener() {
//
//                    @Override
//                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
//                        int month=i1+1;
//
//                        Calendar calendar = Calendar.getInstance();
//                        calendar.set(i, i1, i2);
//
//                        txtBD.setText(i2+"/"+month+"/"+i);
//                        date = calendar.getTimeInMillis();
//
//                        editText_date.setText(date2+"");
//                    }
//                }, year, month,day);
//        datePickerDialog.show();
//
//    }

    public void getListHelp() {
        helps.clear();
        listHelps.clear();
        int selectIndex = -1;

        HelpEntity helpEntity =new HelpEntity();
        helpEntity.setGovernment_Help_Name("กรุณาเลือกความต้องการช่วยเหลือ");
        helpEntity.setGovernment_Help_ID(-99);
        helps.add(helpEntity);
        helps.addAll(dbHelper.getHelp());

        for (int i = 0; i < helps.size(); i++) {
            listHelps.add(helps.get(i).getGovernment_Help_Name());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listHelps);
        spnHelp.setAdapter(adapter);

        spnHelp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mFarmerItem!=null){
                    mFarmerItem.setGovernmentHelpID(helps.get(position).getGovernment_Help_ID());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        for (int i = 0; i < helps.size(); i++) {
            if (mFarmerItem.getGovernmentHelpID()  != null)
                if (helps.get(i).getGovernment_Help_ID() == mFarmerItem.getGovernmentHelpID()) {
                    selectIndex = i;
                }
        }

        spnHelp.setSelection(selectIndex);
    }

    int getUniqueLocalFeedId() {
        int maxId = -1;
        for (FarmStandard farmStd : farmStds) {
            maxId = Math.max(farmStd.getStandardID(), maxId);
        }
        return maxId + 1;
    }

    @OnClick(R.id.btnAdd)
    public void clickBtnAddStdSupportView() {
        FarmStandard farmStandard = new FarmStandard();
        farmStds.add(farmStandard);
        ListAdapter listFarmStdsAdapter = new ListAdapter();
        listFarmStds.setAdapter(listFarmStdsAdapter);
    }


//    public void refreshFarmStdLayouts() {
//
//        llAddStandard.removeAllViews();
//
//        for (FarmStandard farmStd : farmStds) {
//            newFarmStds.add(createStandardView(farmStd));
//            llAddStandard.addView(createStandardView(farmStd));
//        }
//    }

    private void getWorkerInfoFromAPi(Integer workerId) {

        Api api = AppUtils.getApiService();
        Call<ResponseWorker> call = api.getWorker(String.valueOf(workerId));
        call.enqueue(new Callback<ResponseWorker>() {
            @Override
            public void onResponse(Call<ResponseWorker> call, Response<ResponseWorker> response) {
                progressBar.setVisibility(View.GONE);
                if (isAdded()) {
                    if (response.body() != null && response.body().getResponseCode().equals(getResources().getString(R.string.success))) {
                        isOldWorker = true;
                        if(response.body().getData().size()!=0) {
                            mWorkerItem = response.body().getData().get(0);
                            setWorkerInfo();
                        } else {
                            mWorkerItem = null;
                            setWorkerInfo();
                        }
                    } else {
                        try {
                            Toast.makeText(getActivity(), response.body().getResponseMessage(), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseWorker> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                try {
                    t.getMessage();
                }catch (Exception e){
                    e.getMessage();
                }
            }
        });
    }


    private void setRadioGroup() {
        radioGroup.clearCheck();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                RadioButton rbOwn = (RadioButton) group.findViewById(checkedId);
                if (null != rbOwn && checkedId > -1) {
//                    Toast.makeText(getContext(), rbOwn.getText(), Toast.LENGTH_SHORT).show();
                    if (rbOwn.getText() == getString(R.string.owner)) {
                        isFarmer = true;
                        isWorker = false;

                        llAddressInfo.setVisibility(View.GONE);

//                        setFarmerInfo();
                    } else {
                        if(mWorkerItem==null) {
                            mWorkerItem = new Worker();
                        }
                        isWorker = true;
                        isFarmer = false;

                        llAddressInfo.setVisibility(View.VISIBLE);

                        setWorkerInfo();
                    }
                }
            }
        });

    }


    private void setRadio() {
        String worker = mFarmItem.getWorkerID();
        if (worker == null||worker.equals("0") ||worker.equals("")||!checkStringNumeric(worker)) {
            radisoOwner.setChecked(true);
//            setFarmerInfo();
            llAddressInfo.setVisibility(View.GONE);

        } else {
            radioNotOwner.setChecked(true);
//            setWorkerInfo();
            llAddressInfo.setVisibility(View.VISIBLE);
            if (AppUtils.isOnline(getActivity()))
                getWorkerInfoFromAPi(Integer.parseInt(worker));
        }

        Integer isFarm = mFarmItem.getIsFarm();
        if (isFarm!=null&&isFarm==0) {
            radio_farm.setChecked(true);
        } else {
            radio_not_farm.setChecked(true);
        }

        setFarmInfo();
    }

    private boolean checkStringNumeric(String s){

        if(s.matches("\\d+(?:\\.\\d+)?"))
        {
            return true;
//            System.out.println("Matches");
        }
        else
        {
            return false;
//            System.out.println("No Match");
        }
    }


    public void onClear(View v) {
        /* Clears all selected radio buttons to default */
        radioGroup.clearCheck();
    }

    public void onSubmit(View v) {
        RadioButton rb = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
        Toast.makeText(getContext(), rb.getText(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }


    public void getListStandard() {
        listStandard = dbHelper.getStandard();
    }

    int stdId;

    public void setSpnStandard(final FarmStandard farmStd, Spinner spn) {
        TypeFacedArrayAdapter<StandardEntity> adapter = new TypeFacedArrayAdapter<StandardEntity>(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listStandard);
        spn.setAdapter(adapter);

        spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                stdId = listStandard.get(position).getStandard_ID();
                farmStd.setStandardID(stdId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (farmStd.getStandardID() != null && farmStd.getStandardID() != 0 && farmStd.getStandardID() < 4) {
            int position = -1;
            for (int i = 0; i < listStandard.size(); i++) {
                if (listStandard.get(i).getStandard_ID() == farmStd.getStandardID()) {
                    position = i;
                    break;
                }
            }
//            if (BuildConfig.DEBUG && position < 0) {
//                throw new AssertionError();
//            }
            spn.setSelection(position);
        }
    }

    int livestockId;

    public void setSpnLivestockType(final FarmStandard farmStd, Spinner spn) {

        listTypes.addAll(dbHelper.getListTypeName());

        TypeFacedArrayAdapter<TypeEntity> adapter = new TypeFacedArrayAdapter<TypeEntity>(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listTypes);
        spn.setAdapter(adapter);

        spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                livestockId = listTypes.get(position).getFarm_Standard_LivestockType_ID();
                farmStd.setLivestockTypeID(livestockId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (farmStd.getLivestockTypeID() != null && farmStd.getLivestockTypeID() != 0) {
            int position = -1;
            for (int i = 0; i < listTypes.size(); i++) {
                TypeEntity type = listTypes.get(i);
                if (type.getFarm_Standard_LivestockType_ID() == farmStd.getLivestockTypeID()) {
                    position = i;
                    break;
                }
            }
//            if (BuildConfig.DEBUG && position < 0) {
//                throw new AssertionError();
//            }
            spn.setSelection(position);
        }
    }

    public void updateText(Double xyzData) {
        if(txtAnimalWorth!=null&&xyzData!=null) {
            txtAnimalWorth.setText(Double.toString(xyzData));
        }
    }

    public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
        int i;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_standard, null);
            final ViewHolder viewHolder = new ViewHolder(view, new MyCustomEditTextListener());

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            i = position;
            final FarmStandard item = farmStds.get(position);

            setSpnStandard(item, holder.spnStd);

            setSpnLivestockType(item, holder.spnType);

            holder.txtFarmNo.setText(item.getStandardNumber());

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    farmStds.remove(position);
                    notifyDataSetChanged();

                }
            });


        }

        @Override
        public int getItemCount() {
            return farmStds.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {

            private final Spinner spnStd;
            private final Spinner spnType;
            private final MyEditNoHLTextView txtFarmNo;
            private final MyNoHLTextView delete;

            public ViewHolder(View itemView, MyCustomEditTextListener myCustomEditTextListener) {
                super(itemView);

                this.spnStd = (Spinner) itemView.findViewById(R.id.spnStandard);
                this.spnType = (Spinner) itemView.findViewById(R.id.spnLivestockType);
                this.txtFarmNo = (MyEditNoHLTextView) itemView.findViewById(R.id.txtFarmNo);
                this.delete = (MyNoHLTextView) itemView.findViewById(R.id.delete);

                this.txtFarmNo.addTextChangedListener(myCustomEditTextListener);
            }
        }

        private class MyCustomEditTextListener implements TextWatcher {
            private int position;

            public void updatePosition(int position, MyEditNoHLTextView editNoHLTextView) {
                this.position = position;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                farmStds.get(i).setStandardNumber(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        }
    }

    @OnClick(R.id.btnSubmit)
    public void clickBtnSubmit() {

        final Farmer farmer = ((FarmActivity) getActivity()).getFarmer();

        Farm farm = null;
        for (Farm f : farmer.getFarm()) {
            if (f.getFarmID() == farmID) {
                farm = f;
                break;
            }
        }

        farm.setUpdateBy(userID);
        farm.setUpdateDate(AppUtils.getDate());

        if (isWorker) {
            setInfoWorkerToUpdate(farm);
            farm.setWorker(mWorkerItem);

            if (!isOldWorker) {
                farm.setWorkerID(Integer.toString(0));
            }else{
                farm.setWorkerID(farm.getWorkerID());
            }

        }


            setInfoFarmToUpdate();
//        synchronizeToModel();


        HashSet<FarmStandard> newFarmStds = new HashSet<>(farmStds);

        HashSet<FarmStandard> deletedFarmStds = new HashSet<>(oldFarmStds);
        deletedFarmStds.removeAll(newFarmStds);

        farm.getFarmStandard().clear();

        for (FarmStandard farmStd : deletedFarmStds) {
            FarmStandard farmStdToUpload = new FarmStandard(farmStd);
            // delete
            farmStdToUpload.setStatusID(-9);
            farm.getFarmStandard().add(farmStdToUpload);
        }

        for (FarmStandard farmStd : farmStds) {
            FarmStandard farmStdToUpload = new FarmStandard(farmStd);
            if (farmStdToUpload.isLocalOnly()) {
                // insert
                farmStdToUpload.setFarmStandardID(0);
                farmStdToUpload.setStatusID(1);
            } else {
                // update
                farmStdToUpload.setStatusID(1);
            }

            farm.getFarmStandard().add(farmStdToUpload);
        }


        if (AppUtils.isOnline(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            AppUtils.updateData(farmer, new UpdateListener() {
                @Override
                public void onSuccess(String message, ResponseInsertFarmer res) {
                    // update data from res to farmer here
//                    for (int i =0;i<res.getFarmerList().get(0).getFarmList().size();i++){
//                        Farm farmRes = res.getFarmerList().get(0).getFarmList().get(i).getFarm();
////                        Farm farm1 = farmer.getFarm().get(0);
//                        if (farmRes.getFarmID()==farmID){
//
//                            for (int j = 0;j<farm1.getFarmStandard().size();j++){
//                                FarmStandard farmStandardDatabase = farm1.getFarmStandard().get(j);
//                                if (farmStandardDatabase.getFarmStandardID()==0){
//
//                                   /* for (int k ;k<farmRes.get)
//                                    if (farmStandardDatabase.getStandardNumber().equals())
//                                    farm1.getFarmStandard().get(j).setFarmStandardID();*/
//                                }
//                            }
//
//                            farmer.getFarm().get(0).setStandardID(farmRes.getStandardID());
//                        }
//                    }
//                    res.getFarmerList().get(0).getFarmer().getStatusID();
                    progressBar.setVisibility(View.GONE);
                    RealmList<FarmStandard> oldStandard = null;
                    for (Farm f : farmer.getFarm()) {
                        if (f.getFarmID() == farmID) {
                            oldStandard = f.getFarmStandard();
                            break;
                        }
                    }
                    RealmList<FarmStandard> newStandard = null;
                    for (FarmList fl : res.getFarmerList().get(0).getFarmList()) {
                        Farm f = fl.getFarm();
                        if (f.getFarmID() == farmID) {
                            newStandard = f.getFarmStandard();
                        }
                    }
                    oldStandard.clear();
                    oldStandard.addAll(newStandard);

                    if (isWorker&&!isOldWorker) { //case add worker

                        RealmObject oldWorker=null;
                        for (Farm f : farmer.getFarm()) {
                            if (f.getFarmID() == farmID) {
                                oldWorker = f.getWorker();
                                break;
                            }
                        }

                        RealmObject newWorker=null;
                        for (FarmList fl : res.getFarmerList().get(0).getFarmList()) {
                            Farm f = fl.getFarm();
                            if (f.getFarmID() == farmID) {
                                newWorker = f.getWorker();
                            }
                        }

                        oldWorker=newWorker;
                    }

                    databaseRealm.upDateDataFarmer(farmer);

                    Toast.makeText(getContext(), getResources().getString(R.string.update_success),
                            Toast.LENGTH_SHORT).show();
                    ((FarmActivity)getActivity()). getViewPager().setCurrentItem(2);
                }

                @Override
                public void onError(String message, ResponseInsertFarmer res) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "เกิดข้อผิดพลาด",
                            Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            databaseRealm.upDateDataFarmer(farmer);
            ((FarmActivity)getActivity()). getViewPager().setCurrentItem(2);
            Toast.makeText(getContext(), getResources().getString(R.string.update_success),
                    Toast.LENGTH_SHORT).show();

        }


    }
//
//    void synchronizeToModel() {
//        for (FarmStandard farmStd : farmStds) {
//            View listViews = newFarmStds.get(0);
//            synchronizeFarmStdViewToFarmStd(listViews,  farmStd);
//        }
//
//
//        for (int i = 0 ; i < farmStds.size(); i++){
//            View listView = listFarmStds.getRootView();
//            FarmStandard farmStd = farmStds.get(i);
//            synchronizeFarmStdViewToFarmStd(listView,  farmStd);
//        }
//    }
//
//    public void synchronizeFarmStdViewToFarmStd(View view, FarmStandard farmStd) {
//        Spinner spnStd = (Spinner) view.findViewById(R.id.spnStandard);
//        Spinner spnType = (Spinner) view.findViewById(R.id.spnLivestockType);
//        MyEditNoHLTextView txtFarmNo = (MyEditNoHLTextView) view.findViewById(R.id.txtFarmNo);
//
//        StandardEntity std = (StandardEntity) spnStd.getSelectedItem();
//        farmStd.setStandardID(std == null ? null : std.getStandard_ID());
//
//        farmStd.setStandardName(std == null ? null : std.getStandard_Name());
//
//        LivestockEntity livestock = (LivestockEntity) spnType.getSelectedItem();
//        farmStd.setLivestockTypeID(livestock.getLivestock_Type_ID());
//        farmStd.setLivestockTypeFullName(livestock.getLivestock_Type_FullName());
//
//        farmStd.setStandardNumber(txtFarmNo.getText().toString());
//
//    }


}
