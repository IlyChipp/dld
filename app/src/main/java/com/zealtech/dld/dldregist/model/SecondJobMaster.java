package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/27/2017 AD.
 */
@Parcel
public class SecondJobMaster implements APIResponse{
    //DBHelper
    public static final String TABLE = "second_job";

    String responseCode;
    String responseMessage;
    ArrayList<SecondJobEntity> data;

    public SecondJobMaster() { }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<SecondJobEntity> getData() {
        return data;
    }

    public void setData(ArrayList<SecondJobEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class SecondJobEntity {
        int second_Job_ID;
        int orderIndex;
        String second_Job_Name;
        int status_ID;

        public SecondJobEntity(){ }

        public int getSecond_Job_ID() {
            return second_Job_ID;
        }

        public void setSecond_Job_ID(int second_Job_ID) {
            this.second_Job_ID = second_Job_ID;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public String getSecond_Job_Name() {
            return second_Job_Name;
        }

        public void setSecond_Job_Name(String second_Job_Name) {
            this.second_Job_Name = second_Job_Name;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String SECOND_JOB_ID = "second_Job_ID";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String SECOND_JOB_NAME = "second_Job_Name";
        public static final String STATUS_ID = "status_ID";
    }
}
