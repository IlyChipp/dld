package com.zealtech.dld.dldregist.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.FarmActivity;
import com.zealtech.dld.dldregist.activity.FindFarmerActivity;
import com.zealtech.dld.dldregist.activity.LoginActivity;
import com.zealtech.dld.dldregist.activity.OfflineActivity;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.fragment.LiveStockFragment.CallUpdateLivestock;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.LivestockMaster.LivestockEntity;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.model.newFarmer.FarmList;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.Livestock;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseInsertFarmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseLivestock;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.util.UpdateListener;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListLiveStockFragment extends BaseBusFragment {

    public final String TAG = "ListLiveStockFragment";

    @BindView(R.id.progressBar)
    FrameLayoutDisable progressBar;

    @BindView(R.id.txt_sum)
    MyNoHBTextView txt_sum;

    @BindView(R.id.llLiveStock)
    LinearLayout llLiveStock;
    @BindView(R.id.listLivestock)
    ScrollView listLivestock;

    private RecyclerView recyclerListType;
//    private ListTypeAdapter listTypeAdapter;

    ArrayList<Livestock> livestockses=new ArrayList<>();
    ArrayList<LivestockEntity> livestockEntities=new ArrayList<>();
    final ArrayList<LivestockEntity> livestockMasters=new ArrayList<>();
    ArrayList<Livestock> outputs=new ArrayList<>();

    DatabaseRealm databaseRealm;
    int farmID;
    String userID="";

    public ArrayList<LivestockEntity> getLivestockEntities() {
        return livestockEntities;
    }

    public void setLivestockEntities(ArrayList<LivestockEntity> livestockEntities) {
        this.livestockEntities = null;
        this.livestockEntities = livestockEntities;
    }

    public void setFarmID(int farmID) {
        this.farmID = farmID;
    }

    public static ListLiveStockFragment newInstance(int rootId, ArrayList<LivestockEntity> livestockEntities
            , int farmID) {

        ListLiveStockFragment fragment = new ListLiveStockFragment();

        fragment.setLivestockEntities(livestockEntities);
        fragment.setFarmID(farmID);

        Bundle bundle = new Bundle();
        bundle.putInt("rootId", rootId);

        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_livestock, container, false);
        ButterKnife.bind(this, rootView);
//        initInstances(rootView);

        databaseRealm=new DatabaseRealm(getContext());

        setUpData();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

//    private void initInstances(View rootView) {
//        // Init 'View' instance(s) with rootView.findViewById here
//        recyclerListType = (RecyclerView) rootView.findViewById(R.id.recyclerLivestock);
//        llLiveStock = (LinearLayout) rootView.findViewById(R.id.llLiveStock);
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

//        setUpData();
    }

    public void setUpData() {

        if (getActivity() == null) {
            return;
        }
        livestockMasters.clear();
        livestockses.clear();

        llLiveStock.removeAllViews();

        userID = Integer.toString(AppUtils.getUserId(getActivity()));

        livestockMasters.addAll(((FarmActivity)getActivity()).getLivestockMasters());
//        livestockMasters.remove(0);

        if(AppUtils.isOnline(getActivity())) {
            getLiveStockInFarm(farmID);
        }else{
//            livestockses.addAll(databaseRealm.getDataLiveStock());
            livestockses.addAll(((FarmActivity) getActivity()).getFarmById(farmID).getLivestock());
            addValueToLivestock();
            initList();
        }
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//    }

    private void initInstances(View rootView) {
//        // Init 'View' instance(s) with rootView.findViewById here
//        recyclerListType = (RecyclerView) rootView.findViewById(R.id.recyclerLivestock);
//        llLiveStock = (LinearLayout) rootView.findViewById(R.id.llLiveStock);
    }

    private void getLiveStockInFarm(final int farmId) {

        listLivestock.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        Api api = AppUtils.getApiService();
        Call<ResponseLivestock> call = api.getLivestock(Integer.toString(farmId));
        call.enqueue(new Callback<ResponseLivestock>() {
            @Override
            public void onResponse(Call<ResponseLivestock> call, Response<ResponseLivestock> response) {
                progressBar.setVisibility(View.GONE);
                listLivestock.setVisibility(View.VISIBLE);
                if(isAdded()) {
                    if (response.body() != null) {
                        Farm farm_=((FarmActivity)getActivity()).getFarmById(farmId);

                        if(farm_.getLivestock()==null||farm_.getLivestock().size()==0){ //get from api when first time (first time livestock is null)
                            RealmList<Livestock> livestocks=new RealmList<Livestock>();
                            livestocks.addAll(response.body().getData());
                            farm_.setLivestock(livestocks);
                        }
                        livestockses.addAll(farm_.getLivestock());

                        addValueToLivestock();
                        initList();
                    } else {
                        try {
                            Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseLivestock> call, Throwable t) {
                listLivestock.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }

        });
    }

//    private void insertLiveStock(ArrayList<Livestock> livestocks) {
//
//        final ProgressBar progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleSmall);
//        progressBar.setVisibility(View.VISIBLE);
//
//        Gson gson = new Gson();
//        String json = gson.toJson(livestocks);
//
//        Api api = AppUtils.getApiService();
//        Call<LivestockModel> call = api.insertLivestock(json);
//        call.enqueue(new Callback<LivestockModel>() {
//            @Override
//            public void onResponse(Call<LivestockModel> call, Response<LivestockModel> response) {
//                progressBar.setVisibility(View.GONE);
//                if (response.body() != null) {
//                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_LONG).show();
//                } else {
//                    try {
//                        Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_LONG).show();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<LivestockModel> call, Throwable t) {
//                progressBar.setVisibility(View.GONE);
//            }
//
//        });
//    }

    private void addValueToLivestock(){
        for(int i=0;i<livestockEntities.size();i++){
            for(int j=0;j<livestockses.size();j++){
                if(livestockEntities.get(i).getLivestock_Type_ID()==livestockses.get(j).getLivestockTypeID()){
                    livestockEntities.get(i).setLivestock_Amount(livestockses.get(j).getLivestockAmount());
                    livestockEntities.get(i).setLivestock_Values(livestockses.get(j).getLivestockValues());
                }
            }
        }
    }

    private void initList(){

        for(int i=0;i<livestockEntities.size();i++){

            int level = checkLevel(livestockEntities.get(i).getLivestock_Type_ID(), 0);

                if (level == 1) {
                    final View view = getLayoutInflater().inflate(R.layout.item_livestock, null);

                    MyNoHBTextView listAnimalType = (MyNoHBTextView) view.findViewById(R.id.listAnimalType);
                    MyEditNoHLTextView txtQuantity = (MyEditNoHLTextView) view.findViewById(R.id.txtQuantity);
                    MyEditNoHLTextView txtPrice = (MyEditNoHLTextView) view.findViewById(R.id.txtAnimalWorth);
                    LinearLayout layout_price = (LinearLayout) view.findViewById(R.id.layout_price);
                    MyNoHLTextView txtUnit = (MyNoHLTextView) view.findViewById(R.id.txtUnit);

                    txtPrice.setVisibility(View.GONE);

                    listAnimalType.setText(livestockEntities.get(i).getLivestock_Type_Name());

                    if(hasChild(livestockEntities.get(i).getLivestock_Type_ID())){
                        layout_price.setVisibility(View.GONE);
                    }else{
                        layout_price.setVisibility(View.VISIBLE);
                        txtQuantity.setVisibility(View.VISIBLE);
                        txtUnit.setVisibility(View.VISIBLE);
                        txtUnit.setText(getUnit(livestockEntities.get(i).getLivestock_Type_ID()));

                        if (livestockEntities.get(i).getLivestock_Amount() == 0) {
                            txtQuantity.setText("");
                            txtQuantity.setHint("0");
                        } else {
                            String amount= Integer.toString(livestockEntities.get(i).getLivestock_Amount());
                            txtQuantity.setText(amount);
                        }

                        if (livestockEntities.get(i).getLivestock_Values() == null) {
                            txtPrice.setText("");
                            txtPrice.setHint("0");
                        } else {
                            txtPrice.setText(Double.toString(livestockEntities.get(i).getLivestock_Values()));
                        }
                    }

                    setTextChangeListener(txtQuantity,txtPrice,i);

                    llLiveStock.addView(view);

                } else {
                    final View view = getLayoutInflater().inflate(R.layout.item_list_livestock, null);

                    MyNoHLTextView txtSubType = (MyNoHLTextView) view.findViewById(R.id.txtSubType);
                    MyEditNoHLTextView txtQuantity = (MyEditNoHLTextView) view.findViewById(R.id.txtQuantity);
                    MyEditNoHLTextView txtPrice = (MyEditNoHLTextView) view.findViewById(R.id.txtAnimalWorth);
                    FrameLayout margin=(FrameLayout) view.findViewById(R.id.margin);
                    MyNoHLTextView txtUnit = (MyNoHLTextView) view.findViewById(R.id.txtUnit);

                    txtPrice.setVisibility(View.GONE);

                    final int i_ = i;

                    int valueInPixels = (int) getResources().getDimension(R.dimen.activity_horizontal_margin) * (level);
                    margin.getLayoutParams().width = valueInPixels;

                    txtSubType.setText(livestockEntities.get(i).getLivestock_Type_Name());

                    if(livestockEntities.get(i).getLivestock_Type_Flgtail().equals("y")){
                        txtUnit.setText(getUnit(livestockEntities.get(i).getLivestock_Type_ID()));
                        txtQuantity.setVisibility(View.VISIBLE);
                        txtUnit.setVisibility(View.VISIBLE);
//                        txtPrice.setVisibility(View.VISIBLE);
                    }else{
                        txtUnit.setVisibility(View.GONE);
                        txtQuantity.setVisibility(View.GONE);
//                        txtPrice.setVisibility(View.GONE);
                    }

                    if (livestockEntities.get(i).getLivestock_Amount() == 0) {
                        txtQuantity.setText("");
                        txtQuantity.setHint("0");
                    } else {
                        String amount= Integer.toString(livestockEntities.get(i).getLivestock_Amount());
                        txtQuantity.setText(amount);
                    }

                    if (livestockEntities.get(i).getLivestock_Values() == null) {
                        txtPrice.setText("");
                        txtPrice.setHint("0");
                    } else {
                        txtPrice.setText(Double.toString(livestockEntities.get(i).getLivestock_Values()));
                    }

                    setTextChangeListener(txtQuantity,txtPrice,i);

                    llLiveStock.addView(view);
                }


        }

        updateSum();

//        recyclerListType.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//        listTypeAdapter = new ListTypeAdapter();
//        recyclerListType.setItemViewCacheSize(livestockEntities.size()-1);
//        recyclerListType.setAdapter(listTypeAdapter);
    }

    private void setTextChangeListener(MyEditNoHLTextView txtQuantity,MyEditNoHLTextView txtPrice
            ,final int pos){
        txtPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String value = charSequence.toString();

                try {
                    if (value.equals("")) {
                        livestockEntities.get(pos).setLivestock_Values(AppUtils.tryParseDouble(value));
                        updateSum();
                    }
                }catch (Exception e){

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        txtQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String value = charSequence.toString();

                if (!value.equals("")) {
                    livestockEntities.get(pos).setLivestock_Amount(Integer.parseInt(value));
                    updateSum();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    private boolean hasChild(int type_id){

        for(int i=0;i<livestockMasters.size();i++){
            if(livestockMasters.get(i).getLivestock_Type_Parent()==type_id){
                return true;
            }
        }

        return false;
    }

    private boolean isRoot(int type_id) {
        for (int i = 0; i < livestockMasters.size(); i++) {
            if (livestockMasters.get(i).getLivestock_Type_ID() == type_id) {
                if (livestockMasters.get(i).getLivestock_Type_Parent() == -1) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    private int checkLevel(int type_id, int level) {
        int level_ = 0;
        for (int i = 0; i < livestockMasters.size(); i++) {
            if (livestockMasters.get(i).getLivestock_Type_ID() == type_id) {
                if (livestockMasters.get(i).getLivestock_Type_Parent() == -1) {
                    return level;
                } else {
                    level += 1;
                    level_ = checkLevel(livestockMasters.get(i).getLivestock_Type_Parent(), level);
                }
            }
        }
        return level_;
    }

    private void updateSum(){
        Double sum=0.0;

        for(int i=0;i<livestockEntities.size();i++){
            int amount=livestockEntities.get(i).getLivestock_Amount();
            Double value=livestockEntities.get(i).getLivestock_Type_Price().doubleValue();
            if(value==null){
                value=new Double(0);
            }
            sum+=value*amount;
//            sum=sum.add(value.multiply(new BigDecimal(amount)));
        }

        ArrayList<Livestock> filter=new ArrayList<>();
        for( int j = 0; j < livestockses.size(); j++) {
            boolean isInCurrentList=false;
            for (int i = 0; i < livestockEntities.size(); i++) {
                if (livestockEntities.get(i).getLivestock_Type_ID() == livestockses.get(j).getLivestockTypeID()) {
                    isInCurrentList=true;
                    break;
                }
            }
            if(!isInCurrentList){
                filter.add(livestockses.get(j));
            }
        }


        Double sum_other = 0.0;
        for (int j = 0; j < filter.size(); j++) {
            sum_other += getPrice(filter.get(j).getLivestockTypeID()) * filter.get(j).getLivestockAmount();
        }

//        final Farmer farmer=((FarmActivity)getActivity()).getFarmer();
//
//        for(int i=0;i< farmer.getFarm().size();i++){
//            if( farmer.getFarm().get(i).getFarmID()==farmID) {
//                farmer.getFarm().get(i).setAnimalWorth(sum);
//            }
//        }
        txt_sum.setText(0+" บาท");
//        txt_sum.setText(Double.toString(sum+sum_other)+" บาท");
    }

    private Double getSum(){
        Double sum=0.0;

        for(int i=0;i<livestockEntities.size();i++){
            int amount=livestockEntities.get(i).getLivestock_Amount();
            Double value=livestockEntities.get(i).getLivestock_Values();
            if(value==null){
                value=new Double(0);
            }
            sum+=value*amount;
//            sum=sum.add(value.multiply(new BigDecimal(amount)));
        }
        return sum;
    }

    private int getExistLiveID(int type_id){
        int id=0;

        for(int i=0;i<livestockses.size();i++){
            if(livestockses.get(i).getLivestockTypeID()==type_id){
                id=livestockses.get(i).getLivestockID();
            }
        }

        return id;
    }

    private String getUnit(int type_id){
        for(int i=0;i<livestockMasters.size();i++){
            if(livestockMasters.get(i).getLivestock_Type_ID()==type_id) {
                return livestockMasters.get(i).getLivestock_Type_Unit();
            }
        }
        return "";
    }

    private Double getPrice(int type_id){
        for(int i=0;i<livestockMasters.size();i++){
            if(livestockMasters.get(i).getLivestock_Type_ID()==type_id) {
                return livestockMasters.get(i).getLivestock_Type_Price().doubleValue();
            }
        }
        return 0.0;
    }

//    public class ListTypeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//
//        RecyclerView mRecyclerView;
//
//        @Override
//        public int getItemViewType(int position) {
//            if(position==livestockEntities.size()){ //last position
//                return 2;
//            }else {
//                int level = checkLevel(livestockEntities.get(position).getLivestock_Type_ID(), 0);
//                if (level == 1) {
//                    return 0;
//                } else {
//                    return 1;
//                }
//            }
//        }
//
//        @Override
//        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//
//            mRecyclerView = (RecyclerView) parent;
//
//            switch (viewType) {
//                case 0:
//                    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_livestock, parent, false);
//                    return new ListTypeHolder(v,new EditTextAmountListener(),new EditTextValueListener());
//                case 1:
//                    View views = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_livestock, parent, false);
//                    return new ListLiveStockHolder(views,new EditTextAmountListener(),new EditTextValueListener());
//                case 2:
//                    View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_livestock_footer, parent, false);
//                    return new FooterHolder(view2);
//            }
//            return null;
//        }
//
//        @Override
//        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//            switch (holder.getItemViewType()) {
//                case 0:
//                    ListTypeHolder listTypeHolder = (ListTypeHolder) holder;
//                    listTypeHolder.mlistAnimalType.setText(livestockEntities.get(position).getLivestock_Type_Name());
//
//                    if(hasChild(livestockEntities.get(position).getLivestock_Type_ID())){
//                        listTypeHolder.layout_price.setVisibility(View.GONE);
//                    }else{
//                        listTypeHolder.layout_price.setVisibility(View.VISIBLE);
//
//                        if (livestockEntities.get(position).getLivestock_Amount() == 0) {
//                            listTypeHolder.mtxtQuantity.setText("");
//                            listTypeHolder.mtxtQuantity.setHint("0");
//                        } else {
//                            String amount= Integer.toString(livestockEntities.get(position).getLivestock_Amount());
//                            listTypeHolder.mtxtQuantity.setText(amount);
//                        }
//
//                        if (livestockEntities.get(position).getLivestock_Values() == null) {
//                            listTypeHolder.mtxtPrice.setText("");
//                            listTypeHolder.mtxtPrice.setHint("0");
//                        } else {
//                            listTypeHolder.mtxtPrice.setText(Double.toString(livestockEntities.get(position).getLivestock_Values()));
//                        }
//                    }
//
//                    listTypeHolder.editTextAmountListener.updatePosition(holder.getAdapterPosition());
//                    listTypeHolder.editTextValueListener.updatePosition(holder.getAdapterPosition());
//
//                    break;
//
//                case 1:
//                    final int position_ = position;
//
//                    final ListLiveStockHolder listLiveStockHolder = (ListLiveStockHolder) holder;
//
//                    int level = checkLevel(livestockEntities.get(position).getLivestock_Type_ID(), 0);
//                    int valueInPixels = (int) getResources().getDimension(R.dimen.activity_horizontal_margin) * (level);
//                    listLiveStockHolder.margin.getLayoutParams().width = valueInPixels;
//
//                    listLiveStockHolder.mtxtSubType.setText(livestockEntities.get(position).getLivestock_Type_Name());
//
//                    if(livestockEntities.get(position).getLivestock_Type_Flgtail().equals("y")){
//                        listLiveStockHolder.mtxtQuantity.setVisibility(View.VISIBLE);
//                        listLiveStockHolder.mtxtPrice.setVisibility(View.VISIBLE);
//                    }else{
//                        listLiveStockHolder.mtxtQuantity.setVisibility(View.GONE);
//                        listLiveStockHolder.mtxtPrice.setVisibility(View.GONE);
//                    }
//
//                    if (livestockEntities.get(position).getLivestock_Amount() == 0) {
//                        listLiveStockHolder.mtxtQuantity.setText("");
//                        listLiveStockHolder.mtxtQuantity.setHint("0");
//                    } else {
//                        String amount= Integer.toString(livestockEntities.get(position).getLivestock_Amount());
//                        listLiveStockHolder.mtxtQuantity.setText(amount);
//                    }
//
//                    if (livestockEntities.get(position).getLivestock_Values() == null) {
//                        listLiveStockHolder.mtxtPrice.setText("");
//                        listLiveStockHolder.mtxtPrice.setHint("0");
//                    } else {
//                        listLiveStockHolder.mtxtPrice.setText(Double.toString(livestockEntities.get(position).getLivestock_Values()));
//                    }
//
//                    listLiveStockHolder.editTextAmountListener.updatePosition(holder.getAdapterPosition());
//                    listLiveStockHolder.editTextValueListener.updatePosition(holder.getAdapterPosition());
//
//                    break;
//
//                case 2:
//                    final FooterHolder footerHolder = (FooterHolder) holder;
//
//                    footerHolder.btn_update.setOnClickListener(new OnClickListener() {
//                        @Override
//                        public void onClick(View views) {
//                            updateLiveStock();
//                        }
//                    });
//
//                    footerHolder.txt_sum.setText(Double.toString(getSum()));
//
//                    break;
//            }
//        }
//
//        @Override
//        public int getItemCount() {
//            return livestockEntities.size()+1;
//        }
//
//        public class ListTypeHolder extends RecyclerView.ViewHolder {
//
//            private final MyNoHBTextView mlistAnimalType;
//            private final RecyclerView recyclerLiveStock;
//            private final LinearLayout layout_price;
//            private MyEditNoHLTextView mtxtQuantity;
//            private MyEditNoHLTextView mtxtPrice;
//            public EditTextValueListener editTextValueListener;
//            public EditTextAmountListener editTextAmountListener;
//
//            public ListTypeHolder(View itemView, EditTextAmountListener editTextAmountListener
//                    , EditTextValueListener editTextValueListener) {
//                super(itemView);
//
//                mlistAnimalType = (MyNoHBTextView) itemView.findViewById(R.id.listAnimalType);
//                recyclerLiveStock = (RecyclerView) itemView.findViewById(R.id.recyclerLivestock);
//                mtxtQuantity = (MyEditNoHLTextView) itemView.findViewById(R.id.txtQuantity);
//                mtxtPrice = (MyEditNoHLTextView) itemView.findViewById(R.id.txtAnimalWorth);
//                layout_price = (LinearLayout) itemView.findViewById(R.id.layout_price);
//
//                this.editTextAmountListener = editTextAmountListener;
//                this.mtxtQuantity.addTextChangedListener(editTextAmountListener);
//
//                this.editTextValueListener = editTextValueListener;
//                this.mtxtPrice.addTextChangedListener(editTextValueListener);
//            }
//        }
//
//        public class ListLiveStockHolder extends RecyclerView.ViewHolder {
//
//            private final MyNoHLTextView mtxtSubType;
//            private MyEditNoHLTextView mtxtQuantity;
//            private MyEditNoHLTextView mtxtPrice;
//            private FrameLayout margin;
//            public EditTextValueListener editTextValueListener;
//            public EditTextAmountListener editTextAmountListener;
//
//            public ListLiveStockHolder(View itemView, EditTextAmountListener editTextAmountListener
//                    , EditTextValueListener editTextValueListener) {
//                super(itemView);
//
//                mtxtSubType = (MyNoHLTextView) itemView.findViewById(R.id.txtSubType);
//                mtxtQuantity = (MyEditNoHLTextView) itemView.findViewById(R.id.txtQuantity);
//                mtxtPrice = (MyEditNoHLTextView) itemView.findViewById(R.id.txtAnimalWorth);
//                margin=(FrameLayout) itemView.findViewById(R.id.margin);
//
//                this.editTextAmountListener = editTextAmountListener;
//                this.mtxtQuantity.addTextChangedListener(editTextAmountListener);
//
//                this.editTextValueListener = editTextValueListener;
//                this.mtxtPrice.addTextChangedListener(editTextValueListener);
//
//            }
//        }
//
//        public class FooterHolder extends RecyclerView.ViewHolder {
//
//            private final MyNoHBTextView txt_sum;
//            private final LinearLayout btn_update;
//
//            public FooterHolder(View itemView) {
//                super(itemView);
//
//                txt_sum = (MyNoHBTextView) itemView.findViewById(R.id.txt_sum);
//                btn_update = (LinearLayout) itemView.findViewById(R.id.btn_update);
//            }
//        }
//    }
//
//    private class EditTextAmountListener implements TextWatcher {
//        private int position;
//
//        public void updatePosition(int position) {
//            this.position = position;
//        }
//
//        @Override
//        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
//            // no op
//        }
//
//        @Override
//        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
//
//            String value = charSequence.toString();
//
//            if (!value.equals("")) {
//                livestockEntities.get(position).setLivestock_Amount(Integer.parseInt(value));
//            }
//
//        }
//
//        @Override
//        public void afterTextChanged(Editable editable) {
//            // no op
//        }
//    }
//
//    private class EditTextValueListener implements TextWatcher {
//        private int position;
//
//        public void updatePosition(int position) {
//            this.position = position;
//        }
//
//        @Override
//        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
//            // no op
//        }
//
//        @Override
//        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
//
//            String value = charSequence.toString();
//
//            if (!value.equals("")) {
//                livestockEntities.get(position).setLivestock_Values(Double.parseDouble(value));
//            }
//
//        }
//
//        @Override
//        public void afterTextChanged(Editable editable) {
//            // no op
//        }
//    }

    private void updateExistingLivestock(Livestock update,Livestock value){
        update.setStatus_ID(value.getStatus_ID());
        update.setLivestockID(value.getLivestockID());
        update.setUuid(value.getUuid());
        update.setCreateBy(value.getCreateBy());
        update.setCreateDate(value.getCreateDate());
        update.setUpdateDate(value.getUpdateDate());
        update.setUpdateBy(value.getUpdateBy());
        update.setLivestockValues(value.getLivestockValues());
        update.setLivestockAmount(value.getLivestockAmount());
        update.setLivestock_Type_Other(value.getLivestock_Type_Other());
    }

    public Farmer getOutputAndUpdate(){
        outputs.clear();

        for (int j = 0; j < livestockEntities.size(); j++) {
            if (livestockEntities.get(j).getLivestock_Amount() != 0) {
                LivestockEntity livestockEntity = livestockEntities.get(j);
                Livestock livestocks = new Livestock();

                livestocks.setLivestockAmount(livestockEntity.getLivestock_Amount());
                livestocks.setLivestockValues(livestockEntity.getLivestock_Values());
                livestocks.setLivestockTypeID(livestockEntity.getLivestock_Type_ID());

                String uuid = UUID.randomUUID().toString();
                livestocks.setUuid(UUID.randomUUID().toString());

                livestocks.setLivestockID(getExistLiveID(livestockEntities.get(j).getLivestock_Type_ID()));

                if(getExistLiveID(livestockEntities.get(j).getLivestock_Type_ID())!=0) { // case update
                    livestocks.setCreateDate(livestockEntity.getCreateDate());
                }else{//case insert
                    livestocks.setCreateDate(AppUtils.getDate());
                }
                livestocks.setUpdateDate(AppUtils.getDate());
                livestocks.setUpdateBy(userID);
                livestocks.setCreateBy(userID);
                livestocks.setStatus_ID(1);
                outputs.add(livestocks);
            }else{
                if(getExistLiveID(livestockEntities.get(j).getLivestock_Type_ID())!=0){ //case delete
                    Livestock livestocks = new Livestock();
                    LivestockEntity livestockEntity = livestockEntities.get(j);

                    livestocks.setLivestockAmount(livestockEntity.getLivestock_Amount());
                    livestocks.setLivestockValues(livestockEntity.getLivestock_Values());
                    livestocks.setLivestockTypeID(livestockEntity.getLivestock_Type_ID());

                    String uuid = UUID.randomUUID().toString();
                    livestocks.setUuid(UUID.randomUUID().toString());

                    livestocks.setLivestockID(getExistLiveID(livestockEntities.get(j).getLivestock_Type_ID()));

                    livestocks.setCreateDate(AppUtils.getDate());
                    livestocks.setUpdateDate(AppUtils.getDate());
                    livestocks.setUpdateBy(userID);
                    livestocks.setCreateBy(userID);
                    livestocks.setStatus_ID(1);
                    outputs.add(livestocks);
                }
            }
        }


        final Farmer farmer=((FarmActivity)getActivity()).getFarmer();

        for(int i=0;i< farmer.getFarm().size();i++){
            if( farmer.getFarm().get(i).getFarmID()==farmID) {
                Farm farm=farmer.getFarm().get(i);
//                RealmList<Livestock> livestockRealmList=new RealmList<Livestock>();
                ArrayList<Livestock> newLivestock=new ArrayList<>();

                for(int j=0;j<outputs.size();j++){
                    boolean isExist=false;
                    for(int k=0;k<farm.getLivestock().size();k++){
                        //update existing
                        if(farm.getLivestock().get(k).getLivestockTypeID()==outputs.get(j).getLivestockTypeID()){
                            isExist=true;
                            updateExistingLivestock(farm.getLivestock().get(k),outputs.get(j));
                        }
                    }

                    if(!isExist){
                        newLivestock.add(outputs.get(j));
                    }
                }
                //add new elements
                farm.getLivestock().addAll(newLivestock);

                farm.setUpdateBy(userID);
                farm.setUpdateDate(AppUtils.getDate());
                farm.setAnimalWorth(AppUtils.getDoubleFromEdt(txt_sum.getText().toString().replace(" บาท","")));
//                farm.setLivestock(livestockRealmList);
            }
        }
        farmer.setUpdateDate(AppUtils.getDate());
        farmer.setUpdateBy(userID);

        return farmer;
    }

    @OnClick(R.id.btn_update)
    public void updateLiveStock() {

        final Farmer farmer=getOutputAndUpdate();

        if (AppUtils.isOnline(getActivity())) {
//            ArrayList<Farmer> farmers=new ArrayList<>();
//            farmers.add(farmer);
            progressBar.setVisibility(View.VISIBLE);
            AppUtils.updateData(farmer, new UpdateListener() {
                @Override
                public void onSuccess(String message, ResponseInsertFarmer responseInsertFarmer) {
                    progressBar.setVisibility(View.GONE);
                    ArrayList<FarmList> farmLists=new ArrayList<FarmList>();
                    farmLists.addAll(responseInsertFarmer.getFarmerList().get(0).getFarmList());

                    //get livestock update
                    ArrayList<Livestock> returnLive=new ArrayList<Livestock>();
                    for(int i=0;i< farmLists.size();i++) {
                        if(farmLists.get(i).getFarm().getFarmID()==farmID) {
                            returnLive.addAll(farmLists.get(i).getLivestock());
                        }
                    }

                    //update id
                    for(int i=0;i< farmer.getFarm().size();i++) {
                        if(farmer.getFarm().get(i).getFarmID()==farmID) { //get farm
                            for(int j=0;j<farmer.getFarm().get(i).getLivestock().size();j++){
                                for (int k=0;k<returnLive.size();k++) {
                                    if (farmer.getFarm().get(i).getLivestock().get(j).getLivestockTypeID()
                                            ==returnLive.get(k).getLivestockTypeID()){
                                        farmer.getFarm().get(i).getLivestock().get(j).setLivestockID(returnLive.get(k).getLivestockID());
                                        livestockses.add(returnLive.get(k)); // insert new to current list (livestocks, livestockEntities)
                                    }
                                }
                            }
                        }
                    }

                    databaseRealm.upDateDataFarmer(farmer);
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    ((FarmActivity)getActivity()).getViewPager().getAdapter().notifyDataSetChanged();

                    promptBack();
                }

                @Override
                public void onError(String message, ResponseInsertFarmer responseInsertFarmer) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                }
            });
        } else {; //ref to activity
            //no need to check cause it always has 1 elements
            databaseRealm.upDateDataFarmer(farmer);
            Toast.makeText(getContext(), getResources().getString(R.string.update_success), Toast.LENGTH_LONG).show();
            ((FarmActivity)getActivity()).getViewPager().getAdapter().notifyDataSetChanged();
            promptBack();
        }
    }


    private void promptBack(){

        new AlertDialog.Builder(getActivity())
                .setTitle(getResources().getString(R.string.update_success))
                .setMessage(getResources().getString(R.string.backtopage))
                .setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        int from=((FarmActivity)getActivity()).getFrom();

                        if(from==1) { //register from search
                            Intent intent = new Intent(getActivity(), FindFarmerActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }else if (from==3){ //case register from login page
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            MyPreferenceManager myPreferenceManager = new MyPreferenceManager(getContext());
                            intent.putExtra(AppUtils.LOGINTYPE,myPreferenceManager.getLoginType());
                            startActivity(intent);
                        }else {
                            if(!AppUtils.isOnline(getActivity())) { //case offline
                                Intent intent = new Intent(getActivity(), OfflineActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }else{
                                getActivity().finish();
                            }
                        }

                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel),null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }

    @Subscribe
    public void recieveOnlickUpdate(CallUpdateLivestock even) {
        getOutputAndUpdate();
//        getBus().post(new CallUpdateTabColor(even.pos));
    }

//    public class CallUpdateTabColor {
//        int pos;
//
//        public CallUpdateTabColor(int pos) {
//            this.pos = pos;
//        }
//
//        public int getName() {
//            return pos;
//        }
//
//        public void setName(int pos) {
//            this.pos = pos;
//        }
//    }



//    @Override
//    public void onPause() {
//        super.onPause();
//        new AlertDialog.Builder(getContext()).setTitle(getResources().getString(R.string.alert))
//                .setMessage(getResources().getString(R.string.alert_update))
//                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        updateLiveStock();
//                    }
//                })
//                .setNegativeButton(getResources().getString(R.string.next_page), new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // do nothing
//                    }
//                })
//                .show();
//    }
}
