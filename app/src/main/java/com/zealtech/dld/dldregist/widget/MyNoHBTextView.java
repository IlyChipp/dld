package com.zealtech.dld.dldregist.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.zealtech.dld.dldregist.R;


/**
 * Created by fanqfang on 9/7/2017 AD.
 */

public class MyNoHBTextView extends android.support.v7.widget.AppCompatTextView{
    public MyNoHBTextView(Context context) {
        super(context);
        initialize();
    }

    public MyNoHBTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public MyNoHBTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    private void initialize() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), getResources().getString(R.string.font_en_bold));
        setTypeface(tf ,1);
    }
}
