package com.zealtech.dld.dldregist.model.newFarmer;

import android.view.View;

/**
 * Created by fanqfang on 10/25/2017 AD.
 */

public class CheckListItem  {
    View view;
    int name;

    public CheckListItem(View view, int name) {
        this.view = view;
        this.name = name;
    }
}
