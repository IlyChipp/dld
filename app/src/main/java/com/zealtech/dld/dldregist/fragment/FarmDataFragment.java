package com.zealtech.dld.dldregist.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.camera.CameraModule;
import com.esafirm.imagepicker.features.camera.ImmediateCameraModule;
import com.esafirm.imagepicker.features.camera.OnImageReadyListener;
import com.esafirm.imagepicker.model.Image;
import com.squareup.picasso.Picasso;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.FarmActivity;
import com.zealtech.dld.dldregist.activity.MapActivity;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.model.AmphurMaster.AmphurEntity;
import com.zealtech.dld.dldregist.model.ProvinceMaster.ProvinceEntity;
import com.zealtech.dld.dldregist.model.StandardMaster;
import com.zealtech.dld.dldregist.model.StandardMaster.StandardEntity;
import com.zealtech.dld.dldregist.model.SupportStandardMaster.SupportEntity;
import com.zealtech.dld.dldregist.model.TambolMaster.TambolEntity;
import com.zealtech.dld.dldregist.model.VillageMaster.VillageEntity;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.model.newFarmer.FarmImage;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseImageFarm;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseInsertFarmer;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.util.UpdateListener;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MySpinnerAdapter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Windows 8.1 on 26/10/2560.
 */

public class FarmDataFragment extends Fragment {

    final private static String TAG_FARM = "FarmDataFragment";

    public static final int RESULT_CANCELED = 0;
    public static final String IMAGE_DIRECTORY = "/demonuts";
    private List<String> imageArrayList = new ArrayList<>();

    ArrayList<Bitmap> bitmaps = new ArrayList<>();
    ArrayList<String> listVillage = new ArrayList<>();
    ArrayList<String> listTambol = new ArrayList<>();
    ArrayList<String> listAmphur = new ArrayList<>();
    ArrayList<String> listProvince = new ArrayList<>();
    ArrayList<String> listStandard = new ArrayList<>();
    ArrayList<String> listSupport = new ArrayList<>();

    ArrayList<VillageEntity> villages = new ArrayList<>();
    ArrayList<TambolEntity> tambols = new ArrayList<>();
    ArrayList<AmphurEntity> amphurs = new ArrayList<>();
    ArrayList<ProvinceEntity> provinces = new ArrayList<>();
    ArrayList<StandardMaster.StandardEntity> standards = new ArrayList<>();
    ArrayList<SupportEntity> supports = new ArrayList<>();

    ImageView imageView;
    int click = 0;
    private PictureAdapter pictureAdapter;
    private ImageView btnAddPhoto;
    private LinearLayout llAddphoto;
    private LinearLayout llFarmAddress;
    LinearLayout farmStatus;
    private MyNoHBTextView txtTopicAddress;
    private View view;
    //    private Spinner spnSupport;
    private Spinner spnStandard;
    private Spinner spnProvince;
    private Spinner spnAmphur;
    private Spinner spnTambol;
    private Spinner spnVillage;
    private MyEditNoHLTextView txtFarmName;
    private MyEditNoHLTextView txtPhone;
    //    private MyEditNoHLTextView txtSalary;
//    private MyEditNoHLTextView txtProblem;
    private MyEditNoHLTextView txtMobile;
    private MyEditNoHLTextView txtNgan;
    private MyEditNoHLTextView txtRai;
    private MyEditNoHLTextView txtLong;
    private MyEditNoHLTextView txtLat;
    private MyEditNoHLTextView txtPostCode;
    private MyEditNoHLTextView txtStreet;
    private MyEditNoHLTextView txtSoi;
    private MyEditNoHLTextView txtHomeNo;
//    private MyEditNoHLTextView txtVillage;
    private MyEditNoHLTextView txtEmail;

    private CheckBox chk_address;

//    private EditText txtSample;

    private Farm mFarmItem ;
    private Farmer mFarmerItem ;
    DBHelper dbHelper;
    Boolean isEmpty = false,isOnline;
    Context context ;
    Boolean isPopulate = true;
    DatabaseRealm databaseRealm;
    Realm realm;
//    int position;
    private RadioGroup radioGroup,radio_isfarm;
    private RadioButton radioCancel,radio_farm,radio_not_farm;
    private RadioButton radioNotCancel;
    private RecyclerView recyclerView_image;
    private int oldImageSize;
    private FrameLayoutDisable progressBar;
    int farmID;
    private ArrayList<Image> images = new ArrayList<>();
    private static final int RC_CODE_PICKER = 2000;
    private static final int RC_CAMERA = 3000;
    private static final int WRITE_STORAGE = 4000;
    private CameraModule cameraModule;

    public void setFarmID(int farmID) {
        this.farmID = farmID;
    }
    public FarmDataFragment() {
        super();
    }

    public static FarmDataFragment newInstance(int farmID) {
        FarmDataFragment fragment = new FarmDataFragment();
        Bundle args = new Bundle();
        fragment.setFarmID(farmID);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_farm_address, container, false);


        view = getActivity().getLayoutInflater().inflate(R.layout.item_pic, null);

        ButterKnife.bind(this,rootView);
        ButterKnife.bind(this,view);
        init(view,rootView);
        dbHelper = new DBHelper(getActivity());
        context=getContext();

        isOnline=  AppUtils.isOnline(getActivity());

        databaseRealm=new DatabaseRealm(getActivity());
        realm = Realm.getDefaultInstance();

        setUpData();

        return rootView;
    }

    public void setUpData() {
        String uuid =((FarmActivity)getActivity()).getFarmer_uuid();

        if (databaseRealm.getDataFarmer(uuid)!=null) {
            mFarmerItem = realm.copyFromRealm(databaseRealm.getDataFarmer(uuid));
            if (!AppUtils.isOnline(getActivity())) { //offline check size for isPopulate
                if(mFarmerItem.getFarm().size()==0){
                    isPopulate=false;
                }
            }
        }

//        position  = getIntent().getIntExtra("position",0);
        if (AppUtils.isOnline(getActivity())) {
            mFarmItem = ((FarmActivity) getActivity()).getFarmById(((FarmActivity) getActivity()).getCurrent_FarmID());
        }else { //offline check size for isPopulate
            mFarmItem = ((FarmActivity) getActivity()).getFarmByPosition(((FarmActivity) getActivity()).getCurrent_FarmPosition());
        }
        //            mFarmItem=new Farm();}


        radio_not_farm.setChecked(true);

        setTypeFaceRadio();
//        addTotalSpinner();
//        getStandard();
//        getSupport();
        if(mFarmItem!=null) { //case not new register
            setRadioIsfarm();
            setRadio();
            setInfoToView();
//
//            chk_address.setVisibility(View.GONE);

        }

        chk_address.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    txtHomeNo.setText(mFarmerItem.getHomeNo());
                    txtSoi.setText(mFarmerItem.getSoi());
                    txtStreet.setText(mFarmerItem.getRoad());
                    txtPostCode.setText(mFarmerItem.getPostCode());
                    txtLat.setText(mFarmerItem.getLatitude());
                    txtLong.setText(mFarmerItem.getLongitude());

                    txtMobile.setText(mFarmerItem.getMobile());
                    txtPhone.setText(mFarmerItem.getPhone());
                    txtEmail.setText(mFarmerItem.getEmail());

                    mFarmItem.setProvinceID(mFarmerItem.getProvinceID());
                    mFarmItem.setAmphurID(mFarmerItem.getAmphurID());
                    mFarmItem.setTambolID(mFarmerItem.getTambolID());
                    mFarmItem.setVillageID(mFarmerItem.getVillageID());

                    getListProvince();

                }

            }
        });

        if (isOnline&&mFarmItem!=null){
            getImage(String.valueOf(mFarmItem.getFarmID()));
            txtFarmName.setEnabled(false);
        }else {
            txtFarmName.setEnabled(true);
        }

        if(mFarmItem==null){
            mFarmItem=new Farm();
        }

        if(((FarmActivity)getActivity()).getFrom()==1||((FarmActivity)getActivity()).getFrom()==3||mFarmItem.getFarmID() == 0) {
            farmStatus.setVisibility(View.GONE);
        }else {
            farmStatus.setVisibility(View.VISIBLE);
        }

        getListProvince();
    }

    private void setTypeFaceRadio() {
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.font_en_light));
        radioCancel.setTypeface(tf);
        radioNotCancel.setTypeface(tf);
        radio_farm.setTypeface(tf);
        radio_not_farm.setTypeface(tf);
    }
    private void init(View view,View rootView) {
        progressBar= (FrameLayoutDisable) rootView.findViewById(R.id.progressBar);
        recyclerView_image= (RecyclerView) rootView.findViewById(R.id.recyclerView_image);
        radioGroup = (RadioGroup) rootView.findViewById(R.id.radio);
        llAddphoto = (LinearLayout) rootView.findViewById(R.id.llAddphoto);
        farmStatus = (LinearLayout) rootView.findViewById(R.id.farm_status);
        btnAddPhoto = (ImageView) rootView.findViewById(R.id.btnAddPhoto);
        llFarmAddress = (LinearLayout) rootView.findViewById(R.id.llFarmAddress);
        txtTopicAddress = (MyNoHBTextView) rootView.findViewById(R.id.txtTopicAddress);
        txtFarmName = (MyEditNoHLTextView)rootView. findViewById(R.id.txtFarmName);
        txtHomeNo = (MyEditNoHLTextView) rootView.findViewById(R.id.txtFarmNo);
        txtSoi = (MyEditNoHLTextView) rootView.findViewById(R.id.txtSoi);
//        txtVillage = (MyEditNoHLTextView) rootView.findViewById(R.id.txtVillage);
        txtStreet = (MyEditNoHLTextView) rootView.findViewById(R.id.txtStreet);
        txtPostCode = (MyEditNoHLTextView) rootView.findViewById(R.id.txtPostCode);
        txtLat = (MyEditNoHLTextView) rootView.findViewById(R.id.txtLatitude);
        txtLong = (MyEditNoHLTextView) rootView.findViewById(R.id.txtLongitude);
        txtRai = (MyEditNoHLTextView) rootView.findViewById(R.id.txtRai);
        txtNgan = (MyEditNoHLTextView) rootView.findViewById(R.id.txtNgan);
        txtMobile = (MyEditNoHLTextView) rootView.findViewById(R.id.txtMobile);
        txtPhone = (MyEditNoHLTextView) rootView.findViewById(R.id.txtPhone);
//        txtProblem = (MyEditNoHLTextView) findViewById(R.id.txtProblem);
//        txtSalary = (MyEditNoHLTextView) findViewById(R.id.txtRevenue);
        txtEmail = (MyEditNoHLTextView) rootView.findViewById(R.id.txtEmail);
        spnVillage = (Spinner) rootView.findViewById(R.id.spnVillage);
        spnTambol = (Spinner) rootView.findViewById(R.id.spnTambol);
        spnAmphur = (Spinner) rootView.findViewById(R.id.spnAmphur);
        spnProvince = (Spinner) rootView.findViewById(R.id.spnProvince);
        spnStandard = (Spinner) rootView.findViewById(R.id.spnStandard);
//        spnSupport = (Spinner) findViewById(R.id.spnStandard_sub);
        radioCancel= (RadioButton) rootView.findViewById(R.id.radioCancel);
        radioNotCancel= (RadioButton)rootView. findViewById(R.id.radioNotCancel);
        radio_farm= (RadioButton) rootView.findViewById(R.id.radio_farm);
        radio_not_farm= (RadioButton) rootView.findViewById(R.id.radio_not_farm);
        radio_isfarm= (RadioGroup)rootView. findViewById(R.id.radio_isfarm);

        chk_address = (CheckBox)rootView.findViewById(R.id.chk_address);

        LinearLayoutManager managerImage = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        recyclerView_image.setLayoutManager(managerImage);
        recyclerView_image.setVisibility(View.VISIBLE);

        spnAmphur.setEnabled(false);
        spnTambol.setEnabled(false);
        spnVillage.setEnabled(false);

//        txtSample = (EditText) rootView.findViewById(R.id.txtSample);

    }
    private void setRadio() {
        Integer isCancel = mFarmItem.getIsCancel();
        if (isCancel==0) {
            radioNotCancel.setChecked(true);

        } else {
            radioCancel.setChecked(true);

        }

    }

    private void setRadioIsfarm() {
        Integer isFarm = mFarmItem.getIsFarm();
        if (isFarm!=null&&isFarm==0) {
            radio_farm.setChecked(true);
        } else {
            radio_not_farm.setChecked(true);
        }

    }
    public void addTotalSpinner() {
        ProvinceEntity provinceEntity = new ProvinceEntity();
        provinceEntity.setProvince_NameTh("กรุณาเลือกจังหวัด");
        provinceEntity.setProvince_ID(-99);

        provinces.add(provinceEntity);

        AmphurEntity amphurEntity = new AmphurEntity();
        amphurEntity.setAmphur_nameTh("กรุณาเลือกอำเภอ");
        amphurEntity.setAmphur_ID(-99);

        amphurs.add(amphurEntity);

        TambolEntity tambolEntity = new TambolEntity();
        tambolEntity.setTambol_NameTh("กรุณาเลือกตำบล");
        tambolEntity.setTambol_ID(-99);

        tambols.add(tambolEntity);

        VillageEntity villageEntity = new VillageEntity();
        villageEntity.setVillage_Name("กรุณาเลือกหมู่บ้าน");
        villageEntity.setVillage_ID(-99);

        villages.add(villageEntity);
    }

    int provinceID;
    int amphurID;
    int tambolID;
    int villageID;
    String zipcode;

    public void getListProvince() {
        listProvince.clear();
        provinces.clear();
        ProvinceEntity provinceEntity =new ProvinceEntity();
        provinceEntity.setProvince_NameTh("กรุณาเลือกจังหวัด");
        provinceEntity.setProvince_ID(-99);
        provinces.add(provinceEntity);
        int selectIndex = -99;

        provinces.addAll(dbHelper.getProvinceList());

        for (int i = 0; i < provinces.size(); i++) {
            listProvince.add(provinces.get(i).getProvince_NameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                context,
                android.R.layout.simple_dropdown_item_1line,
                listProvince);

        spnProvince.setAdapter(adapter);

        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                getListAmphur(province.get(position).getProvince_ID());
//                provinceID = province.get(position).getProvince_ID();
//                spnAmphur.setEnabled(true);
//                spnTambol.setEnabled(false);
//                spnVillage.setEnabled(false);

                provinceID=provinces.get(position).getProvince_ID();
                if(provinces.get(position).getProvince_ID()!=-99) {
//                    spnTambol.setEnabled(true);
                    spnAmphur.setEnabled(true);
                    getListAmphur(provinces.get(position).getProvince_ID());
                }else{
                    spnAmphur.setEnabled(false);
                    amphurID=-99;
                    spnTambol.setEnabled(false);
                    tambolID=-99;
                    spnVillage.setEnabled(false);
                    villageID=-99;
                }
                zipcode="";
                txtPostCode.setText(zipcode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < provinces.size(); i++) {
            if (mFarmItem.getFarmID() != null) {
                if (provinces.get(i).getProvince_ID() == mFarmItem.getProvinceID()) {
                    selectIndex = i;
                    spnProvince.setSelection(selectIndex);
                    break;
                }
            }
        }

    }

    public void getListAmphur(int position) {
        listAmphur.clear();
        amphurs.clear();
        AmphurEntity amphurEntity =new AmphurEntity();
        amphurEntity.setAmphur_nameTh("กรุณาเลือกอำเภอ");
        amphurEntity.setAmphur_ID(-99);
        amphurs.add(amphurEntity);

        int selectIndex = -99;

        amphurs.addAll(dbHelper.getAmphur(position));

        for (int i = 0; i < amphurs.size(); i++) {
            listAmphur.add(amphurs.get(i).getAmphur_nameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                context,
                android.R.layout.simple_dropdown_item_1line,
                listAmphur);
        spnAmphur.setAdapter(adapter);

        spnAmphur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                getListTambol(amphur.get(position).getAmphur_ID());
//
//                amphurID = amphur.get(position).getAmphur_ID();
//                spnTambol.setEnabled(true);
//                spnVillage.setEnabled(false);
                amphurID = amphurs.get(position).getAmphur_ID();
                if (amphurs.get(position).getAmphur_ID() != -99) {
                    spnTambol.setEnabled(true);
                    getListTambol(amphurs.get(position).getAmphur_ID());
                } else {
                    spnTambol.setEnabled(false);
                    tambolID = -99;
                    spnVillage.setEnabled(false);
                    villageID=-99;
                }
                zipcode="";
                txtPostCode.setText(zipcode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < amphurs.size(); i++) {
            if (mFarmItem.getFarmID() != null) {
                if (amphurs.get(i).getAmphur_ID() == mFarmItem.getAmphurID()) {
                    selectIndex = i;
                    spnAmphur.setSelection(selectIndex);
                    break;
                }
            }
        }

    }

    public void getListTambol(int position) {
        listTambol.clear();
        tambols.clear();
        TambolEntity tambolEntity =new TambolEntity();
        tambolEntity.setTambol_NameTh("กรุณาเลือกตำบล");
        tambolEntity.setTambol_ID(-99);
        tambols.add(tambolEntity);

        int selectIndex = -99;

        tambols.addAll(dbHelper.getTambol(position));

        for (int i = 0; i < tambols.size(); i++) {
            listTambol.add(tambols.get(i).getTambol_NameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                context,
                android.R.layout.simple_dropdown_item_1line,
                listTambol);
        spnTambol.setAdapter(adapter);

        spnTambol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                getListVillage(tambol.get(position).getTambol_ID());
//
//                tambolID = tambol.get(position).getTambol_ID();
//
//                txtPost.setText(zipcode);

                tambolID=tambols.get(position).getTambol_ID();
                if(tambols.get(position).getTambol_ID()!=-99) {
//                    spnTambol.setEnabled(true);
                    spnVillage.setEnabled(true);
                    zipcode = tambols.get(position).getZipcode();
                    txtPostCode.setText(zipcode);
                    getListVillage(tambols.get(position).getTambol_ID());
                }else{
                    spnVillage.setEnabled(false);
                    zipcode="";
                    villageID=-99;
                }


//                spnVillage.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < tambols.size(); i++) {
            if (mFarmItem.getFarmID() != null) {
                if (tambols.get(i).getTambol_ID() == mFarmItem.getTambolID()) {
                    selectIndex = i;
                    spnTambol.setSelection(selectIndex);
                    break;
                }
            }
        }
    }


//    public void getListProvince() {
//        ProvinceEntity provinceEntity =new ProvinceEntity();
//        provinceEntity.setProvince_NameTh("ทั้งหมด");
//        provinceEntity.setProvince_ID(-99);
//        province.add(provinceEntity);
//
//        province.addAll(dbHelper.getProvinceList());
//
//        for (int i = 0; i < province.size(); i++) {
//            listProvince.add(province.get(i).getProvince_NameTh());
//        }
//        MySpinnerAdapter adapter = new MySpinnerAdapter(
//                RegisterActivity.this,
//                android.R.layout.simple_dropdown_item_1line,
//                listProvince);
//
//        spnProvince.setAdapter(adapter);
//
//        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                provinceID=province.get(position).getProvince_ID();
//                if(province.get(position).getProvince_ID()!=-99) {
//                    spnTambol.setEnabled(true);
//                    spnAmphur.setEnabled(true);
//                    getListAmphur(province.get(position).getProvince_ID());
//                }else{
//                    spnAmphur.setEnabled(false);
//                    amphurID=-99;
//                    spnTambol.setEnabled(false);
//                    tambolID=-99;
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }
//
//    public void getListAmphur(int position) {
//        listAmphur.clear();
//        amphur.clear();
//        AmphurEntity amphurEntity =new AmphurEntity();
//        amphurEntity.setAmphur_nameTh("ทั้งหมด");
//        amphurEntity.setAmphur_ID(-99);
//        amphur.add(amphurEntity);
//
//        amphur.addAll(dbHelper.getAmphur(position));
//
//        for (int i = 0; i < amphur.size(); i++) {
//            listAmphur.add(amphur.get(i).getAmphur_nameTh());
//        }
//
//        MySpinnerAdapter adapter = new MySpinnerAdapter(
//                RegisterActivity.this,
//                android.R.layout.simple_dropdown_item_1line,
//                listAmphur);
//        spnAmphur.setAdapter(adapter);
//
//        spnAmphur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                amphurID=amphur.get(position).getAmphur_ID();
//                if(amphur.get(position).getProvince_ID()!=-99) {
//                    spnTambol.setEnabled(true);
//                    getListTambol(amphur.get(position).getAmphur_ID());
//                }else{
//                    spnTambol.setEnabled(false);
//                    tambolID=-99;
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }
//
//    public void getListTambol(int position) {
//        listTambol.clear();
//        tambol.clear();
//        TambolEntity tambolEntity =new TambolEntity();
//        tambolEntity.setTambol_NameTh("ทั้งหมด");
//        tambolEntity.setTambol_ID(-99);
//        tambol.add(tambolEntity);
//
//        tambol.addAll( dbHelper.getTambol(position));
//
//        for (int i = 0; i < tambol.size(); i++) {
//            listTambol.add(tambol.get(i).getTambol_NameTh());
//        }
//
//        MySpinnerAdapter adapter = new MySpinnerAdapter(
//                RegisterActivity.this,
//                android.R.layout.simple_dropdown_item_1line,
//                listTambol);
//        spnTambol.setAdapter(adapter);
//
//        spnTambol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                tambolID=tambol.get(position).getTambol_ID();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }

    public void getListVillage(int position) {

        listVillage.clear();
        villages.clear();
        VillageEntity villageEntity =new VillageEntity();
        villageEntity.setVillage_Name("กรุณาเลือกหมู่บ้าน");
        villageEntity.setVillage_ID(-99);
        villages.add(villageEntity);

        int selectIndex = -99;

        villages.addAll(dbHelper.getVillage(position));

        for (int i = 0; i < villages.size(); i++) {
            listVillage.add(villages.get(i).getVillage_Name());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                context,
                android.R.layout.simple_dropdown_item_1line,
                listVillage);
        spnVillage.setAdapter(adapter);

        spnVillage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                villageID=villages.get(position).getVillage_ID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < villages.size(); i++) {
            if (mFarmItem.getFarmID() != null) {
                if (villages.get(i).getVillage_ID() == mFarmItem.getVillageID()) {
                    selectIndex = i;
                    spnVillage.setSelection(selectIndex);
                    break;
                }
            }
        }

    }

    public void getStandard() {

        listStandard.clear();
        standards.clear();
        StandardEntity standardEntity=new StandardEntity();
        standardEntity.setStandard_Name("กรุณาเลือกมาตราฐาน");
        standardEntity.setStandard_ID(-99);
        standards.add(standardEntity);
        int selectIndex = -1;
        standards.addAll(dbHelper.getStandard());

        for (int i = 0; i < standards.size(); i++) {
            listStandard.add(standards.get(i).getStandard_Name());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getActivity(),
                android.R.layout.simple_dropdown_item_1line,
                listStandard);
        spnStandard.setAdapter(adapter);

        spnStandard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mFarmItem != null) {
                    mFarmItem.setStandardID(standards.get(position).getStandard_ID());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < standards.size(); i++) {

            if (mFarmItem != null) {
                if (standards.get(i).getStandard_ID() == mFarmItem.getStandardID()){
                    selectIndex = i;
                    spnStandard.setSelection(selectIndex);
                    break;
                }
            }
        }

    }

 /*   public void getSupport() {
        int selectIndex = -1;
        supports = dbHelper.getSupport();

        for (int i = 0; i < supports.size(); i++) {
            listSupport.add(supports.get(i).getSupport_Standard_Name());
            Log.d("STANDARD LIST -->> ", String.valueOf(supports.get(i).getSupport_Standard_Name()));
            if (mFarmItem != null) {
                if (supports.get(i).getSupport_Standard_ID() == mFarmItem.getSupportStandardID()) {
                    selectIndex = i;
                }
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                context,
                android.R.layout.simple_dropdown_item_1line,
                listSupport);
        spnSupport.setAdapter(adapter);

        spnSupport.setSelection(selectIndex);

        spnSupport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mFarmItem != null) {
                    mFarmItem.setSupportStandardID(supports.get(position).getSupport_Standard_ID());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }*/

    @Optional
    @OnClick(R.id.btnAddPhoto)
    public void addPhoto() {

        if (imageArrayList.size()<5){
            start();
        }else {
            Toast.makeText(context, "สามารถเพิ่มรูปได้ไม่เกิน 5 รูป", Toast.LENGTH_SHORT).show();
        }

    }

    public void start() {
        ImagePicker imagePicker = ImagePicker.create(this)
                .single()
                .imageTitle("Tap to select"); // image selection title

//        if (useCustomImageLoader) {
//            imagePicker.imageLoader(new GrayscaleImageLoader());
//        }

//        if (isSingleMode) {
//            imagePicker.single();
//        } else {
//            imagePicker.multi(); // multi mode (default mode)
//        }

        imagePicker.limit(1) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
                .imageFullDirectory(Environment.getExternalStorageDirectory().getPath()) // can be full path
                .origin(images) // original selected images, used in multi mode
                .start(RC_CODE_PICKER); // start image picker activity with request code
    }
    @Optional
    @OnClick(R.id.btnLatLong)
    public void clickbtnLatLong() {
        if (!AppUtils.checkPermissionLocation(getActivity())) {
            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    AppUtils.RequestPermissionCodeLocation);
        } else {
            startActivityForResult(new Intent(getActivity(), MapActivity.class), AppUtils.REQUEST_LOCATION);
        }
    }

    @Optional
    @OnClick(R.id.btnSubmit)
    public void clickBtnSubmit() {

        setInfoToDatabase();
        mFarmerItem.setUpdateBy(String.valueOf(AppUtils.getUserId(getActivity())));
        mFarmerItem.setUpdateDate(AppUtils.getDate());
        mFarmItem.setUpdateDate(AppUtils.getDate());
        mFarmItem.setUpdateBy(String.valueOf(AppUtils.getUserId(getActivity())));
//            if (position==-1){
//                mFarmItem.setCreateBy(String.valueOf(AppUtils.getUserId(getActivity())));
//                mFarmItem.setCreateDate(getDate());
//                mFarmItem.setStatusID(1);
//                mFarmItem.setFarmID(0);
//                mFarmerItem.getFarm().add(mFarmItem);
//            }

        if (mFarmItem.getFarmID()==0) {
            mFarmItem.setCreateBy(String.valueOf(AppUtils.getUserId(getActivity())));
            mFarmItem.setCreateDate(AppUtils.getDate());
            mFarmItem.setStatusID(1);
            mFarmItem.setFarmID(0);
            mFarmerItem.getFarm().add(mFarmItem);
        } else {
            for (int i = 0; i < mFarmerItem.getFarm().size(); i++ ) {
                Farm farmObj = mFarmerItem.getFarm().get(i);
                if (farmObj.getFarmID().equals(mFarmItem.getFarmID())) {
                    mFarmerItem.getFarm().remove(i);
                    break;
                }
            }

            mFarmerItem.getFarm().add(mFarmItem);
        }

        if (isOnline) {

            ArrayList<Farmer> farmerArrayList = new ArrayList<>();
            farmerArrayList.add(mFarmerItem);
            progressBar.setVisibility(View.VISIBLE);

            AppUtils.updateDataArray(farmerArrayList, new UpdateListener() {
                @Override
                public void onSuccess(String message, ResponseInsertFarmer responseInsertFarmer) {
                    progressBar.setVisibility(View.GONE);

                    //Update FarmID (always single farm)
                    if(mFarmItem.getFarmID()==0){ // case addfarm
                        int farmID=0;
                        for(int i=0;i<responseInsertFarmer.getFarmerList().get(0).getFarmList().size();i++){
                            boolean isExist=false;
                            for(int j=0;j<mFarmerItem.getFarm().size();j++){
                                if(responseInsertFarmer.getFarmerList().get(0).getFarmList().get(i).getFarm().getFarmID()
                                        ==mFarmerItem.getFarm().get(j).getFarmID()){
                                    isExist=true;
                                }
                            }
                            if(!isExist){
                                farmID=responseInsertFarmer.getFarmerList().get(0).getFarmList().get(i).getFarm().getFarmID();
                            }
                        }
                        mFarmerItem.getFarm().get(mFarmerItem.getFarm().size()-1).setFarmID(farmID); //always last one (add)

                        databaseRealm = new DatabaseRealm(getActivity());
                        databaseRealm.upDateDataFarmer(mFarmerItem);
                        ((FarmActivity)getActivity()).setFarmer(mFarmerItem);
                        ((FarmActivity)getActivity()).setMode(FarmActivity.MODE_ADD);
                        // refresh all
                        ((FarmActivity)getActivity()).getMainFarm(farmID, "");
//                        ((FarmActivity)getActivity()).getViewPager().setCurrentItem(1); //TODO : test flow register again
                    }else { //update farm
                        databaseRealm = new DatabaseRealm(getActivity());
                        databaseRealm.upDateDataFarmer(mFarmerItem);
                        ((FarmActivity)getActivity()).setFarmer(mFarmerItem);
                        //goto info
                        ((FarmActivity)getActivity()).getViewPager().setCurrentItem(1);
                    }

                    Toast.makeText(getActivity(), "อัพเดทข้อมูลสำเร็จ", Toast.LENGTH_SHORT).show();
                   /* Intent intent = new Intent(context, FarmActivity.class);
                    intent.putExtra(UUID, uuid);
                    intent.putExtra("mode", "farm");
                    intent.putExtra(AppUtils.TYPE,4);
                    intent.putExtra(AppUtils.SENDFROM,2);
                    startActivity(intent);*/

//                    getActivity().finish();
                }

                @Override
                public void onError(String message, ResponseInsertFarmer responseInsertFarmer) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "อัพเดตข้อมูลไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                }
            });


        } else {

            databaseRealm = new DatabaseRealm(getActivity());
            boolean upDateDataFarmer = databaseRealm.upDateDataFarmer(mFarmerItem);

            if (upDateDataFarmer == true) {
//                Toast.makeText(getActivity(), "อัพเดทข้อมูลสำเร็จ", Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(getActivity(), OfflineActivity.class);
//                startActivity(intent);
//                getActivity().finish();
                if(!isPopulate){ //add farm refresh
                    ((FarmActivity)getActivity()).setFarmer(mFarmerItem);
                    // refresh all
                    ((FarmActivity)getActivity()).getMainFarm(farmID,"");
                    ((FarmActivity)getActivity()).setMode(FarmActivity.MODE_ADD);
//                    ((FarmActivity)getActivity()).getViewPager().setCurrentItem(1); //handle thread
                }else{
                    ((FarmActivity)getActivity()).setFarmer(mFarmerItem);
                    //goto info
                    ((FarmActivity)getActivity()).getViewPager().setCurrentItem(1);
                }


            } else {
                Toast.makeText(getActivity(), "อัพเดตข้อมูลไม่สำเร็จ", Toast.LENGTH_SHORT).show();
            }


        }
    }

    public void setInfoToDatabase(){

        if(mFarmItem==null){
            mFarmItem=new Farm();
        }

//        mFarmItem.setFarmName(txtSample.getText().toString());
        mFarmItem.setHomeNo(txtHomeNo.getText().toString());
        mFarmItem.setSoi(txtSoi.getText().toString());
        mFarmItem.setRoad(txtStreet.getText().toString());
        mFarmItem.setPostCode(txtPostCode.getText().toString());
        mFarmItem.setLatitude(txtLat.getText().toString());
        mFarmItem.setLongitude(txtLong.getText().toString());
        mFarmItem.setAreaRai(AppUtils.tryParseDouble(txtRai.getText().toString()));
        mFarmItem.setAreaNgan(AppUtils.tryParseDouble(txtNgan.getText().toString()));
        mFarmItem.setMobile(txtMobile.getText().toString());
        mFarmItem.setPhone(txtPhone.getText().toString());
        mFarmItem.setEmail(txtEmail.getText().toString());
//        mFarmItem.setFarmProblemDesc(txtProblem.getText().toString());
       /* if (txtSalary.getText().toString().length()>0) {
            mFarmItem.setRevenueOfLivestock(Double.parseDouble(txtSalary.getText().toString()));
        }*/
//        mFarmItem.setMoo(txtVillage.getText().toString());

//        if (position==-1){
//            mFarmerItem.getFarm().add(mFarmItem);
//        }

        if (radioCancel.isChecked()==true){
            mFarmItem.setIsCancel(1);
        }else {
            mFarmItem.setIsCancel(0);
        }

        if (radio_farm.isChecked()==true){
            mFarmItem.setIsFarm(0);
        }else {
            mFarmItem.setIsFarm(1);
        }

        mFarmItem.setProvinceID(provinceID);
        mFarmItem.setAmphurID(amphurID);
        mFarmItem.setTambolID(tambolID);
        mFarmItem.setVillageID(villageID);

    }
    public void setInfoToView(){

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                txtSample.setText(mFarmItem.getFarmName());
                txtHomeNo.setText(mFarmItem.getHomeNo());
                txtSoi.setText(mFarmItem.getSoi());
                txtStreet.setText(mFarmItem.getRoad());
                txtPostCode.setText(mFarmItem.getPostCode());
                txtLat.setText(mFarmItem.getLatitude());
                txtLong.setText(mFarmItem.getLongitude());
//                txtSample.setText(mFarmItem.getSoi());

//        if (mFarmItem.getAreaRai()!=null){
//            txtRai.setText(""+mFarmItem.getAreaRai());
//        }
//        if (mFarmItem.getAreaNgan()!=null){
//            txtNgan.setText(""+mFarmItem.getAreaNgan());
//        }
//
                txtRai.setText(AppUtils.checkNullDouble(mFarmItem.getAreaRai()));
                txtNgan.setText(AppUtils.checkNullDouble(mFarmItem.getAreaNgan()));

      /*  if (mFarmItem.getFarmImage().size()>0){
            recyclerView_image.setVisibility(View.VISIBLE);
        }else {
            recyclerView_image.setVisibility(View.GONE);
        }*/

                txtMobile.setText(mFarmItem.getMobile());
                txtPhone.setText(mFarmItem.getPhone());
                txtEmail.setText(mFarmItem.getEmail());
//        txtProblem.setText(mFarmItem.getFarmProblemDesc());
//                txtVillage.setText(mFarmItem.getMoo());
/*        if (mFarmItem.getRevenueOfLivestock()!=null){
            txtSalary.setText(""+mFarmItem.getRevenueOfLivestock());
        }*/


            }
        });

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }



    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (!AppUtils.checkPermissionGallery(getActivity())) {
                                    requestPermissions(
                                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                            AppUtils.RequestPermissionCodeGallery);
                                } else {
                                    choosePhotoFromGallary();
                                }

                                break;
                            case 1:
                                if (!AppUtils.checkPermissionCamera(getActivity())) {
                                    requestPermissions(
                                            new String[]{Manifest.permission.CAMERA},
                                            AppUtils.RequestPermissionCodeCamera);
                                } else {
                                    takePhotoFromCamera();
                                }

                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent,  AppUtils.GALLERY);
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void takePhotoFromCamera() {

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;

        if (currentapiVersion == android.os.Build.VERSION_CODES.LOLLIPOP) {
            TakePictureIntent();
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, AppUtils.CAMERA);
        }

    }
    private void TakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent,AppUtils. CAMERA);
            }
        }
    }

    String mCurrentPhotoPath;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == RC_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            images = (ArrayList<Image>) ImagePicker.getImages(data);

            if (images != null && !images.isEmpty()) {
                setValueImage(images.get(0));
            }
            return;
        }

        else if (requestCode == RC_CAMERA && resultCode == RESULT_OK) {
            getCameraModule().getImage(getActivity(), data, new OnImageReadyListener() {
                @Override
                public void onImageReady(List<Image> resultImages) {
                    images = (ArrayList<Image>) resultImages;

                    if (images != null && !images.isEmpty()) {
                        setValueImage(images.get(0));
                    }


                    Log.d("IMAGE CAMERA", "DONE");
                }
            });
        }


     else if (requestCode == AppUtils.REQUEST_LOCATION) {
            String latitude = "", longitude = "";
            if (data != null) {

                latitude = data.getStringExtra("lat") != null ? data.getStringExtra("lat") : "";
                longitude = data.getStringExtra("lng") != null ? data.getStringExtra("lng") : "";
//                address = data.getStringExtra("address");
            }
            txtLat.setText(latitude);
            txtLong.setText(longitude);
            mFarmItem.setLatitude(latitude );
            mFarmItem.setLongitude( longitude);
        }
    }
    private void captureImage() {
        startActivityForResult(
                getCameraModule().getCameraIntent(getActivity()), RC_CAMERA);
    }

    private ImmediateCameraModule getCameraModule() {
        if (cameraModule == null) {
            cameraModule = new ImmediateCameraModule();
        }
        return (ImmediateCameraModule) cameraModule;
    }

    public void setValueImage(Image img) {
        Bitmap bitmap = null;

        bitmap = BitmapFactory.decodeFile(img.getPath()); // load

        Matrix matrix = new Matrix();
        matrix.postRotate(AppUtils.getImageOrientation(img.getPath()));
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                bitmap.getHeight(), matrix, true);

        String imgString;
        if (rotatedBitmap != null) {
            Bitmap bitmapShow = AppUtils.scaleBitmap(rotatedBitmap, 200, 200);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

//            imgProfile.setImageBitmap(bitmapShow);

            bitmapShow.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            byte[] profileImage = outputStream.toByteArray();

            imgString = Base64.encodeToString(profileImage,
                    Base64.NO_WRAP);
        } else {
            imgString = "";
        }
        imageArrayList.add(imgString);
        pictureAdapter = new FarmDataFragment.PictureAdapter();
        recyclerView_image.setAdapter(pictureAdapter);
        FarmImage farmImage = new FarmImage();
        farmImage.setImageString(imgString);
        farmImage.setOrderIndex(imageArrayList.size());
        farmImage.setStatus_ID(0);


        mFarmItem.getFarmImage().add(farmImage);

        int a=1;
//        recyclerView_image.setVisibility(View.VISIBLE);

      /*  if (mFarmItem.getFarmImage().size()>0){
            recyclerView_image.setVisibility(View.VISIBLE);
        }else {
            recyclerView_image.setVisibility(View.GONE);
        }*/
    }

    @Optional
    @OnClick(R.id.btn_back)
    public void clickBtnBack() {
        getActivity().finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case RC_CAMERA :
                if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    captureImage();
                }

                break;
            case WRITE_STORAGE :
                if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
            case AppUtils.RequestPermissionCodeLocation:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    if (StoragePermission) {
                        startActivityForResult(new Intent(getActivity(), MapActivity.class), AppUtils.REQUEST_LOCATION);
                    } else {
                        Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }


    public class PictureAdapter extends RecyclerView.Adapter<PictureAdapter.ViewHolder>  {
        private static final String TAG = "PictureAdapter";

        Context context;

        @Override
        public FarmDataFragment.PictureAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            context=parent.getContext();
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pic, parent, false);

            return new FarmDataFragment.PictureAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final FarmDataFragment.PictureAdapter.ViewHolder viewHolder, final int position) {

            if (position<oldImageSize){


                Picasso.with(context)
                        .load(imageArrayList.get(position))
                        .error(R.drawable.default_image)
                        .into( viewHolder .imageView_photo);
                viewHolder.imageView_del.setVisibility(View.GONE);

            }else {
          /*      Picasso.with(context)
                        .load(StringToBitMap(context,imageArrayList.get(position)))
                        .error(R.drawable.default_image)
                        .into( viewHolder .imageView_photo);*/
                viewHolder.imageView_photo.setImageBitmap(StringToBitMap(imageArrayList.get(position)));
                viewHolder.imageView_del.setVisibility(View.VISIBLE);
            }


            viewHolder. imageView_del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageArrayList.remove(position);
                    pictureAdapter = new PictureAdapter();
                    recyclerView_image.setAdapter(pictureAdapter);



//                    mFarmItem.getFarmImage().remove(position);
                }


            });
        }
        public Bitmap StringToBitMap(String encodedString){
            try{
                byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
                Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                return bitmap;
            }catch(Exception e){
                e.getMessage();
                return null;
            }
        }
        @Override
        public int getItemCount() {
//        return NewsItems.size();
            return imageArrayList.size();
        }
   /*     public Uri  StringToBitMap(Context context,String encodedString){


                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null);
                return Uri.parse(path);

       }
*/

        public class ViewHolder extends RecyclerView.ViewHolder  {
            public Context context;

            public ImageView imageView_photo,imageView_del;

            public ViewHolder(View itemView) {
                super(itemView);

                imageView_photo = (ImageView) itemView.findViewById(R.id.imageView_photo);
                imageView_del = (ImageView) itemView.findViewById(R.id.imageView_del);

            }
        }
    }

    private void getImage(String farmId){
        progressBar.setVisibility(View.VISIBLE);
        Api api = AppUtils.getApiService();

        Call<ResponseImageFarm> call = api.getImageFarm(farmId);
        call.enqueue(new Callback<ResponseImageFarm>() {
            @Override
            public void onResponse(Call<ResponseImageFarm> call, Response<ResponseImageFarm> response) {

                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {

                    if (response.body().getResponseCode().equals("200")) {


                        oldImageSize =response.body().getData().size();

                        for (int i =0;i<oldImageSize;i++){

                            imageArrayList.add(response.body().getData().get(i).getImageUrl());
                        }

                        pictureAdapter = new PictureAdapter();
                        recyclerView_image.setAdapter(pictureAdapter);

                    } else {
                        Toast.makeText(context, "ไม่สามารถโหลดรูปแบบภาพได้", Toast.LENGTH_LONG).show();

                    }
                } else {
                    Toast.makeText(context, "ไม่สามารถโหลดรูปแบบภาพได้", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseImageFarm> call, Throwable t) {
                Toast.makeText(context, "ไม่สามารถโหลดรูปแบบภาพได้", Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
            }

        });
    }

}
