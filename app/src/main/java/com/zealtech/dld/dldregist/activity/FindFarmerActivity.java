package com.zealtech.dld.dldregist.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.zealtech.dld.dldregist.MainApplication;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.fragment.FarmerCorporateFragment;
import com.zealtech.dld.dldregist.fragment.FarmerDepartmentFragment;
import com.zealtech.dld.dldregist.fragment.FarmerGeneralFragment;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.newFarmer.Permission;
import com.zealtech.dld.dldregist.model.newFarmer.ResponsePermission;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;
import com.zealtech.dld.dldregist.widget.MySpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FindFarmerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ViewPager viewPager;
    private Spinner spnType,spnRegister;
    ArrayList<String> typeFind = new ArrayList<>();
    DatabaseRealm databaseRealm;
    private ListView listView;
    private LinearLayout linearLayout_list;
    private MyNoHLTextView btn_register;
    int loginType = 0;

    public Permission permission;

    DrawerLayout drawer;
    Toolbar toolbar;
    Tracker mTracker;
    public Permission getPermission() {
        return permission;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_farmer);
        ButterKnife.bind(this);

        MainApplication application = (MainApplication) getApplication();
        mTracker = application.getDefaultTracker();

        databaseRealm=new DatabaseRealm(this);
        permission=new Permission();
        loginType = getIntent().getIntExtra(AppUtils.LOGINTYPE,0);


        loadPermission();
    }

    @Override
    protected void onResume() {
        super.onResume();
        databaseRealm.deleteDataFarmerOnline();
        mTracker.setScreenName("หน้าเค้นหา");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void init() {
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        // Init 'View' instance(s) with rootView.findViewById here
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.open_drawer, R.string.close_drawer);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_dash).setVisible(false);
        nav_Menu.findItem(R.id.nav_farmer).setVisible(false);
        nav_Menu.findItem(R.id.nav_farm).setVisible(false);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
//        tabLayout = (TabLayout) findViewById(R.id.tabs);
        spnType= (Spinner) findViewById(R.id.spnType);
        btn_register= (MyNoHLTextView) findViewById(R.id.btn_register);
        listView= (ListView) findViewById(R.id.spnRegister);
        linearLayout_list= (LinearLayout) findViewById(R.id.linearLayout_list);
       /* tabLayout.setupWithViewPager(viewPager);
        tabLayout.setBackgroundColor(getResources().getColor(R.color.slidingTab));*/

        setupViewPager(viewPager);
        getListTypeFind();
        listView.setVisibility(View.GONE);
        linearLayout_list.setVisibility(View.GONE);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(FindFarmerActivity.this, MainActivity.class);
                intent.putExtra(AppUtils.TYPE,i);
                intent.putExtra(AppUtils.SENDFROM,1);
                startActivity(intent);

                linearLayout_list.setVisibility(View.GONE);
                listView.setVisibility(View.GONE);
            }
        });

        linearLayout_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (linearLayout_list.getVisibility()== View.VISIBLE ){
                    linearLayout_list.setVisibility(View.GONE);
                    listView.setVisibility(View.GONE);
                }else {
                    linearLayout_list.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.VISIBLE);
                }
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (linearLayout_list.getVisibility()== View.VISIBLE ){
                    linearLayout_list.setVisibility(View.GONE);
                    listView.setVisibility(View.GONE);
                }else {
                    linearLayout_list.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(this.getSupportFragmentManager());

        adapter.addFragment(new FarmerGeneralFragment(), getString(R.string.general));
        adapter.addFragment(new FarmerCorporateFragment(), getString(R.string.corporate));
        adapter.addFragment(new FarmerDepartmentFragment(), getString(R.string.company));

        viewPager.addOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                spnType.setSelection(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            Intent intent = new Intent(FindFarmerActivity.this, SelectTypeActivity.class);
            startActivity(intent);
            MyPreferenceManager myPreferenceManager = new MyPreferenceManager(FindFarmerActivity.this);
            myPreferenceManager.setUserId(0);
            myPreferenceManager.setUserPid(0);
            finishAffinity();
        } else if (id == R.id.nav_updatedata) {
            Intent intent = new Intent(FindFarmerActivity.this, OfflineActivity.class);
            startActivity(intent);
        }else if (id == R.id.nav_aboutus) {
            Intent intent = new Intent(FindFarmerActivity.this, AboutUsActivity.class);
            startActivity(intent);
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void loadPermission (){


        MyPreferenceManager myPreferenceManager=new MyPreferenceManager(FindFarmerActivity.this);

        Api api = AppUtils.getApiService();
        Call<ResponsePermission> call = api.getPermission(Integer.toString(myPreferenceManager.getUserId()));
        call.enqueue(new Callback<ResponsePermission>() {
            @Override
            public void onResponse(Call<ResponsePermission> call, Response<ResponsePermission> response) {
                if (response.body() != null) {
                    permission=response.body().getData();
                    init();
                } else {
                    try {
                        Toast.makeText(FindFarmerActivity.this, response.errorBody().string(), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponsePermission> call, Throwable t) {

            }

        });

    }

    public void getListTypeFind() {

        int selectIndex=0;
        typeFind.add(getString(R.string.general));
        typeFind.add(getString(R.string.corporate));
        typeFind.add(getString(R.string.company));

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                this,
                android.R.layout.simple_dropdown_item_1line,
                typeFind);
        spnType.setAdapter(adapter);
/*


        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.item_list_dialog, null);
        alertDialog.setView(convertView);
        alertDialog.setTitle("List");
        ListView lv = (ListView) convertView.findViewById(R.id.listView);
        lv.setAdapter(adapter);
        alertDialog.show();*/
        listView.setAdapter(adapter);

        spnType.setSelection(selectIndex);

        spnType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spnType.setSelection(position);

                if (position==0){
                    viewPager.setCurrentItem(0);
                }else if (position==1){
                    viewPager.setCurrentItem(1);
                }else {
                    viewPager.setCurrentItem(2);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Optional
    @OnClick(R.id.btn_back)
    public void clickBtnBack() {

        new AlertDialog.Builder(FindFarmerActivity.this).setTitle(getResources().getString(R.string.alert))
                .setMessage(getResources().getString(R.string.alert_logout))
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MyPreferenceManager myPreferenceManager = new MyPreferenceManager(FindFarmerActivity.this);
                        myPreferenceManager.setUserId(0);
                        myPreferenceManager.setUserPid(0);
                        Intent intent = new Intent(FindFarmerActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(AppUtils.LOGINTYPE,myPreferenceManager.getLoginType());
                        startActivity(intent);
//                        finish();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }


    @Override
    public void onBackPressed() {
        if (AppUtils.isOnline(this)==true){
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
//                super.onBackPressed();
            }
        }

        new AlertDialog.Builder(FindFarmerActivity.this).setTitle(getResources().getString(R.string.alert))
                .setMessage(getResources().getString(R.string.alert_logout))
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MyPreferenceManager myPreferenceManager = new MyPreferenceManager(FindFarmerActivity.this);
                        myPreferenceManager.setUserId(0);
                        myPreferenceManager.setUserPid(0);
                        Intent intent = new Intent(FindFarmerActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(AppUtils.LOGINTYPE,myPreferenceManager.getLoginType());
                        startActivity(intent);
//                        finish();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }
}
