package com.zealtech.dld.dldregist.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.DailyActivity;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.model.LivestockDaily;
import com.zealtech.dld.dldregist.model.LivestockDaily.LivestockDailyListEntity;
import com.zealtech.dld.dldregist.model.LivestockDailyMaster.LivestockDailyEntity;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListDailyLiveStockFragment extends Fragment {

    public final String TAG = "ListLiveStockFragment";

    @BindView(R.id.progressBar)
    FrameLayoutDisable progressBar;

    @BindView(R.id.llLiveStock)
    LinearLayout llLiveStock;
    @BindView(R.id.listLivestock)
    ScrollView listLivestock;

    private RecyclerView recyclerListType;
//    private ListTypeAdapter listTypeAdapter;

    ArrayList<LivestockDailyListEntity> livestockses=new ArrayList<>();
    ArrayList<LivestockDailyEntity> livestockEntities=new ArrayList<>();
    final ArrayList<LivestockDailyEntity> livestockMasters=new ArrayList<>();
    ArrayList<LivestockDailyListEntity> outputs=new ArrayList<>();

    DatabaseRealm databaseRealm;
    int farmID;
    String userID="";
    String dateFind="";

    public ArrayList<LivestockDailyEntity> getLivestockEntities() {
        return livestockEntities;
    }

    public void setLivestockEntities(ArrayList<LivestockDailyEntity> livestockEntities) {
        this.livestockEntities = null;
        this.livestockEntities = livestockEntities;
    }

    public void setFarmID(int farmID) {
        this.farmID = farmID;
    }

    public static ListDailyLiveStockFragment newInstance(int rootId, ArrayList<LivestockDailyEntity> livestockEntities
            , int farmID) {

        ListDailyLiveStockFragment fragment = new ListDailyLiveStockFragment();

        fragment.setLivestockEntities(livestockEntities);
        fragment.setFarmID(farmID);

        Bundle bundle = new Bundle();
        bundle.putInt("rootId", rootId);

        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_daily_livestock, container, false);
        ButterKnife.bind(this, rootView);
//        initInstances(rootView);

        databaseRealm=new DatabaseRealm(getContext());
        setUpData();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

//    private void initInstances(View rootView) {
//        // Init 'View' instance(s) with rootView.findViewById here
//        recyclerListType = (RecyclerView) rootView.findViewById(R.id.recyclerLivestock);
//        llLiveStock = (LinearLayout) rootView.findViewById(R.id.llLiveStock);
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

//        setUpData();
    }

    public void setUpData() {

        if (getActivity() == null) {
            return;
        }
        livestockMasters.clear();
        livestockses.clear();

        llLiveStock.removeAllViews();

        userID = Integer.toString(AppUtils.getUserId(getActivity()));

        livestockMasters.addAll(((DailyActivity)getActivity()).getLivestockMasters());

        if(AppUtils.isOnline(getActivity())) {
            getLiveStockDaily(((DailyActivity)getActivity()).getCurrent_FarmID());
        }

    }


    private void initInstances(View rootView) {

    }

    private void getLiveStockDaily(final int farmId) {

        listLivestock.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        dateFind=((DailyActivity)getActivity()).getFindDate();

        Api api = AppUtils.getApiService();
        Call<LivestockDaily> call = api.getLivestockDaily(Integer.toString(farmId),dateFind);
        call.enqueue(new Callback<LivestockDaily>() {
            @Override
            public void onResponse(Call<LivestockDaily> call, Response<LivestockDaily> response) {
                progressBar.setVisibility(View.GONE);
                listLivestock.setVisibility(View.VISIBLE);
                if(isAdded()) {
                    if (response.body() != null) {

                        if(response.body().getResponseCode().equals("200")) {
                            livestockses.addAll(response.body().getData());
//                        livestockses.addAll(farm_.getLivestock());

                            addValueToLivestock();
                            initList();
                        }else{
                            addValueToLivestock();
                            initList();
                            Toast.makeText(getActivity(), response.body().getResponseMessage(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        try {
                            Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<LivestockDaily> call, Throwable t) {
                listLivestock.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }

        });
    }


    private void addValueToLivestock(){

        for(int i=0;i<livestockEntities.size();i++){
            for(int j=0;j<livestockses.size();j++){
                if(livestockEntities.get(i).getLivestock_NoteType_ID()==livestockses.get(j).getLivestock_NoteType_ID()){
                    livestockEntities.get(i).setLivestock_Out(livestockses.get(j).getLivestock_Out());
                    livestockEntities.get(i).setLivestock_In(livestockses.get(j).getLivestock_In());
                    livestockEntities.get(i).setLivestock_Product(livestockses.get(j).getLivestock_Product());
                }
            }
        }

    }

    private void initList(){

        for(int i=0;i<livestockEntities.size();i++){

            int level = checkLevel(livestockEntities.get(i).getLivestock_NoteType_ID(), 0);

                if (level == 1) {
                    final View view = getLayoutInflater().inflate(R.layout.item_livestock, null);

                    MyNoHBTextView listAnimalType = (MyNoHBTextView) view.findViewById(R.id.listAnimalType);
                    LinearLayout layout_price = (LinearLayout) view.findViewById(R.id.layout_price);

                    layout_price.setVisibility(View.GONE);

                    listAnimalType.setText(livestockEntities.get(i).getLivestock_NoteType_Name());

                    llLiveStock.addView(view);

                } else {
                    final View view = getLayoutInflater().inflate(R.layout.item_list_daily_livestock, null);

                    MyNoHLTextView txtSubType = (MyNoHLTextView) view.findViewById(R.id.txtSubType);
                    MyEditNoHLTextView txtin = (MyEditNoHLTextView) view.findViewById(R.id.txtin);
                    MyEditNoHLTextView txtout = (MyEditNoHLTextView) view.findViewById(R.id.txtout);
                    MyEditNoHLTextView txtpro = (MyEditNoHLTextView) view.findViewById(R.id.txtpro);
                    FrameLayout margin=(FrameLayout) view.findViewById(R.id.margin);

                    final int i_ = i;

                    int valueInPixels = (int) getResources().getDimension(R.dimen.activity_horizontal_margin) * (level);
                    margin.getLayoutParams().width = valueInPixels;

                    txtSubType.setText(livestockEntities.get(i).getLivestock_NoteType_Name());

                    if (livestockEntities.get(i).getLivestock_In() == 0) {
                        txtin.setText("");
                        txtin.setHint("0");
                    } else {
                        String amount= Integer.toString(livestockEntities.get(i).getLivestock_In());
                        txtin.setText(amount);
                    }

                    if (livestockEntities.get(i).getLivestock_Out() == 0) {
                        txtout.setText("");
                        txtout.setHint("0");
                    } else {
                        txtout.setText(Integer.toString(livestockEntities.get(i).getLivestock_Out()));
                    }

                    if (livestockEntities.get(i).getLivestock_Product() == 0) {
                        txtpro.setText("");
                        txtpro.setHint("0");
                    } else {
                        txtpro.setText(Integer.toString(livestockEntities.get(i).getLivestock_Product()));
                    }

                    setTextChangeListener(txtin,txtout,txtpro,i);

                    llLiveStock.addView(view);
                }


        }

    }


    private boolean hasChild(int type_id){

        for(int i=0;i<livestockMasters.size();i++){
            if(livestockMasters.get(i).getLivestock_NoteType_Parent_ID()==type_id){
                return true;
            }
        }

        return false;
    }

    private boolean isRoot(int type_id) {
        for (int i = 0; i < livestockMasters.size(); i++) {
            if (livestockMasters.get(i).getLivestock_NoteType_ID() == type_id) {
                if (livestockMasters.get(i).getLivestock_NoteType_Parent_ID() == -1) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    private int checkLevel(int type_id, int level) {
        int level_ = 0;
        for (int i = 0; i < livestockMasters.size(); i++) {
            if (livestockMasters.get(i).getLivestock_NoteType_ID() == type_id) {
                if (livestockMasters.get(i).getLivestock_NoteType_Parent_ID() == -1) {
                    return level;
                } else {
                    level += 1;
                    level_ = checkLevel(livestockMasters.get(i).getLivestock_NoteType_Parent_ID(), level);
                }
            }
        }
        return level_;
    }


    private int getExistLiveID(int type_id){
        int id=0;

        for(int i=0;i<livestockses.size();i++){
            if(livestockses.get(i).getLivestock_NoteType_ID()==type_id){
                id=livestockses.get(i).getLivestock_Note_ID();
            }
        }

        return id;
    }

    private String getUnit(int type_id){
        for(int i=0;i<livestockMasters.size();i++){
            if(livestockMasters.get(i).getLivestock_NoteType_ID()==type_id) {
                return livestockMasters.get(i).getLivestock_NoteType_Unit_Name();
            }
        }
        return "";
    }

    private void updateExistingLivestock(LivestockDailyListEntity update,LivestockDailyListEntity value){
        update.setStatus_ID(value.getStatus_ID());
        update.setLivestock_Note_ID(value.getLivestock_Note_ID());
//        update.setUuid(value.getUuid());
        update.setCreateBy(value.getCreateBy());
        update.setCreateDate(value.getCreateDate());
        update.setUpdateDate(value.getUpdateDate());
        update.setUpdateBy(value.getUpdateBy());
        update.setLivestock_In(value.getLivestock_In());
        update.setLivestock_Out(value.getLivestock_Out());
        update.setLivestock_Product(value.getLivestock_Product());
    }

    private void setTextChangeListener(MyEditNoHLTextView txtIn,MyEditNoHLTextView txtOut,MyEditNoHLTextView txtPro
            ,final int pos){
        txtIn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String value = charSequence.toString();

                try {
                    if (value.equals("")) {
                        livestockEntities.get(pos).setLivestock_In(AppUtils.tryParseInt(value));
                    }
                }catch (Exception e){

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        txtOut.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String value = charSequence.toString();

                if (!value.equals("")) {
                    livestockEntities.get(pos).setLivestock_Out(AppUtils.tryParseInt(value));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        txtPro.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String value = charSequence.toString();

                if (!value.equals("")) {
                    livestockEntities.get(pos).setLivestock_Product(AppUtils.tryParseInt(value));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @OnClick(R.id.btn_update)
    public void updateLiveStock() {
        outputs.clear();

        listLivestock.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        for(int i=0;i<livestockEntities.size();i++){
            LivestockDailyEntity livestockEntity = livestockEntities.get(i);
            LivestockDailyListEntity livestocks = new LivestockDailyListEntity();

            livestocks.setLivestock_In(livestockEntity.getLivestock_In());
            livestocks.setLivestock_Out(livestockEntity.getLivestock_Out());
            livestocks.setLivestock_Product(livestockEntity.getLivestock_Product());
            livestocks.setLivestock_NoteType_ID(livestockEntity.getLivestock_NoteType_ID());

            livestocks.setLivestock_Note_ID(getExistLiveID(livestockEntities.get(i).getLivestock_NoteType_ID()));

            livestocks.setCreateDate(AppUtils.getDate());
            livestocks.setUpdateDate(AppUtils.getDate());
            livestocks.setUpdateBy(userID);
            livestocks.setCreateBy(userID);
            livestocks.setStatus_ID(1);
//            livestocks.setFarm_ID(4502702);
            livestocks.setFarm_ID(((DailyActivity)getActivity()).getCurrent_FarmID());

            outputs.add(livestocks);
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        String json = gson.toJson(outputs);

        Api api = AppUtils.getApiService();
        Call<LivestockDaily> call = api.insertDailyLiveStock(json);
        call.enqueue(new Callback<LivestockDaily>() {
            @Override
            public void onResponse(Call<LivestockDaily> call, Response<LivestockDaily> response) {
                progressBar.setVisibility(View.GONE);
                listLivestock.setVisibility(View.VISIBLE);
                if(isAdded()) {
                    if (response.body() != null) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.update_success), Toast.LENGTH_LONG).show();
                    } else {
                        try {
                            Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<LivestockDaily> call, Throwable t) {
                listLivestock.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }

        });

    }

}
