package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/28/2017 AD.
 */
@Parcel
public class FarmerTypeMaster implements APIResponse{
    //DBHelper
    public static final String TABLE = "farmer_type";

    String responseCode;
    String responseMessage;
    ArrayList<FarmerTypeEntity> data;

    public FarmerTypeMaster(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<FarmerTypeEntity> getData() {
        return data;
    }

    public void setData(ArrayList<FarmerTypeEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class FarmerTypeEntity {
        int farmer_Type_ID;
        String farmer_Type_Name;
        int orderIndex;
        int status_ID;

        public FarmerTypeEntity(){ }

        public int getFarmer_Type_ID() {
            return farmer_Type_ID;
        }

        public void setFarmer_Type_ID(int farmer_Type_ID) {
            this.farmer_Type_ID = farmer_Type_ID;
        }

        public String getFarmer_Type_Name() {
            return farmer_Type_Name;
        }

        public void setFarmer_Type_Name(String farmer_Type_Name) {
            this.farmer_Type_Name = farmer_Type_Name;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String FARMER_TYPE_ID = "farmer_Type_ID";
        public static final String FARMER_TYPE_NAME = "farmer_Type_Name";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
    }
}
