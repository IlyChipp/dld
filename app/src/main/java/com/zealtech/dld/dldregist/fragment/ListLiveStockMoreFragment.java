package com.zealtech.dld.dldregist.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.FarmActivity;
import com.zealtech.dld.dldregist.activity.FindFarmerActivity;
import com.zealtech.dld.dldregist.activity.LoginActivity;
import com.zealtech.dld.dldregist.activity.OfflineActivity;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.LivestockMaster.LivestockEntity;
import com.zealtech.dld.dldregist.model.LivestockModel;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.model.newFarmer.FarmList;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.Livestock;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseInsertFarmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseLivestock;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.util.UpdateListener;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;
import com.zealtech.dld.dldregist.widget.MySpinnerAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListLiveStockMoreFragment extends Fragment {

    @BindView(R.id.progressBar)
    FrameLayoutDisable progressBar;
    @BindView(R.id.txt_total)
    MyNoHBTextView txt_total;

    @BindView(R.id.layout_update)
    LinearLayout layout_update;

    private LinearLayout llAddAnimal;
    private LinearLayout btnAddAnimal;
    private Spinner spnTypeAminal;

    private ArrayList<Livestock> livestockses=new ArrayList<>();
    ArrayList<LivestockEntity> livestockEntities=new ArrayList<>();
    final ArrayList<LivestockEntity> livestockMasters=new ArrayList<>();

    private ArrayList<Livestock> outputs=new ArrayList<>();

    ArrayList<String> listSpinner=new ArrayList<>();
    ArrayList<View> viewArrayList=new ArrayList<>();

    private boolean isConnect=false;
    DatabaseRealm databaseRealm;

    String userID="";
    int farmID;

    public void setLivestockEntities(ArrayList<LivestockEntity> livestockEntities) {
        this.livestockEntities = livestockEntities;
    }

    public void setFarmID(int farmID) {
        this.farmID = farmID;
    }

    public ListLiveStockMoreFragment() {
        super();
    }

    public static ListLiveStockMoreFragment newInstance(int rootId, ArrayList<LivestockEntity> livestockEntities
            , int farmID) {

        ListLiveStockMoreFragment fragment = new ListLiveStockMoreFragment();

        fragment.setLivestockEntities(livestockEntities);
        fragment.setFarmID(farmID);

        Bundle bundle = new Bundle();
        bundle.putInt("rootId", rootId);

        fragment.setArguments(bundle);

        return fragment;
    }

    public void setUpData() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_livstock_more, container, false);
        ButterKnife.bind(this, rootView);
        initInstances(rootView);

        databaseRealm=new DatabaseRealm(getContext());

        setUpData();

        return rootView;
    }

    public void stUpData() {

        livestockMasters.clear();
        livestockses.clear();
        listSpinner.clear();

        userID=Integer.toString(AppUtils.getUserId(getActivity()));

        livestockMasters.addAll(((FarmActivity)getActivity()).getLivestockMasters());
//        livestockMasters.remove(0);

        if(AppUtils.isOnline(getActivity())) {
            getLiveStock(farmID);
        }else{
//            livestockses.addAll(databaseRealm.getDataLiveStock());

            livestockses.addAll(((FarmActivity) getActivity()).getFarmById(farmID).getLivestock());
            initList();
        }
    }

    private String getUnit(int type_id){
        for(int i=0;i<livestockMasters.size();i++){
            if(livestockMasters.get(i).getLivestock_Type_ID()==type_id) {
                return livestockMasters.get(i).getLivestock_Type_Unit();
            }
        }
        return "";
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initList(){
        addValueToLivestock();

        for(int k=0;k<livestockEntities.size();k++){
            listSpinner.add(livestockEntities.get(k).getLivestock_Type_Name());
        }
        listSpinner.add("อื่นๆ");

        for(int i=0;i<livestockses.size();i++){
            if(getRootParentId(livestockses.get(i).getLivestockTypeID())==67) {
                final int i_=i;

               final View view = getLayoutInflater().inflate(R.layout.item_list_livestock_more, null);

                Spinner spnTypeAminal = (Spinner) view.findViewById(R.id.spnTypeAminal);
               final MyEditNoHLTextView txt_type = (MyEditNoHLTextView) view.findViewById(R.id.txt_type);
                MyEditNoHLTextView txt_amount = (MyEditNoHLTextView) view.findViewById(R.id.txt_amount);
                MyEditNoHLTextView txt_price = (MyEditNoHLTextView) view.findViewById(R.id.txt_price);
                MyNoHLTextView txtUnit = (MyNoHLTextView) view.findViewById(R.id.txtUnit);
//                MyNoHLTextView delete = (MyNoHLTextView) views.findViewById(R.id.delete);

//                delete.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View view_) {
//                        llAddAnimal.removeView(views);
//                        viewArrayList.remove(views);
//                        updateSum();
//                    }
//                });

                txtUnit.setText(getUnit(livestockses.get(i).getLivestockTypeID()));

                MySpinnerAdapter adapter = new MySpinnerAdapter(
                        getContext(),
                        android.R.layout.simple_dropdown_item_1line,
                        listSpinner);

                spnTypeAminal.setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        ((TextView) view).setText(listSpinner.get(i));
                        if(listSpinner.get(i).equals("อื่นๆ")){
                            setEnableEdt(txt_type,true);
                        }else {
                            setEnableEdt(txt_type,false);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                spnTypeAminal.setAdapter(null);

                spnTypeAminal.setAdapter(adapter);

                for (int j = 0; j < listSpinner.size(); j++) {
                    if (listSpinner.get(j).equals(getTypeNameById(livestockses.get(i).getLivestockTypeID()))) {
                        spnTypeAminal.setSelection(j);
                    }
                }

                spnTypeAminal.setFocusable(false);
                if(livestockses.get(i).getLivestockTypeID()==-99){
                    setEnableEdt(txt_type,true);
                }else{
                    setEnableEdt(txt_type,false);
                }
//                spnTypeAminal.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.grey1a));

                txt_type.setFocusable(false);
                txt_type.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.grey1a));

                txt_amount.setText(Integer.toString(livestockses.get(i).getLivestockAmount()));
                txt_price.setText(Double.toString(livestockses.get(i).getLivestockValues()));

                llAddAnimal.addView(view);
                viewArrayList.add(view);
            }
        }
        updateSum();
    }

    private String getTypeNameById(int type_id){
        String type_name="";

        for(int i=0;i<livestockEntities.size();i++){
            if(livestockEntities.get(i).getLivestock_Type_ID()==type_id){
                type_name=livestockEntities.get(i).getLivestock_Type_Name();
            }
        }

        return  type_name;
    }

    private int getTypeIdByName(String type_name){
        int type_id=0;

        for(int i=0;i<livestockEntities.size();i++){
            if(livestockEntities.get(i).getLivestock_Type_Name().equals(type_name)){
                type_id=livestockEntities.get(i).getLivestock_Type_ID();
            }
        }

        return  type_id;
    }

    private void getLiveStock(final int farmId) {
        layout_update.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        Api api = AppUtils.getApiService();
        Call<ResponseLivestock> call = api.getLivestock(Integer.toString(farmId));
        call.enqueue(new Callback<ResponseLivestock>() {
            @Override
            public void onResponse(Call<ResponseLivestock> call, Response<ResponseLivestock> response) {
                layout_update.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    Farm farm_=((FarmActivity)getActivity()).getFarmById(farmId);

                    if(farm_.getLivestock()==null||farm_.getLivestock().size()==0){ //get from api when first time (first time livestock is null)
                        RealmList<Livestock> livestocks=new RealmList<Livestock>();
                        livestocks.addAll(response.body().getData());
                        farm_.setLivestock(livestocks);
                    }
                    livestockses.addAll(farm_.getLivestock());

                    initList();
                } else {
                    try {
                        Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseLivestock> call, Throwable t) {
                layout_update.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }

        });
    }

    private void insertData(final ArrayList<Farmer> farmer) {
        progressBar.setVisibility(View.VISIBLE);
        Api api = AppUtils.getApiService();
        String s = String.valueOf(farmer);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        gsonBuilder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = gsonBuilder.create();

        String json = gson.toJson(farmer);

        Call<ResponseInsertFarmer> call = api.setInsertData(json);
        call.enqueue(new Callback<ResponseInsertFarmer>() {
            @Override
            public void onResponse(Call<ResponseInsertFarmer> call, Response<ResponseInsertFarmer> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {

                    if (response.body().getResponseCode().equals(getString(R.string.success))) {

                        Toast.makeText(getContext(), response.body().getResponseMessage(), Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getContext(), response.body().getResponseMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        Toast.makeText(getContext(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseInsertFarmer> call, Throwable t) {
                progressBar.setVisibility(View.GONE);

            }

        });

    }

    private void insertLiveStock(ArrayList<Livestock> livestocks) {

        final ProgressBar progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleSmall);
        progressBar.setVisibility(View.VISIBLE);

        Gson gson = new Gson();
        String json = gson.toJson(livestocks);

        Api api = AppUtils.getApiService();
        Call<LivestockModel> call = api.insertLivestock(json);
        call.enqueue(new Callback<LivestockModel>() {
            @Override
            public void onResponse(Call<LivestockModel> call, Response<LivestockModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_LONG).show();
                } else {
                    try {
                        Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LivestockModel> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }

        });
    }

    private int getRootParentId(int type_id){
        int id=0;
        for(int i=0;i<livestockMasters.size();i++){
            if(livestockMasters.get(i).getLivestock_Type_ID()==type_id){
                if(livestockMasters.get(i).getLivestock_Type_Parent()==-1) {
                    return livestockMasters.get(i).getLivestock_Type_ID();
                }else{
                    id= getRootParentId(livestockMasters.get(i).getLivestock_Type_Parent());
                }
            }
        }
        return id;
    }

    private void addValueToLivestock(){
        for(int i=0;i<livestockEntities.size();i++){
            for(int j=0;j<livestockses.size();j++){
                if(livestockEntities.get(i).getLivestock_Type_ID()==livestockses.get(j).getLivestockTypeID()){
                    livestockEntities.get(i).setLivestock_Amount(livestockses.get(j).getLivestockAmount());
                    livestockEntities.get(i).setLivestock_Values(livestockses.get(j).getLivestockValues());
                }
            }
        }
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        btnAddAnimal = (LinearLayout) rootView.findViewById(R.id.btnAddAnimal);
        llAddAnimal = (LinearLayout) rootView.findViewById(R.id.llAddAnimal);
    }


    @OnClick(R.id.btnAddAnimal)
    public void addAnimal(){

        final View view = getLayoutInflater().inflate(R.layout.item_list_livestock_more, null);

        final Spinner spnTypeAminal = (Spinner) view.findViewById(R.id.spnTypeAminal);
        final MyEditNoHLTextView txt_type = (MyEditNoHLTextView) view.findViewById(R.id.txt_type);
        MyEditNoHLTextView txt_amount = (MyEditNoHLTextView) view.findViewById(R.id.txt_amount);
        MyEditNoHLTextView txt_price = (MyEditNoHLTextView) view.findViewById(R.id.txt_price);
//        MyNoHLTextView delete = (MyNoHLTextView) views.findViewById(R.id.delete);

        txt_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                updateSum();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        txt_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                updateSum();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

//        delete.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view_) {
//                llAddAnimal.removeView(views);
//                viewArrayList.remove(views);
//                updateSum();
//            }
//        });

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listSpinner);

        spnTypeAminal.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) view).setText(listSpinner.get(i));
                if(listSpinner.get(i).equals("อื่นๆ")){
                    setEnableEdt(txt_type,true);
                }else {
                    setEnableEdt(txt_type,false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spnTypeAminal.setAdapter(adapter);
        spnTypeAminal.setSelection(listSpinner.size()-1);//อื่นๆ

        llAddAnimal.addView(view);
        viewArrayList.add(view);

    }

    private void setEnableEdt(MyEditNoHLTextView textView,boolean enable){
        textView.setEnabled(enable);

        if(enable) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                textView.setBackground(getResources().getDrawable(R.drawable.shape_edit_text, getContext().getTheme()));
            } else {
                textView.setBackground(getResources().getDrawable(R.drawable.shape_edit_text));
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                textView.setBackground(getResources().getDrawable(R.drawable.shape_edit_text_disable, getContext().getTheme()));
            } else {
                textView.setBackground(getResources().getDrawable(R.drawable.shape_edit_text_disable));
            }
        }
    }

    private int getExistLiveID(int type_id){
        int id=0;

        for(int i=0;i<livestockses.size();i++){
            if(livestockses.get(i).getLivestockTypeID()==type_id){
                id=livestockses.get(i).getLivestockID();
            }
        }

        return id;
    }

    @OnClick(R.id.btn_update)
    public void updateData(){
       getData();
    }

    private void updateSum(){
        double sum=0;

        for(int i=0;i<viewArrayList.size();i++){
            final View view = viewArrayList.get(i);

            Spinner spnTypeAminal = (Spinner) view.findViewById(R.id.spnTypeAminal);
            MyEditNoHLTextView txt_type = (MyEditNoHLTextView) view.findViewById(R.id.txt_type);
            MyEditNoHLTextView txt_amount = (MyEditNoHLTextView) view.findViewById(R.id.txt_amount);
            MyEditNoHLTextView txt_price = (MyEditNoHLTextView) view.findViewById(R.id.txt_price);

            sum += AppUtils.getIntFromEdt(txt_amount.getText().toString()) * AppUtils.getDoubleFromEdt(txt_price.getText().toString());
        }


        Double sum_other = 0.0;
        for( int j = 0; j < livestockses.size(); j++) {
            if (getRootParentId(livestockses.get(j).getLivestockTypeID()) != 67) {
                sum_other += getPrice(livestockses.get(j).getLivestockTypeID()) * livestockses.get(j).getLivestockAmount();
            }
        }

//        final Farmer farmer=((FarmActivity)getActivity()).getFarmer();
//
//        for(int i=0;i< farmer.getFarm().size();i++){
//            if( farmer.getFarm().get(i).getFarmID()==farmID) {
//                farmer.getFarm().get(i).setAnimalWorth(sum);
//            }
//        }
        txt_total.setText(0+" บาท");
//        txt_total.setText(Double.toString(sum+sum_other)+" บาท");
    }

    private void updateExistingLivestock(Livestock update,Livestock value){
        update.setStatus_ID(value.getStatus_ID());
        update.setLivestockID(value.getLivestockID());
        update.setUuid(value.getUuid());
        update.setCreateBy(value.getCreateBy());
        update.setCreateDate(value.getCreateDate());
        update.setUpdateDate(value.getUpdateDate());
        update.setUpdateBy(value.getUpdateBy());
        update.setLivestockValues(value.getLivestockValues());
        update.setLivestockAmount(value.getLivestockAmount());
        update.setLivestock_Type_Other(value.getLivestock_Type_Other());
    }

    private Double getPrice(int type_id){
        for(int i=0;i<livestockMasters.size();i++){
            if(livestockMasters.get(i).getLivestock_Type_ID()==type_id) {
                return livestockMasters.get(i).getLivestock_Type_Price().doubleValue();
            }
        }
        return 0.0;
    }

    private void getData(){

        outputs.clear();
        double sum=0;

        for(int i=0;i<viewArrayList.size();i++){
            final View view = viewArrayList.get(i);
            final int i_=i;

            Spinner spnTypeAminal = (Spinner) view.findViewById(R.id.spnTypeAminal);
            MyEditNoHLTextView txt_type = (MyEditNoHLTextView) view.findViewById(R.id.txt_type);
            MyEditNoHLTextView txt_amount = (MyEditNoHLTextView) view.findViewById(R.id.txt_amount);
            MyEditNoHLTextView txt_price = (MyEditNoHLTextView) view.findViewById(R.id.txt_price);

            Livestock livestocks = new Livestock();

            if(!txt_amount.getText().toString().equals("0")) {

                livestocks.setLivestockAmount(AppUtils.getIntFromEdt(txt_amount.getText().toString()));
                livestocks.setLivestockValues(AppUtils.getDoubleFromEdt(txt_price.getText().toString()));

                String type_name = spnTypeAminal.getSelectedItem().toString();

                if (type_name.equals("อื่นๆ")) {
                    livestocks.setLivestockTypeID(0);
                    if(txt_type.getText().length()==0){
                        livestocks.setLivestock_Type_Other("");
                    }else{
                        livestocks.setLivestock_Type_Other(txt_type.getText().toString());
                    }
                } else {
                    livestocks.setLivestockTypeID(getTypeIdByName(type_name));
                }

//            livestocks.setFarmID(Integer.parseInt(((FarmActivity)getActivity()).getFarm0ID()));
                String uuid = UUID.randomUUID().toString();
                livestocks.setUuid(UUID.randomUUID().toString());

                livestocks.setLivestockID(getExistLiveID(livestockEntities.get(i).getLivestock_Type_ID()));

                if(getExistLiveID(livestockEntities.get(i).getLivestock_Type_ID())!=0) { // case update
                    livestocks.setCreateDate(livestockEntities.get(i).getCreateDate());
                }else{//case insert
                    livestocks.setCreateDate(AppUtils.getDate());
                }
                livestocks.setCreateDate(AppUtils.getDate());
                livestocks.setUpdateDate(AppUtils.getDate());
                livestocks.setUpdateBy(userID);
                livestocks.setCreateBy(userID);
                livestocks.setStatus_ID(1);
            }else{

                if(getExistLiveID(livestockEntities.get(i).getLivestock_Type_ID())!=0) { //case delete
                    LivestockEntity livestockEntity = livestockEntities.get(i);

                    livestocks.setLivestockAmount(livestockEntity.getLivestock_Amount());
                    livestocks.setLivestockValues(livestockEntity.getLivestock_Values());
                    livestocks.setLivestockTypeID(livestockEntity.getLivestock_Type_ID());

                    String uuid = UUID.randomUUID().toString();
                    livestocks.setUuid(UUID.randomUUID().toString());

                    livestocks.setLivestockID(getExistLiveID(livestockEntities.get(i).getLivestock_Type_ID()));
                    livestocks.setCreateDate(AppUtils.getDate());
                    livestocks.setUpdateDate(AppUtils.getDate());
                    livestocks.setUpdateBy(userID);
                    livestocks.setCreateBy(userID);
                    livestocks.setStatus_ID(1);
                    outputs.add(livestocks);
                }

            }

            outputs.add(livestocks);
        }

        final Farmer farmer=((FarmActivity)getActivity()).getFarmer();

        for(int i=0;i< farmer.getFarm().size();i++){
            if( farmer.getFarm().get(i).getFarmID()==farmID) {
                Farm farm=farmer.getFarm().get(i);
//                RealmList<Livestock> livestockRealmList=new RealmList<Livestock>();
                ArrayList<Livestock> newLivestock=new ArrayList<>();

                for(int j=0;j<outputs.size();j++){
                    boolean isExist=false;
                    for(int k=0;k<farm.getLivestock().size();k++){
                        //update existing
                        if(farm.getLivestock().get(k).getLivestockTypeID()==outputs.get(j).getLivestockTypeID()){
                            isExist=true;
                            updateExistingLivestock(farm.getLivestock().get(k),outputs.get(j));
                        }
                    }

                    if(!isExist){
                        newLivestock.add(outputs.get(j));
                    }
                }
                //add new elements
                farm.getLivestock().addAll(newLivestock);

                farm.setUpdateBy(userID);
                farm.setUpdateDate(AppUtils.getDate());
                farm.setAnimalWorth(AppUtils.getDoubleFromEdt(txt_total.getText().toString().replace(" บาท","")));
            }
        }

        farmer.setUpdateBy(userID);
        farmer.setUpdateDate(AppUtils.getDate());

        if (AppUtils.isOnline(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            AppUtils.updateData(farmer, new UpdateListener() {
                @Override
                public void onSuccess(String message, ResponseInsertFarmer responseInsertFarmer) {
                    progressBar.setVisibility(View.GONE);
                    ArrayList<FarmList> farmLists=new ArrayList<FarmList>();
                    farmLists.addAll(responseInsertFarmer.getFarmerList().get(0).getFarmList());

                    //get livestock update
                    ArrayList<Livestock> returnLive=new ArrayList<Livestock>();
                    for(int i=0;i< farmLists.size();i++) {
                        if(farmLists.get(i).getFarm().getFarmID()==farmID) {
                            returnLive.addAll(farmLists.get(i).getLivestock());
                        }
                    }

                    //update id
                    for(int i=0;i< farmer.getFarm().size();i++) {
                        if( farmer.getFarm().get(i).getFarmID()==farmID) { //get farm
                            for(int j=0;j<farmer.getFarm().get(i).getLivestock().size();j++){
                                for (int k=0;k<returnLive.size();k++) {
                                    if (farmer.getFarm().get(i).getLivestock().get(j).getLivestockTypeID()
                                            ==returnLive.get(k).getLivestockTypeID()){
                                        farmer.getFarm().get(i).getLivestock().get(j).setLivestockID(returnLive.get(k).getLivestockID());
                                        livestockses.add(returnLive.get(k)); // insert new to current list (livestocks, livestockEntities)
                                    }
                                }
                            }
                        }
                    }

                    databaseRealm.upDateDataFarmer(farmer);
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    ((FarmActivity)getActivity()).getViewPager().getAdapter().notifyDataSetChanged();
                    promptBack();
                }

                @Override
                public void onError(String message, ResponseInsertFarmer responseInsertFarmer) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                }
            });
        } else {; //ref to activity
            //no need to check cause it always has 1 elements
            databaseRealm.upDateDataFarmer(farmer);
            Toast.makeText(getContext(), getResources().getString(R.string.update_success), Toast.LENGTH_LONG).show();
            ((FarmActivity)getActivity()).getViewPager().getAdapter().notifyDataSetChanged();
            promptBack();
        }

    }

    private void promptBack(){

        new AlertDialog.Builder(getActivity())
                .setTitle(getResources().getString(R.string.update_success))
                .setMessage(getResources().getString(R.string.backtopage))
                .setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        int from=((FarmActivity)getActivity()).getFrom();

                        if(from==1) { //register from search
                            Intent intent = new Intent(getActivity(), FindFarmerActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }else if (from==3){ //case register from login page
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            MyPreferenceManager myPreferenceManager = new MyPreferenceManager(getContext());
                            intent.putExtra(AppUtils.LOGINTYPE,myPreferenceManager.getLoginType());
                            startActivity(intent);
                        }else {
                            if(!AppUtils.isOnline(getActivity())) { //case offline
                                Intent intent = new Intent(getActivity(), OfflineActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }else{
                                getActivity().finish();
                            }
                        }

                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel),null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }


}
