package com.zealtech.dld.dldregist.listener;

import java.util.HashMap;

/**
 * Created by Jirat on 12/21/2017.
 */

public interface PIDScannerListener {
    void onCompleteScan(HashMap<String,String> result);
}
