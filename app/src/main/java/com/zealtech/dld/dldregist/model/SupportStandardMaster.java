package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 10/3/2017 AD.
 */
@Parcel
public class SupportStandardMaster implements APIResponse{

    public static final String TABLE = "support_standard";

    String responseCode;
    String responseMessage;
    ArrayList<SupportEntity> data ;

    public SupportStandardMaster() { }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<SupportEntity> getData() {
        return data;
    }

    public void setData(ArrayList<SupportEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class SupportEntity {
        int support_Standard_ID;
        int orderIndex;
        int status_ID;
        String support_Standard_Name;

        public SupportEntity() { }

        public int getSupport_Standard_ID() {
            return support_Standard_ID;
        }

        public void setSupport_Standard_ID(int support_Standard_ID) {
            this.support_Standard_ID = support_Standard_ID;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        public String getSupport_Standard_Name() {
            return support_Standard_Name;
        }

        public void setSupport_Standard_Name(String support_Standard_Name) {
            this.support_Standard_Name = support_Standard_Name;
        }
    }
    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String SUPPORT_STANDARD_ID = "support_Standard_ID";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
        public static final String SUPPORT_STANDARD_NAME = "support_Standard_Name";
    }
}
