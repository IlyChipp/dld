package com.zealtech.dld.dldregist.model.newFarmer;

import java.util.ArrayList;

/**
 * Created by Windows 8.1 on 26/10/2560.
 */

public class FarmerList {
    private  Farmer farmer;
    private ArrayList<FarmList> farmList;

    public ArrayList<FarmList> getFarmList() {
        return farmList;
    }

    public void setFarmList(ArrayList<FarmList> farmList) {
        this.farmList = farmList;
    }

    public Farmer getFarmer() {
        return farmer;
    }

    public void setFarmer(Farmer farmer) {
        this.farmer = farmer;
    }

}
