package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/26/2017 AD.
 */
@Parcel
public class LivestockDaily implements APIResponse{
    //DBHelper

    String responseCode;
    String responseMessage;
    ArrayList<LivestockDailyListEntity> data;

    public LivestockDaily(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<LivestockDailyListEntity> getData() {
        return data;
    }

    public void setData(ArrayList<LivestockDailyListEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class LivestockDailyListEntity{
        int livestock_NoteType_ID;
        int livestock_Note_ID;
        String createBy;
        String createDate;
        int status_ID;
        String updateBy;
        String updateDate;

        int livestock_In;
        int livestock_Out;
        int livestock_Product;
        int farm_ID;

        public LivestockDailyListEntity(){

        }

        public int getLivestock_Note_ID() {
            return livestock_Note_ID;
        }

        public void setLivestock_Note_ID(int livestock_Note_ID) {
            this.livestock_Note_ID = livestock_Note_ID;
        }

        public int getLivestock_NoteType_ID() {
            return livestock_NoteType_ID;
        }

        public void setLivestock_NoteType_ID(int livestock_NoteType_ID) {
            this.livestock_NoteType_ID = livestock_NoteType_ID;
        }

        public String getCreateBy() {
            return createBy;
        }

        public void setCreateBy(String createBy) {
            this.createBy = createBy;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        public String getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(String updateBy) {
            this.updateBy = updateBy;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public int getLivestock_In() {
            return livestock_In;
        }

        public void setLivestock_In(int livestock_In) {
            this.livestock_In = livestock_In;
        }

        public int getLivestock_Out() {
            return livestock_Out;
        }

        public void setLivestock_Out(int livestock_Out) {
            this.livestock_Out = livestock_Out;
        }

        public int getLivestock_Product() {
            return livestock_Product;
        }

        public void setLivestock_Product(int livestock_Product) {
            this.livestock_Product = livestock_Product;
        }

        public int getFarm_ID() {
            return farm_ID;
        }

        public void setFarm_ID(int farm_ID) {
            this.farm_ID = farm_ID;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String LIVESTOCK_TYPE_ID = "livestock_Type_ID";
        public static final String CREATE_BY = "createBy";
        public static final String CREATE_DATE = "createDate";
        public static final String LIVESTOCK_TYPE_CODE = "livestock_Type_Code";
        public static final String LIVESTOCK_TYPE_FLGTAIL = "livestock_Type_Flgtail";
        public static final String LIVESTOCK_TYPE_FULLNAME = "livestock_Type_Fullname";
        public static final String LIVESTOCK_TYPE_NAME = "livestock_Type_Name";
        public static final String LIVESTOCK_TYPE_OLD_ID = "livestock_Type_Old_ID";
        public static final String LIVESTOCK_TYPE_PARENT = "livestock_Type_Parent";
        public static final String LIVESTOCK_TYPE_PRICE = "livestock_Type_Price";
        public static final String LIVESTOCK_TYPE_UNIT = "livestock_Type_Unit";
        public static final String LIVESTOCK_TYPE_UNITLIMIT = "livestock_Type_UnitLimit";
        public static final String UPDATE_BY = "updateBy";
        public static final String UPDATE_DATE = "updateDate";
        public static final String STATUS_ID = "status_ID";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String LIVESTOCK_TYPE_ROOT = "livestock_Type_Root";
    }
}
