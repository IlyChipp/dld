package com.zealtech.dld.dldregist.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryFragment extends Fragment {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

    ArrayList<String> arrayList=new ArrayList<>();

    int mode;

    public HistoryFragment() {
        super();
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public static HistoryFragment newInstance(int mode) {
        HistoryFragment fragment = new HistoryFragment();
        fragment.setMode(mode);
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this,rootView);

        arrayList.add("");
        arrayList.add("");
        initList();

        return rootView;
    }

    private void getHistory(){

    }

    private void initList(){
        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        ListAdapter listFarmAdapter = new ListAdapter();
        recyclerview.setAdapter(listFarmAdapter);
    }


    public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);

            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {

            int order=position+1;
            holder.txt_order.setText("ลำดับที่ "+order);

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private final MyNoHBTextView txt_order;
            private final MyNoHLTextView txt_date;
            private final MyNoHLTextView btn_check;

            private final MyNoHLTextView txt_name;
            private final MyNoHLTextView txt_status;
            private final MyNoHLTextView txt_editor;
            private final MyNoHLTextView txt_status2;


            public ViewHolder(View itemView) {
                super(itemView);

                txt_order = (MyNoHBTextView) itemView.findViewById(R.id.txt_order);

                txt_name = (MyNoHLTextView) itemView.findViewById(R.id.txt_name);
                txt_status = (MyNoHLTextView) itemView.findViewById(R.id.txt_status);
                txt_editor = (MyNoHLTextView) itemView.findViewById(R.id.txt_editor);
                txt_status2 = (MyNoHLTextView) itemView.findViewById(R.id.txt_status2);

                btn_check = (MyNoHLTextView) itemView.findViewById(R.id.btn_check);
                txt_date = (MyNoHLTextView) itemView.findViewById(R.id.txt_date);


            }
        }

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }



}
