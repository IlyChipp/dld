package com.zealtech.dld.dldregist.fragment;

import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;

import com.squareup.otto.Bus;
import com.zealtech.dld.dldregist.manager.BusProvider;


/**
 * Created by layer on 17/6/2558.
 */
public class BaseBusFragment extends Fragment {

    @Override
    public void onResume() {
        super.onResume();
        try {
            getBus().register(this);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }catch (RuntimeException er){
            er.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getBus().unregister(this);
    }

    private Bus mBus;

    protected Bus getBus() {
        if (mBus == null) {
            mBus = BusProvider.getInstance();
        }
        return mBus;
    }

    protected void setBus(Bus bus) {
        mBus = bus;
    }

    private final Handler mainThread = new Handler(Looper.getMainLooper());

    public void postOnMain(final Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            getBus().post(event);
        } else {
            mainThread.post(new Runnable() {
                @Override
                public void run() {
                    getBus().post(event);

                }
            });
        }
    }
}