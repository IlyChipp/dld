package com.zealtech.dld.dldregist.fragment;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.FarmerListActivity;
import com.zealtech.dld.dldregist.activity.FindFarmerActivity;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.config.SingleShotLocationProvider;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.manager.Contextor;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.AmphurMaster.AmphurEntity;
import com.zealtech.dld.dldregist.model.FarmerModel.FarmerEntity;
import com.zealtech.dld.dldregist.model.ProvinceMaster.ProvinceEntity;
import com.zealtech.dld.dldregist.model.SearchModel;
import com.zealtech.dld.dldregist.model.TambolMaster.TambolEntity;
import com.zealtech.dld.dldregist.model.farmerList.FarmerSearchList;
import com.zealtech.dld.dldregist.model.newFarmer.Permission;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;
import com.zealtech.dld.dldregist.widget.MySpinnerAdapter;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FarmerGeneralFragment extends Fragment {

    private static final int REQUEST_BARCODE = 3;
    private static final String CLASS_NAME = "FarmerGeneralFragment";

    @BindView(R.id.edt_name)
    MyEditNoHLTextView edt_name;
    @BindView(R.id.edt_lastname)
    MyEditNoHLTextView edt_lastname;
    @BindView(R.id.progressBar)
    FrameLayoutDisable progressBar;

    @BindView(R.id.linearLayout_near)
    LinearLayout linearLayout_near;
    @BindView(R.id.edt_lat)
    MyEditNoHLTextView edt_lat;
    @BindView(R.id.edt_long)
    MyEditNoHLTextView edt_long;

    DBHelper dbHelper;
    private MyEditNoHLTextView txtPID;
    private MyNoHLTextView txtIncorrect;
    private Spinner spnProvince;
    private Spinner spnAmphur;
    private Spinner spnTambol,spnTypeFind,spnTypeFarmer;
    private LinearLayout linearLayout_name,linearLayout_address,linearLayout_pid;
    final private static String FARMERINFO = "farmerInfo";
    FarmerEntity mFarmer = new FarmerEntity();
    ArrayList<String> listProvince = new ArrayList<>();
    ArrayList<String> listTambol = new ArrayList<>();
    ArrayList<String> listAmphur = new ArrayList<>();
    ArrayList<String> typeFind = new ArrayList<>();
    ArrayList<String> typeFarmer = new ArrayList<>();

    ArrayList<ProvinceEntity> province = new ArrayList<>();
    ArrayList<AmphurEntity> amphur = new ArrayList<>();
    ArrayList<TambolEntity> tambol = new ArrayList<>();

    int amphurID=-99,tambolID=-99,provinceID=-99;

    int mode=0;

    SearchModel searchModel;
    DatabaseRealm databaseRealm;

    Permission permission;

    private Boolean flag = false;
    private String lat; // 004
    private String lng;
    private Boolean isLoading = false;

    public FarmerGeneralFragment() {
        super();
    }

    public static FarmerGeneralFragment newInstance() {
        FarmerGeneralFragment fragment = new FarmerGeneralFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_farmer_general, container, false);

        ButterKnife.bind(FarmerGeneralFragment.this, rootView);
        initInstances(rootView);

        permission=((FindFarmerActivity)getActivity()).getPermission();

        databaseRealm=new DatabaseRealm(getContext());
        dbHelper = new DBHelper(getContext());
//        txtPID.setText("3141200423007");
        if(province.size()==0) {
            getListProvince();
        }
        if(typeFind.size()==0) {
            getListTypeFind();
        }
        if(typeFarmer.size()==0) {
            getListTypeFarmer();
        }

        setPermission();

        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        txtPID = (MyEditNoHLTextView) rootView.findViewById(R.id.txtPID);
        txtIncorrect = (MyNoHLTextView) rootView.findViewById(R.id.txtIncorrect);
        spnProvince = (Spinner) rootView.findViewById(R.id.spnProvince);
        spnAmphur = (Spinner) rootView.findViewById(R.id.spnAmphur);
        spnTambol = (Spinner) rootView.findViewById(R.id.spnTambol);
        spnTypeFind= (Spinner) rootView.findViewById(R.id.spnTypeFind);
        spnTypeFarmer= (Spinner) rootView.findViewById(R.id.spnTypeFarmer);
        linearLayout_name= (LinearLayout) rootView.findViewById(R.id.linearLayout_name);
        linearLayout_address= (LinearLayout) rootView.findViewById(R.id.linearLayout_address);
        linearLayout_pid= (LinearLayout) rootView.findViewById(R.id.linearLayout_pid);
    }

    private void setPermission(){

            int organizeLevelID = permission.getOrganizeLevelID();
            String orgCode = permission.getOrgCode();
            int parentID = permission.getOrgParentID();

            if (organizeLevelID == 3) {//province

                for (int i = 0; i < province.size(); i++) {
                    if (province.get(i).getProvince_ID() == Integer.parseInt(orgCode)) {
                        spnProvince.setSelection(i);
                    }
                }
                spnProvince.setEnabled(false);

            } else if (organizeLevelID == 4) {//amphur

                for (int i = 0; i < province.size(); i++) {
                    if (province.get(i).getProvince_ID() == parentID) {
                        spnProvince.setSelection(i);
                    }
                }
                spnProvince.setEnabled(false);
            }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    private void savePermission(){
        MyPreferenceManager myPreferenceManager=new MyPreferenceManager(getContext());
        myPreferenceManager.setPROID(provinceID);
        myPreferenceManager.setAMID(amphurID);
        myPreferenceManager.setTAMID(tambolID);
    }

    private void deletePermission(){
        MyPreferenceManager myPreferenceManager=new MyPreferenceManager(getContext());
        myPreferenceManager.setPROID(-99);
        myPreferenceManager.setAMID(-99);
        myPreferenceManager.setTAMID(-99);
    }

    @OnClick(R.id.btnFind)
    public void searchName(){
        if(edt_name.getText().length()!=0||edt_lastname.getText().length()!=0){

            String firstname=null;
            String lastname=null;

            if(edt_name.getText().length()!=0){
                firstname= AppUtils.getStringFromEdt(edt_name.getText());
            }

            if(edt_lastname.getText().length()!=0){
                lastname=AppUtils.getStringFromEdt(edt_lastname.getText());
            }
//            String name= AppUtils.getStringFromEdt(edt_name.getText())
//                    +AppUtils.getStringFromEdt(edt_lastname.getText());

            savePermission();

            searchModel=new SearchModel();
            searchModel.setFirstname(firstname);
            searchModel.setLastname(lastname);
            searchModel.setTambolId(tambolID);
            searchModel.setAmphurId(amphurID);
            searchModel.setProvinnceId(provinceID);

            Integer proID=null;
            if(provinceID!=-99){
                proID=provinceID;
            }

            Integer amID=null;
            if(amphurID!=-99){
                amID=amphurID;
            }

            Integer tamID=null;
            if(tambolID!=-99){
                tamID=tambolID;
            }

            getListFarmer(null, null, null, null, null, null, null
                    , 1, null, null, null
                    , null, firstname, lastname);
        }
    }

    @OnClick(R.id.btnNear)
    public void searchNear() {
        if(edt_lat.getText().length()!=0&&edt_long.getText().length()!=0){
            searchModel = new SearchModel();
            searchModel.setLat_(edt_lat.getText().toString());
            searchModel.setLong_(edt_long.getText().toString());
            searchModel.setDistance("5");
            searchModel.setTambolId(tambolID);
            searchModel.setAmphurId(amphurID);
            searchModel.setProvinnceId(provinceID);

            savePermission();

            Integer proID=null;
            if(provinceID!=-99){
                proID=provinceID;
            }

            Integer amID=null;
            if(amphurID!=-99){
                amID=amphurID;
            }

            Integer tamID=null;
            if(tambolID!=-99){
                tamID=tambolID;
            }

            getListFarm(null, null, null, proID, amID, tamID, null, 1, edt_lat.getText().toString()
                    , edt_long.getText().toString(), "5");
        }
    }

    @OnClick(R.id.btnSubmit)
    public void searchPID() {
        //pid
        if (txtPID.getText().length() != 0) {
            searchModel = new SearchModel();
            searchModel.setPid(Long.parseLong(txtPID.getText().toString()));
            searchModel.setTambolId(tambolID);
            searchModel.setAmphurId(amphurID);
            searchModel.setProvinnceId(provinceID);

            savePermission();

            Integer proID=null;
            if(provinceID!=-99){
                proID=provinceID;
            }

            Integer amID=null;
            if(amphurID!=-99){
                amID=amphurID;
            }

            Integer tamID=null;
            if(tambolID!=-99){
                tamID=tambolID;
            }

            getListFarmer(null, null, null, null, null, null
                    , null, 1, null, null, null
                    , txtPID.getText().toString(), null, null);
        }

    }

    @OnClick(R.id.btnSubmitArea)
    public void searchArea(){
        searchModel=new SearchModel();

        searchModel.setProvinnceId(provinceID);
        searchModel.setAmphurId(amphurID);
        searchModel.setTambolId(tambolID);

        savePermission();

        Integer proID=null;
        if(provinceID!=-99){
            proID=provinceID;
        }

        Integer amID=null;
        if(amphurID!=-99){
            amID=amphurID;
        }

        Integer tamID=null;
        if(tambolID!=-99){
            tamID=tambolID;
        }

        getListFarm(0, 20, null, proID
                , amID, tamID, null, 1,null,null,null);
    }

    @OnClick(R.id.btnScan)
    public void clickBtnScan() {
        if(AppUtils.hasPermission(getActivity())) {
            AppUtils.hideKeyboard(getActivity());

//            IntentIntegrator integrator = new IntentIntegrator(getActivity());
//            integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
//            integrator.initiateScan();


            IntentIntegrator integrator = IntentIntegrator.forSupportFragment(this);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
            integrator.initiateScan();
        }else{
            AppUtils.askPermission(getActivity(),REQUEST_BARCODE);
        }
//        Intent intent = new Intent(getContext(), MainActivity.class);
//        startActivity(intent);
    }

//    @OnClick(R.id.btnRead)
//    public void clickBtnread() {
////        PIDScanActivity pidScanActivity= new PIDScanActivity(getActivity());
////        pidScanActivity.onCreateActivity();
//    }


//    private void loginPID(long idcard) {
//        Api api = AppUtils.getApiService();
//        Call<ResponseFarmer> call = api.getFarmerByPID(idcard);
//        call.enqueue(new Callback<ResponseFarmer>() {
//            @Override
//            public void onResponse(Call<ResponseFarmer> call, Response<ResponseFarmer> response) {
//
//                if (response.body() != null) {
//                    if (response.body().getResponseCode().equals(getString(R.string.success))) {
//
////                        if (!dbHelper.isFarmerPopulated(response.body().getData().getPid())) {
////                            dbHelper.addFarmer(response.body().getData());
////                            mFarmer = dbHelper.getInfoFarmer(response.body().getData().getFarmer_ID());
////                        }else{
////                            mFarmer = dbHelper.getInfoFarmer(response.body().getData().getFarmer_ID());
////                        }
//                        databaseRealm.deleteDataFarmerOnline();
//                        Farmer data = response.body().getData();
//                        data.setOnline(true);
//                        databaseRealm.addDataFarmer(data);
//
//
////                        Intent intent = new Intent(getContext(), MainActivity.class);
////                        intent.putExtra(FARMERINFO, Parcels.wrap(mFarmer));
////                        startActivity(intent);
//                        Intent intent = new Intent(getContext(), FarmerListActivity.class);
////                        intent.putExtra(FARMERINFO, Parcels.wrap(mFarmer));
//                        startActivity(intent);
//                    } else {
//                        txtPID.setBackgroundResource(R.drawable.shape_edit_text_incorrect);
//                        txtIncorrect.setVisibility(View.VISIBLE);
//                    }
//                } else {
//                    Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
//                }
//
//            }
//
//
//            @Override
//            public void onFailure(Call<ResponseFarmer> call, Throwable t) {
//                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private void getListFarmer(Integer iDisplayStart, Integer iDisplayLength, String sSearch, Integer ProvinceId
            , Integer AmphurId, Integer TambolId, Integer VillageId, int userType,String latitude
            ,String longitude,String distanceKilometer,String Pid,String FirstName,String LastName){
        progressBar.setVisibility(View.VISIBLE);
        Api api = AppUtils.getApiService();
        Call<FarmerSearchList> call = api.getListFarmer(iDisplayStart,iDisplayLength,sSearch,ProvinceId
                ,AmphurId,TambolId,VillageId,userType,latitude,longitude,distanceKilometer,Pid,FirstName,LastName);
        call.enqueue(new Callback<FarmerSearchList>() {
            @Override
            public void onResponse(Call<FarmerSearchList> call, Response<FarmerSearchList> response) {
                progressBar.setVisibility(View.GONE);

                    if (response.body() != null) {
//                    if (response.body().getResponseCode().equals(getString(R.string.success))) {
                        if (response.body().getAaData() !=null&&response.body().getAaData().size()!=0) {
                            Intent intent = new Intent(getContext(), FarmerListActivity.class);
                            intent.putExtra("farmersList", Parcels.wrap(response.body().getAaData()));
                            intent.putExtra("searchModel", Parcels.wrap(searchModel));
                            intent.putExtra("mode", mode);
                            intent.putExtra("userType", 1);
                            startActivity(intent);
                        } else {
                            Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.notfound), Toast.LENGTH_SHORT).show();
                        }
//                    } else {
//                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
//                    }
                    } else {
                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
                    }


            }

            @Override
            public void onFailure(Call<FarmerSearchList> call, Throwable t) {

                progressBar.setVisibility(View.GONE);
                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getListFarm(Integer iDisplayStart, Integer iDisplayLength, String sSearch, Integer ProvinceId
            , Integer AmphurId, Integer TambolId, Integer VillageId, int userType,String latitude
            ,String longitude,String distanceKilometer){
        progressBar.setVisibility(View.VISIBLE);
        Api api = AppUtils.getApiService();
        Call<FarmerSearchList> call = api.getListFarmD(iDisplayStart,iDisplayLength,sSearch,ProvinceId
                ,AmphurId,TambolId,VillageId,userType,latitude,longitude,distanceKilometer);
        call.enqueue(new Callback<FarmerSearchList>() {
            @Override
            public void onResponse(Call<FarmerSearchList> call, Response<FarmerSearchList> response) {
                progressBar.setVisibility(View.GONE);

                if (response.body() != null) {
//                    if (response.body().getResponseCode().equals(getString(R.string.success))) {
                    if(response.body().getAaData()!=null) {
                        if (response.body().getAaData().size() != 0) {
                            Intent intent = new Intent(getContext(), FarmerListActivity.class);
                            intent.putExtra("farmersList", Parcels.wrap(response.body().getAaData()));
                            intent.putExtra("searchModel", Parcels.wrap(searchModel));
                            intent.putExtra("mode", mode);
                            intent.putExtra("userType", 1);
                            startActivity(intent);
                        } else {
                            Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.notfound), Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.notfound), Toast.LENGTH_SHORT).show();
                    }
//                    } else {
//                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
//                    }
                } else {
                    Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<FarmerSearchList> call, Throwable t) {

                progressBar.setVisibility(View.GONE);
                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void getListProvince() {
        ProvinceEntity provinceEntity =new ProvinceEntity();
        provinceEntity.setProvince_NameTh("ทั้งหมด");
        provinceEntity.setProvince_ID(-99);
        province.add(provinceEntity);

        province.addAll(dbHelper.getProvinceList());

        for (int i = 0; i < province.size(); i++) {
            listProvince.add(province.get(i).getProvince_NameTh());
        }
        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listProvince);

        spnProvince.setAdapter(adapter);

        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                provinceID=province.get(position).getProvince_ID();
                if(province.get(position).getProvince_ID()!=-99) {
                    spnTambol.setEnabled(true);
                    spnAmphur.setEnabled(true);
                    getListAmphur(province.get(position).getProvince_ID());
                }else{
                    spnAmphur.setEnabled(false);
                    amphurID=-99;
                    spnTambol.setEnabled(false);
                    tambolID=-99;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListAmphur(int position) {
        listAmphur.clear();
        amphur.clear();
        AmphurEntity amphurEntity =new AmphurEntity();
        amphurEntity.setAmphur_nameTh("ทั้งหมด");
        amphurEntity.setAmphur_ID(-99);
        amphur.add(amphurEntity);

        amphur.addAll(dbHelper.getAmphur(position));

        for (int i = 0; i < amphur.size(); i++) {
            listAmphur.add(amphur.get(i).getAmphur_nameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listAmphur);
        spnAmphur.setAdapter(adapter);

        spnAmphur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                amphurID=amphur.get(position).getAmphur_ID();
                if(amphur.get(position).getProvince_ID()!=-99) {
                    spnTambol.setEnabled(true);
                    getListTambol(amphur.get(position).getAmphur_ID());
                }else{
                    spnTambol.setEnabled(false);
                    tambolID=-99;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        int organizeLevelID = permission.getOrganizeLevelID();
        String orgCode = permission.getOrgCode();
        if(organizeLevelID==4) {
            for (int i = 0; i < amphur.size(); i++) {
                if (amphur.get(i).getAmphur_ID() == Integer.parseInt(orgCode)) {
                    spnAmphur.setSelection(i);
                }
            }
            spnAmphur.setEnabled(false);
        }
    }

    public void getListTambol(int position) {
        listTambol.clear();
        tambol.clear();
        TambolEntity tambolEntity =new TambolEntity();
        tambolEntity.setTambol_NameTh("ทั้งหมด");
        tambolEntity.setTambol_ID(-99);
        tambol.add(tambolEntity);

        tambol.addAll( dbHelper.getTambol(position));

        for (int i = 0; i < tambol.size(); i++) {
            listTambol.add(tambol.get(i).getTambol_NameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listTambol);
        spnTambol.setAdapter(adapter);

        spnTambol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tambolID=tambol.get(position).getTambol_ID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListTypeFind() {

        int selectIndex=0;
        typeFind.add("พื้นที่");
        typeFind.add("ชื่อเกษตรกร");
        typeFind.add("พื้นที่ใกล้เคียง");
        typeFind.add("เลขบัตรประจำตัวประชาชน");

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                typeFind);
        spnTypeFind.setAdapter(adapter);
        spnTypeFind.setSelection(selectIndex);

        spnTypeFind.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spnTypeFind.setSelection(position);

                mode=position;

                if (position==0){
                    linearLayout_near.setVisibility(View.GONE);
                    linearLayout_name.setVisibility(View.GONE);
                    linearLayout_address.setVisibility(View.VISIBLE);
                    linearLayout_pid.setVisibility(View.GONE);
                }else if (position==1){
                    linearLayout_near.setVisibility(View.GONE);
                    linearLayout_name.setVisibility(View.VISIBLE);
                    linearLayout_address.setVisibility(View.GONE);
                    linearLayout_pid.setVisibility(View.GONE);
                }else if (position==2){

                    if (!AppUtils.checkPermissionLocation(getContext())) {
                        requestPermissions(
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                AppUtils.RequestPermissionCodeLocation);
                    }else {
                        showNear();
                    }
                }else {
                    linearLayout_near.setVisibility(View.GONE);
                    linearLayout_name.setVisibility(View.GONE);
                    linearLayout_address.setVisibility(View.GONE);
                    linearLayout_pid.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void showNear(){
        flag = displayGpsStatus();
        if (flag) {
            isLoading = true;
            progressBar.setVisibility(View.VISIBLE);

            loadLocation(getContext());

        } else {
            Toast.makeText(getContext(), "Gps Status!!, Your GPS is: OFF", Toast.LENGTH_SHORT).show();
        }

        linearLayout_near.setVisibility(View.VISIBLE);
        linearLayout_name.setVisibility(View.GONE);
        linearLayout_address.setVisibility(View.GONE);
        linearLayout_pid.setVisibility(View.GONE);
    }


    public void getListTypeFarmer() {

        int selectIndex=0;
        typeFarmer.add("เกษตรทั่วไป");
        typeFarmer.add("เกษตรต่างด้าว");


        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                typeFarmer);
        spnTypeFarmer.setAdapter(adapter);
        spnTypeFarmer.setSelection(selectIndex);

        spnTypeFarmer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spnTypeFind.setSelection(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /*----Method to Check GPS is enable or disable ----- */
    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = getActivity().getBaseContext()
                .getContentResolver();
        boolean gpsStatus = Settings.Secure
                .isLocationProviderEnabled(contentResolver,
                        LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;

        } else {
            return false;
        }
    }

    public void loadLocation(Context context) {
        // when you need location
        // if inside activity context = this;

        SingleShotLocationProvider.requestSingleUpdate(context,
                new SingleShotLocationProvider.LocationCallback() {
                    @Override
                    public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                        android.util.Log.d("Location", "my location is " + location.toString());

                        lng = "" + location.longitude;
//                        android.util.Log.v(CLASS_NAME, lng);
                        lat = "" + location.latitude;
//                        android.util.Log.v(CLASS_NAME, lat);
                        edt_lat.setText(lat);
                        edt_long.setText(lng);

                        progressBar.setVisibility(View.GONE);

                        isLoading = false;
                    }

                    @Override
                    public void onNewLocationFail() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data!=null) {
            IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (scanResult != null) {
                String re = scanResult.getContents();
                txtPID.setText(re);
               /* if (re.length() != 0) {
                    loginPID(re);
                } else {
                    Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.field_pid), Toast.LENGTH_LONG).show();
                }*/

            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case AppUtils.RequestPermissionCodeLocation:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    if (StoragePermission) {
                        showNear();
                    } else {
                        Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;

        }
    }
}
