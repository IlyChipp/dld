package com.zealtech.dld.dldregist.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;

import butterknife.ButterKnife;

public class FamilyFragment extends Fragment {

    private MyEditNoHLTextView txtPID;
    private Spinner spnSex;
    private MyEditNoHLTextView txtName;
    private MyEditNoHLTextView txtlastname;
    private Spinner spnStatus;
    private Spinner spnDOB;
    private Spinner spnStatusHelp;
    private Spinner spnEducate;
    private LinearLayout llMember;
    private LinearLayout addMember;
    private LinearLayout btnAddMember;

    public FamilyFragment() {
        super();
    }

    public static FamilyFragment newInstance() {
        FamilyFragment fragment = new FamilyFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_family, container, false);
        View addMember = inflater.inflate(R.layout.item_family, container, false);
        ButterKnife.bind(this, rootView);
        initInstances(rootView, addMember);
        llMember.addView(addMember);

        btnAddMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View addMember = inflater.inflate(R.layout.item_family, container, false);
                llMember.addView(addMember);
            }
        });
        return rootView;
    }

    private void initInstances(View rootView, View view) {
        // Init 'View' instance(s) with rootView.findViewById here
        txtPID = (MyEditNoHLTextView) view.findViewById(R.id.txtPID);
        spnSex = (Spinner) view.findViewById(R.id.spnSex);
        txtName = (MyEditNoHLTextView) view.findViewById(R.id.txtName);
        txtlastname = (MyEditNoHLTextView) view.findViewById(R.id.txtlastname);
        spnStatus = (Spinner) view.findViewById(R.id.spnStatus);
        spnDOB = (Spinner) view.findViewById(R.id.spnDOB);
        spnStatusHelp = (Spinner) view.findViewById(R.id.spnStatusHelp);
        spnEducate = (Spinner) view.findViewById(R.id.spnEducate);
        llMember = (LinearLayout) rootView.findViewById(R.id.llMember);
        btnAddMember = (LinearLayout) rootView.findViewById(R.id.btnAddMember);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }
}
