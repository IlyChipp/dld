package com.zealtech.dld.dldregist.util;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Created by fanqfang on 10/22/2017 AD.
 */

public class DecimalDigitsInputFilter implements InputFilter {
    int numDigits;

    public DecimalDigitsInputFilter(int numDigits) {
        this.numDigits = numDigits;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end,
                               Spanned dest, int dstart, int dend) {
        String s = (String)source;
        String[] spl = s.split("\\.");
        if (spl.length == 1)
            return s;
        if (spl.length == 2) {
            return spl[0] + "." + spl[1].substring(0, Math.min(numDigits, spl[1].length()));
        }
        return null;
    }
}
