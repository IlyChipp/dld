package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 10/3/2017 AD.
 */
@Parcel
public class StandardMaster implements APIResponse{

    public static final String TABLE = "standard";

    String responseCode;
    String responseMessage;
    ArrayList<StandardEntity> data;

    public StandardMaster() { }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<StandardEntity> getData() {
        return data;
    }

    public void setData(ArrayList<StandardEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class StandardEntity {
        int standard_ID = 0 ;
        int orderIndex;
        String standard_Name;
        int status_ID;

        public StandardEntity(){ }

        public int getStandard_ID() {
            return standard_ID;
        }

        public void setStandard_ID(int standard_ID) {
            this.standard_ID = standard_ID;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public String getStandard_Name() {
            return standard_Name;
        }

        public void setStandard_Name(String standard_Name) {
            this.standard_Name = standard_Name;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        @Override
        public String toString() {
            return standard_Name;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String STANDARD_ID = "standard_ID";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STANDARD_NAME = "standard_Name";
        public static final String STATUS_ID = "status_ID";
    }
}


