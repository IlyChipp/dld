package com.zealtech.dld.dldregist.model;

/**
 * Created by Jirat on 1/9/2018.
 */

public class UpdateData {

    Double animalWorth;

    public Double getAnimalWorth() {
        return animalWorth;
    }

    public void setAnimalWorth(Double animalWorth) {
        this.animalWorth = animalWorth;
    }
}
