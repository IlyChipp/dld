package com.zealtech.dld.dldregist.model.newFarmer;

import org.parceler.Parcel;

/**
 * Created by Windows 8.1 on 26/10/2560.
 */
@Parcel
public class CompanyType {
    public static final String TABLE = "CompanyType";
    String companyTypeName;
    int id;
    int isCompany;

    public String getCompanyTypeName() {
        return companyTypeName;
    }

    public void setCompanyTypeName(String companyTypeName) {
        this.companyTypeName = companyTypeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIsCompany() {
        return isCompany;
    }

    public void setIsCompany(int isCompany) {
        this.isCompany = isCompany;
    }
}
