package com.zealtech.dld.dldregist.model.newFarmer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by fanqfang on 10/29/2017 AD.
 */

public class FarmStandardSupport extends RealmObject {

    @SerializedName("support_Standard_ID")
    @Expose
    private Integer supportStandardID;
    @SerializedName("orderIndex")
    @Expose
    private Integer orderIndex;
    @SerializedName("status_ID")
    @Expose
    private Integer statusID;
    @SerializedName("support_Standard_Name")
    @Expose
    private String supportStandardName;

    private boolean localOnly;

    public boolean isLocalOnly() {
        return localOnly;
    }

    public void setLocalOnly(boolean localOnly) {
        this.localOnly = localOnly;
    }

    public Integer getSupportStandardID() {
        return supportStandardID;
    }

    public void setSupportStandardID(Integer supportStandardID) {
        this.supportStandardID = supportStandardID;
    }

    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    public Integer getStatusID() {
        return statusID;
    }

    public void setStatusID(Integer statusID) {
        this.statusID = statusID;
    }

    public String getSupportStandardName() {
        return supportStandardName;
    }

    public void setSupportStandardName(String supportStandardName) {
        this.supportStandardName = supportStandardName;
    }
}
