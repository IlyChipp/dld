package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/28/2017 AD.
 */
@Parcel
public class FeedKindMaster implements APIResponse{
    //DBHelper
    public static final String TABLE = "feed_kind";

    String responseCode;
    String responseMessage;
    ArrayList<FeedKindEntity> data;

    public FeedKindMaster(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<FeedKindEntity> getData() {
        return data;
    }

    public void setData(ArrayList<FeedKindEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class FeedKindEntity{
        int feed_Kind_ID;
        String feed_Kind_NameEn;
        String feed_Kind_NameTh;
        int orderIndex;
        int status_ID;

        public FeedKindEntity(){ }

        public int getFeed_Kind_ID() {
            return feed_Kind_ID;
        }

        public void setFeed_Kind_ID(int feed_Kind_ID) {
            this.feed_Kind_ID = feed_Kind_ID;
        }

        public String getFeed_Kind_NameEn() {
            return feed_Kind_NameEn;
        }

        public void setFeed_Kind_NameEn(String feed_Kind_NameEn) {
            this.feed_Kind_NameEn = feed_Kind_NameEn;
        }

        public String getFeed_Kind_NameTh() {
            return feed_Kind_NameTh;
        }

        public void setFeed_Kind_NameTh(String feed_Kind_NameTh) {
            this.feed_Kind_NameTh = feed_Kind_NameTh;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        @Override
        public String toString() {
            return feed_Kind_NameTh;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String FEED_KIND_ID = "feed_Kind_ID";
        public static final String FEED_KIND_NAME_EN = "feed_Kind_NameEn";
        public static final String FEED_KIND_NAME_TH = "feed_Kind_NameTh";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
    }
}
