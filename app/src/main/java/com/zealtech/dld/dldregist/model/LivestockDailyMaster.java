package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/26/2017 AD.
 */
@Parcel
public class LivestockDailyMaster implements APIResponse{
    //DBHelper
    public static final String TABLE = "list_livestock";

    String responseCode;
    String responseMessage;
    ArrayList<LivestockDailyEntity> data;

    public LivestockDailyMaster(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<LivestockDailyEntity> getData() {
        return data;
    }

    public void setData(ArrayList<LivestockDailyEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class LivestockDailyEntity{
        int livestock_NoteType_ID;
        String createBy;
        String createDate;
        String livestock_NoteType_FlgTail;
        String livestock_NoteType_Name;
        int livestock_NoteType_Parent_ID;
        String livestock_NoteType_Unit_Name;
        int orderIndex;
        int status_ID;
        String updateBy;
        String updateDate;

        int livestock_In;
        int livestock_Out;
        int livestock_Product;

        public LivestockDailyEntity(LivestockDailyEntity livestockDailyEntity){
            this.livestock_NoteType_ID=livestockDailyEntity.getLivestock_NoteType_ID();
            this.createBy=livestockDailyEntity.getCreateBy();
            this.createDate=livestockDailyEntity.getCreateDate();
            this.livestock_NoteType_FlgTail=livestockDailyEntity.getLivestock_NoteType_FlgTail();
            this.livestock_NoteType_Name=livestockDailyEntity.getLivestock_NoteType_Name();
            this.livestock_NoteType_Parent_ID=livestockDailyEntity.getLivestock_NoteType_Parent_ID();
            this.livestock_NoteType_Unit_Name=livestockDailyEntity.getLivestock_NoteType_Unit_Name();
            this.orderIndex=livestockDailyEntity.getOrderIndex();
            this.status_ID=livestockDailyEntity.getStatus_ID();
            this.updateBy=livestockDailyEntity.getUpdateBy();
            this.updateDate=livestockDailyEntity.getUpdateDate();
        }

        public LivestockDailyEntity(){

        }

        public int getLivestock_NoteType_ID() {
            return livestock_NoteType_ID;
        }

        public void setLivestock_NoteType_ID(int livestock_NoteType_ID) {
            this.livestock_NoteType_ID = livestock_NoteType_ID;
        }

        public String getCreateBy() {
            return createBy;
        }

        public void setCreateBy(String createBy) {
            this.createBy = createBy;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getLivestock_NoteType_FlgTail() {
            return livestock_NoteType_FlgTail;
        }

        public void setLivestock_NoteType_FlgTail(String livestock_NoteType_FlgTail) {
            this.livestock_NoteType_FlgTail = livestock_NoteType_FlgTail;
        }

        public String getLivestock_NoteType_Name() {
            return livestock_NoteType_Name;
        }

        public void setLivestock_NoteType_Name(String livestock_NoteType_Name) {
            this.livestock_NoteType_Name = livestock_NoteType_Name;
        }

        public int getLivestock_NoteType_Parent_ID() {
            return livestock_NoteType_Parent_ID;
        }

        public void setLivestock_NoteType_Parent_ID(int livestock_NoteType_Parent_ID) {
            this.livestock_NoteType_Parent_ID = livestock_NoteType_Parent_ID;
        }

        public String getLivestock_NoteType_Unit_Name() {
            return livestock_NoteType_Unit_Name;
        }

        public void setLivestock_NoteType_Unit_Name(String livestock_NoteType_Unit_Name) {
            this.livestock_NoteType_Unit_Name = livestock_NoteType_Unit_Name;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        public String getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(String updateBy) {
            this.updateBy = updateBy;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public int getLivestock_In() {
            return livestock_In;
        }

        public void setLivestock_In(int livestock_In) {
            this.livestock_In = livestock_In;
        }

        public int getLivestock_Out() {
            return livestock_Out;
        }

        public void setLivestock_Out(int livestock_Out) {
            this.livestock_Out = livestock_Out;
        }

        public int getLivestock_Product() {
            return livestock_Product;
        }

        public void setLivestock_Product(int livestock_Product) {
            this.livestock_Product = livestock_Product;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String LIVESTOCK_TYPE_ID = "livestock_Type_ID";
        public static final String CREATE_BY = "createBy";
        public static final String CREATE_DATE = "createDate";
        public static final String LIVESTOCK_TYPE_CODE = "livestock_Type_Code";
        public static final String LIVESTOCK_TYPE_FLGTAIL = "livestock_Type_Flgtail";
        public static final String LIVESTOCK_TYPE_FULLNAME = "livestock_Type_Fullname";
        public static final String LIVESTOCK_TYPE_NAME = "livestock_Type_Name";
        public static final String LIVESTOCK_TYPE_OLD_ID = "livestock_Type_Old_ID";
        public static final String LIVESTOCK_TYPE_PARENT = "livestock_Type_Parent";
        public static final String LIVESTOCK_TYPE_PRICE = "livestock_Type_Price";
        public static final String LIVESTOCK_TYPE_UNIT = "livestock_Type_Unit";
        public static final String LIVESTOCK_TYPE_UNITLIMIT = "livestock_Type_UnitLimit";
        public static final String UPDATE_BY = "updateBy";
        public static final String UPDATE_DATE = "updateDate";
        public static final String STATUS_ID = "status_ID";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String LIVESTOCK_TYPE_ROOT = "livestock_Type_Root";
    }
}
