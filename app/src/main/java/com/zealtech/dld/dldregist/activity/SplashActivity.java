package com.zealtech.dld.dldregist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.manager.Contextor;
import com.zealtech.dld.dldregist.model.SearchModel;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseFarmer;
import com.zealtech.dld.dldregist.util.AppUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.zealtech.dld.dldregist.util.AppUtils.UUID;

public class SplashActivity extends AppCompatActivity {

    Handler handler;
    Runnable runnable;
    long delay_time;
    long time = 3000L;
    SearchModel searchModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final int userId = AppUtils.getUserId(SplashActivity.this);
        final int userType=AppUtils.getUserType(SplashActivity.this);
        final long userPid = AppUtils.getUserPid(SplashActivity.this);

//        if(userId==2){ //case user admin
//            MyPreferenceManager myPreferenceManager = new MyPreferenceManager(SplashActivity.this);
//            myPreferenceManager.setUserId(0);
//            myPreferenceManager.setUserType(0);
//            myPreferenceManager.setUserPid(0);
//        }

        handler = new Handler();

        runnable = new Runnable() {
                public void run() {
                    if (userId!=0){ //user exist
                        if (userType ==1){
                            Intent intent = new Intent(SplashActivity.this, FindFarmerActivity.class);
                            startActivity(intent);
                            finish();
                        }else if (userType==2){
                            searchModel = new SearchModel();
                            searchModel.setPid(userPid);
//                            getListFarmer(null, null, String.valueOf(userPid), null, null, null, null, 1,null,null,null);
                            getFarmerByPID(userPid);
                        }else {
                            Intent intent = new Intent(SplashActivity.this, SelectTypeActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }else {
                        Intent intent = new Intent(SplashActivity.this, SelectTypeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            };



    }

    public void onResume() {
        super.onResume();
        delay_time = time;
        handler.postDelayed(runnable, delay_time);
        time = System.currentTimeMillis();
    }

    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
        time = delay_time - (System.currentTimeMillis() - time);
    }
//    private void getListFarmer(Integer iDisplayStart, Integer iDisplayLength, String sSearch, Integer ProvinceId
//            , Integer AmphurId, Integer TambolId, Integer VillageId, int userType,String latitude
//            ,String longitude,String distanceKilometer ){
////        progressBar.setVisibility(View.VISIBLE);
//        Api api = AppUtils.getApiService();
//        Call<FarmerSearchList> call = api.getListFarmer(iDisplayStart,iDisplayLength,sSearch,ProvinceId
//                ,AmphurId,TambolId,VillageId,userType,latitude,longitude,distanceKilometer);
//        call.enqueue(new Callback<FarmerSearchList>() {
//            @Override
//            public void onResponse(Call<FarmerSearchList> call, Response<FarmerSearchList> response) {
////                progressBar.setVisibility(View.GONE);
//                if (response.body() != null) {
////                    if (response.body().getResponseCode().equals(getString(R.string.success))) {
//                    if(response.body().getAaData().size()!=0) {
//                        Intent intent = new Intent(SplashActivity.this, FarmerListActivity.class);
//                        intent.putExtra("farmersList", Parcels.wrap(response.body().getAaData()));
//                        intent.putExtra("searchModel", Parcels.wrap(searchModel));
//                        intent.putExtra("mode", 3); //case pid
//                        intent.putExtra("userType", 1); //case farmer
//                        startActivity(intent);
//                        finish();
//                    }else{
//                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.notfound), Toast.LENGTH_SHORT).show();
//                    }
////                    } else {
////                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
////                    }
//                } else {
//                    Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<FarmerSearchList> call, Throwable t) {
//
////                progressBar.setVisibility(View.GONE);
//                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private void getFarmerByPID(long idcard) {
//        progressBar2.setVisibility(View.VISIBLE);
        Api api = AppUtils.getApiService();
        Call<ResponseFarmer> call = api.getFarmerByPID(idcard);
        call.enqueue(new Callback<ResponseFarmer>() {
            @Override
            public void onResponse(Call<ResponseFarmer> call, Response<ResponseFarmer> response) {
//                progressBar2.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getResponseCode().equals(getString(R.string.success))) {

                        DatabaseRealm databaseRealm = new DatabaseRealm(SplashActivity.this);
//                        databaseRealm.deleteDataFarmerOnline();
                        Farmer data = response.body().getData();
//                        AppUtils.addDefaultToNull(data);
                        data.setOnline(true);
                        String uuid=databaseRealm.addDataFarmerUUID(data);

                        Intent intent = new Intent(SplashActivity.this, FarmActivity.class); //mode DashBoard > list farm
                        intent.putExtra(UUID, uuid);
                        intent.putExtra("mode", "dashBoard");
                        intent.putExtra("farmId", 0);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseFarmer> call, Throwable t) {
//                progressBar2.setVisibility(View.GONE);
                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
