package com.zealtech.dld.dldregist.config;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by User on 8/7/2559.
 */
public class SingleShotLocationProvider {
    public static interface LocationCallback {
        public void onNewLocationAvailable(GPSCoordinates location);
        public void onNewLocationFail();
    }

    // calls back to calling thread, note this is for low grain: if you want higher precision, swap the
    // contents of the else and if. Also be sure to check gps permission/settings are allowed.
    // call usually takes <10ms
    public static void requestSingleUpdate(final Context context, final LocationCallback callback) {
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (isNetworkEnabled) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_COARSE);

            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestSingleUpdate(criteria, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Geocoder gcd = new Geocoder(context,
                            Locale.getDefault());
                    List<Address> addresses;
                    String AddressLine = null;
                    String AddressLine2 = null;
                    String AddressLine3 = null;
                    try {
                        addresses = gcd.getFromLocation(location.getLatitude(), location
                                .getLongitude(), 1);
                        if (addresses.size() > 0)

                            AddressLine = addresses.get(0).getAddressLine(0);
                        AddressLine2 = addresses.get(0).getAddressLine(1);
                        AddressLine3 = addresses.get(0).getAddressLine(2);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    callback.onNewLocationAvailable(new GPSCoordinates(location.getLongitude(), location.getLatitude(), AddressLine, AddressLine2, AddressLine3));
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                    callback.onNewLocationFail();
                }

                @Override
                public void onProviderEnabled(String provider) {
                    callback.onNewLocationFail();
                }

                @Override
                public void onProviderDisabled(String provider) {
                    callback.onNewLocationFail();
                }
            }, null);
        } else {
            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (isGPSEnabled) {
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                locationManager.requestSingleUpdate(criteria, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {

                        Geocoder gcd = new Geocoder(context,
                                Locale.getDefault());
                        List<Address> addresses;
                        String AddressLine = null;
                        String AddressLine2 = null;
                        String AddressLine3 = null;
                        try {
                            addresses = gcd.getFromLocation(location.getLatitude(), location
                                    .getLongitude(), 1);
                            if (addresses.size() > 0)

                                AddressLine = addresses.get(0).getAddressLine(0);
                            AddressLine2 = addresses.get(0).getAddressLine(1);
                            AddressLine3 = addresses.get(0).getAddressLine(2);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        callback.onNewLocationAvailable(new GPSCoordinates(location.getLongitude(), location.getLatitude(), AddressLine, AddressLine2, AddressLine3));
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {
                        callback.onNewLocationFail();
                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                        callback.onNewLocationFail();
                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                        callback.onNewLocationFail();
                    }


                }, null);
            }else {
                callback.onNewLocationFail();
            }
        }
    }

    public static void stopingleUpdate(Context context) {
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.removeUpdates((LocationListener) context);

    }
    // consider returning Location instead of this dummy wrapper class
    public static class GPSCoordinates {
        public float longitude = -1;
        public float latitude = -1;
        public String AddressLine =null;
        public String AddressLine2 =null;
        public String AddressLine3 =null;

        public GPSCoordinates(float theLongitude, float theLatitude, String theAddressLine, String theAddressLine2, String theAddressLine3) {
            longitude = theLongitude;
            latitude = theLatitude;
            AddressLine = theAddressLine;
            AddressLine2 = theAddressLine2;
            AddressLine3 = theAddressLine3;
        }

        public GPSCoordinates(double theLongitude, double theLatitude, String theAddressLine, String theAddressLine2, String theAddressLine3) {
            longitude = (float) theLongitude;
            latitude = (float) theLatitude;
            AddressLine =  theAddressLine;
            AddressLine2 =  theAddressLine2;
            AddressLine3 = theAddressLine3;
        }
    }
}