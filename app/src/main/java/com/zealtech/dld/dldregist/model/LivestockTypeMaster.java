package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import java.util.ArrayList;

/**
 * Created by fanqfang on 11/7/2017 AD.
 */

public class LivestockTypeMaster implements APIResponse{

    public static final String TABLE = "livestock_type";

    String responseCode;
    String responseMessage;
    ArrayList<TypeEntity> data;

    @Override
    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<TypeEntity> getData() {
        return data;
    }

    public void setData(ArrayList<TypeEntity> data) {
        this.data = data;
    }

    public static class TypeEntity {
        int farm_Standard_LivestockType_ID;
        String farm_Standard_LivestockType_Name;
        int orderIndex;
        int status_ID;

        public int getFarm_Standard_LivestockType_ID() {
            return farm_Standard_LivestockType_ID;
        }

        public void setFarm_Standard_LivestockType_ID(int farm_Standard_LivestockType_ID) {
            this.farm_Standard_LivestockType_ID = farm_Standard_LivestockType_ID;
        }

        public String getFarm_Standard_LivestockType_Name() {
            return farm_Standard_LivestockType_Name;
        }

        public void setFarm_Standard_LivestockType_Name(String farm_Standard_LivestockType_Name) {
            this.farm_Standard_LivestockType_Name = farm_Standard_LivestockType_Name;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        @Override
        public String toString() {
            return farm_Standard_LivestockType_Name;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String FARM_STANDARD_LIVESTOCK_TYPE_ID = "farm_Standard_LivestockType_ID";
        public static final String FARM_STANDARD_LIVESTOCK_TYPE_NAME = "farm_Standard_LivestockType_Name";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
    }
}
