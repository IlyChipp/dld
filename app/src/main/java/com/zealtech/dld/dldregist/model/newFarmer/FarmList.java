package com.zealtech.dld.dldregist.model.newFarmer;

import java.util.ArrayList;

/**
 * Created by Windows 8.1 on 26/10/2560.
 */

public class FarmList {
    private ArrayList<Feed> feed;
    private ArrayList<FarmStandard> farm_standard;
    private ArrayList<Livestock> livestock;
    private ArrayList<FarmImage> farm_image;

    Farm farm;
    Worker worker;

    public ArrayList<Feed> getFeed() {
        return feed;
    }

    public void setFeed(ArrayList<Feed> feed) {
        this.feed = feed;
    }

    public ArrayList<FarmStandard> getFarm_standard() {
        return farm_standard;
    }

    public void setFarm_standard(ArrayList<FarmStandard> farm_standard) {
        this.farm_standard = farm_standard;
    }

    public ArrayList<Livestock> getLivestock() {
        return livestock;
    }

    public void setLivestock(ArrayList<Livestock> livestock) {
        this.livestock = livestock;
    }

    public ArrayList<FarmImage> getFarm_image() {
        return farm_image;
    }

    public void setFarm_image(ArrayList<FarmImage> farm_image) {
        this.farm_image = farm_image;
    }

    public Farm getFarm() {
        return farm;
    }

    public void setFarm(Farm farm) {
        this.farm = farm;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }
}
