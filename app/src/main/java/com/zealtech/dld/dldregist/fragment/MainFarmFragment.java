package com.zealtech.dld.dldregist.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.zealtech.dld.dldregist.MainApplication;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.FarmActivity;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.model.AmphurMaster.AmphurEntity;
import com.zealtech.dld.dldregist.model.ProvinceMaster.ProvinceEntity;
import com.zealtech.dld.dldregist.model.TambolMaster.TambolEntity;
import com.zealtech.dld.dldregist.model.VillageMaster.VillageEntity;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MySpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainFarmFragment extends Fragment {

    private static final String CLASS_NAME = "MainFarmFragment";

    @BindView(R.id.spn_farm)
    Spinner spn_farm;
    @BindView(R.id.relative_select_farm)
    RelativeLayout relative_select_farm;

    ViewPager viewPager;

    private TabLayout tabLayout;
    private final List<String> titleList = new ArrayList<>();
    ArrayList<Farm> farms = new ArrayList<>();
    ArrayList<String> farmName = new ArrayList<>();

    FarmDataFragment farmData;
    FarmInfoMainFragment farmInfoMain;
    FeedHeadFragment feedHead;
    LiveStockFragment liveStock;

    ArrayList<VillageEntity> villages = new ArrayList<>();
    ArrayList<TambolEntity> tambols = new ArrayList<>();
    ArrayList<AmphurEntity> amphurs = new ArrayList<>();
    ArrayList<ProvinceEntity> provinces = new ArrayList<>();

    DBHelper dbHelper;
    int farmid;
    Tracker mTracker;
    public MainFarmFragment() {
        super();
    }

    public static MainFarmFragment newInstance() {
        MainFarmFragment fragment = new MainFarmFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        initInstances(rootView);
        ButterKnife.bind(this,rootView);

        dbHelper = new DBHelper(getActivity());
        MainApplication application = (MainApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
//        Bundle args = getArguments();
//        data = (FarmerModel) args.getSerializable("LoginPID");
        /*relative_select_farm= (RelativeLayout) rootView.findViewById(R.id.relative_select_farm);
        relative_select_farm.setVisibility(View.GONE)*/;
        farms.addAll(((FarmActivity) (getActivity())).getAllFarm());

        if(farms.size()!=0) {//case not new register (have farm)
            relative_select_farm.setVisibility(View.VISIBLE);
            for (int i = 0; i < farms.size(); i++) {
//                farmName.add(farms.get(i).getFarmName());
                farmName.add(getAddress(farms.get(i)));
            }

            MySpinnerAdapter adapter = new MySpinnerAdapter(
                    getContext(),
                    android.R.layout.simple_dropdown_item_1line,
                    farmName);

            spn_farm.setAdapter(adapter);

            spn_farm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    farmid=farms.get(position).getFarmID();
                    ((FarmActivity) getActivity()).setCurrent_FarmID(farms.get(position).getFarmID()); // for online
                    ((FarmActivity) getActivity()).setCurrent_FarmPosition(position); //for offline

                    if (farmData != null) {
                        farmData.setUpData();
                        farmInfoMain.setUpData();
                        feedHead.setUpData();
                        liveStock.setUpData();
                    } else {
                        initLayout();
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            int position=0;
            for (int i = 0; i < farms.size(); i++) {
                if(farms.get(i).getFarmID()==((FarmActivity) getActivity()).getCurrent_FarmID()){
                    position=i;
                    break;
                }
            }
            spn_farm.setSelection(position);

        }else{ //case not have farm
            relative_select_farm.setVisibility(View.GONE);
            initLayout();
        }


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        mTracker.setScreenName("หน้าข้อมูลฟาร์ม");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    private void getMasters(Farm farm_a){
        dbHelper.getProvinceById(farm_a.getProvinceID());
//        getAmphurById();
//        getTambolById();
//        getVillageById();
    }

    private String getAddress(Farm farm_a){
        String address="";
        String home="";
        String soi="";
        String road="";
        String post="";

        String province= dbHelper.getProvinceById(farm_a.getProvinceID()).getProvince_NameTh();
        String amphur= dbHelper.getAmphurById(farm_a.getAmphurID()).getAmphur_nameTh();
        String tambol= dbHelper.getTambolById(farm_a.getTambolID()).getTambol_NameTh();

        if(farm_a.getSoi()!=null&&!farm_a.getSoi().equals("")){
            soi=" ซอย "+farm_a.getSoi();
        }

        if(farm_a.getRoad()!=null&&!farm_a.getRoad().equals("")){
            road=" ถนน "+farm_a.getRoad();
        }

        if(farm_a.getPostCode()!=null&&!farm_a.getPostCode().equals("")){
            post=""+farm_a.getPostCode();
        }

        if(farm_a.getHomeNo()!=null&&!farm_a.getHomeNo().equals("")){
            home=""+farm_a.getHomeNo();
        }

        address=home+soi+road
                + " " +tambol
                + " " +amphur
                + " " +province
                +" "+post;

        return address;
    }

    final private static String UUID = "uuid";
    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
    }

    private void initLayout(){

        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setBackgroundColor(getResources().getColor(R.color.slidingTab));

        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        setTabs();
    }

    private void setupViewPager(ViewPager viewPager) {
        if (viewPager.getAdapter() != null) {
            viewPager.setAdapter(null);

        }
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
//        adapter.addFragment(new ListFarmFragment(), getString(R.string.all_farms));

        farmData = new FarmDataFragment();
        farmInfoMain = new FarmInfoMainFragment();
        feedHead = new FeedHeadFragment();
        liveStock = new LiveStockFragment();

        adapter.addFragment(farmData, getString(R.string.all_farms));
        adapter.addFragment(farmInfoMain, getString(R.string.husbandry));
        adapter.addFragment(feedHead, getString(R.string.feed));
        adapter.addFragment(liveStock, getString(R.string.livestock));
        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(adapter);
        if (((FarmActivity) getActivity()).getMode() != null) {
            if (((FarmActivity) getActivity()).getMode().equals("live")) {
                viewPager.setCurrentItem(3);
            }else if (((FarmActivity) getActivity()).getMode().equals(FarmActivity.MODE_ADD)){
                viewPager.setCurrentItem(1);
            }
        }
        ((FarmActivity) getActivity()).setViewPager(viewPager);

        viewPager.getAdapter().notifyDataSetChanged();
    }

    private void setTabCat(int i) {

        MyNoHBTextView tabOne = (MyNoHBTextView) LayoutInflater.from(getActivity()).inflate(R.layout.item_tab, null);
        tabOne.setText(titleList.get(i));
        tabLayout.getTabAt(i).setCustomView(tabOne);

    }

    private void setTabs() {
        for (int i = 0; i < 4; i++) {
            setTabCat(i);
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        Double aniWorth=0.0;

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
            titleList.add(title);
        }

//        @Override
//        public int getItemPosition(Object object) {
//            return POSITION_NONE; //refresh fragment for update data
//        }

//        public void update(Double xyzData) {
//            this.aniWorth = xyzData;
//            notifyDataSetChanged();
//        }
//
        @Override
        public int getItemPosition(Object object) {
            if (object instanceof FarmInfoMainFragment) {
                ((FarmInfoMainFragment) object).update(((FarmActivity)getActivity()).getFarmById(farmid).getAnimalWorth());
            }
            //don't return POSITION_NONE, avoid fragment recreation.
            return super.getItemPosition(object);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    public void replaceCategoryFragment(Fragment fragment) {
        replaceFragment(fragment);
    }

    public void replaceFragment(Fragment fragment) {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentContainer, fragment)
                .addToBackStack(null)
                .commit();
    }


}
