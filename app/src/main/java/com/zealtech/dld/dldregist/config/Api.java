package com.zealtech.dld.dldregist.config;


import com.zealtech.dld.dldregist.model.AmphurMaster;
import com.zealtech.dld.dldregist.model.CompanyTypeMaster;
import com.zealtech.dld.dldregist.model.FarmModel;
import com.zealtech.dld.dldregist.model.FarmOwnerMaster;
import com.zealtech.dld.dldregist.model.FarmProblemMaster;
import com.zealtech.dld.dldregist.model.FarmerProblemMaster;
import com.zealtech.dld.dldregist.model.FarmerTypeMaster;
import com.zealtech.dld.dldregist.model.FeedKindMaster;
import com.zealtech.dld.dldregist.model.FeedSubTypeFormatMaster;
import com.zealtech.dld.dldregist.model.FeedSubTypeMaster;
import com.zealtech.dld.dldregist.model.FeedTypeMaster;
import com.zealtech.dld.dldregist.model.HelpMaster;
import com.zealtech.dld.dldregist.model.HoldingLandMaster;
import com.zealtech.dld.dldregist.model.LivestockDaily;
import com.zealtech.dld.dldregist.model.LivestockDailyMaster;
import com.zealtech.dld.dldregist.model.LivestockMaster;
import com.zealtech.dld.dldregist.model.LivestockModel;
import com.zealtech.dld.dldregist.model.LivestockTypeMaster;
import com.zealtech.dld.dldregist.model.LoginModel;
import com.zealtech.dld.dldregist.model.MainJobMaster;
import com.zealtech.dld.dldregist.model.PrefixMaster;
import com.zealtech.dld.dldregist.model.ProvinceMaster;
import com.zealtech.dld.dldregist.model.SecondJobMaster;
import com.zealtech.dld.dldregist.model.StandardMaster;
import com.zealtech.dld.dldregist.model.SupportStandardMaster;
import com.zealtech.dld.dldregist.model.TambolMaster;
import com.zealtech.dld.dldregist.model.VillageMaster;
import com.zealtech.dld.dldregist.model.farmerList.FarmerSearchList;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseBase;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseFarm;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseFarmStandard;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseFarmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseFeed;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseImage;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseImageFarm;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseInsertFarmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseLivestock;
import com.zealtech.dld.dldregist.model.newFarmer.ResponsePermission;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseRegister;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseWorker;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface Api {

    //    @Multipart
//    @POST(ApplicationConfig.UPLOAD_API_URL)
//    Call<ResponseUploadFileItem> uploadFile(@Part MultipartBody.Part file1, @Query("fileType") String fileType, @Query("readGroups") ArrayList<String> allowedRoles, @Header("token") String token);
//
    @POST("farmerApi/checkPidDuplicate/post")
    Call<ResponseBase> checkDuplicateID(@Query("keyDuplicate") String keyDuplicate,@Query("userType") int userType);

    @POST("farmerApi/getFarmerData/post")
    Call<ResponseFarmer> getFarmerByPID(@Query("Pid") long Pid);

    @POST("farmerApi/getListFarmData/post")
    Call<FarmerSearchList> getListFarmD(@Query("iDisplayStart") Integer iDisplayStart, @Query("iDisplayLength") Integer iDisplayLength
            , @Query("sSearch") String sSearch, @Query("ProvinceId") Integer ProvinceId, @Query("AmphurId") Integer AmphurId
            , @Query("TambolId") Integer TambolId, @Query("VillageId") Integer VillageId, @Query("userType") Integer userType
            , @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("distanceKilometer") String distanceKilometer);

    @POST("farmerApi/getListFarmerData/post")
    Call<FarmerSearchList> getListFarmer(@Query("iDisplayStart") Integer iDisplayStart, @Query("iDisplayLength") Integer iDisplayLength
            , @Query("sSearch") String sSearch, @Query("ProvinceId") Integer ProvinceId, @Query("AmphurId") Integer AmphurId
            , @Query("TambolId") Integer TambolId, @Query("VillageId") Integer VillageId, @Query("userType") Integer userType
            , @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("distanceKilometer") String distanceKilometer
            , @Query("Pid") String Pid, @Query("FirstName") String FirstName, @Query("LastName") String LastName);

    @POST("loginApi/post")
    Call<LoginModel> login(@Query("username") String username, @Query("password") String password);

    @POST("amphurMasterData/getList/post")
    Call<AmphurMaster> getListAmphur();

    @POST("prefixMasterData/getList/post")
    Call<PrefixMaster> getListPrefix();

    @POST("provinceMasterData/getList/post")
    Call<ProvinceMaster> getListProvince();

    @POST("tambolMasterData/getList/post")
    Call<TambolMaster> getListTambol();

    @POST("villageMasterData/getList/post")
    Call<VillageMaster> getListVillage();

    @POST("farmApi/getfarmById/post")
    Call<FarmModel> getFarm(@Query("farmId") String farmId);

    @POST("farmOwnerTypeMasterData/getList/post")
    Call<FarmOwnerMaster> getFarmOwner();

    @POST("farmProblemMasterData/getList/post")
    Call<FarmProblemMaster> getFarmProblem();

    @POST("farmerProblemMasterData/getList/post")
    Call<FarmerProblemMaster> getFarmerProblem();

    @POST("farmerTypeMasterData/getList/post")
    Call<FarmerTypeMaster> getFarmerType();

    @POST("feedApi/getList/post")
    Call<ResponseFeed> getFeed(@Query("farmId") String farmId);

    @POST("feedKindMasterData/getList/post")
    Call<FeedKindMaster> getFeedKind();

    @POST("feedSubTypeFormatMasterData/getList/post")
    Call<FeedSubTypeFormatMaster> getFeedSubTypeFormat();

    @POST("feedSubTypeMasterData/getList/post")
    Call<FeedSubTypeMaster> getFeedSubType();

    @POST("feedTypeMasterData/getList/post")
    Call<FeedTypeMaster> getFeedType();

    @POST("governmentHelpMasterData/getList/post")
    Call<HelpMaster> getHelp();

    @POST("holdingLandMasterData/getList/post")
    Call<HoldingLandMaster> getHoldingLand();

    @POST("farmApi/getfarmList/post")
    Call<ResponseFarm> getListFarm(@Query("farmerId") String farmerId,@Query("provinceId") Integer provinceId
            ,@Query("amphurId") Integer amphurId,@Query("tambolId") Integer tambolId);

    @POST("farmApi/insertFarmData/post")
    Call<FarmModel> insertFarm(@Query("farmInformation") String farmInformation);

    @POST("mainJobMasterData/getList/post")
    Call<MainJobMaster> getListMainJob();

    @POST("secondJobMasterData/getList/post")
    Call<SecondJobMaster> getListSecondJob();

    @POST("livestockMasterData/getList/post")
    Call<LivestockMaster> getListLivestock();

    @POST("livestockNoteApi/getMSTypeList/post")
    Call<LivestockDailyMaster> getDailyLivestockMS();

    @FormUrlEncoded
    @POST("livestockNoteApi/insert/post")
    Call<LivestockDaily> insertDailyLiveStock(@Field("livestockNoteInformation") String livestockNoteInformation);

    @POST("livestockApi/getList/post")
    Call<ResponseLivestock> getLivestock(@Query("farmId") String farmId);

    @POST("livestockNoteApi/getList/post")
    Call<LivestockDaily> getLivestockDaily(@Query("farmId") String farmId,@Query("dateFind") String dateFind);

    @POST("livestockApi/insertLivestockData/post")
    Call<LivestockModel> insertLivestock(@Query("livestockInformation") String livestockInformation);

    @POST("standardMasterData/getList/post")
    Call<StandardMaster> getStandard();

    @POST("supportStandardMasterData/getList/post")
    Call<SupportStandardMaster> getSupportStandard();

    @FormUrlEncoded
    @POST("insertMobileDataApi/insert/post")
    Call<ResponseInsertFarmer> setInsertData(@Field ("mobileData") String mobileData);

    @Multipart
    @POST("insertObjectMobileDataApi/insert/post")
    Call<ResponseInsertFarmer> setInsertObjectData(@Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("farmerApi/insertFarmerData/post")
    Call<ResponseRegister> setRegister(@Field ("farmerListInformation") String farmerListInformation, @Field ("flgRegister") int flgRegister);

    @POST("companyTypeMasterData/getList/post")
    Call<CompanyTypeMaster> getCompany();

    @POST("workerData/getList/post")
    Call<ResponseWorker> getWorker(@Query("workerId") String workerId);

    @POST("farmStandardData/getList/post")
    Call<ResponseFarmStandard> getFarmStandard(@Query("farmId") String farmId);

    @POST("permissionApi/post")
    Call<ResponsePermission> getPermission(@Query("userId") String userId);

    @POST("imageData/getImageFarmer/post")
    Call<ResponseImage> getImageFarmer(@Query("pid") String pid);

    @POST("imageData/getImageFarm/post")
    Call<ResponseImageFarm> getImageFarm(@Query("farmId") String farmId);

    @POST("farmStandardData/getLivestockTypeList/post")
    Call<LivestockTypeMaster> getTypeMaster();

}


