
package com.zealtech.dld.dldregist.model.newFarmer;

import com.google.gson.annotations.SerializedName;


public class Permission {

    @SerializedName("approveBy")
    private Integer approveBy;
    @SerializedName("approveDate")
    private Object approveDate;
    @SerializedName("createBy")
    private Integer createBy;
    @SerializedName("createDate")
    private String createDate;
    @SerializedName("email")
    private String email;
    @SerializedName("firstname")
    private String firstname;
    @SerializedName("firstnameEn")
    private String firstnameEn;
    @SerializedName("lastLoginDate")
    private Object lastLoginDate;
    @SerializedName("lastname")
    private String lastname;
    @SerializedName("lastnameEn")
    private String lastnameEn;
    @SerializedName("lvl")
    private Integer lvl;
    @SerializedName("officerId")
    private Integer officerId;
    @SerializedName("orgId")
    private Integer orgId;
    @SerializedName("organizeLevelID")
    private Integer organizeLevelID;
    @SerializedName("orgCode")
    private String orgCode;
    @SerializedName("orgName")
    private String orgName;
    @SerializedName("orgNameEng")
    private String orgNameEng;
    @SerializedName("orgParentID")
    private Integer orgParentID;
    @SerializedName("password")
    private String password;
    @SerializedName("pid")
    private String pid;
    @SerializedName("prefixId")
    private Integer prefixId;
    @SerializedName("salt")
    private Object salt;
    @SerializedName("statusId")
    private Integer statusId;
    @SerializedName("statusName")
    private String statusName;
    @SerializedName("tel")
    private String tel;
    @SerializedName("title")
    private String title;
    @SerializedName("updateBy")
    private Integer updateBy;
    @SerializedName("updateDate")
    private String updateDate;
    @SerializedName("userId")
    private Integer userId;
    @SerializedName("userTypeId")
    private Integer userTypeId;
    @SerializedName("userTypeName")
    private String userTypeName;
    @SerializedName("username")
    private String username;

    public Integer getApproveBy() {
        return approveBy;
    }

    public void setApproveBy(Integer approveBy) {
        this.approveBy = approveBy;
    }

    public Object getApproveDate() {
        return approveDate;
    }

    public void setApproveDate(Object approveDate) {
        this.approveDate = approveDate;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstnameEn() {
        return firstnameEn;
    }

    public void setFirstnameEn(String firstnameEn) {
        this.firstnameEn = firstnameEn;
    }

    public Object getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Object lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastnameEn() {
        return lastnameEn;
    }

    public void setLastnameEn(String lastnameEn) {
        this.lastnameEn = lastnameEn;
    }

    public Integer getLvl() {
        return lvl;
    }

    public void setLvl(Integer lvl) {
        this.lvl = lvl;
    }

    public Integer getOfficerId() {
        return officerId;
    }

    public void setOfficerId(Integer officerId) {
        this.officerId = officerId;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public Integer getOrganizeLevelID() {
        return organizeLevelID;
    }

    public void setOrganizeLevelID(Integer organizeLevelID) {
        this.organizeLevelID = organizeLevelID;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgNameEng() {
        return orgNameEng;
    }

    public void setOrgNameEng(String orgNameEng) {
        this.orgNameEng = orgNameEng;
    }

    public Integer getOrgParentID() {
        return orgParentID;
    }

    public void setOrgParentID(Integer orgParentID) {
        this.orgParentID = orgParentID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Integer getPrefixId() {
        return prefixId;
    }

    public void setPrefixId(Integer prefixId) {
        this.prefixId = prefixId;
    }

    public Object getSalt() {
        return salt;
    }

    public void setSalt(Object salt) {
        this.salt = salt;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Integer userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getUserTypeName() {
        return userTypeName;
    }

    public void setUserTypeName(String userTypeName) {
        this.userTypeName = userTypeName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
