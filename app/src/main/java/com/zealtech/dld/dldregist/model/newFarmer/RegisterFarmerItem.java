
package com.zealtech.dld.dldregist.model.newFarmer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class RegisterFarmerItem {
    @Expose
    private    String username;
    @Expose
    private    String password;


    @Expose
    @SerializedName("farmer_ID")
    private Integer farmerID = 0;
    @Expose
    @SerializedName("amphur_ID")
    private Integer amphurID= 0;
    @Expose
    @SerializedName("birthDay")
    private String birthDay="";
    @Expose
    @SerializedName("company_Type_ID")
    private Integer companyTypeID= 0;
    @Expose
    @SerializedName("createBy")
    private String createBy= "";
    @Expose
    @SerializedName("createDate")
    private String createDate="";
    @Expose
    @SerializedName("debtAmount")
    private Integer debtAmount= 0;
    @Expose
    @SerializedName("debtsAmountIn")
    private Integer debtsAmountIn= 0;
    @Expose
    @SerializedName("debtsAmountOut")
    private Integer debtsAmountOut= 0;
    @Expose
    @SerializedName("email")
    private String email= "";
    @Expose
    @SerializedName("farmer_Group_ID")
    private Integer farmerGroupID= 0;
    @Expose
    @SerializedName("farmer_Problem_ID")
    private Integer farmerProblemID= 0;
    @Expose
    @SerializedName("farmer_Problem_Other")
    private String farmerProblemOther= "";
    @Expose
    @SerializedName("farmer_Status_ID")
    private Integer farmerStatusID= 0;
    @Expose
    @SerializedName("farmer_Type_ID")
    private Integer farmerTypeID= 0;
    @Expose
    @SerializedName("first_Name")
    private String firstName= "";
    @Expose
    @SerializedName("gender_ID")
    private Integer genderID= 0;
    @Expose
    @SerializedName("government_Help_ID")
    private Integer governmentHelpID= 0;
    @Expose
    @SerializedName("holding_Equipment")
    private String holdingEquipment= "";
    @Expose
    @SerializedName("holding_Land_ID")
    private Integer holdingLandID= 0;
    @Expose
    @SerializedName("homeNo")
    private String homeNo="";
    @Expose
    @SerializedName("home_ID")
    private long homeID= 0;
    @Expose
    @SerializedName("isAgency")
    private Integer isAgency= 0;
    @Expose
    @SerializedName("isCancel")
    private Integer isCancel= 0;
    @Expose
    @SerializedName("isDead")
    private Integer isDead= 0;
    @Expose
    @SerializedName("last_Name")
    private String lastName= "";
    @Expose
    @SerializedName("latitude")
    private String latitude= "";
    @Expose
    @SerializedName("longitude")
    private String longitude= "";
    @Expose
    @SerializedName("main_Job_ID")
    private Integer mainJobID= 0;
    @Expose
    @SerializedName("marital_Status_ID")
    private Integer maritalStatusID= 0;
    @Expose
    @SerializedName("mobile")
    private String mobile= "";
    @Expose
    @SerializedName("moo")
    private String moo= "";
    @Expose
    @SerializedName("phone")
    private String phone= "";
    @Expose
    @SerializedName("pid")
    private long pid= 0;
    @Expose
    @SerializedName("postCode")
    private String postCode= "";
    @Expose
    @SerializedName("prefix_ID")
    private Integer prefixID= 0;
    @Expose
    @SerializedName("province_ID")
    private Integer provinceID= 0;
    @Expose
    @SerializedName("revenue")
    private Integer revenue= 0;
    @Expose
    @SerializedName("road")
    private String road= "";
    @Expose
    @SerializedName("second_Job_ID")
    private Integer secondJobID= 0;
    @Expose
    @SerializedName("soi")
    private String soi= "";
    @Expose
    @SerializedName("status_ID")
    private Integer statusID= 0;
    @Expose
    @SerializedName("tambol_ID")
    private Integer tambolID= 0;
    @Expose
    @SerializedName("tax_ID")
    private Long taxID= 0L;
    @Expose
    @SerializedName("updateBy")
    private String updateBy= "";
    @Expose
    @SerializedName("updateDate")
    private String updateDate= "";
    @Expose
    @SerializedName("village_ID")
    private Integer villageID= 0;
    @Expose
    @SerializedName("farmer_image")
    private String farmerImage= "";
    @Expose
    @SerializedName("company_Name")
    private String company_Name="";
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCompany_Name() {
        return company_Name;
    }

    public void setCompany_Name(String company_Name) {
        this.company_Name = company_Name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getFarmerID() {
        return farmerID;
    }

    public void setFarmerID(Integer farmerID) {
        this.farmerID = farmerID;
    }


    public Integer getAmphurID() {
        return amphurID;
    }


    public void setAmphurID(Integer amphurID) {
        this.amphurID = amphurID;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public Integer getCompanyTypeID() {
        return companyTypeID;
    }

    public void setCompanyTypeID(Integer companyTypeID) {
        this.companyTypeID = companyTypeID;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Integer getDebtAmount() {
        return debtAmount;
    }

    public void setDebtAmount(Integer debtAmount) {
        this.debtAmount = debtAmount;
    }

    public Integer getDebtsAmountIn() {
        return debtsAmountIn;
    }

    public void setDebtsAmountIn(Integer debtsAmountIn) {
        this.debtsAmountIn = debtsAmountIn;
    }

    public Integer getDebtsAmountOut() {
        return debtsAmountOut;
    }

    public void setDebtsAmountOut(Integer debtsAmountOut) {
        this.debtsAmountOut = debtsAmountOut;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getFarmerGroupID() {
        return farmerGroupID;
    }

    public void setFarmerGroupID(Integer farmerGroupID) {
        this.farmerGroupID = farmerGroupID;
    }

    public Integer getFarmerProblemID() {
        return farmerProblemID;
    }

    public void setFarmerProblemID(Integer farmerProblemID) {
        this.farmerProblemID = farmerProblemID;
    }

    public String getFarmerProblemOther() {
        return farmerProblemOther;
    }

    public void setFarmerProblemOther(String farmerProblemOther) {
        this.farmerProblemOther = farmerProblemOther;
    }

    public Integer getFarmerStatusID() {
        return farmerStatusID;
    }

    public void setFarmerStatusID(Integer farmerStatusID) {
        this.farmerStatusID = farmerStatusID;
    }

    public Integer getFarmerTypeID() {
        return farmerTypeID;
    }

    public void setFarmerTypeID(Integer farmerTypeID) {
        this.farmerTypeID = farmerTypeID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getGenderID() {
        return genderID;
    }

    public void setGenderID(Integer genderID) {
        this.genderID = genderID;
    }

    public Integer getGovernmentHelpID() {
        return governmentHelpID;
    }

    public void setGovernmentHelpID(Integer governmentHelpID) {
        this.governmentHelpID = governmentHelpID;
    }

    public String getHoldingEquipment() {
        return holdingEquipment;
    }

    public void setHoldingEquipment(String holdingEquipment) {
        this.holdingEquipment = holdingEquipment;
    }

    public Integer getHoldingLandID() {
        return holdingLandID;
    }

    public void setHoldingLandID(Integer holdingLandID) {
        this.holdingLandID = holdingLandID;
    }

    public String getHomeNo() {
        return homeNo;
    }

    public void setHomeNo(String homeNo) {
        this.homeNo = homeNo;
    }

    public long getHomeID() {
        return homeID;
    }

    public void setHomeID(long homeID) {
        this.homeID = homeID;
    }



    public Integer getIsAgency() {
        return isAgency;
    }

    public void setIsAgency(Integer isAgency) {
        this.isAgency = isAgency;
    }

    public Integer getIsCancel() {
        return isCancel;
    }

    public void setIsCancel(Integer isCancel) {
        this.isCancel = isCancel;
    }

    public Integer getIsDead() {
        return isDead;
    }

    public void setIsDead(Integer isDead) {
        this.isDead = isDead;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Integer getMainJobID() {
        return mainJobID;
    }

    public void setMainJobID(Integer mainJobID) {
        this.mainJobID = mainJobID;
    }

    public Integer getMaritalStatusID() {
        return maritalStatusID;
    }

    public void setMaritalStatusID(Integer maritalStatusID) {
        this.maritalStatusID = maritalStatusID;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMoo() {
        return moo;
    }

    public void setMoo(String moo) {
        this.moo = moo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public Integer getPrefixID() {
        return prefixID;
    }

    public void setPrefixID(Integer prefixID) {
        this.prefixID = prefixID;
    }

    public Integer getProvinceID() {
        return provinceID;
    }

    public void setProvinceID(Integer provinceID) {
        this.provinceID = provinceID;
    }

    public Integer getRevenue() {
        return revenue;
    }

    public void setRevenue(Integer revenue) {
        this.revenue = revenue;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public Integer getSecondJobID() {
        return secondJobID;
    }

    public void setSecondJobID(Integer secondJobID) {
        this.secondJobID = secondJobID;
    }

    public String getSoi() {
        return soi;
    }

    public void setSoi(String soi) {
        this.soi = soi;
    }

    public Integer getStatusID() {
        return statusID;
    }

    public void setStatusID(Integer statusID) {
        this.statusID = statusID;
    }

    public Integer getTambolID() {
        return tambolID;
    }

    public void setTambolID(Integer tambolID) {
        this.tambolID = tambolID;
    }

    public Long getTaxID() {
        return taxID;
    }

    public void setTaxID(Long taxID) {
        this.taxID = taxID;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getVillageID() {
        return villageID;
    }

    public void setVillageID(Integer villageID) {
        this.villageID = villageID;
    }

    public String getFarmerImage() {
        return farmerImage;
    }

    public void setFarmerImage(String farmerImage) {
        this.farmerImage = farmerImage;
    }




}
