package com.zealtech.dld.dldregist.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.FarmActivity;
import com.zealtech.dld.dldregist.activity.MainActivity;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.model.FarmerProblemMaster.FarmerProblemEntity;
import com.zealtech.dld.dldregist.model.HelpMaster.HelpEntity;
import com.zealtech.dld.dldregist.model.HoldingLandMaster.LandEntity;
import com.zealtech.dld.dldregist.model.MainJobMaster.JobEntity;
import com.zealtech.dld.dldregist.model.SecondJobMaster.SecondJobEntity;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseInsertFarmer;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.util.UpdateListener;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;
import com.zealtech.dld.dldregist.widget.MySpinnerAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.zealtech.dld.dldregist.util.AppUtils.UUID;


public class JobTypeFragment extends Fragment {

    private static final String CLASS_NAME = "JobTypeFragment";
    private Spinner spnJob;
    private Spinner spnSecondJob;
    private Spinner spnLand;
    private MyEditNoHLTextView spnMachine;
    private MyEditNoHLTextView txtRevenue;
    private MyEditNoHLTextView txtDebtAmount;
    private MyEditNoHLTextView txtDebtAmountIn;
    private MyEditNoHLTextView txtDebtAmountOut;

    ArrayList<String> listJobs = new ArrayList<>();
    ArrayList<String> listSecondJobs = new ArrayList<>();
    ArrayList<String> listLands = new ArrayList<>();
    ArrayList<String> listMachines = new ArrayList<>();
    ArrayList<String> listHelps = new ArrayList<>();
    ArrayList<String> listProblems = new ArrayList<>();


    ArrayList<JobEntity> jobs = new ArrayList<>();
    ArrayList<SecondJobEntity> secondJobs = new ArrayList<>();
    ArrayList<LandEntity> lands = new ArrayList<>();
    ArrayList<HelpEntity> helps = new ArrayList<>();
    ArrayList<FarmerProblemEntity> farmerProblems = new ArrayList<>();
    private FrameLayoutDisable progressBar;


    DBHelper dbHelper;
    private Spinner spnProblem;
    private Spinner spnHelp;
    Boolean isPopulate = true;
    private Farmer mFarmerItem;

    int currentProblemID = 0;

    public JobTypeFragment() {
        super();
    }

    public static JobTypeFragment newInstance() {
        JobTypeFragment fragment = new JobTypeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_job_type, container, false);

        initInstances(rootView);
        ButterKnife.bind(this, rootView);
        dbHelper = new DBHelper(getContext());
        isOnline=  AppUtils.isOnline(getActivity());

        /*if (!dbHelper.isTablePopulated(FarmerModel.TABLE)) {
            mFarmerItem = new Farmer();
        } else {
            mFarmerItem = ((MainActivity) getActivity()).getFarmerItem();
            setInfoFarmer();
        }*/

        mFarmerItem = ((MainActivity) getActivity()).getFarmerItem();
        if (mFarmerItem.getPid() == 0) {
//            mFarmerItem = new Farmer();
            isPopulate = false;
        }else {
            isPopulate = true;
        }
        setInfoFarmer();

        getListMainJob();
        getListSecondJob();
        getListLand();
        getListHelp();
        getListFarmerProblem();

        txtDebtAmountIn.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                String s1 = txtDebtAmountOut.getText().toString();
                int amount;
                int charSequence;
                if (s1.length()<1){
                    amount=0;
                }else {
                    amount= Integer.parseInt(s1);
                }

                if ( s.length()<1){
                    charSequence=0;
                }else {
                    charSequence=Integer.parseInt(String.valueOf(s)) ;
                }

                txtDebtAmount.setText(""+(charSequence+amount));
            }
        });


        txtDebtAmountOut.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                String s1 = txtDebtAmountIn.getText().toString();
                int amount;
                int charSequence;

                if (s1.length()<1){
                    amount=0;
                }else {
                    amount= Integer.parseInt(s1);
                }

                if ( s.length()<1){
                    charSequence=0;
                }else {
                    charSequence=Integer.parseInt(String.valueOf(s)) ;
                }

                txtDebtAmount.setText(""+(charSequence+amount));
            }
        });

        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser){
            mFarmerItem = ((MainActivity) getActivity()).getFarmerItem();

            if(mFarmerItem!=null) {
                if (mFarmerItem.getPid() == 0) {
//            mFarmerItem = new Farmer();
                    isPopulate = false;
                } else {
                    isPopulate = true;
                }
            }
        }
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        spnJob = (Spinner) rootView.findViewById(R.id.spnJob);
        spnSecondJob = (Spinner) rootView.findViewById(R.id.spnSecondJob);
        spnLand = (Spinner) rootView.findViewById(R.id.spnArea);
        spnHelp = (Spinner) rootView.findViewById(R.id.spnHelp);
        spnProblem = (Spinner) rootView.findViewById(R.id.spnProblem);
        spnMachine = (MyEditNoHLTextView) rootView.findViewById(R.id.txtEquipment);
        txtRevenue = (MyEditNoHLTextView) rootView.findViewById(R.id.txtRevenue);
        txtDebtAmount = (MyEditNoHLTextView) rootView.findViewById(R.id.txtDebtAmount);
        txtDebtAmountIn = (MyEditNoHLTextView) rootView.findViewById(R.id.txtDebtAmountIn);
        txtDebtAmountOut = (MyEditNoHLTextView) rootView.findViewById(R.id.txtDebtAmountOut);
        progressBar = (FrameLayoutDisable) rootView.findViewById(R.id.progressBar);
    }

    public void setInfoFarmer() {
        if (isOnline==true||mFarmerItem.getUuid() != null) {
            if (mFarmerItem.getRevenue()!=null){
                txtRevenue.setText(String.valueOf(mFarmerItem.getRevenue()));
            }
            if (mFarmerItem.getDebtAmount()!=null){
                txtDebtAmount.setText(String.valueOf(mFarmerItem.getDebtAmount()));
            }

            if (mFarmerItem.getDebtsAmountIn()!=null){
                txtDebtAmountIn.setText(String.valueOf(mFarmerItem.getDebtsAmountIn()));
            }
            if (mFarmerItem.getDebtsAmountOut()!=null){
                txtDebtAmountOut.setText(String.valueOf(mFarmerItem.getDebtsAmountOut()));
            }

            spnMachine.setText(mFarmerItem.getHoldingEquipment());
       }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
//        outState.putParcelable(CLASS_NAME, Parcels.wrap());
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }
    private DatabaseRealm databaseRealm;
    Boolean isOnline;
    @OnClick(R.id.btnSubmit)
    public void clickBtnSubmit() {
        Log.d("submit", "check");

        setInfoToDatabase();

        if (mFarmerItem.getPid()==0){
            Toast.makeText(getActivity(), "กรุณากดส่งข้อมูลหน้าข้อมูลเกษตรกร", Toast.LENGTH_SHORT).show();
        }else {

            if (isOnline == true) {
                try {
                    mFarmerItem.setCreateDate(AppUtils.convertDate(mFarmerItem.getCreateDate()));
                    mFarmerItem.setUpdateDate(AppUtils.convertDate(mFarmerItem.getUpdateDate()));
                    mFarmerItem.setUpdateBy(String.valueOf(AppUtils.getUserId(getActivity())));

                    mFarmerItem.setBirthDay(AppUtils.convertDate(mFarmerItem.getBirthDay()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mFarmerItem.setUpdateDate(AppUtils.getDate());
                ArrayList<Farmer> farmer = new ArrayList<>();
                farmer.add(mFarmerItem);
                insertData(farmer);

            } else {
                if (!isPopulate) {
                    databaseRealm = new DatabaseRealm(getContext());
                    mFarmerItem.setOnline(false);
                    boolean addDataFarmer = databaseRealm.addDataFarmer(mFarmerItem);

                    if (addDataFarmer == true) {
                        Toast.makeText(getActivity(), "อัพเดทข้อมูลสำเร็จ", Toast.LENGTH_SHORT).show();
                        /*Intent intent = new Intent(getActivity(), OfflineActivity.class);
                        startActivity(intent);
                        getActivity().finish();*/

                        if (mFarmerItem.getFarm().size() == 0) {
                            Intent intent = new Intent(getActivity(), FarmActivity.class); //mode MainFarm To FarmData
                            intent.putExtra(UUID, mFarmerItem.getUuid());
                            intent.putExtra("mode", "farm");
                            intent.putExtra("farmId", 0);
                            int fr=((MainActivity) getActivity()).getFrom();
                            intent.putExtra(AppUtils.SENDFROM, fr);
                            startActivity(intent);
//                            getActivity().finish();
                            if(!(fr==1||fr==3)) {
                                getActivity().finish();
                            }
                        }

                    } else {
                        Toast.makeText(getActivity(), "อัพเดตข้อมูลไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    databaseRealm = new DatabaseRealm(getActivity());
                    boolean upDateDataFarmer = databaseRealm.upDateDataFarmer(mFarmerItem);

                    if (upDateDataFarmer == true) {
                        Toast.makeText(getActivity(), "อัพเดทข้อมูลสำเร็จ", Toast.LENGTH_SHORT).show();
                       /* Intent intent = new Intent(getActivity(), OfflineActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                        */
                        if (mFarmerItem.getFarm().size() == 0) {
                            Intent intent = new Intent(getActivity(), FarmActivity.class); //mode MainFarm To FarmData
                            intent.putExtra(UUID, mFarmerItem.getUuid());
                            intent.putExtra("mode", "farm");
                            intent.putExtra("farmId", 0);
                            int fr=((MainActivity) getActivity()).getFrom();
                            intent.putExtra(AppUtils.SENDFROM, fr);
                            startActivity(intent);
//                            getActivity().finish();
                            if(!(fr==1||fr==3)) {
                                getActivity().finish();
                            }
                        }

                    } else {
                        Toast.makeText(getActivity(), "อัพเดตข้อมูลไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                    }
                }


            }

        }

        /*
        if (mFarmerItem != null) {
            Log.d("submit", "update");
            setInfoToDatabase();
            dbHelper.updateFarmer(mFarmerItem, mFarmerItem.getFarmerID());
        } else {
            Log.d("submit", "add");
            setInfoToDatabase();
            dbHelper.addFarmer(mFarmerItem);
        }*/
    }

    private void insertData(final ArrayList<Farmer> farmer) {
        progressBar.setVisibility(View.VISIBLE);
        AppUtils.updateDataArray(farmer, new UpdateListener() {
            @Override
            public void onSuccess(String message, ResponseInsertFarmer responseInsertFarmer) {
                //delete all
                progressBar.setVisibility(View.GONE);
                mFarmerItem = responseInsertFarmer.getFarmerList().get(0).getFarmer();
                mFarmerItem.setUuid(((MainActivity) getActivity()).getUUID());
                databaseRealm = new DatabaseRealm(getContext());

                String uu="";

                if (databaseRealm.getDataFarmer(((MainActivity) getActivity()).getUUID()) != null) {
                    databaseRealm.upDateDataFarmer(mFarmerItem);
                    ((MainActivity) getActivity()).setUuid(mFarmerItem.getUuid());
                    uu = mFarmerItem.getUuid();
                } else {
                    uu = databaseRealm.addDataFarmerUUID(mFarmerItem);
                    ((MainActivity) getActivity()).setUuid(uu);
                }

                Toast.makeText(getActivity(), "อัพเดทข้อมูลสำเร็จ", Toast.LENGTH_SHORT).show();

                if(mFarmerItem.getFarm().size() == 0) {
                    Intent intent = new Intent(getActivity(), FarmActivity.class); //mode MainFarm To FarmData
                    intent.putExtra(UUID, uu);
                    intent.putExtra("mode", "farm");  //skip dashboard
                    intent.putExtra("farmId", 0);
                    int fr=((MainActivity) getActivity()).getFrom();
                    intent.putExtra(AppUtils.SENDFROM, fr);
                    startActivity(intent);
//                    getActivity().finish();
                    if(!(fr==1||fr==3)) { //1=FindFarmer,3=Register
                        getActivity().finish();
                    }
                }
//                if (!isPopulate) {
//                    Intent intent = new Intent(getActivity(), FarmActivity.class); //mode MainFarm To FarmData
//                    intent.putExtra(UUID, uu);
//                    intent.putExtra("mode", "farm");
//                    intent.putExtra("farmId", 0);
//                    startActivity(intent);
//                    getActivity().finish();
//                }

            }

            @Override
            public void onError(String message, ResponseInsertFarmer responseInsertFarmer) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "อัพเดตข้อมูลไม่สำเร็จ", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void setInfoToDatabase() {
        if (!txtRevenue.getText().toString().equals("")){
            mFarmerItem.setRevenue(Integer.valueOf(txtRevenue.getText().toString()));

        }
        if (!txtDebtAmount.getText().toString().equals("")){
            mFarmerItem.setDebtAmount(Integer.valueOf(txtDebtAmount.getText().toString()));

        }
        if (!txtDebtAmountIn.getText().toString().equals("")){
            mFarmerItem.setDebtsAmountIn(Integer.valueOf(txtDebtAmountIn.getText().toString()));

        }
        if (!txtDebtAmountOut.getText().toString().equals("")){
            mFarmerItem.setDebtsAmountOut(Integer.valueOf(txtDebtAmountOut.getText().toString()));
        }
        mFarmerItem.setHoldingEquipment(spnMachine.getText().toString());

    }

    public void getListMainJob() {
        jobs.clear();
        listJobs.clear();
        JobEntity jobEntity =new JobEntity();
        jobEntity.setMain_Job_Name("กรุณาเลือกอาชีพหลัก");
        jobEntity.setMain_Job_ID(-99);
        jobs.add(jobEntity);
        int selectIndex = -1;
        jobs.addAll(dbHelper.getMainJob());

        for (int i = 0; i < jobs.size(); i++) {
            listJobs.add(jobs.get(i).getMain_Job_Name());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listJobs);
        spnJob.setAdapter(adapter);

        spnJob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mFarmerItem!=null) {

                    mFarmerItem.setMainJobID(jobs.get(position).getMain_Job_ID());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < jobs.size(); i++) {
            if (mFarmerItem.getMainJobID()!=null)
                if (jobs.get(i).getMain_Job_ID() == mFarmerItem.getMainJobID()) {
                    selectIndex = i;
                    spnJob.setSelection(selectIndex);
                    break;
                }
        }
    }

    public void getListSecondJob() {

        listSecondJobs.clear();
        secondJobs.clear();
        SecondJobEntity secondJobEntity =new SecondJobEntity();
        secondJobEntity.setSecond_Job_Name("กรุณาเลือกอาชีพรอง");
        secondJobEntity.setSecond_Job_ID(-99);
        secondJobs.add(secondJobEntity);

        int selectIndex = -1;
        secondJobs.addAll(dbHelper.getSecondJob());

        for (int i = 0; i < secondJobs.size(); i++) {
            listSecondJobs.add(secondJobs.get(i).getSecond_Job_Name());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listSecondJobs);
        spnSecondJob.setAdapter(adapter);

        spnSecondJob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mFarmerItem!=null) {

                    mFarmerItem.setSecondJobID(secondJobs.get(position).getSecond_Job_ID());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < secondJobs.size(); i++) {
            if (mFarmerItem.getSecondJobID()  != null)
                if (secondJobs.get(i).getSecond_Job_ID() == mFarmerItem.getSecondJobID()) {
                    selectIndex = i;
                    spnSecondJob.setSelection(selectIndex);
                    break;
                }
        }

    }

    public void getListLand() {
        listLands.clear();
        lands.clear();

        LandEntity landEntity=new LandEntity();
        landEntity.setHolding_Land_Name("กรุณาเลือกการถือครองที่ดิน");
        landEntity.setHolding_Land_ID(-99);
        lands.add(landEntity);
        int selectIndex = -1;
        lands.addAll(dbHelper.getLand());

        for (int i = 0; i < lands.size(); i++) {
            listLands.add(lands.get(i).getHolding_Land_Name());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listLands);
        spnLand.setAdapter(adapter);

        spnLand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mFarmerItem!=null) {
                    mFarmerItem.setHoldingLandID(lands.get(position).getHolding_Land_ID());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < lands.size(); i++) {
            if (mFarmerItem.getHoldingLandID()  != null)
                if (lands.get(i).getHolding_Land_ID() == mFarmerItem.getHoldingLandID()) {
                    selectIndex = i;
                    spnLand.setSelection(selectIndex);
                    break;
                }
        }

    }

    public void getListHelp() {
        listHelps.clear();
        helps.clear();
        HelpEntity helpEntity=new HelpEntity();
        helpEntity.setGovernment_Help_Name("กรุณาเลือกความต้องการความช่วยเหลือ");
        helpEntity.setGovernment_Help_ID(-99);
        helps.add(helpEntity);
        int selectIndex = -1;
        helps.addAll(dbHelper.getHelp());

        for (int i = 0; i < helps.size(); i++) {
            listHelps.add(helps.get(i).getGovernment_Help_Name());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listHelps);
        spnHelp.setAdapter(adapter);

        spnHelp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mFarmerItem!=null){
                    mFarmerItem.setGovernmentHelpID(helps.get(position).getGovernment_Help_ID());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < helps.size(); i++) {
            if (mFarmerItem.getGovernmentHelpID()  != null)
                if (helps.get(i).getGovernment_Help_ID() == mFarmerItem.getGovernmentHelpID()) {
                    selectIndex = i;
                    spnHelp.setSelection(selectIndex);
                    break;
                }
        }
    }

    public void getListFarmerProblem() {
        listProblems.clear();
        farmerProblems.clear();
        FarmerProblemEntity farmerProblemEntity=new FarmerProblemEntity();
        farmerProblemEntity.setFarmer_Problem_Name("กรุณาเลือกปัญหาเบื้องต้นที่ประสบ");
        farmerProblemEntity.setFarmer_Problem_ID(-99);
        farmerProblems.add(farmerProblemEntity);
        int selectIndex = -1;
        farmerProblems.addAll(dbHelper.getFarmerProblem());

        for (int i = 0; i < farmerProblems.size(); i++) {
            listProblems.add(farmerProblems.get(i).getFarmer_Problem_Name());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listProblems);
        spnProblem.setAdapter(adapter);

        spnProblem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mFarmerItem!=null) {

                    mFarmerItem.setFarmerProblemID(farmerProblems.get(position).getFarmer_Problem_ID());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        for (int i = 0; i < farmerProblems.size(); i++) {
            if (mFarmerItem.getFarmerProblemID() != null) {
                if (farmerProblems.get(i).getFarmer_Problem_ID() == mFarmerItem.getFarmerProblemID()) {
                    selectIndex = i;
                    spnProblem.setSelection(selectIndex);
                    break;
                }
            }
        }

    }

}
