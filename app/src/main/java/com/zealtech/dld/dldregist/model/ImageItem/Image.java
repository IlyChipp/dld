
package com.zealtech.dld.dldregist.model.ImageItem;

import com.google.gson.annotations.SerializedName;


public class Image {

    @SerializedName("createBy")
    private Integer createBy;
    @SerializedName("createDate")
    private CreateDate createDate;
    @SerializedName("farmerId")
    private Integer farmerId;
    @SerializedName("imageName")
    private String imageName;
    @SerializedName("imagePath")
    private String imagePath;
    @SerializedName("imageSize")
    private String imageSize;
    @SerializedName("imageUrl")
    private String imageUrl;
    @SerializedName("orderIndex")
    private Integer orderIndex;
    @SerializedName("pid")
    private String pid;
    @SerializedName("statusId")
    private Integer statusId;
    @SerializedName("updateBy")
    private Integer updateBy;
    @SerializedName("updateDate")
    private UpdateDate updateDate;

    public Integer getCreateBy() {
        return createBy;
    }

    public CreateDate getCreateDate() {
        return createDate;
    }

    public Integer getFarmerId() {
        return farmerId;
    }

    public String getImageName() {
        return imageName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getImageSize() {
        return imageSize;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Integer getOrderIndex() {
        return orderIndex;
    }

    public String getPid() {
        return pid;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public UpdateDate getUpdateDate() {
        return updateDate;
    }
}
