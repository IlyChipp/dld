package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/26/2017 AD.
 */

@Parcel
public class AmphurMaster implements APIResponse {

    //DBHelper
    public static final String TABLE = "amphur";

    String responseCode;
    String responseMessage;
    ArrayList<AmphurEntity> data;

    public AmphurMaster(){

    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<AmphurEntity> getData() {
        return data;
    }

    public void setData(ArrayList<AmphurEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class AmphurEntity {
        int amphur_ID;
        int amphur_Code;
        String amphur_NameEn;
        String amphur_NameTh;
        Double latitude;
        Double longitude;
        int orderIndex;
        int province_ID;
        int status_ID;
        String zipcode;


        public AmphurEntity(){

        }

        public int getAmphur_ID() {
            return amphur_ID;
        }

        public void setAmphur_ID(int amphur_ID) {
            this.amphur_ID = amphur_ID;
        }

        public int getAmphur_Code() {
            return amphur_Code;
        }

        public void setAmphur_Code(int amphur_Code) {
            this.amphur_Code = amphur_Code;
        }

        public String getAmphur_NameEn() {
            return amphur_NameEn;
        }

        public void setAmphur_NameEn(String amphur_NameEn) {
            this.amphur_NameEn = amphur_NameEn;
        }

        public String getAmphur_nameTh() {
            return amphur_NameTh;
        }

        public void setAmphur_nameTh(String amphur_nameTh) {
            this.amphur_NameTh = amphur_nameTh;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getlongitude() {
            return longitude;
        }

        public void setlongitude(Double longitude) {
            this.longitude = longitude;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getProvince_ID() {
            return province_ID;
        }

        public void setProvince_ID(int province_ID) {
            this.province_ID = province_ID;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        public String getAmphur_NameTh() {
            return amphur_NameTh;
        }

        public void setAmphur_NameTh(String amphur_NameTh) {
            this.amphur_NameTh = amphur_NameTh;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String AMPHUR_ID = "amphur_ID";
        public static final String AMPHUR_CODE = "amphur_Code";
        public static final String AMPHUR_NAME_EN = "amphur_NameEn";
        public static final String AMPHUR_NAME_TH = "amphur_NameTh";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String PROVINCE_ID = "province_ID";
        public static final String STATUS_ID = "status_ID";
        public static final String ZIPCODE = "zipcode";
    }
}
