package com.zealtech.dld.dldregist.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zealtech.dld.dldregist.MainApplication;
import com.zealtech.dld.dldregist.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class AboutUsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private SupportMapFragment mapFragment;
    Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ButterKnife.bind(this);
        CoordinatorLayout appBar = (CoordinatorLayout) findViewById(R.id.app_bar_main);

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        MainApplication application = (MainApplication) getApplication();
        mTracker = application.getDefaultTracker();


        mapFragment.getMapAsync(this);

    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("หน้าเกี่ยวกับเรา");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Optional
    @OnClick(R.id.btn_back)
    public void clickBtnBack() {
        finish();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng zti_latlng = new LatLng(13.755567, 100.533862);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(zti_latlng, 16));

        // You can customize the marker image using images bundled with
        // your app, or dynamically generated bitmaps.
        googleMap.addMarker(new MarkerOptions()
                .anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
                .title("กรมปศุสัตว์")
//                .snippet(address)
                .position(zti_latlng)).showInfoWindow();

        // Do something with Google Map
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
    }
}
