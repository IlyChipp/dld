package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/28/2017 AD.
 */
@Parcel
public class HelpMaster implements APIResponse{
    //DBHelper
    public static final String TABLE = "help";

    String responseCode;
    String responseMessage;
    ArrayList<HelpEntity> data;

    public HelpMaster(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<HelpEntity> getData() {
        return data;
    }

    public void setData(ArrayList<HelpEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class HelpEntity{
        int government_Help_ID;
        String government_Help_Name;
        int orderIndex;
        int status_ID;

        public HelpEntity(){ }

        public int getGovernment_Help_ID() {
            return government_Help_ID;
        }

        public void setGovernment_Help_ID(int government_Help_ID) {
            this.government_Help_ID = government_Help_ID;
        }

        public String getGovernment_Help_Name() {
            return government_Help_Name;
        }

        public void setGovernment_Help_Name(String government_Help_Name) {
            this.government_Help_Name = government_Help_Name;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }
    }
    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String GOVERNMENT_HELP_ID = "government_Help_ID";
        public static final String GOVERNMENT_HELP_NAME = "government_Help_Name";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
    }
}
