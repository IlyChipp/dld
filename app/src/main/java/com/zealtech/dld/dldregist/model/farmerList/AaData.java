
package com.zealtech.dld.dldregist.model.farmerList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class AaData {

    @Expose
    @SerializedName("0")
    private Integer _0;
    @Expose
    @SerializedName("1")
    private String _1;
    @Expose
    @SerializedName("2")
    private String _2;
    @Expose
    @SerializedName("3")
    private String _3;
    @Expose
    @SerializedName("4")
    private String _4;
    @Expose
    @SerializedName("5")
    private String _5;
    @Expose
    @SerializedName("DT_RowId")
    private Integer dTRowId;
    @Expose
    @SerializedName("DT_RowData")
    private DTRowData dTRowData;

    public Integer get_0() {
        return _0;
    }

    public void set_0(Integer _0) {
        this._0 = _0;
    }

    public String get_1() {
        return _1;
    }

    public void set_1(String _1) {
        this._1 = _1;
    }

    public String get_2() {
        return _2;
    }

    public void set_2(String _2) {
        this._2 = _2;
    }

    public String get_3() {
        return _3;
    }

    public void set_3(String _3) {
        this._3 = _3;
    }

    public String get_4() {
        return _4;
    }

    public void set_4(String _4) {
        this._4 = _4;
    }

    public String get_5() {
        return _5;
    }

    public void set_5(String _5) {
        this._5 = _5;
    }

    public Integer getdTRowId() {
        return dTRowId;
    }

    public void setdTRowId(Integer dTRowId) {
        this.dTRowId = dTRowId;
    }

    public DTRowData getdTRowData() {
        return dTRowData;
    }

    public void setdTRowData(DTRowData dTRowData) {
        this.dTRowData = dTRowData;
    }



}
