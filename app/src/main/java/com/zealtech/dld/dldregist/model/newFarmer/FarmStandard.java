
package com.zealtech.dld.dldregist.model.newFarmer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Field;

import io.realm.RealmObject;

public class FarmStandard extends RealmObject {

    @Expose
    @SerializedName("farm_ID")
    private Integer farmID = 0;

    @Expose
    @SerializedName("farm_Standard_ID")
    private Integer farmStandardID = 0;

    @Expose
    @SerializedName("livestock_Type_ID")
    private Integer livestockTypeID = 0;

    @Expose
    @SerializedName("livestock_Type_FullName")
    private String livestockTypeFullName = "";

    @Expose
    @SerializedName("orderIndex")
    private Integer orderIndex = 0;

    @Expose
    @SerializedName("standard_ID")
    private Integer standardID = 1;

    @Expose
    @SerializedName("standard_Name")
    private String standardName = "";

    @Expose
    @SerializedName("standard_Number")
    private String standardNumber = "";

    @Expose
    @SerializedName("status_ID")
    private Integer statusID = 0;

    @Expose
    @SerializedName("status_Name")
    private String statusName = "";

    private boolean localOnly;


    public FarmStandard() { }

    public FarmStandard(FarmStandard other) {
        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                field.set(this, field.get(other));
            } catch (IllegalAccessException ex) { throw new AssertionError(ex); }
        }
    }

    @Override
    public int hashCode() {
        return farmStandardID;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof Feed))
            return false;
        FarmStandard other = (FarmStandard) obj;
        if (this.farmStandardID == other.farmStandardID)
            return true;
        return false;
    }

    public String getLivestockTypeFullName() {
        return livestockTypeFullName;
    }

    public void setLivestockTypeFullName(String livestockTypeFullName) {
        this.livestockTypeFullName = livestockTypeFullName;
    }

    public String getStandardName() {
        return standardName;
    }

    public void setStandardName(String standardName) {
        this.standardName = standardName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public boolean isLocalOnly() {
        return localOnly;
    }

    public void setLocalOnly(boolean localOnly) {
        this.localOnly = localOnly;
    }

    public Integer getFarmID() {
        return farmID;
    }

    public void setFarmID(Integer farmID) {
        this.farmID = farmID;
    }

    public Integer getFarmStandardID() {
        return farmStandardID;
    }

    public void setFarmStandardID(Integer farmStandardID) {
        this.farmStandardID = farmStandardID;
    }

    public Integer getLivestockTypeID() {
        return livestockTypeID;
    }

    public void setLivestockTypeID(Integer livestockTypeID) {
        this.livestockTypeID = livestockTypeID;
    }

    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    public Integer getStandardID() {
        return standardID;
    }

    public void setStandardID(Integer standardID) {
        this.standardID = standardID;
    }

    public String getStandardNumber() {
        return standardNumber;
    }

    public void setStandardNumber(String standardNumber) {
        this.standardNumber = standardNumber;
    }

    public Integer getStatusID() {
        return statusID;
    }

    public void setStatusID(Integer statusID) {
        this.statusID = statusID;
    }
}
