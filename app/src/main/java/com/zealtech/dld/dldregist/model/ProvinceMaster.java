package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/26/2017 AD.
 */

@Parcel
public class ProvinceMaster implements APIResponse {

    //DBHelper
    public static final String TABLE = "province";

    String responseCode;
    String responseMessage;
    ArrayList<ProvinceEntity> data;

    public ProvinceMaster(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<ProvinceEntity> getData() {
        return data;
    }

    public void setData(ArrayList<ProvinceEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class ProvinceEntity {
        int province_ID;
        int countryRanch_ID;
        int geo_ID;
        Double latitude;
        Double longitude;
        int orderIndex;
        String province_NameEn;
        String province_NameTh;
        int province_Code;
        int status_ID;

        public ProvinceEntity(){ }

        public int getProvince_ID() {
            return province_ID;
        }

        public void setProvince_ID(int province_ID) {
            this.province_ID = province_ID;
        }

        public int getCountryRanch_ID() {
            return countryRanch_ID;
        }

        public void setCountryRanch_ID(int countryRanch_ID) {
            this.countryRanch_ID = countryRanch_ID;
        }

        public int getGeo_ID() {
            return geo_ID;
        }

        public void setGeo_ID(int geo_ID) {
            this.geo_ID = geo_ID;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public String getProvince_NameEn() {
            return province_NameEn;
        }

        public void setProvince_NameEn(String province_NameEn) {
            this.province_NameEn = province_NameEn;
        }

        public String getProvince_NameTh() {
            return province_NameTh;
        }

        public void setProvince_NameTh(String province_NameTh) {
            this.province_NameTh = province_NameTh;
        }

        public int getProvince_Code() {
            return province_Code;
        }

        public void setProvince_Code(int province_Code) {
            this.province_Code = province_Code;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String PROVINCE_ID = "province_ID";
        public static final String COUNTRYRANCH_ID = "countryRanch_ID";
        public static final String GEO_ID = "geo_ID";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String PROVINCE_NAME_EN = "province_NameEn";
        public static final String PROVINCE_NAME_TH = "province_NameTh";
        public static final String PROVINCE_CODE = "province_Code";
        public static final String STATUS_ID =  "status_ID";
    }
}
