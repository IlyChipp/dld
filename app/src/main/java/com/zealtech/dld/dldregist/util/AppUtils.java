package com.zealtech.dld.dldregist.util;

//import com.zti.android.siiscan.activity.PatientInfoActivity;

import android.Manifest;
import android.Manifest.permission;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.config.ApplicationConfig;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.FarmerModel;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseInsertFarmer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.realm.RealmList;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppUtils {
    public static final int REQUEST_LOCATION = 3;
    private static final String CLASS_NAME = "Utils";
    public static final int RequestPermissionCodeGallery = 2;
    public static final int RequestPermissionCodeCamera = 1;
    public static int GALLERY = 1, CAMERA = 2;
    public static final String UUID = "uuid";
    public static final String TYPE = "type";
    public static final String SENDFROM = "from";
    public static final String LOGINTYPE = "loginType";
    public static final int RequestFarmList = 4;

    public static OkHttpClient okHttpClient;
    //    private static final int REQUEST_PERMISSION = 1;
//
//    public static void showAlertDialog(String title,String message,Activity activity){
//        new AlertDialog.Builder(activity)
//                .setTitle(title)
//                .setMessage(message)
//                .setPositiveButton(activity.getResources().getString(R.string.confirm), new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                })
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .show();
//    }
//
//    public static void showAlertDialogOk(String title,String message,Activity activity){
//        new AlertDialog.Builder(activity)
//                .setTitle(title)
//                .setMessage(message)
//                .setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                })
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .show();
//    }
    public static final int RequestPermissionCodeLocation = 3;

    public static void setUserProfile(FarmerModel.FarmerEntity pidModel, MyPreferenceManager myPreferenceManager) {
        Gson gson = new Gson();
//        if(fbModel != null && fbModel.size() > 0) {
//            String json = gson.toJson(fbModel.get(0));
//            myPreferenceManager.setUserProfile(json);
//        }
        String json = gson.toJson(pidModel);
        myPreferenceManager.setUserProfile(json);
    }

//    public static FarmerEntity getUserProfile(Context context) {
//        MyPreferenceManager myPreferenceManager = new MyPreferenceManager(context);
//        Gson gson = new Gson();
//        String json = myPreferenceManager.getUserProfile();
//        return gson.fromJson(json, FarmerEntity.class);
//    }

//    public static void setProvince(ArrayList<ProvinceEntity> pvModel, MyPreferenceManager myPreferenceManager) {
//        Gson gson = new Gson();
//        String json;
//        if (pvModel != null && pvModel.size() > 0) {
//            json = gson.toJson(pvModel);
//            myPreferenceManager.setProvince(json);
//        }
//    }
//    public static void setAmphur(ArrayList<AmphurEntity> amModel, MyPreferenceManager myPreferenceManager){
//        Gson gson = new Gson();
//        String json;
//        if(amModel != null && amModel.size() > 0){
//            json = gson.toJson(amModel);
//            myPreferenceManager.setAmphur(json);
//        }
//    }
//    public static void setTambol(ArrayList<TambolEntity> tmModel, MyPreferenceManager myPreferenceManager){
//        Gson gson = new Gson();
//        String json;
//        if(tmModel != null && tmModel.size() > 0){
//            json = gson.toJson(tmModel);
//            myPreferenceManager.setTambol(json);
//        }
//    }
//    public static void setVillage(ArrayList<VillageEntity> vlModel, MyPreferenceManager myPreferenceManager){
//        Gson gson = new Gson();
//        String json;
//        if (vlModel != null && vlModel.size() > 0){
//            json = gson.toJson(vlModel);
//            myPreferenceManager.setVillage(json);
//        }
//    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    public static void askPermission(final Activity activity, final int request_code) {
        // BEGIN_INCLUDE(camera_permission_request)

        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(activity,
                permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.

            new AlertDialog.Builder(activity)
                    .setTitle(activity.getResources().getString(R.string.need_permission))
                    .setMessage(R.string.permission_rationale)
                    .setPositiveButton(activity.getResources().getString(R.string.confirm), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE
                                            , Manifest.permission.WRITE_EXTERNAL_STORAGE
                                            , permission.CAMERA},
                                    request_code);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE
                            , Manifest.permission.WRITE_EXTERNAL_STORAGE
                            , permission.CAMERA},
                    request_code);
        }
    }

    public static boolean hasPermission(Activity activity) {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity, permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }


    public static Api getApiService() {

        okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(40, TimeUnit.SECONDS)
                .writeTimeout(40, TimeUnit.SECONDS)
                .readTimeout(40, TimeUnit.SECONDS)
                .build();

//        Gson gson = new GsonBuilder()
//                .setDateFormat("yyy-MM-dd'T'HH:mm:ssZ")
//                .create();

//        //Keep Session
//        CookieManager cookieManager = new CookieManager();
//        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
//        okHttpClient.setCookieHandler(cookieManager);

        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.getApiBaseUrl())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return client.create(Api.class);
    }

    public static Api getApiServiceMaster() {

//        Gson gson = new GsonBuilder()
//                .setDateFormat("yyy-MM-dd'T'HH:mm:ssZ")
//                .create();

        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.getApiBaseUrlMaster())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return client.create(Api.class);
    }

    public static String checkNullBigdecimal(BigDecimal object) {

        if (object != null)
            return object.toPlainString();
        else
            return "";
    }

    public static BigDecimal checkNullBigdecimalInput(String object) {
        Log.d("OBJECT >>", object);
        if (object != null && !object.equals(""))
            return new BigDecimal(object);
        else
            return null;
    }

    public static String checkNullDouble(Double d) {
        if (d != null && d != 0)
            return d.toString();
        else
            return "";
    }

    public static String checkNullInteger(Integer i) {
        if (i != null && i != 0)
            return i.toString();
        else
            return "";
    }

    public static String checkNullLong(Long l) {
        if (l != null && l != 0)
            return l.toString();
        else
            return "";
    }


    public static int getIntFromEdt(String object) {
        try {
            return Integer.parseInt(object);
        } catch (Exception e) {
            return 0;
        }
    }

    public static Double getDoubleFromEdt(String object) {
        try {
            return Double.parseDouble(object);
        } catch (Exception e) {
            return 0.0;
        }
    }

    public static String getStringFromEdt(Editable editable) {
        if (editable.length() != 0) {
            return editable.toString();
        } else {
            return "";
        }
    }

    public static Integer getIntegerFromEdt(String str) {
        if (str.length() != 0) {
            return Integer.parseInt(str);
        } else {
            return 0;
        }
    }

    public static String getDate() {
        Calendar c = Calendar.getInstance();
//        SimpleDateFormat df = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss aa");
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return df.format(c.getTime());
    }

    public static boolean isNetworkConnected(Activity activity) {//Check connection
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static boolean isOnline(Activity activity) { //Check mode Offline/Online
        MyPreferenceManager myPreferenceManager = new MyPreferenceManager(activity);
        return myPreferenceManager.isOnline();
    }

    public static int getUserId(Activity activity) { //Check mode Offline/Online
        MyPreferenceManager myPreferenceManager = new MyPreferenceManager(activity);
        return myPreferenceManager.getUserId();
    }

    public static int getUserType(Activity activity) { //Check mode Offline/Online
        MyPreferenceManager myPreferenceManager = new MyPreferenceManager(activity);
        return myPreferenceManager.getUserType();
    }

    public static long getUserPid(Activity activity) { //Check mode Offline/Online
        MyPreferenceManager myPreferenceManager = new MyPreferenceManager(activity);
        return myPreferenceManager.getUserPid();
    }

    public static double tryParseDouble(String s) {
        try {
            return Double.parseDouble(s);

        } catch (Exception ex) {

        }
        return 0.0;
    }

    public static Long tryParseLong(String s) {
        try {
            return Long.parseLong(s);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Integer tryParseInt(String s) {
        try {
            return Integer.parseInt(s);

        } catch (Exception ex) {

        }
        return 0;
    }


    public static String convertDateThai(String date) {

        String[] parts = date.split("/");
        String d = parts[0]; // 004
        String m = parts[1];
        String y = parts[2];

        if (m.substring(1).equals("0")) {
            m.replace("0", "");
        }

        String sMonth = theMonth(Integer.parseInt(m) - 1);

        return d + " " + sMonth + " " + (Integer.parseInt(y) + 543);
    }

    public static String convertDateEng(String date) {
if (date.length()>0) {
    String[] parts = date.split(" ");
    String d = parts[0]; // 004
    String m = parts[1];
    String y = parts[2];

    String sMonth = theMonthEng(m);

    return d + "/" + sMonth + "/" + (Integer.parseInt(y) - 543);
}else {
    return "";
}
    }

    public static String theMonth(int month) {
        String[] monthNames = {"ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."};
        return monthNames[month];
    }

    public static String theMonthEng(String month) {
        String[] monthNames = {"ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."};
        for (int i = 0; i < monthNames.length; i++) {
            if (month.equals(monthNames[i])) {
                String s = "";
                if (i < 10) {
                    s = "0" + String.valueOf(i);
                } else {
                    s = String.valueOf(i);
                }

                return s;
            }
        }
        return null;

    }


    public static String convertDate(String date) throws ParseException {
//        Nov 9, 2017 10:48:55 AM
        if (date.length()>0) {
            List<String> formatStrings = Arrays.asList("MMM dd, yyyy hh:mm:ss a", "yyyy-MM-dd", "dd/MM/yyyy"
                    , "d/MM/yyyy", "MMM d, yyyy hh:mm:ss a");
            DateFormat targetFormat = new SimpleDateFormat("dd/MM/yyyy");
            for (String formatString : formatStrings) {
                try {
                    Date dateF = new SimpleDateFormat(formatString).parse(date);
                    return targetFormat.format(dateF);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
//        try {
//            DateFormat originalFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
//            DateFormat targetFormat = new SimpleDateFormat("dd/MM/yyyy");
//            Date newDate = originalFormat.parse(date);
//            String formattedDate = targetFormat.format(newDate);
//
//            return formattedDate;
//        }catch (Exception e){
//            return "";
//        }
    }

    public static boolean checkPermissionLocation(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                ;
    }

    public static void updateData(Farmer mFarmerItem, final UpdateListener updateListener) {

        if (mFarmerItem.getFarm() == null) { //add def to farm
//            mFarmerItem.setFarm(new RealmList<Farm>());
        }

        for (int i = 0; i < mFarmerItem.getFarm().size(); i++) {
            Farm farm = mFarmerItem.getFarm().get(i);

            if (farm.getWorker() == null) {
//                farm.setWorker(new Worker());
            } else {
                addDefaultToNull(farm.getWorker());
            }

            if (farm.getLivestock() == null) {
//                farm.setLivestock(new RealmList<Livestock>());
            } else {
                for (int ls = 0; ls < farm.getLivestock().size(); ls++) {
                    addDefaultToNull(farm.getLivestock().get(ls));
                }
            }

            if (farm.getFeed() == null) {
//                farm.setFeed(new RealmList<Feed>());
            } else {
                for (int fd = 0; fd < farm.getFeed().size(); fd++) {
                    addDefaultToNull(farm.getFeed().get(fd));
                }
            }

            if (farm.getFarmImage() == null) {
//                farm.setFarmImage(new RealmList<FarmImage>());
            } else {
                for (int fi = 0; fi < farm.getFarmImage().size(); fi++) {
                    addDefaultToNull(farm.getFarmImage().get(fi));
                }
            }

            if (farm.getFarmStandard() == null) {
//                farm.setFarmStandard(new RealmList<FarmStandard>());
            } else {
                for (int fs = 0; fs < farm.getFarmStandard().size(); fs++) {
                    addDefaultToNull(farm.getFarmStandard().get(fs));
                }
            }

            addDefaultToNull(farm);
        }

        addDefaultToNull(mFarmerItem);

        ArrayList<Farmer> farmer = new ArrayList<>();
        farmer.add(mFarmerItem);

        Api api = AppUtils.getApiService();
        String s = String.valueOf(farmer);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        gsonBuilder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = gsonBuilder.create();

        String json = gson.toJson(farmer);

//        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), json);


        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/dldregist");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        String fname = "farmer.json";

        final File file = new File(myDir, fname);
//        Uri.fromFile(file);

        if (file.exists()) {
            file.delete();
        }
        json = json.replace(System.getProperty("line.separator"), "");
        try {
            FileOutputStream out = new FileOutputStream(file);
            out.write(json.getBytes());
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        File filep = new File(file.getPath());

        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), filep);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", filep.getName(), requestBody);

        Call<ResponseInsertFarmer> call = api.setInsertObjectData(fileToUpload);
        call.enqueue(new Callback<ResponseInsertFarmer>() {
            @Override
            public void onResponse(Call<ResponseInsertFarmer> call, Response<ResponseInsertFarmer> response) {
                if (file.exists()) {
                    file.delete();
                }
                if (response.body() != null) {

                    if (response.body().getResponseCode().equals("200")) {

                        updateListener.onSuccess(response.body().getResponseMessage(), response.body());


                    } else {
                        updateListener.onError(response.body().getResponseMessage(), response.body());
                    }
                } else {
                    updateListener.onError(response.message(), null);
                }
            }

            @Override
            public void onFailure(Call<ResponseInsertFarmer> call, Throwable t) {
                if (file.exists()) {
                    file.delete();
                }
                updateListener.onError(t.getMessage(), null);
            }

        });

    }

    public static void updateDataArray(ArrayList<Farmer> farmers, final UpdateListener updateListener) {

        for (int a = 0; a < farmers.size(); a++) {
            Farmer mFarmerItem = farmers.get(a);
            if (mFarmerItem.getFarm() == null) { //add def to farm
//                mFarmerItem.setFarm(new RealmList<Farm>());
            }

            for (int i = 0; i < mFarmerItem.getFarm().size(); i++) {
                Farm farm = mFarmerItem.getFarm().get(i);

                if (farm.getWorker() == null) {
//                    farm.setWorker(new Worker());
                } else {
                    addDefaultToNull(farm.getWorker());
                }

                if (farm.getLivestock() == null) {
//                    farm.setLivestock(new RealmList<Livestock>());
                } else {
                    for (int ls = 0; ls < farm.getLivestock().size(); ls++) {
                        addDefaultToNull(farm.getLivestock().get(ls));
                    }
                }

                if (farm.getFeed() == null) {
//                    farm.setFeed(new RealmList<Feed>());
                } else {
                    for (int fd = 0; fd < farm.getFeed().size(); fd++) {
                        addDefaultToNull(farm.getFeed().get(fd));
                    }
                }

                if (farm.getFarmImage() == null) {
//                    farm.setFarmImage(new RealmList<FarmImage>());
                } else {
                    for (int fi = 0; fi < farm.getFarmImage().size(); fi++) {
                        addDefaultToNull(farm.getFarmImage().get(fi));
                    }
                }

                if (farm.getFarmStandard() == null) {
//                    farm.setFarmStandard(new RealmList<FarmStandard>());
                } else {
                    for (int fs = 0; fs < farm.getFarmStandard().size(); fs++) {
                        addDefaultToNull(farm.getFarmStandard().get(fs));
                    }
                }

                addDefaultToNull(farm);
            }

            addDefaultToNull(mFarmerItem);
        }

        Api api = AppUtils.getApiService();
        String s = String.valueOf(farmers);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        gsonBuilder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = gsonBuilder.create();

        String json = gson.toJson(farmers);

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/dldregist");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        String fname = "farmer.json";

        final File file = new File(myDir, fname);
//        Uri.fromFile(file);

        if (file.exists()) {
            file.delete();
        }
        json = json.replace(System.getProperty("line.separator"), "");
        try {
            FileOutputStream out = new FileOutputStream(file);
            out.write(json.getBytes());
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        File filep = new File(file.getPath());

        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), filep);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", filep.getName(), requestBody);
//        progressBar.setVisibility(View.VISIBLE);
        Call<ResponseInsertFarmer> call = api.setInsertObjectData(fileToUpload);
        call.enqueue(new Callback<ResponseInsertFarmer>() {
            @Override
            public void onResponse(Call<ResponseInsertFarmer> call, Response<ResponseInsertFarmer> response) {
//                progressBar.setVisibility(View.GONE);
//                if (file.exists ()) {file.delete ();}
                if (response.body() != null) {

                    if (response.body().getResponseCode().equals("200")) {

                        updateListener.onSuccess(response.body().getResponseMessage(), response.body());

                    } else {
                        updateListener.onError(response.body().getResponseMessage(), response.body());
                    }
                } else {
                    updateListener.onError(response.message(), null);
                }
            }

            @Override
            public void onFailure(Call<ResponseInsertFarmer> call, Throwable t) {
                if (file.exists()) {
                    file.delete();
                }
                updateListener.onError(t.getMessage(), null);
            }

        });

    }

    public static void addDefaultToNull(Object obj) {
        try {
            Field[] a = obj.getClass().getDeclaredFields();
            for (int i = 0; i < a.length; i++) {
                a[i].setAccessible(true);
                if (a[i].get(obj) == null) {
                    Type type = a[i].getType();
                    if (type.equals(Integer.class)) {
                        a[i].set(obj, 0);
                    } else if (type.equals(String.class)) {
                        a[i].set(obj, "");
                    } else if (type.equals(Long.class)) {
                        a[i].set(obj, 0L);
                    } else if (type.equals(Double.class)) {
                        a[i].set(obj, 0.0);
                    } else if (type.equals(Float.class)) {
                        a[i].set(obj, 0.0);
                    } else if (type.equals(RealmList.class)) {
//                        String c=((RealmList)type).toArray().getClass().toString();
//                        Type compType=((RealmList)type).toArray().getClass().getComponentType();
//                        if(compType==Farm.class) {
//                            a[i].set(obj, new RealmList<Farm>());
//                        }else if(compType== Livestock.class){
//                            a[i].set(obj, new ArrayList<Livestock>());
//                        }
                    } else {
                        a[i].set(obj, null);
                    }
                }

                //Change date format for date fields
                if ((a[i].getName().toLowerCase().contains("date") &&
                        !a[i].getName().toLowerCase().contains("by"))
                        || a[i].getName().toLowerCase().contains("day")) {
                    String name = a[i].getName();
                    a[i].set(obj, convertDate((String) a[i].get(obj)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean checkPermissionGallery(final Context context) {

        return ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                ;
    }

    public static boolean checkPermissionCamera(final Context context) {

        return ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                ;
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static int getImageOrientation(String imagePath) {
        int rotate = 0;
        try {

            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotate;
    }

    public static Bitmap scaleBitmap(Bitmap bm, int maxWidth, int maxHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();

        Log.v("Pictures", "Width and height are " + width + "--" + height);

        if (width > height) {
            // landscape
            float ratio = (float) width / maxWidth;
            width = maxWidth;
            height = (int) (height / ratio);
        } else if (height > width) {
            // portrait
            float ratio = (float) height / maxHeight;
            height = maxHeight;
            width = (int) (width / ratio);
        } else {
            // square
            height = maxHeight;
            width = maxWidth;
        }

        Log.v("Pictures", "after scaling Width and height are " + width + "--" + height);

        bm = Bitmap.createScaledBitmap(bm, width, height, true);
        return bm;
    }


}
