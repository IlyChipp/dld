package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 10/19/2017 AD.
 */
@Parcel
public class FeedTypeMaster implements APIResponse{
    //DBHelper
    public static final String TABLE = "feed_type";

    String responseCode;
    String responseMessage;
    ArrayList<FeedTypeEntity> data;

    public FeedTypeMaster(){ }

    @Override
    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<FeedTypeEntity> getData() {
        return data;
    }

    public void setData(ArrayList<FeedTypeEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class FeedTypeEntity {
        int feed_Type_ID;
        String feed_Type_Name;
        int orderIndex;
        int status_ID;

        public FeedTypeEntity(){ }

        public int getFeed_Type_ID() {
            return feed_Type_ID;
        }

        public void setFeed_Type_ID(int feed_Type_ID) {
            this.feed_Type_ID = feed_Type_ID;
        }

        public String getFeed_Type_Name() {
            return feed_Type_Name;
        }

        public void setFeed_Type_Name(String feed_Type_Name) {
            this.feed_Type_Name = feed_Type_Name;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String FEED_TYPE_ID = "feed_Type_ID";
        public static final String FEED_TYPE_NAME = "feed_Type_Name";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
    }
}
