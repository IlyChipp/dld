package com.zealtech.dld.dldregist.model.newFarmer;

/**
 * Created by Windows 8.1 on 1/11/2560.
 */

public class Image {
    private  Integer farmerId = 0;
    private  String createDate = "";
    private  String imageName = "";
    private  String imagePath = "";
    private  String imageSize = "";
    private  String imageUrl = "";
    private  Integer orderIndex = 0;
    private  String pid = "";
    private  Integer statusId = 0;
    private  String farmImage_Id = "";

    public String getFarmImage_Id() {
        return farmImage_Id;
    }

    public Integer getFarmerId() {
        return farmerId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public String getImageName() {
        return imageName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getImageSize() {
        return imageSize;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Integer getOrderIndex() {
        return orderIndex;
    }

    public String getPid() {
        return pid;
    }

    public Integer getStatusId() {
        return statusId;
    }
}
