package com.zealtech.dld.dldregist.util;

/**
 * Created by dev on 9/27/17.
 */

public interface DataValidator<T> {
    // return null on success, return error String on failure
    public String validate(T data);
}
