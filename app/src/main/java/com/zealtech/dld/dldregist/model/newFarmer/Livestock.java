
package com.zealtech.dld.dldregist.model.newFarmer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Livestock extends RealmObject {

    @PrimaryKey
    private String uuid;

    @Expose
    @SerializedName("livestock_ID")
    private Integer livestockID=0;

    @Expose
    @SerializedName("createBy")
    private String createBy="";

    @Expose
    @SerializedName("createDate")
    private String createDate="";

    @Expose
    @SerializedName("livestock_Amount")
    private Integer livestockAmount=0;

    @Expose
    @SerializedName("livestock_Type_ID")
    private Integer livestockTypeID=0;

    @Expose
    @SerializedName("livestock_Values")
    private Double livestockValues=0.0;

    @Expose
    @SerializedName("updateBy")
    private String updateBy="";

    @Expose
    @SerializedName("updateDate")
    private String updateDate="";

    @Expose
    @SerializedName("livestock_Type_Other")
    private String livestock_Type_Other="";

    @Expose
    @SerializedName("status_ID")
    private Integer status_ID=0;

    public String getLivestock_Type_Other() {
        return livestock_Type_Other;
    }

    public void setLivestock_Type_Other(String livestock_Type_Other) {
        this.livestock_Type_Other = livestock_Type_Other;
    }

    public String getUuid() {
        return uuid;
    }

    public Integer getStatus_ID() {
        return status_ID;
    }

    public void setStatus_ID(Integer status_ID) {
        this.status_ID = status_ID;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getLivestockID() {
        return livestockID;
    }

    public void setLivestockID(Integer livestockID) {
        this.livestockID = livestockID;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Integer getLivestockAmount() {
        return livestockAmount;
    }

    public void setLivestockAmount(Integer livestockAmount) {
        this.livestockAmount = livestockAmount;
    }

    public Integer getLivestockTypeID() {
        return livestockTypeID;
    }

    public void setLivestockTypeID(Integer livestockTypeID) {
        this.livestockTypeID = livestockTypeID;
    }

    public Double getLivestockValues() {
        return livestockValues;
    }

    public void setLivestockValues(Double livestockValues) {
        this.livestockValues = livestockValues;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

}
