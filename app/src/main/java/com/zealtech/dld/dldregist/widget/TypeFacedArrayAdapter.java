package com.zealtech.dld.dldregist.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


/**
 * Created by fanqfang on 10/28/2017 AD.
 */

public class TypeFacedArrayAdapter<T> extends ArrayAdapter<T> {
    Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/Prompt-Light.ttf");

    public TypeFacedArrayAdapter(@NonNull Context context,
                                 @LayoutRes int resource,
                                 @NonNull List<T> objects) {
        super(context, resource, objects);
    }

    public TypeFacedArrayAdapter(@NonNull Context context,
                                 @LayoutRes int resource,
                                 @NonNull T[] objects) {
        super(context, resource, objects);
    }

    // Affects default (closed) state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setTypeface(tf);
        return view;
    }

    // Affects opened state of the spinner
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setTypeface(tf);
        return view;
    }
}
