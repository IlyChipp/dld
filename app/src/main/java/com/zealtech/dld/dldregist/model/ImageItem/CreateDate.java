
package com.zealtech.dld.dldregist.model.ImageItem;

import com.google.gson.annotations.SerializedName;


public class CreateDate {

    @SerializedName("date")
    private Integer date;
    @SerializedName("day")
    private Integer day;
    @SerializedName("hours")
    private Integer hours;
    @SerializedName("minutes")
    private Integer minutes;
    @SerializedName("month")
    private Integer month;
    @SerializedName("nanos")
    private Integer nanos;
    @SerializedName("seconds")
    private Integer seconds;
    @SerializedName("time")
    private Integer time;
    @SerializedName("timezoneOffset")
    private Integer timezoneOffset;
    @SerializedName("year")
    private Integer year;

    public Integer getDate() {
        return date;
    }

    public Integer getDay() {
        return day;
    }

    public Integer getHours() {
        return hours;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public Integer getMonth() {
        return month;
    }

    public Integer getNanos() {
        return nanos;
    }

    public Integer getSeconds() {
        return seconds;
    }

    public Integer getTime() {
        return time;
    }

    public Integer getTimezoneOffset() {
        return timezoneOffset;
    }

    public Integer getYear() {
        return year;
    }
}
