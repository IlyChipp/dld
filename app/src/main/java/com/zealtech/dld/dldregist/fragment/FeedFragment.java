package com.zealtech.dld.dldregist.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.BuildConfig;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.FarmActivity;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.model.FeedKindMaster.FeedKindEntity;
import com.zealtech.dld.dldregist.model.FeedSubTypeFormatMaster.SubTypeFormatEntity;
import com.zealtech.dld.dldregist.model.FeedSubTypeMaster.SubTypeEntity;
import com.zealtech.dld.dldregist.model.FeedTypeMaster.FeedTypeEntity;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.model.newFarmer.FarmList;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.Feed;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseFeed;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseInsertFarmer;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.util.UpdateListener;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;
import com.zealtech.dld.dldregist.widget.MySpinnerAdapter;
import com.zealtech.dld.dldregist.widget.TypeFacedArrayAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedFragment extends Fragment {

    private static final String TAG = "FeedFragment";


    static final int SELL = 1;
    static final int USE = 2;

    LinearLayout llSellFeeds;
    LinearLayout btnAddSell;
    LinearLayout llUseFeeds;
    LinearLayout btnAddUse;

    int farmID;
    String userID = "";


    private DatabaseRealm databaseRealm;

    public void setFarmID(int farmID) {
        this.farmID = farmID;
    }


    @BindView(R.id.spnTypeName)
    Spinner spnTypeName;

    ArrayList<FeedKindEntity> feedKinds = new ArrayList<>();
    ArrayList<SubTypeFormatEntity> feedSubTypeFormats = new ArrayList<>();
    ArrayList<SubTypeEntity> feedSubTypes = new ArrayList<>();
    ArrayList<FeedTypeEntity> feedTypes = new ArrayList<>();
    ArrayList<Feed> feeds = new ArrayList<>();
    HashSet<Feed> oldFeeds = new HashSet<>();

    HashMap<Integer, FeedHeadFragment.GUIDataHolder.FeedData> guiData;

    @BindView(R.id.progressBar)
    FrameLayoutDisable progressBar;


    private Farm mFarmItem;
    DBHelper dbHelper;
    Realm realm;
    int selectedFeedTypeId;
    private LinearLayout llFeed;

    public FeedFragment() {
        super();
    }

    public static FeedFragment newInstance(
            int farmID,
            HashMap<Integer, FeedHeadFragment.GUIDataHolder.FeedData> guiData) {

        FeedFragment fragment = new FeedFragment();
        fragment.setFarmID(farmID);
        fragment.guiData = guiData;

        Bundle bundle = new Bundle();

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: farmID=" + farmID);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {



        Log.d(TAG, "onCreateView: farmID=" + farmID);

        View rootView = inflater.inflate(R.layout.fragment_feed, container, false);

        ButterKnife.bind( this, rootView);

        llFeed = (LinearLayout) rootView.findViewById(R.id.llFeed);

        dbHelper = new DBHelper(getContext());
        databaseRealm = new DatabaseRealm(getContext());

        userID = Integer.toString(AppUtils.getUserId(getActivity()));

        realm = Realm.getDefaultInstance();

        setupFeedTypeSpinner(spnTypeName);
        getFeedProperties();


        initializeGuiData(rootView);


        if (AppUtils.isOnline(getActivity())) {
            fetchFeeds();
        } else {
            // databaseRealm.deleteFeeds();
            if (isAdded()) {
                feeds.addAll(((FarmActivity) getActivity()).getFarmById(farmID).getFeed());
                oldFeeds.addAll(((FarmActivity) getActivity()).getFarmById(farmID).getFeed());
                refreshFeedLayouts();
            }
        }


        return rootView;
    }

    void initializeGuiData(View rootView) {

        llSellFeeds = (LinearLayout) rootView.findViewById(R.id.llAddSell);
        btnAddSell = (LinearLayout) rootView.findViewById(R.id.btnAddSell);
        llUseFeeds = (LinearLayout) rootView.findViewById(R.id.llAddUse);
        btnAddUse = (LinearLayout) rootView.findViewById(R.id.btnAdd);

        btnAddSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewFeed(SELL);
            }
        });

        btnAddUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewFeed(USE);
            }
        });

        for (FeedTypeEntity ft : feedTypes) {
            FeedHeadFragment.GUIDataHolder.FeedData d = new FeedHeadFragment.GUIDataHolder.FeedData();
            for (SubTypeEntity st : feedSubTypes) {
                FeedHeadFragment.GUIDataHolder.FeedData.SubtypeData d2 = new FeedHeadFragment.GUIDataHolder.FeedData.SubtypeData();
                int subTypeId = st.getFeed_SubType_ID();
                if (subTypeId == SELL) {
                    d2.layout = llSellFeeds;
                    d2.button = btnAddSell;
                } else if (subTypeId == USE) {
                    d2.layout = llUseFeeds;
                    d2.button = btnAddUse;
                }
                d.subtypes.put(st.getFeed_SubType_ID(), d2);
            }
            guiData.put(ft.getFeed_Type_ID(), d);
        }

    }


    /* Get all the feed related properties. */
    void getFeedProperties() {
        feedTypes = dbHelper.getListFeedType();
        feedSubTypes = dbHelper.getListFeedSubType();
        feedKinds = dbHelper.getListFeedKind();
        feedSubTypeFormats = dbHelper.getListFeedSubTypeFormat();

//        ArrayList<FeedKindEntity> lFeedKinds = new ArrayList<>();
//        lFeedKinds = dbHelper.getListFeedKind();
//        for (FeedKindEntity feedKind: lFeedKinds) {
//            feedKinds.put(feedKind.getFeed_Kind_ID(), feedKind);
//        }
//
//        ArrayList<SubTypeFormatEntity> lSubtypeFormats = new ArrayList<>();
//        lSubtypeFormats = dbHelper.getListFeedSubTypeFormat();
//        for (SubTypeFormatEntity stf : lSubtypeFormats) {
//            feedSubTypeFormats.put(stf.getMS_Feed_SubType_Format_ID(), stf);
//        }
    }

    int getUniqueLocalFeedId() {
        int maxId = -1;
        for (Feed feed : feeds) {
            maxId = Math.max(feed.getFeedID(), maxId);
        }
        return maxId + 1;
    }

    public void addNewFeed(int subTypeId) {
        Feed feed = new Feed();
        // Local feed id is required before data is actually uploaded to the web api.
        //int feedId = getUniqueLocalFeedId();
        feed.setUuid(UUID.randomUUID().toString());
        feed.setFeedID(0);
        feed.setFeedTypeID(selectedFeedTypeId);
        feed.setFeedSubTypeID(subTypeId);
        feed.setLocalOnly(true);

        feeds.add(feed);
        View feedView = createFeedView(feed);
        FeedHeadFragment.GUIDataHolder.FeedData.SubtypeData data =
                guiData.get(selectedFeedTypeId).subtypes.get(subTypeId);
        data.views.put(feed.getUuid(), feedView);
        data.layout.addView(feedView);
    }

    public View createFeedView(final Feed feed) {

        final View view = getLayoutInflater().inflate(R.layout.item_feed, null);

        LinearLayout llSubTypeFormat = view.findViewById(R.id.llSubTypeFormat);
        LinearLayout llProduct = view.findViewById(R.id.llProduct);
        Spinner spnFormat = (Spinner) view.findViewById(R.id.spnFormat);
        Spinner spnFeedKind = (Spinner) view.findViewById(R.id.spnFeedKind);
        MyEditNoHLTextView txtOtherKind = (MyEditNoHLTextView) view.findViewById(R.id.txtOtherKind);
        MyEditNoHLTextView txtRai = (MyEditNoHLTextView) view.findViewById(R.id.txtRai);
        MyEditNoHLTextView txtNgan = (MyEditNoHLTextView) view.findViewById(R.id.txtNgan);
        MyEditNoHLTextView txtWa = (MyEditNoHLTextView) view.findViewById(R.id.txtWa);
        MyNoHLTextView delete = (MyNoHLTextView) view.findViewById(R.id.delete);
        MyEditNoHLTextView txtProduct = (MyEditNoHLTextView) view.findViewById(R.id.txtProduct);

        if (feed.getFeedPerRai() == 0)
            txtProduct.setText("");
        else
            txtProduct.setText(String.valueOf(feed.getFeedPerRai()));

        if (feed.getFeedAreaRai() == 0)
            txtRai.setText("");
        else
            txtRai.setText(String.valueOf(feed.getFeedAreaRai()));

        if (feed.getFeedAreaWa() == 0)
            txtWa.setText("");
        else
            txtWa.setText(String.valueOf(feed.getFeedAreaWa()));

        if (feed.getFeedAreaNgan() == 0)
            txtNgan.setText("");
        else
            txtNgan.setText(String.valueOf(feed.getFeedAreaNgan()));

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int subTypeId = feed.getFeedSubTypeID();
                if (subTypeId == 1) {
                    llSellFeeds.removeView(view);
                    feeds.remove(feed);
                } else {
                    llUseFeeds.removeView(view);
                    feeds.remove(feed);
                }
            }
        });

        if (feed.getFeedTypeID() == 3) {
            llSubTypeFormat.setVisibility(View.VISIBLE);
            setSpnSubFormat(spnFormat, feed);
        }if (feed.getFeedTypeID() == 1){
            llProduct.setVisibility(View.VISIBLE);
        }

        setSpnFeedKind(spnFeedKind, feed, txtOtherKind);

        return view;

    }


    public void setSpnFeedKind(final Spinner spn, final Feed feed, final MyEditNoHLTextView txtOtherKind) {

        TypeFacedArrayAdapter<FeedKindEntity> adapter = new TypeFacedArrayAdapter<FeedKindEntity>(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                feedKinds);
        spn.setAdapter(adapter);

        if (feed.getFeedKindID() != null) {
            int position = -1;
            for (int i = 0; i < feedKinds.size(); i++) {
                FeedKindEntity feedKind = feedKinds.get(i);
                if (feedKind.getFeed_Kind_ID() == feed.getFeedKindID()) {
                    position = i;
                    break;
                }
            }
//            if (BuildConfig.DEBUG && position < 0) {
//                throw new AssertionError();
//            }
            spn.setSelection(position);

            if (feed.getFeedKindID() == 12 || feed.getFeedKindID() == 23) {
                txtOtherKind.setEnabled(true);
                txtOtherKind.setText(feed.getFeedKindOther());
                txtOtherKind.setBackgroundResource(R.drawable.shape_edit_text);
            } else {
                txtOtherKind.setText("");
                txtOtherKind.setEnabled(false);
                txtOtherKind.setBackgroundResource(R.drawable.shape_edit_text_disable);
            }

            spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (feedKinds.get(position).getFeed_Kind_ID() == 12 || feedKinds.get(position).getFeed_Kind_ID() == 23) {
                        txtOtherKind.setEnabled(true);
                        txtOtherKind.setText(feed.getFeedKindOther());
                        txtOtherKind.setBackgroundResource(R.drawable.shape_edit_text);
                    } else {
                        txtOtherKind.getEditableText().clear();
                        txtOtherKind.getEditableText().clearSpans();
                        txtOtherKind.setText("");
                        txtOtherKind.setEnabled(false);
                        txtOtherKind.setBackgroundResource(R.drawable.shape_edit_text_disable);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FeedKindEntity ft = (FeedKindEntity) spn.getSelectedItem();
                if (ft.getFeed_Kind_ID() == 12 || ft.getFeed_Kind_ID() == 23) {
                    // disable
                    txtOtherKind.setEnabled(true);
                    txtOtherKind.setBackgroundResource(R.drawable.shape_edit_text);
                } else {
                    // enable
                    txtOtherKind.setEnabled(false);
                    txtOtherKind.setBackgroundResource(R.drawable.shape_edit_text_disable);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void setSpnSubFormat(final Spinner spn, final Feed feed) {

        TypeFacedArrayAdapter<SubTypeFormatEntity> adapter =
                new TypeFacedArrayAdapter<SubTypeFormatEntity>(
                        getContext(),
                        android.R.layout.simple_dropdown_item_1line,
                        feedSubTypeFormats);
        spn.setAdapter(adapter);

        if (feed.getFeedSubTypeFormatID() != null && !feed.getFeedSubTypeFormatID().equals("")) {
            int position = -1;
            for (int i = 0; i < feedSubTypeFormats.size(); i++) {
                SubTypeFormatEntity stf = feedSubTypeFormats.get(i);
                if (stf.getMS_Feed_SubType_Format_ID() == feed.getFeedSubTypeFormatID()) {
                    position = i;
                    break;
                }
            }
//            if (BuildConfig.DEBUG && position < 0) {
//                throw new AssertionError();
//            }
            spn.setSelection(position);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
        Log.d(TAG, "onSaveInstanceState: farmID=" + farmID);
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
        Log.d(TAG, "onActivityCreated: farmID=" + farmID);
    }

    public void fetchFeeds() {

        Log.d(TAG, "fetchFeeds: farmID=" + farmID);

        final ProgressBar progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleSmall);
        progressBar.setVisibility(View.VISIBLE);

        Api api = AppUtils.getApiService();
        Call<ResponseFeed> call = api.getFeed(Integer.toString(farmID));
        call.enqueue(new Callback<ResponseFeed>() {
            @Override
            public void onResponse(Call<ResponseFeed> call, Response<ResponseFeed> response) {
                progressBar.setVisibility(View.GONE);
                if (isAdded()) {
                    if (response.body() != null) {
                        feeds.clear();
                        feeds.addAll(response.body().getData());

                        for (Feed f: feeds) {
                            f.setUuid(UUID.randomUUID().toString());
                        }

                        oldFeeds.clear();
                        oldFeeds.addAll(response.body().getData());

                        refreshFeedLayouts();
                    } else {
                        try {
                            Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseFeed> call, Throwable t) {
                Log.d(TAG, "Failed to fetch feeds: " + t);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void setupFeedTypeSpinner(final Spinner spnFeedType_) {

        final ArrayList<FeedTypeEntity> feedTypes = dbHelper.getListFeedType();

        ArrayList<String> listFeedType = new ArrayList<>();
        for (int i = 0; i < feedTypes.size(); i++) {
            listFeedType.add(feedTypes.get(i).getFeed_Type_Name());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listFeedType);

        spnFeedType_.setAdapter(adapter);
        spnFeedType_.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedFeedTypeId = feedTypes.get(position).getFeed_Type_ID();
                refreshFeedLayouts();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void refreshFeedLayouts() {
        Log.d(TAG, "refreshFeedLayouts: " + selectedFeedTypeId);

        llSellFeeds.removeAllViews();
        llUseFeeds.removeAllViews();
        for (Feed feed : feeds) {
            Log.d(TAG, feed.toString());
            int typeId = feed.getFeedTypeID();
            int subTypeId = feed.getFeedSubTypeID();
            if (subTypeId < 1 || subTypeId > 2)
                continue;
            FeedHeadFragment.GUIDataHolder.FeedData.SubtypeData data =
                    guiData.get(typeId).subtypes.get(subTypeId);
            if (!data.views.containsKey(feed.getUuid()))
                data.views.put(feed.getUuid(), createFeedView(feed));
            if (feed.getFeedTypeID() == selectedFeedTypeId) {
                Log.d(TAG, "adding to layout " + data.layout);
                data.layout.addView(data.views.get(feed.getUuid()));
            }
        }
    }

    @OnClick(R.id.btnSubmit)
    public void clickBtnSubmit() {

        progressBar.setVisibility(View.VISIBLE);
        synchronizeToModel();

        final Farmer farmer = ((FarmActivity) getActivity()).getFarmer();

        Farm farm = null;
        for (Farm f : farmer.getFarm()) {
            if (f.getFarmID() == farmID) {
                farm = f;
                break;
            }
        }

        farm.setUpdateBy(userID);
        farm.setUpdateDate(AppUtils.getDate());

        HashSet<Feed> newFeeds = new HashSet<>(feeds);

        HashSet<Feed> deletedFeeds = new HashSet<>(oldFeeds);
        deletedFeeds.removeAll(newFeeds);

        farm.getFeed().clear();

        for (Feed feed : deletedFeeds) {
            Feed feedToUpload = new Feed(feed);
            // delete
            feedToUpload.setStatusID(-9);
            farm.getFeed().add(feedToUpload);
        }

        for (Feed feed : feeds) {
            Feed feedToUpload = new Feed(feed);
            if (feedToUpload.isLocalOnly()) {
                // insert
                feedToUpload.setFeedID(0);
                feedToUpload.setStatusID(1);
            } else {
                // update
                feedToUpload.setStatusID(1);
            }

            farm.getFeed().add(feedToUpload);
        }

        for (Feed feed : farm.getFeed()) {
            feed.setCreateBy(userID);
            feed.setCreateDate(AppUtils.getDate());
            feed.setUpdateBy(userID);
            feed.setUpdateDate(AppUtils.getDate());
        }

        if (AppUtils.isOnline(getActivity())) {
            AppUtils.updateData(farmer, new UpdateListener() {
                @Override
                public void onSuccess(String message, ResponseInsertFarmer res) {
                    // save data from api result to database
                    progressBar.setVisibility(View.GONE);
                    RealmList<Feed> oldFeeds = null;
                    for (Farm f : farmer.getFarm()) {
                        if (f.getFarmID() == farmID) {
                            oldFeeds = f.getFeed();
                            break;
                        }
                    }
                    RealmList<Feed> newFeeds = null;
                    for (FarmList fl : res.getFarmerList().get(0).getFarmList()) {
                        Farm f = fl.getFarm();
                        if (f.getFarmID() == farmID) {
                            newFeeds = f.getFeed();
                        }
                    }
                    oldFeeds.clear();
                    oldFeeds.addAll(newFeeds);
                    for (Feed f: newFeeds) {
                        f.setUuid(UUID.randomUUID().toString());
                    }
                    // Need to remove all data from farmer and add again.
                    // Update is not what we want.
                    // TODO:
                    // - Make sure all the old feeds are removed to not fill database with
                    //   dangling feeds.
//                    databaseRealm.deleteDataFarmer(farmer);
//                    databaseRealm.addDataFarmer(farmer);
                    databaseRealm.upDateDataFarmer(farmer);
                    ((FarmActivity)getActivity()). getViewPager().setCurrentItem(3);
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                }

                @Override
                public void onError(String message, ResponseInsertFarmer res) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            progressBar.setVisibility(View.GONE);
            databaseRealm.upDateDataFarmer(farmer);
            ((FarmActivity)getActivity()). getViewPager().setCurrentItem(3);
            Toast.makeText(getContext(), getResources().getString(R.string.update_success), Toast.LENGTH_LONG).show();
        }

    }

    void synchronizeToModel() {
        for (Feed feed : feeds) {
            Log.d(TAG, "synchronizeToModel: " + feed.getUuid());
            int typeId = feed.getFeedTypeID();
            int subTypeId = feed.getFeedSubTypeID();
            FeedHeadFragment.GUIDataHolder.FeedData.SubtypeData data =
                    guiData.get(typeId).subtypes.get(subTypeId);
            View feedView = data.views.get(feed.getUuid());
            if (BuildConfig.DEBUG && feedView == null)
                throw new AssertionError();
            synchronizeFeedViewToFeed(feedView, feed);
        }
    }

//    public static <T> T tryGetFromList(List<T> list, int index, T defaultValue) {
//        if (index < 0)
//            return defaultValue;
//        return list.get(index);
//    }
//
//    public static <T> T tryGetFromList(List<T> list, int index) {
//        return tryGetFromList(list, index, null);
//    }

    public void synchronizeFeedViewToFeed(View view, Feed feed) {

        Spinner spnFormat = (Spinner) view.findViewById(R.id.spnFormat);
        Spinner spnFeedKind = (Spinner) view.findViewById(R.id.spnFeedKind);
        MyEditNoHLTextView txtOtherKind = (MyEditNoHLTextView) view.findViewById(R.id.txtOtherKind);
        MyEditNoHLTextView txtRai = (MyEditNoHLTextView) view.findViewById(R.id.txtRai);
        MyEditNoHLTextView txtNgan = (MyEditNoHLTextView) view.findViewById(R.id.txtNgan);
        MyEditNoHLTextView txtWa = (MyEditNoHLTextView) view.findViewById(R.id.txtWa);

        Log.d(TAG, feed + " spinner values: " + spnFormat.getSelectedItemPosition() + " " +
                spnFeedKind.getSelectedItemPosition());

        SubTypeFormatEntity stf = (SubTypeFormatEntity) spnFormat.getSelectedItem();
        feed.setFeedSubTypeFormatID(stf == null ? null : stf.getMS_Feed_SubType_Format_ID());

        FeedKindEntity feedKind = (FeedKindEntity) spnFeedKind.getSelectedItem();
        feed.setFeedKindID(feedKind.getFeed_Kind_ID());

        feed.setFeedKindOther(txtOtherKind.getText().toString());
        feed.setFeedAreaRai(AppUtils.tryParseDouble(txtRai.getText().toString()));
        feed.setFeedAreaNgan(AppUtils.tryParseDouble(txtNgan.getText().toString()));
        feed.setFeedAreaWa(AppUtils.tryParseDouble(txtWa.getText().toString()));

    }
}
