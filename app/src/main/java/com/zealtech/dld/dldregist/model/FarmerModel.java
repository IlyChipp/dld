package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.math.BigDecimal;

/**
 * Created by fanqfang on 9/18/2017 AD.
 */

@Parcel
public class FarmerModel implements APIResponse {
    //DBHelper
    public static final String TABLE = "farmer";

    String responseCode;
    String responseMessage;
    FarmerEntity data;

    public FarmerModel(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public FarmerEntity getData() {
        return data;
    }

    public void setData(FarmerEntity data) {
        this.data = data;
    }

    @Parcel
    public static class FarmerEntity {

        BigDecimal farmer_ID;
        int amphur_ID;
        String birthDay;
        int company_Type_ID;
        String createBy;
        String createDate;
        BigDecimal debtAmount;
        BigDecimal debtsAmountIn;
        BigDecimal debtsAmountOut;
        String email;
        int farmer_Group_ID;
        int farmer_Problem_ID;
        String farmer_Problem_Other;
        int farmer_Status_ID;
        int farmer_Type_ID;
        String first_Name;
        int gender_ID;
        int government_Help_ID;
        int holding_Equipment;
        int holding_Land_ID;
        BigDecimal home_ID;
        String homeNo;
        int image_ID;
        int isAgency;
        int isCancel;
        int isDead;
        String last_Name;
        BigDecimal latitude;
        BigDecimal longitude;
        int main_Job_ID;
        int marital_Status_ID;
        String mobile;
        String phone;
        BigDecimal pid;
        String postCode;
        int prefix_ID;
        int province_ID;
        BigDecimal revenue;
        String road;
        int second_Job_ID;
        String soi;
        int status_ID;
        int tambol_ID;
        int tax_ID;
        String updateBy;
        String updateDate;
        String village;
        int village_ID;
        boolean localOnly;

        public boolean isLocalOnly() {
            return localOnly;
        }

        public void setLocalOnly(boolean localOnly) {
            this.localOnly = localOnly;
        }

        public BigDecimal getFarmer_ID() {
            return farmer_ID;
        }

        public void setFarmer_ID(BigDecimal farmer_ID) {
            this.farmer_ID = farmer_ID;
        }

        public int getAmphur_ID() {
            return amphur_ID;
        }

        public void setAmphur_ID(int amphur_ID) {
            this.amphur_ID = amphur_ID;
        }

        public String getBirthDay() {
            return birthDay;
        }

        public void setBirthDay(String birthDay) {
            this.birthDay = birthDay;
        }

        public int getCompany_Type_ID() {
            return company_Type_ID;
        }

        public void setCompany_Type_ID(int company_Type_ID) {
            this.company_Type_ID = company_Type_ID;
        }

        public String getCreateBy() {
            return createBy;
        }

        public void setCreateBy(String createBy) {
            this.createBy = createBy;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public BigDecimal getDebtAmount() {
            return debtAmount;
        }

        public void setDebtAmount(BigDecimal debtAmount) {
            this.debtAmount = debtAmount;
        }

        public BigDecimal getDebtsAmountIn() {
            return debtsAmountIn;
        }

        public void setDebtsAmountIn(BigDecimal debtsAmountIn) {
            this.debtsAmountIn = debtsAmountIn;
        }

        public BigDecimal getDebtsAmountOut() {
            return debtsAmountOut;
        }

        public void setDebtsAmountOut(BigDecimal debtsAmountOut) {
            this.debtsAmountOut = debtsAmountOut;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public int getFarmer_Group_ID() {
            return farmer_Group_ID;
        }

        public void setFarmer_Group_ID(int farmer_Group_ID) {
            this.farmer_Group_ID = farmer_Group_ID;
        }

        public int getFarmer_Problem_ID() {
            return farmer_Problem_ID;
        }

        public void setFarmer_Problem_ID(int farmer_Problem_ID) {
            this.farmer_Problem_ID = farmer_Problem_ID;
        }

        public String getFarmer_Problem_Other() {
            return farmer_Problem_Other;
        }

        public void setFarmer_Problem_Other(String farmer_Problem_Other) {
            this.farmer_Problem_Other = farmer_Problem_Other;
        }

        public int getFarmer_Status_ID() {
            return farmer_Status_ID;
        }

        public void setFarmer_Status_ID(int farmer_Status_ID) {
            this.farmer_Status_ID = farmer_Status_ID;
        }

        public int getFarmer_Type_ID() {
            return farmer_Type_ID;
        }

        public void setFarmer_Type_ID(int farmer_Type_ID) {
            this.farmer_Type_ID = farmer_Type_ID;
        }

        public String getFirst_Name() {
            return first_Name;
        }

        public void setFirst_Name(String first_Name) {
            this.first_Name = first_Name;
        }

        public int getGender_ID() {
            return gender_ID;
        }

        public void setGender_ID(int gender_ID) {
            this.gender_ID = gender_ID;
        }

        public int getGovernment_Help_ID() {
            return government_Help_ID;
        }

        public void setGovernment_Help_ID(int government_Help_ID) {
            this.government_Help_ID = government_Help_ID;
        }

        public int getHolding_Equipment() {
            return holding_Equipment;
        }

        public void setHolding_Equipment(int holding_Equipment) {
            this.holding_Equipment = holding_Equipment;
        }

        public int getHolding_Land_ID() {
            return holding_Land_ID;
        }

        public void setHolding_Land_ID(int holding_Land_ID) {
            this.holding_Land_ID = holding_Land_ID;
        }

        public BigDecimal getHome_ID() {
            return home_ID;
        }

        public void setHome_ID(BigDecimal home_ID) {
            this.home_ID = home_ID;
        }

        public String getHomeNo() {
            return homeNo;
        }

        public void setHomeNo(String homeNo) {
            this.homeNo = homeNo;
        }

        public int getImage_ID() {
            return image_ID;
        }

        public void setImage_ID(int image_ID) {
            this.image_ID = image_ID;
        }

        public int getIsAgency() {
            return isAgency;
        }

        public void setIsAgency(int isAgency) {
            this.isAgency = isAgency;
        }

        public int getIsCancel() {
            return isCancel;
        }

        public void setIsCancel(int isCancel) {
            this.isCancel = isCancel;
        }

        public int getIsDead() {
            return isDead;
        }

        public void setIsDead(int isDead) {
            this.isDead = isDead;
        }

        public String getLast_Name() {
            return last_Name;
        }

        public void setLast_Name(String last_Name) {
            this.last_Name = last_Name;
        }

        public BigDecimal getLatitude() {
            return latitude;
        }

        public void setLatitude(BigDecimal latitude) {
            this.latitude = latitude;
        }

        public BigDecimal getLongitude() {
            return longitude;
        }

        public void setLongitude(BigDecimal longitude) {
            this.longitude = longitude;
        }

        public int getMain_Job_ID() {
            return main_Job_ID;
        }

        public void setMain_Job_ID(int main_Job_ID) {
            this.main_Job_ID = main_Job_ID;
        }

        public int getMarital_Status_ID() {
            return marital_Status_ID;
        }

        public void setMarital_Status_ID(int marital_Status_ID) {
            this.marital_Status_ID = marital_Status_ID;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public BigDecimal getPid() {
            return pid;
        }

        public void setPid(BigDecimal pid) {
            this.pid = pid;
        }

        public String getPostCode() {
            return postCode;
        }

        public void setPostCode(String postCode) {
            this.postCode = postCode;
        }

        public int getPrefix_ID() {
            return prefix_ID;
        }

        public void setPrefix_ID(int prefix_ID) {
            this.prefix_ID = prefix_ID;
        }

        public int getProvince_ID() {
            return province_ID;
        }

        public void setProvince_ID(int province_ID) {
            this.province_ID = province_ID;
        }

        public BigDecimal getRevenue() {
            return revenue;
        }

        public void setRevenue(BigDecimal revenue) {
            this.revenue = revenue;
        }

        public String getRoad() {
            return road;
        }

        public void setRoad(String road) {
            this.road = road;
        }

        public int getSecond_Job_ID() {
            return second_Job_ID;
        }

        public void setSecond_Job_ID(int second_Job_ID) {
            this.second_Job_ID = second_Job_ID;
        }

        public String getSoi() {
            return soi;
        }

        public void setSoi(String soi) {
            this.soi = soi;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        public int getTambol_ID() {
            return tambol_ID;
        }

        public void setTambol_ID(int tambol_ID) {
            this.tambol_ID = tambol_ID;
        }

        public int getTax_ID() {
            return tax_ID;
        }

        public void setTax_ID(int tax_ID) {
            this.tax_ID = tax_ID;
        }

        public String getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(String updateBy) {
            this.updateBy = updateBy;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public String getVillage() {
            return village;
        }

        public void setVillage(String village) {
            this.village = village;
        }

        public int getVillage_ID() {
            return village_ID;
        }

        public void setVillage_ID(int village_ID) {
            this.village_ID = village_ID;
        }

        public FarmerEntity(){ }


    }

    public class Column{

        public static final String ID = BaseColumns._ID;
        public static final String FARMER_ID = "farmer_ID";
        public static final String AMPHUR_ID = "amphur_ID";
        public static final String BIRTHDAY = "birthDay";
        public static final String COMPANY_TYPE_ID = "company_Type_ID";
        public static final String CREATE_BY = "createBy";
        public static final String CREATE_DATE = "createDate";
        public static final String DEBTAMOUNT = "debtAmount";
        public static final String DEBTSAMOUNT_IN = "debtAmountIn";
        public static final String DEBTSAMOUNT_OUT = "debtsAmountOut";
        public static final String EMAIL = "email";
        public static final String FARMER_GROUP_ID = "farmer_Group_ID";
        public static final String FARMER_PROBLEM_ID = "farmer_Problem_ID";
        public static final String FARMER_PROBLEM_OTHER = "farmer_Problem_Other";
        public static final String FARMER_STATUS_ID = "farmer_Status_ID";
        public static final String FARMER_TYPE_ID = "farmer_Type_ID";
        public static final String FIRST_NAME = "first_Name";
        public static final String GENDER_ID = "gender_ID";
        public static final String GOVERNMENT_HELP_ID = "government_Help_ID";
        public static final String HOLDING_EQUIPMENT = "holding_Equipment";
        public static final String HOLDING_LAND_ID ="holding_Land_ID";
        public static final String HOME_ID = "home_ID";
        public static final String HOME_NO = "homeNo";
        public static final String IMAGE_ID = "image_ID";
        public static final String IS_AGENCY = "isAgency";
        public static final String IS_CANCEL = "isCancel";
        public static final String IS_DEAD = "isDead";
        public static final String LASTNAME = "last_Name";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String MAIN_JOB_ID = "main_Job_ID";
        public static final String MARITAL_STATUS_ID = "marital_Status_ID";
        public static final String MOBILE = "mobile";
        public static final String PHONE = "phone";
        public static final String PID = "pid";
        public static final String POSTCODE = "postCode";
        public static final String PREFIX_ID = "prefix_ID";
        public static final String PROVINCE_ID = "province_ID";
        public static final String REVENUE = "revenue";
        public static final String ROAD = "road";
        public static final String SECOND_JOB_ID = "second_Job_ID";
        public static final String SOI = "soi";
        public static final String STATUS_ID = "status_ID";
        public static final String TAMBOL_ID = "tambol_ID";
        public static final String TAX_ID = "tax_ID";
        public static final String UPDATE_BY = "updateBy";
        public static final String UPDATE_DATE = "updateDate";
        public static final String VILLAGE = "village";
        public static final String VILLAGE_ID = "village_ID";
        public static final String LOCAL_ONLY = "localOnly";


    }


}
