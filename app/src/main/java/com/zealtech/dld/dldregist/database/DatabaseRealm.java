package com.zealtech.dld.dldregist.database;

import android.content.Context;

import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.Feed;
import com.zealtech.dld.dldregist.model.newFarmer.Livestock;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by Windows 8.1 on 24/10/2560.
 */

public class DatabaseRealm {
    private static final String DB_NAME = "dld.db";
    private static final int DB_VERSION = 1;
    public RealmConfiguration realmConfiguration;
    Realm realm;
    private Context context;


    public DatabaseRealm(Context context) {
        this.context = context;
        Realm.init(context);
        realm = Realm.getDefaultInstance();
        realm.setAutoRefresh(true);
    }

    public boolean addDataFarmer(Farmer farmer) {
        String uuid = UUID.randomUUID().toString();
        farmer.setUuid(uuid);
        realm.beginTransaction();
        realm.copyToRealm(farmer);
        realm.commitTransaction();
        return true;
    }

    public String addDataFarmerUUID(Farmer farmer) {
        String uuid = UUID.randomUUID().toString();
        farmer.setUuid(uuid);
        realm.beginTransaction();
        realm.copyToRealm(farmer);
        realm.commitTransaction();
        return uuid;
    }


    public Farmer addDataFarmerReturn(Farmer farmer) {
        String uuid = UUID.randomUUID().toString();
        farmer.setUuid(uuid);
        realm.beginTransaction();
        realm.copyToRealm(farmer);
        realm.commitTransaction();
        return farmer;
    }
//    boolean sanityCheckFarmer(Farmer farmer) {
//        // Make sure we don't have any duplicate ids. That would corrupt the database.
//        ArrayList<Integer> id = new ArrayList<>();
//        int size = 0;
//        for (Farm farm : farmer.getFarm()) {
//            for (Feed feed : farm.getFeed()) {
//                size = farm.getFeed().size();
//                id.add(feed.getFeedID());
//            }
//        }
//        HashSet<Integer> feedIds = new HashSet<>(id);
//
//        if (feedIds.size() == size)
//            return false;
//
//        return true;
//    }

    public boolean upDateDataFarmer(Farmer farmer) {

//        if (BuildConfig.DEBUG && !sanityCheckFarmer(farmer)) {
//            throw new AssertionError("Trying to add corrupted data to database");
//
        RealmResults<Farmer> farmers = realm.where(Farmer.class).equalTo("uuid", farmer.getUuid()).findAll();

        if (farmers.size() > 0) {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(farmer);
            realm.commitTransaction();

            return true;
        } else {
            return false;
        }

    }

    public boolean upDateDataFarmerByFarmerID(Farmer farmer) {

        RealmResults<Farmer> farmers = realm.where(Farmer.class).equalTo("farmerID", farmer.getFarmerID()).findAll();

        if (farmers.size() > 0) {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(farmer);
            realm.commitTransaction();

            return true;
        } else {
            return false;
        }

    }

    /*public boolean upDateDataFarmerProvinceID(int farmerID) {

        Farmer farmers = realm.where(Farmer.class).equalTo("farmerID", farmerID).findFirst();

        if (farmers!=null) {
            realm.beginTransaction();
            farmers.setProvinceID();
            realm.copyToRealmOrUpdate(farmer);
            realm.commitTransaction();

            return true;
        } else {
            return false;
        }

    }
*/

    public RealmResults<Farmer> getListDataFarmer(Boolean isOnline) {
        RealmResults<Farmer> farmers = realm.where(Farmer.class).equalTo("isOnline", isOnline).findAll();
        return farmers;
    }


    public boolean deleteDataFarmer(Farmer farmer) {
        Farmer farmers = realm.where(Farmer.class).equalTo("uuid", farmer.getUuid()).findFirst();
        if (farmers != null) {
            realm.beginTransaction();
            farmers.deleteFromRealm();

            realm.commitTransaction();

            return true;
        } else {
            return false;
        }
    }

    public boolean deleteDataFarmerOnline() {
        RealmResults<Farmer> isOnline = realm.where(Farmer.class).equalTo("isOnline", true).findAll();
        if (isOnline.size() > 0) {
            realm.beginTransaction();
            isOnline.deleteAllFromRealm();
            realm.commitTransaction();
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteDataFarmerOffline() {
        RealmResults<Farmer> isOnline = realm.where(Farmer.class).equalTo("isOnline", false).findAll();
        if (isOnline.size() > 0) {
            realm.beginTransaction();
            isOnline.deleteAllFromRealm();
            realm.commitTransaction();
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<Farmer> getDataFarmerArray(Boolean isOnline) {
        RealmResults<Farmer> farmers = realm.where(Farmer.class).equalTo("isOnline", isOnline).findAll();
        ArrayList<Farmer> farmers1 = new ArrayList<>();
        farmers1.addAll(realm.copyFromRealm(farmers));
        return farmers1;
    }

    public Farmer getDataFarmer(String uuid) {
        Farmer farmers = realm.where(Farmer.class).equalTo("uuid", uuid).findFirst();
        return farmers;
    }

    public boolean isFarmerExist(int farmerId) {
        if (realm.where(Farmer.class).equalTo("farmerID", farmerId).findFirst() != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isFarmerExistOnline(int farmerId) {
        if (realm.where(Farmer.class).equalTo("farmerID", farmerId)
                .equalTo("isOnline",true).findFirst() != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean addDataLiveStock(ArrayList<Livestock> livestock, int farmID) {
        RealmResults<Livestock> deleteStock = realm.where(Livestock.class).equalTo("farmID", farmID).findAll();
        realm.beginTransaction();
        deleteStock.deleteAllFromRealm();
        realm.commitTransaction();

        realm.beginTransaction();
        realm.copyToRealm(livestock);
        realm.commitTransaction();
        return true;
    }

    public ArrayList<Livestock> getDataLiveStockCopy() {
        RealmResults<Livestock> livestocks = realm.where(Livestock.class).findAll();
        ArrayList<Livestock> livestocks1 = new ArrayList<>();
        livestocks1.addAll(realm.copyFromRealm(livestocks));
        return livestocks1;
    }

    public RealmResults<Livestock> getDataLiveStock() {
        RealmResults<Livestock> livestocks = realm.where(Livestock.class).findAll();

        return livestocks;
    }

    public void addFeeds(ArrayList<Feed> feeds) {
        realm.beginTransaction();
        realm.where(Feed.class).findAll().deleteAllFromRealm();
        realm.copyToRealm(feeds);
        realm.commitTransaction();
    }

    public List<Feed> getFeeds() {
        List<Feed> list = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Feed> feeds = realm
                .where(Feed.class)
                .findAll();
        return realm.copyFromRealm(feeds);
    }

    public void deleteFeeds() {
        realm.beginTransaction();
        realm.where(Feed.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }

    public boolean addDataFarm(ArrayList<Farm> farm) {
        realm.beginTransaction();
        realm.copyToRealm(farm);
        realm.commitTransaction();
        return true;
    }


}
