package com.zealtech.dld.dldregist.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.MainActivity;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;


public class MainFragment extends Fragment {

    private static final String CLASS_NAME = "MainFragment";

    ViewPager viewPager;
private RelativeLayout relative_select_farm;
    private TabLayout tabLayout;
    private final List<String> titleList = new ArrayList<>();


    public MainFragment() {
        super();
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    public  void openFragmentFarmer(int i){
        setTabCat(i);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        initInstances(rootView);
        ButterKnife.bind(this,rootView);
//        Bundle args = getArguments();
//        data = (FarmerModel) args.getSerializable("LoginPID");
        relative_select_farm= (RelativeLayout) rootView.findViewById(R.id.relative_select_farm);
        relative_select_farm.setVisibility(View.GONE);

        int from = ((MainActivity) getActivity()).getFrom();

        if (from==2){ // from register
            viewPager.setCurrentItem(0);
//            viewPager.setCurrentItem(1);
        }

        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);

        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setBackgroundColor(getResources().getColor(R.color.slidingTab));
        tabLayout.setTabMode(TabLayout.GRAVITY_CENTER);

        setTabs();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new FarmerFragment(), getString(R.string.farmer));
        adapter.addFragment(new JobTypeFragment(), getString(R.string.job_type));
//        adapter.addFragment(new ListFarmFragment(), getString(R.string.list_farm));
//        adapter.addFragment(new FamilyFragment(), getString(R.string.family_member));
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);

        ((MainActivity)getActivity()).setViewPager(viewPager);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
            titleList.add(title);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void setTabCat(int i) {

        MyNoHBTextView tabOne = (MyNoHBTextView) LayoutInflater.from(getActivity()).inflate(R.layout.item_tab, null);
        tabOne.setText(titleList.get(i));
        tabLayout.getTabAt(i).setCustomView(tabOne);

    }
    private void setTabs() {
        for (int i = 0; i < 2; i++) {
            setTabCat(i);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }
    public void replaceCategoryFragment(Fragment fragment){
        replaceFragment(fragment);
    }


    public void replaceFragment(Fragment fragment){
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentContainer, fragment)
                .addToBackStack(null)
                .commit();
    }


}
