package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/27/2017 AD.
 */
@Parcel
public class HoldingLandMaster implements APIResponse{
    //DBHelper
    public static final String TABLE = "holding_land";

    String responseCode;
    String responseMessage;
    ArrayList<LandEntity> data;

    public HoldingLandMaster(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<LandEntity> getData() {
        return data;
    }

    public void setData(ArrayList<LandEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class LandEntity{
        int holding_Land_ID;
        String holding_Land_Name;
        int orderIndex;
        int status_ID;

        public LandEntity(){ }

        public int getHolding_Land_ID() {
            return holding_Land_ID;
        }

        public void setHolding_Land_ID(int holding_Land_ID) {
            this.holding_Land_ID = holding_Land_ID;
        }

        public String getHolding_Land_Name() {
            return holding_Land_Name;
        }

        public void setHolding_Land_Name(String holding_Land_Name) {
            this.holding_Land_Name = holding_Land_Name;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String HOLDING_LAND_ID = "holding_Land_ID";
        public static final String HOLDING_LAND_NAME = "holding_Land_Name";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
    }
}
