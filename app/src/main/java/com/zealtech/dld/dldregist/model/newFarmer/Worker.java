package com.zealtech.dld.dldregist.model.newFarmer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by fanqfang on 10/28/2017 AD.
 */

public class Worker extends RealmObject implements Serializable {

    @PrimaryKey
    private String uuid;

    @Expose
    @SerializedName("amphur_ID")
    private Integer amphur_ID=0;

    @Expose
    @SerializedName("email")
    private String email="";

    @Expose
    @SerializedName("first_Name")
    private String first_Name="";

    @Expose
    @SerializedName("homeNo")
    private String homeNo="";

    @Expose
    @SerializedName("last_Name")
    private String last_Name="";

    @Expose
    @SerializedName("mobile")
    private String mobile="";

    @Expose
    @SerializedName("moo")
    private String moo="";

    @Expose
    @SerializedName("phone")
    private String phone="";

    @Expose
    @SerializedName("pid")
    private Long pid =0L;

    @Expose
    @SerializedName("postCode")
    private Integer postCode=0;

    @Expose
    @SerializedName("prefix_ID")
    private Integer prefix_ID=0;

    @Expose
    @SerializedName("province_ID")
    private Integer province_ID=0;

    @Expose
    @SerializedName("road")
    private String road="";

    @Expose
    @SerializedName("soi")
    private String soi="";

    @Expose
    @SerializedName("status_ID")
    private Integer status_ID=0;

    @Expose
    @SerializedName("tambol_ID")
    private Integer tambol_ID=0;

    @Expose
    @SerializedName("village_ID")
    private Integer village_ID=0;

    @Expose
    @SerializedName("worker_ID")
    private String worker_ID="0";

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getAmphur_ID() {
        return amphur_ID;
    }

    public void setAmphur_ID(Integer amphur_ID) {
        this.amphur_ID = amphur_ID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_Name() {
        return first_Name;
    }

    public void setFirst_Name(String first_Name) {
        this.first_Name = first_Name;
    }

    public String getHomeNo() {
        return homeNo;
    }

    public void setHomeNo(String homeNo) {
        this.homeNo = homeNo;
    }

    public String getLast_Name() {
        return last_Name;
    }

    public void setLast_Name(String last_Name) {
        this.last_Name = last_Name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMoo() {
        return moo;
    }

    public void setMoo(String moo) {
        this.moo = moo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Integer getPostCode() {
        return postCode;
    }

    public void setPostCode(Integer postCode) {
        this.postCode = postCode;
    }

    public Integer getPrefix_ID() {
        return prefix_ID;
    }

    public void setPrefix_ID(Integer prefix_ID) {
        this.prefix_ID = prefix_ID;
    }

    public Integer getProvince_ID() {
        return province_ID;
    }

    public void setProvince_ID(Integer province_ID) {
        this.province_ID = province_ID;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getSoi() {
        return soi;
    }

    public void setSoi(String soi) {
        this.soi = soi;
    }

    public Integer getStatus_ID() {
        return status_ID;
    }

    public void setStatus_ID(Integer status_ID) {
        this.status_ID = status_ID;
    }

    public Integer getTambol_ID() {
        return tambol_ID;
    }

    public void setTambol_ID(Integer tambol_ID) {
        this.tambol_ID = tambol_ID;
    }

    public Integer getVillage_ID() {
        return village_ID;
    }

    public void setVillage_ID(Integer village_ID) {
        this.village_ID = village_ID;
    }

    public String getWorker_ID() {
        return worker_ID;
    }

    public void setWorker_ID(String worker_ID) {
        this.worker_ID = worker_ID;
    }
}
