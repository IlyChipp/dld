package com.zealtech.dld.dldregist.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.DailyActivity;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;
import com.zealtech.dld.dldregist.widget.MySpinnerAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainDailyFragment extends Fragment {

    private static final String CLASS_NAME = "MainDailyFragment";

    @BindView(R.id.spn_farm)
    Spinner spn_farm;
    @BindView(R.id.relative_select_farm)
    RelativeLayout relative_select_farm;
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.txtBD)
    MyEditNoHLTextView txtBD;

    private final List<String> titleList = new ArrayList<>();
    ArrayList<Farm> farms = new ArrayList<>();
    ArrayList<String> farmName = new ArrayList<>();

    FarmDataFragment farmData;
    FarmInfoMainFragment farmInfoMain;
    FeedHeadFragment feedHead;
    LiveStockFragment liveStock;

    DBHelper dbHelper;
    int farmid;

    public MainDailyFragment() {
        super();
    }

    public static MainDailyFragment newInstance() {
        MainDailyFragment fragment = new MainDailyFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_daily_main, container, false);

        initInstances(rootView);
        ButterKnife.bind(this,rootView);

        dbHelper = new DBHelper(getActivity());
//        Bundle args = getArguments();
//        data = (FarmerModel) args.getSerializable("LoginPID");
        /*relative_select_farm= (RelativeLayout) rootView.findViewById(R.id.relative_select_farm);
        relative_select_farm.setVisibility(View.GONE)*/;
        farms.addAll(((DailyActivity) (getActivity())).getAllFarm());

        if(farms.size()!=0) {//case not new register (have farm)
            relative_select_farm.setVisibility(View.VISIBLE);
            for (int i = 0; i < farms.size(); i++) {
//                farmName.add(farms.get(i).getFarmName());
                farmName.add(getAddress(farms.get(i)));
            }

            MySpinnerAdapter adapter = new MySpinnerAdapter(
                    getContext(),
                    android.R.layout.simple_dropdown_item_1line,
                    farmName);

            spn_farm.setAdapter(adapter);

            spn_farm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    farmid=farms.get(position).getFarmID();
                    ((DailyActivity) getActivity()).setCurrent_FarmID(farms.get(position).getFarmID()); // for online
                    ((DailyActivity) getActivity()).setCurrent_FarmPosition(position); //for offline

                    initLayout();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            int position=0;
            for (int i = 0; i < farms.size(); i++) {
                if(farms.get(i).getFarmID()==((DailyActivity) getActivity()).getCurrent_FarmID()){
                    position=i;
                    break;
                }
            }
            spn_farm.setSelection(position);

        }else{ //case not have farm
            relative_select_farm.setVisibility(View.GONE);
            initLayout();
        }

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        txtBD.setText(day+"/"+month+"/"+year);
        ((DailyActivity)getActivity()).setFindDate(day+"/"+month+"/"+year);

        txtBD.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate();
            }
        });

        return rootView;
    }

    public void setDate() {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        int month = i1 + 1;

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(i, i1, i2);
                        txtBD.setText( i2+"/"+month+"/"+i);
                        ((DailyActivity)getActivity()).setFindDate( i2+"/"+month+"/"+i);

                        initLayout();
//                        date = calendar.getTimeInMillis();


//                        editText_date.setText(date2+"");
                    }


                }, year, month, day);
        datePickerDialog.show();
    }

    private String getAddress(Farm farm_a){
        String address="";
        String home="";
        String soi="";
        String road="";
        String post="";

        String province= dbHelper.getProvinceById(farm_a.getProvinceID()).getProvince_NameTh();
        String amphur= dbHelper.getAmphurById(farm_a.getAmphurID()).getAmphur_nameTh();
        String tambol= dbHelper.getTambolById(farm_a.getTambolID()).getTambol_NameTh();

        if(farm_a.getSoi()!=null&&!farm_a.getSoi().equals("")){
            soi=" ซอย "+farm_a.getSoi();
        }

        if(farm_a.getRoad()!=null&&!farm_a.getRoad().equals("")){
            road=" ถนน "+farm_a.getRoad();
        }

        if(farm_a.getPostCode()!=null&&!farm_a.getPostCode().equals("")){
            post=""+farm_a.getPostCode();
        }

        if(farm_a.getHomeNo()!=null&&!farm_a.getHomeNo().equals("")){
            home=""+farm_a.getHomeNo();
        }

        address=home+soi+road
                + " " +tambol
                + " " +amphur
                + " " +province
                +" "+post;

        return address;
    }

    final private static String UUID = "uuid";
    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
    }

    private void initLayout(){
        DailyFragment dailyFragment=DailyFragment.newInstance();

        getChildFragmentManager().beginTransaction()
                .replace(R.id.container, dailyFragment,"DailyFragment")
                .commit();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    public void replaceCategoryFragment(Fragment fragment) {
        replaceFragment(fragment);
    }

    public void replaceFragment(Fragment fragment) {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentContainer, fragment)
                .addToBackStack(null)
                .commit();
    }


}
