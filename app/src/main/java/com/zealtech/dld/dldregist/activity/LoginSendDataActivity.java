package com.zealtech.dld.dldregist.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.LoginModel;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginSendDataActivity extends AppCompatActivity {

    Context context;

    private MyEditNoHLTextView edit_username,edit_pass;
    private String useranme,pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_send_data);
        context=this;
        ButterKnife.bind(this);

        edit_username = findViewById(R.id.txtUsername);
        edit_pass = findViewById(R.id.txtPassword);

    }

    @OnClick(R.id.btnLogOnline)
    public void clickLoginOnline(){
        useranme=edit_username.getText().toString();
        pass=edit_pass.getText().toString();

        login(useranme,pass);
    }


    private void login(String username, String password) {
        Api api = AppUtils.getApiService();
        Call<LoginModel> call = api.login(username, password);
        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if (response.body() != null) {
                    if (response.body().getResponseCode().equals(getString(R.string.success))) {
                        int userId = response.body().getData().getUserId();
                        MyPreferenceManager myPreferenceManager=new MyPreferenceManager(context);
                        myPreferenceManager.setUserId(userId);
                        myPreferenceManager.setOnline(true);
                        Toast.makeText(context, response.body().getResponseMessage(), Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(context, response.body().getResponseMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        Toast.makeText(context, response.errorBody().string(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {

            }

        });

    }

}
