package com.zealtech.dld.dldregist.model;

import org.parceler.Parcel;

/**
 * Created by Jirat on 10/28/2017.
 */
@Parcel
public class SearchModel {

    long pid;
    Integer amphurId;
    Integer provinnceId;
    Integer tambolId;
    String firstname;
    String lastname;
    String lat_;
    String long_;
    String distance;
    String compName;
    String compPid;
    String deptName;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public Integer getAmphurId() {
        return amphurId;
    }

    public void setAmphurId(Integer amphurId) {
        this.amphurId = amphurId;
    }

    public Integer getProvinnceId() {
        return provinnceId;
    }

    public void setProvinnceId(Integer provinnceId) {
        this.provinnceId = provinnceId;
    }

    public Integer getTambolId() {
        return tambolId;
    }

    public void setTambolId(Integer tambolId) {
        this.tambolId = tambolId;
    }

    public String getLat_() {
        return lat_;
    }

    public void setLat_(String lat_) {
        this.lat_ = lat_;
    }

    public String getLong_() {
        return long_;
    }

    public void setLong_(String long_) {
        this.long_ = long_;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getCompPid() {
        return compPid;
    }

    public void setCompPid(String compPid) {
        this.compPid = compPid;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
}
