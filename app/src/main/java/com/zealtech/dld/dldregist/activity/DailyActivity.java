package com.zealtech.dld.dldregist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.zealtech.dld.dldregist.MainApplication;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.fragment.MainDailyFragment;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.FeedModel.FeedEntity;
import com.zealtech.dld.dldregist.model.LivestockDailyMaster;
import com.zealtech.dld.dldregist.model.LivestockDailyMaster.LivestockDailyEntity;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseFarm;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.zealtech.dld.dldregist.util.AppUtils.SENDFROM;
import static com.zealtech.dld.dldregist.util.AppUtils.UUID;

public class DailyActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    final public static String TAG_FARM = "tag_farm";
    final public static String FARMINFO = "farmInfo";
    final public static String MODE_DASHBORD = "dashBoard";
    final public static String MODE_LIVE = "live";
    final public static String MODE_ADD = "add";
    private MyNoHBTextView txtNav;
    private CoordinatorLayout appBar;

    final private ArrayList<LivestockDailyEntity> livestockMasters=new ArrayList<>();
    private ArrayList<FeedEntity> feedEntityArrayList=new ArrayList<>();

//    ArrayList<Farm> farms=new ArrayList<>();
    boolean online;
    Farmer farmer;
    int current_FarmID;
    int current_FarmPosition=0;
    DrawerLayout drawer;
    DatabaseRealm databaseRealm;
    Realm realm;
    String mode="";
    ImageView btnback;
    ImageView btn_back;
    ViewPager viewPager;
    String farmer_uuid="";
    private DBHelper dbHelper;
    int type,from;
    Tracker mTracker;
    String findDate="";

    public String getFindDate() {
        return findDate;
    }

    public void setFindDate(String findDate) {
        this.findDate = findDate;
    }

    public int getCurrent_FarmPosition() {
        return current_FarmPosition;
    }

    public void setCurrent_FarmPosition(int current_FarmPosition) {
        this.current_FarmPosition = current_FarmPosition;
    }

    public ViewPager getViewPager() {
        return viewPager;
    }

    public void setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
    }

    public int getCurrent_FarmID() {
        return current_FarmID;
    }

    public void setCurrent_FarmID(int current_FarmID) {
        this.current_FarmID = current_FarmID;
    }

    public String getFarmer_uuid() {
        return farmer_uuid;
    }

    public Farmer getFarmer() {
        return farmer;
    }

    public void setFarmer(Farmer farmer) {
        this.farmer = farmer;
    }

    public Farm getFarmById(int farmId){
        for(int i=0;i<farmer.getFarm().size();i++){
            if(farmer.getFarm().get(i).getFarmID()==farmId){
                return farmer.getFarm().get(i);
            }
        }
        return null;
    }

    public Farm getFarmByPosition(int position){
        if(farmer.getFarm().size()==0){
            return null;
        }else {
            return farmer.getFarm().get(position);
        }
    }

    public int getFrom() {
        return from;
    }

    boolean isDashBoard=false;

    public List<Farm> getAllFarm(){
        return farmer.getFarm();
    }

    public ArrayList<LivestockDailyEntity> getLivestockMasters() {
        return livestockMasters;
    }

    public ArrayList<FeedEntity> getFeedEntityArrayList() {
        return feedEntityArrayList;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farm);
        ButterKnife.bind(this);

        MainApplication application = (MainApplication) getApplication();
        mTracker = application.getDefaultTracker();

        initInstance();
        online = AppUtils.isOnline(this);
        mode = getIntent().getStringExtra("mode");
        from=getIntent().getIntExtra(SENDFROM,0);//0=offline, 1=FindFarmer,3=Register 4,hamburger

        current_FarmID=getIntent().getIntExtra("farmId",0);
        farmer_uuid = getIntent().getStringExtra(UUID);
        databaseRealm=new DatabaseRealm(this);
        dbHelper = new DBHelper(this);
        realm = Realm.getDefaultInstance();

        getDailyLiveMaster();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("หน้าบันทึกประจำวัน");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void initLayout(){
        checkAppBar();

        if (databaseRealm.getDataFarmer(farmer_uuid)!=null) {
            farmer = realm.copyFromRealm(databaseRealm.getDataFarmer(farmer_uuid));
        }

        if (!AppUtils.isOnline(this)){
            chooseMode();
        }else {
            getListFarm();
        }
    }

    private void getDailyLiveMaster() {
        progressBar.setVisibility(View.VISIBLE);
        Api api = AppUtils.getApiService();
        Call<LivestockDailyMaster> call = api.getDailyLivestockMS();
        call.enqueue(new Callback<LivestockDailyMaster>() {
            @Override
            public void onResponse(Call<LivestockDailyMaster> call, Response<LivestockDailyMaster> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    livestockMasters.addAll(response.body().getData());
                    initLayout();
                } else {
                    try {
                        Toast.makeText(DailyActivity.this, response.errorBody().string(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LivestockDailyMaster> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                try {
                    Toast.makeText(DailyActivity.this, t.getMessage().toString(), Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }

    @OnClick(R.id.btn_back)
    public void clickBtnBack() {
        if (from==0) {
            Intent intent = new Intent(DailyActivity.this, OfflineActivity.class);
            startActivity(intent);
            finish();
        }else   {
            finish();}

    }


    private void initInstance() {
        appBar = (CoordinatorLayout) findViewById(R.id.app_bar_main);
        txtNav = (MyNoHBTextView) appBar.findViewById(R.id.txtNav) ;
        btnback= (ImageView) appBar.findViewById(R.id.btn_back) ;

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");

        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.open_drawer, R.string.close_drawer);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_updatedata).setVisible(false);

        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (from==0) {
                    Intent intent = new Intent(DailyActivity.this, OfflineActivity.class);
                    startActivity(intent);
                    finish();
                }else   {
                    finish();}

            }
        });
//        btnback.setVisibility(View.VISIBLE);
    }

    private void getListFarm() {

        //Permission
        MyPreferenceManager myPreferenceManager=new MyPreferenceManager(this);
        Integer proId=myPreferenceManager.getPROID();
        Integer amId=myPreferenceManager.getAMID();
        Integer tamId=myPreferenceManager.getTAMID();

        if(proId==-99){
            proId=null;
        }

        if(amId==-99){
            amId=null;
        }

        if(tamId==-99){
            tamId=null;
        }

        progressBar.setVisibility(View.VISIBLE);
            Api api = AppUtils.getApiService();
            Call<ResponseFarm> call = api.getListFarm("" + farmer.getFarmerID(),proId,amId,tamId);
            call.enqueue(new Callback<ResponseFarm>() {
                @Override
                public void onResponse(Call<ResponseFarm> call, Response<ResponseFarm> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.body() != null && response.body().getResponseCode().equals(getString(R.string.success))) {

//                        farms.addAll(response.body().getData());
                        RealmList<Farm> farmRealmList=new RealmList<Farm>();
                        farmRealmList.addAll(response.body().getData());
                        farmer.setFarm(farmRealmList);
                        databaseRealm.upDateDataFarmer(farmer);
//                        databaseRealm.addDataFarm(response.body().getData());
//                        getLiveMaster();
                        chooseMode();

                    } else {
                        Toast.makeText(DailyActivity.this, getResources().getString(R.string.error), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseFarm> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        Toast.makeText(DailyActivity.this, t.getMessage().toString(), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
    }

//    private void getLiveMaster() {
//        progressBar.setVisibility(View.VISIBLE);
//        Api api = AppUtils.getApiService();
//        Call<LivestockMaster> call = api.getListLivestock();
//        call.enqueue(new Callback<LivestockMaster>() {
//            @Override
//            public void onResponse(Call<LivestockMaster> call, Response<LivestockMaster> response) {
//                progressBar.setVisibility(View.GONE);
//                if (response.body() != null) {
//                    livestockMasters.addAll(response.body().getData());
//                    chooseMode();
//                } else {
//                    try {
//                        Toast.makeText(FarmActivity.this, response.errorBody().string(), Toast.LENGTH_LONG).show();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<LivestockMaster> call, Throwable t) {
//                progressBar.setVisibility(View.GONE);
//                try {
//                    Toast.makeText(FarmActivity.this, t.getMessage().toString(), Toast.LENGTH_LONG).show();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//        });
//
//    }

//    private void getLiveMasterOffline(){
//        livestockMasters.addAll(dbHelper.getListLivestock());
//        chooseMode();
//    }

    private void chooseMode(){
        setTxtNav();
        getMainFarm(current_FarmID, mode);
    }

    public void getMainFarm(int farmID, String mode) {
        isDashBoard=false;
//        FarmActivityFragment farmActivity = new FarmActivityFragment();
//        getSupportFragmentManager().beginTransaction()
//                .replace(R.id.contentContainer, farmActivity, TAG_FARM)
//                .commit();
        if(!isFinishing()) {
            current_FarmID=farmID;
            MainDailyFragment mainDailyFragment = MainDailyFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                        .replace(R.id.contentContainer, mainDailyFragment, TAG_FARM)
                        .commit();
        }
    }

    private void setTxtNav(){
            txtNav.setText(getString(R.string.daily_title));
            txtNav.setGravity(Gravity.CENTER);
    }

    @Override
    public void onBackPressed() {
        if (AppUtils.isOnline(this)==true){
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        } else {
//            Intent intent = new Intent(FarmActivity.this, OfflineActivity.class);
//            startActivity(intent);
            finish();
//            super.onBackPressed();
        }
    }

    Toolbar toolbar;

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            Intent intent = new Intent(DailyActivity.this, SelectTypeActivity.class);
            startActivity(intent);
            MyPreferenceManager myPreferenceManager = new MyPreferenceManager(DailyActivity.this);
            myPreferenceManager.setUserId(0);
            myPreferenceManager.setUserPid(0);
            finishAffinity();
        }/* else if (id == R.id.nav_findpid) {
            Intent intent = new Intent(MainActivity.this, FindFarmerActivity.class);
            startActivity(intent);

        }*/  else if (id == R.id.nav_dash) {
            Intent intent = new Intent(DailyActivity.this, DailyActivity.class); //mode DashBoard > list farm
            intent.putExtra(UUID,farmer_uuid);
            intent.putExtra("mode", "dashBoard");
            intent.putExtra("farmId", 0);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_farmer) {
            Intent intent = new Intent(DailyActivity.this, MainActivity.class);
            intent.putExtra(UUID,farmer_uuid);
            intent.putExtra(AppUtils.TYPE,0);
            intent.putExtra(SENDFROM,4);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_farm) {
            Intent intent = new Intent(DailyActivity.this, FarmActivity.class); //mode DashBoard > list farm
            intent.putExtra(UUID,farmer_uuid);
            intent.putExtra("mode", "");
            intent.putExtra("farmId", 0);
            startActivity(intent);
            finish();

        }else if (id == R.id.nav_daily) {
//            Intent intent = new Intent(DailyActivity.this, DailyActivity.class); //mode DashBoard > list farm
//            intent.putExtra(UUID,farmer_uuid);
//            intent.putExtra("mode", "");
//            intent.putExtra("farmId", 0);
//            startActivity(intent);
//            finish();
        }else if (id == R.id.nav_updatedata) {
            Intent intent = new Intent(DailyActivity.this, OfflineActivity.class);
            startActivity(intent);
        }/* else if (id == R.id.nav_history) {
            Intent intent = new Intent(FarmActivity.this, HistoryActivity.class);
            startActivity(intent);
        }*/ else if (id == R.id.nav_aboutus) {

            Intent intent = new Intent(DailyActivity.this, AboutUsActivity.class);
            startActivity(intent);

        } /*else if (id == R.id.nav_update) {

        }*/
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void checkAppBar(){

        if (from==3||from==1){
            btnback.setVisibility(View.VISIBLE);

            toolbar.setNavigationIcon(null);
        }else {
            btnback.setVisibility(View.GONE);
        }
    }

}
