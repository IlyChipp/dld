package com.zealtech.dld.dldregist.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.zealtech.dld.dldregist.R;


/**
 * Created by fanqfang on 9/7/2017 AD.
 */

public class MyNoHLTextView extends android.support.v7.widget.AppCompatTextView {

    public MyNoHLTextView(Context context) {
        super(context);
        initialize();
    }

    public MyNoHLTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public MyNoHLTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    private void initialize() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), getResources().getString(R.string.font_en_light));
        setTypeface(tf ,1);
    }
}
