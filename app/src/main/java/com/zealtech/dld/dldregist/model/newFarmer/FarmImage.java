
package com.zealtech.dld.dldregist.model.newFarmer;


import com.google.gson.annotations.Expose;

import io.realm.RealmObject;

public class FarmImage extends RealmObject {
    @Expose
    private String imageString ="";
    @Expose
    private int orderIndex=0;
    @Expose
    private int status_ID=0;


    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }

    public int getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(int orderIndex) {
        this.orderIndex = orderIndex;
    }

    public int getStatus_ID() {
        return status_ID;
    }

    public void setStatus_ID(int status_ID) {
        this.status_ID = status_ID;
    }
}

