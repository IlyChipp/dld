package com.zealtech.dld.dldregist.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.zealtech.dld.dldregist.MainApplication;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.database.DBHelper;
import com.zealtech.dld.dldregist.database.DatabaseRealm;
import com.zealtech.dld.dldregist.manager.Contextor;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.APIResponse;
import com.zealtech.dld.dldregist.model.AmphurMaster;
import com.zealtech.dld.dldregist.model.CompanyTypeMaster;
import com.zealtech.dld.dldregist.model.FarmOwnerMaster;
import com.zealtech.dld.dldregist.model.FarmProblemMaster;
import com.zealtech.dld.dldregist.model.FarmerProblemMaster;
import com.zealtech.dld.dldregist.model.FarmerTypeMaster;
import com.zealtech.dld.dldregist.model.FeedKindMaster;
import com.zealtech.dld.dldregist.model.FeedSubTypeFormatMaster;
import com.zealtech.dld.dldregist.model.FeedSubTypeMaster;
import com.zealtech.dld.dldregist.model.FeedTypeMaster;
import com.zealtech.dld.dldregist.model.HelpMaster;
import com.zealtech.dld.dldregist.model.HoldingLandMaster;
import com.zealtech.dld.dldregist.model.LivestockMaster;
import com.zealtech.dld.dldregist.model.LivestockTypeMaster;
import com.zealtech.dld.dldregist.model.LoginModel;
import com.zealtech.dld.dldregist.model.MainJobMaster;
import com.zealtech.dld.dldregist.model.PrefixMaster;
import com.zealtech.dld.dldregist.model.ProvinceMaster;
import com.zealtech.dld.dldregist.model.SearchModel;
import com.zealtech.dld.dldregist.model.SecondJobMaster;
import com.zealtech.dld.dldregist.model.StandardMaster;
import com.zealtech.dld.dldregist.model.SupportStandardMaster;
import com.zealtech.dld.dldregist.model.TambolMaster;
import com.zealtech.dld.dldregist.model.VillageMaster;
import com.zealtech.dld.dldregist.model.farmerList.FarmerSearchList;
import com.zealtech.dld.dldregist.model.newFarmer.Farmer;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseFarmer;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.util.Uploader;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.zealtech.dld.dldregist.util.AppUtils.RequestPermissionCodeGallery;


public class LoginActivity extends AppCompatActivity {

    final String TAG = "LoginActivity";
    final private static String UUID = "uuid";

    DBHelper DBHelper = new DBHelper(this);
    Realm realm;
    @BindView(R.id.login)
    LinearLayout login;
    @BindView(R.id.progressBar)
    FrameLayout progressBar;
    @BindView(R.id.progressBar2)
    ProgressBar progressBar2;

    @BindView(R.id.txtUsername)
    MyEditNoHLTextView txtUsername;

    @BindView(R.id.txtPassword)
    MyEditNoHLTextView txtPassword;


    @BindView(R.id.btnRegist)
    MyNoHBTextView btnRegistbtn;

    @BindView(R.id.txt_regist)
    MyNoHLTextView txt_regist;


    @BindView(R.id.btnLogOffline)
    MyNoHBTextView LogOffline;

    DatabaseRealm databaseRealm;
    SearchModel searchModel;

    Boolean isDataFailed = false;
    String msgError = "";
    int loginType;

    int userId;
    String pid = "";
    int userTypeId;

    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        MainApplication application = (MainApplication) getApplication();
        mTracker = application.getDefaultTracker();

        databaseRealm = new DatabaseRealm(this);
        loginType = getIntent().getIntExtra(AppUtils.LOGINTYPE, 0);
        MyPreferenceManager myPreferenceManager = new MyPreferenceManager(LoginActivity.this);
        myPreferenceManager.setLoginType(loginType);

        if (loginType == 1) {
            btnRegistbtn.setVisibility(View.GONE);
            txt_regist.setVisibility(View.GONE);
            LogOffline.setVisibility(View.VISIBLE);
        } else if (loginType == 2) {
            btnRegistbtn.setVisibility(View.VISIBLE);
            txt_regist.setVisibility(View.VISIBLE);
            LogOffline.setVisibility(View.GONE);
        }
//        txtUsername.setText("admin");
//        txtPassword.setText("1234");



//        Thread t = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Uploader.init(getBaseContext());
//                populateDatabase();
//            }
//        });
//        t.setName("database-initializer");
//        t.start();
        progressBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        getPermission();

        Uploader.init(getBaseContext());
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                testUploads();
//            }
//        }).start();
    }

    AlertDialog alertDialog;

    private void checkOfflineData(final LoginModel.LoginEntity data) {

        if (isConnectedToInternet()) {

            alertDialog = new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.alert))
                    .setMessage("คุณยังมีข้อมูลที่ยังไม่ได้ส่งข้อมูล ต้องการที่จะส่งข้อมูลหรือไม่")
                    .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intent = new Intent(LoginActivity.this, OfflineActivity.class);
                            startActivity(intent);
//                            finish();
                            alertDialog.cancel();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (data.getUserTypeId() == 1) { //Staff
                                setUserDataPreference();
                                Intent intent = new Intent(LoginActivity.this, FindFarmerActivity.class);
                                startActivity(intent);
//                                finish();
                            } else { // 2 = farmer use search usertype=1 > search for farmer
                                searchModel = new SearchModel();
                                searchModel.setPid(Long.parseLong(data.getPid()));
                                getListFarmer(null, null, null, null, null
                                        , null, null, 1, null
                                        , null, null,data.getPid(),null,null);
                            }

                            alertDialog.cancel();
                        }

                    })
                    .show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mTracker.setScreenName("หน้าเข้าสู่ระบบ");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        MyPreferenceManager myPreferenceManager = new MyPreferenceManager(this);
        myPreferenceManager.setUserId(0);
        myPreferenceManager.setUserPid(0);
        databaseRealm.deleteDataFarmerOnline();
    }

    @SuppressLint("NewApi")
    private void getPermission() {
        if (!AppUtils.checkPermissionGallery(LoginActivity.this)) {
            requestPermissions(
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    RequestPermissionCodeGallery);
        } else {
            getDatabase();
        }
    }


    private void getDatabase() {
//        login.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
//                login.setVisibility(View.GONE);
                if (!isDataFailed) {
                    progressBar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            protected Void doInBackground(Void... params) {
                populateDatabase();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//                login.setVisibility(View.VISIBLE);
                if (isDataFailed) {
                    runOnUiThread(new Runnable() {
                        public void run() {


                            AlertDialog.Builder builder;

                            builder = new AlertDialog.Builder(LoginActivity.this);

                            builder.setTitle("Error.")
                                    .setMessage(msgError)
                                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                            //populateDatabase();
                                            getDatabase();
                                        }
                                    })
                                    .setCancelable(false)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }
                    });
                } else {
                    progressBar.setVisibility(View.GONE);
                }

            }
        }.execute();
    }

    void testUploads() {
        Request r;
        for (int i = 0; i < 10000; i++) {
            r = new Request.Builder()
                    .post(RequestBody.create(MediaType.parse("text/plain"), "request number: " + i))
                    .url("http://192.168.1.103:8080/hey" + i)
                    .build();
            Uploader.enqueue(r);
            Uploader.sleep(1000);
        }
    }


    <T extends APIResponse> Response<T> getFromApi(final Call<T> call, final int numRetries) {
//        for (int i = 0; i < numRetries; i++) {
        final Response<T> res;
        try {
            res = call.execute();
            String url = res.raw().request().url().toString();
            Log.d(TAG, "Executing query: " + url);

//                String msgError = "";
            if (!res.isSuccessful()) {
                Log.d(TAG, "Unexpected HTTP status code on " + url + ": " + res.code());
                msgError = "Internet Problem.";
                isDataFailed = true;
//                    continue;
            } else if (res.body() == null) {
                Log.d(TAG, "Null body on response to " + url);

                msgError = "Null body on response.";
                isDataFailed = true;
//                    continue;
            } else if (!res.body().getResponseCode().equals(getString(R.string.success))) {
                Log.d(TAG, "Unexpected response message on " + url + ": " +
                        res.body().getResponseCode());
                msgError = "Unexpected response message";
                isDataFailed = true;
            } else {
                return res;
            }

        } catch (IOException e) {
            Log.d(TAG, "Exception on API request: " + e);
            isDataFailed = true;
            msgError = "Internet Problem.";
        }

        return null;
    }

    <T extends APIResponse> Response<T> getFromApi(Call<T> call) {
        return getFromApi(call, 5);
    }

    public boolean isConnectedToInternet() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
//                Toast.makeText(getApplicationContext(), activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
//                Toast.makeText(getApplicationContext(), activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
                return true;
            }
        }

        return false;
    }

    void populateDatabase() {
        isDataFailed = false;
//        Api api = AppUtils.getApiServiceMaster();
//        if (isConnectedToInternet()) {
//        runOnUiThread(new Runnable() {
//            public void run() {
//
//                if (!DBHelper.isTablePopulated(PrefixMaster.TABLE)) {
//                    try {
//                        int insertCount = DBHelper.insertFromFile(getApplicationContext(), R.raw.prefix);
//                        Log.d(TAG, "Rows loaded from file= " + insertCount);
////                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
//                    } catch (IOException e) {
////                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
//                        e.printStackTrace();
//                    }
//
//                    ArrayList<PrefixMaster.PrefixEntity> ss = DBHelper.getPrefixList();
//                    for (int i = 0; i < ss.size(); i++) {
//                        Log.d(TAG,"PREFIX NAME : " + ss.get(i).getPrefix_NameTh());
//                    }
//                }
//            }
//        });

        if (!DBHelper.isTablePopulated(PrefixMaster.TABLE)) {
            try {
                int insertCount = DBHelper.insertFromFile(getApplicationContext(), R.raw.prefix);
                Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            ArrayList<PrefixMaster.PrefixEntity> ss = DBHelper.getPrefixList();
            for (int i = 0; i < ss.size(); i++) {
                Log.d(TAG,"PREFIX NAME : " + ss.get(i).getPrefix_NameTh());
            }
        }

            if (!DBHelper.isTablePopulated(ProvinceMaster.TABLE)) {
//                Response<ProvinceMaster> res = getFromApi(api.getListProvince());
//                if (res == null) {
//                    isDataFailed = true;
//                    return;
//                }
//                DBHelper.addProvinces(res.body().getData());

                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.province);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

//                ArrayList<ProvinceMaster.ProvinceEntity> ss = DBHelper.getProvinceList();
//                for (int i = 0; i < ss.size(); i++) {
//                    Log.d(TAG,"PROVINCE NAME : " + ss.get(i).getProvince_NameTh());
//                }

            }

            if (!DBHelper.isTablePopulated(AmphurMaster.TABLE)) {
//                Response<AmphurMaster> res = getFromApi(api.getListAmphur());
//                if (res == null) {
//                    isDataFailed = true;
//                    return;
//                }
//                DBHelper.addAmphurs(res.body().getData());
//
//                Log.d(TAG, DBHelper.getProvinceList().toString());

                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.amphur);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

//                ArrayList<AmphurMaster.AmphurEntity> ss = DBHelper.getAmphurList();
//                for (int i = 0; i < ss.size(); i++) {
//                    Log.d(TAG,"AMPHUR NAME : " + ss.get(i).getAmphur_nameTh());
//                }

            }
            if (!DBHelper.isTablePopulated(TambolMaster.TABLE)) {
//                Response<TambolMaster> res = getFromApi(api.getListTambol());
//                if (res == null) {
//                    isDataFailed = true;
//                    return;
//                }
//                DBHelper.addTambols(res.body().getData());

                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.tambol);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
            if (!DBHelper.isTablePopulated(VillageMaster.TABLE)) {
//                Response<VillageMaster> res = getFromApi(api.getListVillage());
//                if (res == null) {
//                    isDataFailed = true;
//                    return;
//                }
//                DBHelper.addVillages(res.body().getData());
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.village);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(FarmOwnerMaster.TABLE)) {
//                Response<FarmOwnerMaster> res = getFromApi(api.getFarmOwner());
//                if (res == null) {
//                    isDataFailed = true;
//                    return;
//                }
//                DBHelper.addFarmOwner(res.body().getData());

                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.farmowner);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
            if (!DBHelper.isTablePopulated(FarmProblemMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.farmproblem);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(FarmerProblemMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.farmerproblem);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(FarmerTypeMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.farmertype);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(FeedKindMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.feedkind);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(FeedSubTypeFormatMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.feedsubtypeformat);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(FeedSubTypeMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.feedsubtype);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(FeedTypeMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.feedtype);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(HelpMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.governhelp);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(HoldingLandMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.holdingland);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(MainJobMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.mainjob);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(SecondJobMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.secondjob);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(StandardMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.standard);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(SupportStandardMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.supportstandard);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(LivestockMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.livestocktype);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(CompanyTypeMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.companytype);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            if (!DBHelper.isTablePopulated(LivestockTypeMaster.TABLE)) {
                try {
                    int insertCount = DBHelper.insertFromFile(this, R.raw.farmstandardlivestocktype);
                    Log.d(TAG, "Rows loaded from file= " + insertCount);
//                    Toast.makeText(this, "Rows loaded from file= " + insertCount, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
//                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
//        } else {
//
//            runOnUiThread(new Runnable() {
//                public void run() {
//
//                    AlertDialog.Builder builder;
//
//                    builder = new AlertDialog.Builder(LoginActivity.this);
//
//                    builder.setTitle("Internet Problem.")
//                            .setMessage("Please connect to the internet.")
//                            .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//                                    // continue with delete
////                                    populateDatabase();
//                                    getDatabase();
//                                }
//                            })
//                            .setCancelable(false)
//                            .setIcon(android.R.drawable.ic_dialog_alert)
//                            .show();
//                }
//            });
//        }
    }

    @OnClick(R.id.btnLogOnline)
    public void clickBtnLogin() {
        if (txtUsername.getText().length() != 0 && txtPassword.getText().length() != 0) {
            login(txtUsername.getText().toString(), txtPassword.getText().toString());
        } else {
            Toast.makeText(LoginActivity.this, getString(R.string.field_req), Toast.LENGTH_LONG).show();
        }
        MyPreferenceManager myPreferenceManager = new MyPreferenceManager(LoginActivity.this);
        myPreferenceManager.setOnline(true);
    }

    @OnClick(R.id.btnRegist)
    public void clickBtnRegist() {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        intent.putExtra(AppUtils.TYPE, loginType);
        startActivity(intent);

    }

    @OnClick(R.id.btnLogOffline)
    public void clickBtnOfflineMode() {
        Intent intent = new Intent(LoginActivity.this, OfflineActivity.class);
        startActivity(intent);

        MyPreferenceManager myPreferenceManager = new MyPreferenceManager(LoginActivity.this);
        myPreferenceManager.setOnline(false);
    }

    @OnClick(R.id.btn_forgot)
    public void clickBtnForgot() {
        Intent intent = new Intent(LoginActivity.this, ForgotPassActivity.class);
        startActivity(intent);
    }



    private void setUserDataPreference() {
        MyPreferenceManager myPreferenceManager = new MyPreferenceManager(LoginActivity.this);
        myPreferenceManager.setUserId(userId);
        myPreferenceManager.setUserType(userTypeId);
        if(pid!=null&&!pid.equals("")){
            myPreferenceManager.setUserPid(Long.parseLong(pid));
        }else {
            myPreferenceManager.setUserPid(-1);
        }
    }


    private void login(String username, String password) {
        progressBar2.setVisibility(View.VISIBLE);
        Api api = AppUtils.getApiService();
        Call<LoginModel> call = api.login(username, password);
        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                progressBar2.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getResponseCode().equals("200")) {
                        userId = response.body().getData().getUserId();
                        userTypeId = response.body().getData().getUserTypeId();
                        pid = response.body().getData().getPid();

                        databaseRealm = new DatabaseRealm(LoginActivity.this);
                        realm = Realm.getDefaultInstance();
                        RealmResults<Farmer> dataFarmer = databaseRealm.getListDataFarmer(false);
                        if (dataFarmer.size() > 0 && loginType == 1) { //staff
                            checkOfflineData(response.body().getData());
                        } else {
                            if (response.body().getData().getUserTypeId() == 1) {
                                setUserDataPreference();//Staff
                                Intent intent = new Intent(LoginActivity.this, FindFarmerActivity.class);
                                intent.putExtra(AppUtils.LOGINTYPE,loginType );
                                startActivity(intent);
                                //finish();
                            } else { // 2 = farmer use search usertype=1 > search for farmer
                                searchModel = new SearchModel();
                                searchModel.setPid(Long.parseLong(response.body().getData().getPid()));
                                getListFarmer(null, null, null, null
                                        , null, null, null
                                        , 1, null, null, null
                                        ,response.body().getData().getPid(),null,null);
                            }
                        }


                    } else {
                        Toast.makeText(LoginActivity.this, response.body().getResponseMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        Toast.makeText(LoginActivity.this, response.errorBody().string(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                progressBar2.setVisibility(View.GONE);
                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });

    }

    private void getListFarmer(Integer iDisplayStart, Integer iDisplayLength, String sSearch, Integer ProvinceId
            , Integer AmphurId, Integer TambolId, Integer VillageId, int userType, String latitude
            , String longitude, String distanceKilometer,String Pid,String FirstName,String LastName) {
        progressBar2.setVisibility(View.VISIBLE);
        Api api = AppUtils.getApiService();
        Call<FarmerSearchList> call = api.getListFarmer(iDisplayStart, iDisplayLength, sSearch, ProvinceId
                , AmphurId, TambolId, VillageId, userType, latitude, longitude, distanceKilometer,Pid,FirstName,LastName);
        call.enqueue(new Callback<FarmerSearchList>() {
            @Override
            public void onResponse(Call<FarmerSearchList> call, Response<FarmerSearchList> response) {
                progressBar2.setVisibility(View.GONE);
                if (response.body() != null) {
//                    if (response.body().getResponseCode().equals(getString(R.string.success))) {
                    if (response.body().getAaData() != null && response.body().getAaData().size() != 0) {

                        if (response.body().getAaData().get(0).getdTRowData().getFarmId() != null) {
                            getFarmerByPID(response.body().getAaData().get(0).getdTRowData().getPid(),
                                    response.body().getAaData().get(0).getdTRowData().getFarmId());
                        } else {
                            getFarmerByPID(response.body().getAaData().get(0).getdTRowData().getPid(), 0);
                        }
//                        Intent intent = new Intent(LoginActivity.this, FarmerListActivity.class);
//                        intent.putExtra("farmersList", Parcels.wrap(response.body().getAaData()));
//                        intent.putExtra("searchModel", Parcels.wrap(searchModel));
//                        intent.putExtra("mode", 3); //case pid
//                        intent.putExtra("userType", 1); //case farmer
//                        startActivity(intent);
//                        finish();
                    } else {
                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.notfound), Toast.LENGTH_SHORT).show();
                    }
//                    } else {
//                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
//                    }
                } else {
                    Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<FarmerSearchList> call, Throwable t) {

                progressBar2.setVisibility(View.GONE);
                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getFarmerByPID(long idcard, final int farmId) {
        progressBar2.setVisibility(View.VISIBLE);
        Api api = AppUtils.getApiService();
        Call<ResponseFarmer> call = api.getFarmerByPID(idcard);
        call.enqueue(new Callback<ResponseFarmer>() {
            @Override
            public void onResponse(Call<ResponseFarmer> call, Response<ResponseFarmer> response) {
                progressBar2.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getResponseCode().equals(getString(R.string.success))) {

                        setUserDataPreference();

                        databaseRealm.deleteDataFarmerOnline();
                        Farmer data = response.body().getData();
//                        AppUtils.addDefaultToNull(data);
                        data.setOnline(true);
                        String uuid = databaseRealm.addDataFarmerUUID(data);

                        Intent intent = new Intent(LoginActivity.this, FarmActivity.class); //mode DashBoard > list farm
                        intent.putExtra(UUID, uuid);
                        intent.putExtra("mode", "dashBoard");
                        intent.putExtra("farmId", farmId);
                        startActivity(intent);
//                        finish();
                    } else {
                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseFarmer> call, Throwable t) {
                progressBar2.setVisibility(View.GONE);
                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCodeGallery:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    if (StoragePermission) {
                        getDatabase();
                    } else {
                        getPermission();
                        Toast.makeText(LoginActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;

        }
    }

    //    public void loadFarm() {
//        Api api = AppUtils.getApiService();
//        Call<FarmModel> call = api.getListFarm();
//        call.enqueue(new Callback<FarmModel>() {
//            @Override
//            public void onResponse(Call<FarmModel> call, Response<FarmModel> response) {
//                if (response.body() != null && response.body().getResponseMessage().equals(getString(R.string.success))) {
//                    Log.d("MainActivity", "Adding Farm to db....");
//                    final Response<FarmModel> resp = response;
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            DBHelper.addFarm(resp.body().getData());
//                            Log.d("MainActivity", "Added Farm to the db.");
//                        }
//                    }).start();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<FarmModel> call, Throwable t) {
//
//            }
//        });
//
//    }

//    public void loadListFarm() {
//        Api api = AppUtils.getApiService();
//        Call<FarmModel> call = api.getListFarm();
//        call.enqueue(new Callback<FarmModel>() {
//            @Override
//            public void onResponse(Call<FarmModel> call, Response<FarmModel> response) {
//                if (response.body() != null && response.body().getResponseMessage().equals(getString(R.string.success))) {
//                    Log.d("MainActivity", "Adding Farm to db....");
//                    final Response<FarmModel> resp = response;
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            DBHelper.addFarm(resp.body().getData());
//                            Log.d("MainActivity", "Added Farm to the db.");
//                        }
//                    }).start();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<FarmModel> call, Throwable t) {
//
//            }
//        });
//
//    }


    // TODO: SELECT FARM_ID TO FIND FEED
//    public void loadFeed() {
//        Api api = AppUtils.getApiService();
//        Call<FeedModel> call = api.fetchFeeds();
//        call.enqueue(new Callback<FeedModel>() {
//            @Override
//            public void onResponse(Call<FeedModel> call, Response<FeedModel> response) {
//                if (response.body() != null && response.body().getResponseMessage().equals(getString(R.string.success))) {
//                    Log.d("MainActivity", "Adding feed to db....");
//                    final Response<FeedModel> resp = response;
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            DBHelper.addFeeds(resp.body().getData());
//                            Log.d("MainActivity", "Added feed to the db.");
//                        }
//                    }).start();
//                }
//            }
//            @Override
//            public void onFailure(Call<FeedModel> call, Throwable t) {
//
//            }
//        });
//    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, SelectTypeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }
}
