package com.zealtech.dld.dldregist.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.zealtech.dld.dldregist.MainApplication;
import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.fragment.HistoryFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class HistoryActivity extends AppCompatActivity {

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabs;
    Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);

        MainApplication application = (MainApplication) getApplication();
        mTracker = application.getDefaultTracker();

        tabs.setupWithViewPager(viewPager);
        tabs.setBackgroundColor(getResources().getColor(R.color.slidingTab));

        tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        initViewpager();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("หน้าประวัติ");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void initViewpager(){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(HistoryFragment.newInstance(1), getResources().getString(R.string.his_farmer));
        adapter.addFragment(HistoryFragment.newInstance(2), getResources().getString(R.string.his_farm));
        adapter.addFragment(HistoryFragment.newInstance(3), getResources().getString(R.string.his_live));
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(1);
    }
    @Optional
    @OnClick(R.id.btn_back)
    public void clickBtnBack() {
        finish();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
