package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/28/2017 AD.
 */

@Parcel
public class FarmProblemMaster implements APIResponse{
    //DBHelper
    public static final String TABLE = "farm_problem";

    String responseCode;
    String responseMessage;
    ArrayList<FarmProblemEntity> data;

    public FarmProblemMaster(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<FarmProblemEntity> getData() {
        return data;
    }

    public void setData(ArrayList<FarmProblemEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class FarmProblemEntity{
        int farm_Problem_ID;
        String farm_Problem_Name;
        int orderIndex;
        int status_ID;

        public FarmProblemEntity(){ }

        public int getFarm_Problem_ID() {
            return farm_Problem_ID;
        }

        public void setFarm_Problem_ID(int farm_Problem_ID) {
            this.farm_Problem_ID = farm_Problem_ID;
        }

        public String getFarm_Problem_Name() {
            return farm_Problem_Name;
        }

        public void setFarm_Problem_Name(String farm_Problem_Name) {
            this.farm_Problem_Name = farm_Problem_Name;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String FARM_PROBLEM_ID = "farm_Problem_ID";
        public static final String FARM_PROBLEM_NAME = "farm_Problem_Name";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
    }
}
