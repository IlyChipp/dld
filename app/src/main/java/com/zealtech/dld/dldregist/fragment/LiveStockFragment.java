package com.zealtech.dld.dldregist.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.FarmActivity;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.model.LivestockMaster.LivestockEntity;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.model.newFarmer.Livestock;
import com.zealtech.dld.dldregist.model.newFarmer.ResponseLivestock;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.util.ScreenUtils;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;
import com.zealtech.dld.dldregist.widget.MyNoHLTextView;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LiveStockFragment extends BaseBusFragment {

    final public static String LIVESTOCK = "livestock";

    @BindView(R.id.tab)
    LinearLayout tab;
    @BindView(R.id.scr_h)
    HorizontalScrollView scr_h;
    @BindView(R.id.progressBar)
    FrameLayoutDisable progressBar;
    @BindView(R.id.plsAddFarm)
    MyNoHBTextView plsAddFarm;
    @BindView(R.id.contentContainerListAnimal)
    FrameLayout contentContainerListAnimal;


    @BindView(R.id.spn_farm)
    Spinner spn_farm;

    private RecyclerView recyclerListType;
    private FrameLayout contentContainer;

    static final int BTN_MEAT = 0;
    static final int BTN_COW = 1;
    static final int BTN_BUF = 2;
    static final int BTN_PIG = 3;
    static final int BTN_CHICKEN = 4;
    static final int BTN_DUCK = 5;
    static final int BTN_GOAT = 6;
    static final int BTN_ETC = 7;
    private int type;

    private final ArrayList<LivestockEntity> livestockMasters=new ArrayList<>();
    private ArrayList<LivestockEntity> parentTabs=new ArrayList<>();
    private ArrayList<MyNoHLTextView> titles=new ArrayList<>();

    ArrayList<Farm> farms=new ArrayList<>();
    ArrayList<String> farmName=new ArrayList<>();

//    int current_farm_pos=0;
    int current_tab_pos=0;

    ListLiveStockMoreFragment listLiveStockMoreFragment;
    ListLiveStockFragment listLiveStockFragment;

    public LiveStockFragment() {
        super();
    }

    public static LiveStockFragment newInstance() {
        LiveStockFragment fragment = new LiveStockFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_live_stock, container, false);
        ButterKnife.bind(this, rootView);
        initInstances(rootView);

//        livestockMasters.addAll(((FarmActivity)getActivity()).getLivestockMasters());
////        if (livestockMasters.size()>0) {
////            livestockMasters.remove(0);
////        }
//
//        getAllParent();
//
//        if (livestockMasters.size()>0) {
//            initTab();
//            getListLivestock(parentTabs.get(current_tab_pos).getLivestock_Type_ID(),((FarmActivity)getActivity()).getCurrent_FarmID());
//        }

        setUpData();

        return rootView;
    }

    public void setUpData() {

        farms.clear();
        livestockMasters.clear();
        parentTabs.clear();
        titles.clear();

        farms.addAll(((FarmActivity)(getActivity())).getAllFarm());
//
        if(farms.size()!=0){
            contentContainerListAnimal.setVisibility(View.VISIBLE);
            tab.setVisibility(View.VISIBLE);
            plsAddFarm.setVisibility(View.GONE);

            livestockMasters.addAll(((FarmActivity)getActivity()).getLivestockMasters());
//        if (livestockMasters.size()>0) {
//            livestockMasters.remove(0);
//        }

            getAllParent();

            if (livestockMasters.size()>0) {

                if(AppUtils.isOnline(getActivity())) {
                    getLiveStockInFarm(((FarmActivity)getActivity()).getCurrent_FarmID());
                }else{
//                    livestockses.addAll(((FarmActivity) getActivity()).getFarmById(farmID).getLivestock());
                    initTab();
                }
                getListLivestock(parentTabs.get(current_tab_pos).getLivestock_Type_ID(),((FarmActivity)getActivity()).getCurrent_FarmID());
            }
        }else{
            contentContainerListAnimal.setVisibility(View.GONE);
            tab.setVisibility(View.GONE);
            plsAddFarm.setVisibility(View.VISIBLE);
        }
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
//        imgMeat = (ImageView) rootView.findViewById(R.id.imgMeat);
//        txtMeat = (MyNoHLTextView) rootView.findViewById(R.id.txtMeat);
//        imgCow = (ImageView) rootView.findViewById(R.id.imgCow);
//        txtCow = (MyNoHLTextView) rootView.findViewById(R.id.txtCow);
//        imgBuf = (ImageView) rootView.findViewById(R.id.imgBuf);
//        txtBuf = (MyNoHLTextView) rootView.findViewById(R.id.txtBuf);
//        imgPig = (ImageView) rootView.findViewById(R.id.imgPig);
//        txtPig = (MyNoHLTextView) rootView.findViewById(R.id.txtPig);
//        imgChicken = (ImageView) rootView.findViewById(R.id.imgChicken);
//        txtChicken = (MyNoHLTextView) rootView.findViewById(R.id.txtChicken);
//        imgDuck = (ImageView) rootView.findViewById(R.id.imgDuck);
//        txtDuck = (MyNoHLTextView) rootView.findViewById(R.id.txtDuck);
//        imgGoat = (ImageView) rootView.findViewById(R.id.imgGoat);
//        txtGoat = (MyNoHLTextView) rootView.findViewById(R.id.txtGoat);
//        imgEtc = (ImageView) rootView.findViewById(R.id.imgEtc);
//        txtEtc = (MyNoHLTextView) rootView.findViewById(R.id.txtEtc);
    }

//    private void initSpinnerFarm(){
//        for(int i=0;i<farms.size();i++){
//            farmName.add(farms.get(i).getFarmName());
//        }
//
//        MySpinnerAdapter adapter = new MySpinnerAdapter(
//                getContext(),
//                android.R.layout.simple_dropdown_item_1line,
//                farmName);
//
//        spn_farm.setAdapter(adapter);
//
//        spn_farm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                current_farm_pos=position;
//                if (livestockMasters.size()>0) {
//                    initTab();
//                    getListLivestock(parentTabs.get(current_tab_pos).getLivestock_Type_ID(),current_farm_pos);
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//        spn_farm.setSelection(0);
//    }

    private void getLiveStock(int farmId) {
        progressBar.setVisibility(View.VISIBLE);

        Api api = AppUtils.getApiService();
        Call<ResponseLivestock> call = api.getLivestock(Integer.toString(farmId));
        call.enqueue(new Callback<ResponseLivestock>() {
            @Override
            public void onResponse(Call<ResponseLivestock> call, Response<ResponseLivestock> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {


                } else {
                    try {
                        Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseLivestock> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }

        });
    }

    public void getListLivestock(int rootId,int farmID) {

        ArrayList<LivestockEntity> livestockEntities=getAllRootChild(rootId);

        FragmentManager fragmentManager = getChildFragmentManager();

        String tag=Integer.toString(rootId);
//        int farmID=farms.get(pos).getFarmID();

//        if (rootId==67) {
////            ListLiveStockMoreFragment currentFragment = (ListLiveStockMoreFragment) fragmentManager.findFragmentByTag(tag);
////
////            if (currentFragment != null) {
////                fragmentManager.beginTransaction()
////                        .replace(R.id.contentContainerListAnimal, currentFragment, tag)
////                        .commit();
////            } else {
//            if  (listLiveStockMoreFragment == null) {
//                listLiveStockMoreFragment = ListLiveStockMoreFragment
//                        .newInstance(rootId,livestockEntities,farmID);
//                fragmentManager.beginTransaction()
//                        .replace(R.id.contentContainerListAnimal, listLiveStockMoreFragment, tag)
//                        .commit();
//            } else {
//                listLiveStockFragment.livestockEntities = null;
//                listLiveStockFragment.setLivestockEntities(livestockEntities);
//                listLiveStockFragment.farmID = farmID;
//                listLiveStockMoreFragment.setUpData();
//            }
//
//        } else {
////            ListLiveStockFragment currentFragment = (ListLiveStockFragment) fragmentManager.findFragmentByTag(tag);
////
////            if (currentFragment != null) {
////                fragmentManager.beginTransaction()
////                        .replace(R.id.contentContainerListAnimal, currentFragment,tag)
////                        .commit();
////            } else {
//            if (listLiveStockFragment == null) {
//                listLiveStockFragment = ListLiveStockFragment
//                        .newInstance(rootId,livestockEntities,farmID);
//                fragmentManager.beginTransaction()
//                        .replace(R.id.contentContainerListAnimal, listLiveStockFragment,tag)
//                        .commit();
//            } else {
//                listLiveStockFragment.livestockEntities = null;
//                listLiveStockFragment.setLivestockEntities(livestockEntities);
//                listLiveStockFragment.farmID = farmID;
//                listLiveStockFragment.setUpData();
//            }
//
//        }

        if (listLiveStockFragment == null) {
            listLiveStockFragment = ListLiveStockFragment
                    .newInstance(rootId,livestockEntities,farmID);
            fragmentManager.beginTransaction()
                    .replace(R.id.contentContainerListAnimal, listLiveStockFragment,tag)
                    .commit();
        } else {
            listLiveStockFragment.livestockEntities = null;
            listLiveStockFragment.setLivestockEntities(livestockEntities);
            listLiveStockFragment.farmID = farmID;
            listLiveStockFragment.setUpData();
        }

    }

    private LivestockEntity getMasterData(int type_id){
        for(int i=0;i<livestockMasters.size();i++){
            if(livestockMasters.get(i).getLivestock_Type_ID()==type_id) {
                return livestockMasters.get(i);
            }
        }
        return null;
    }

    private ArrayList<LivestockEntity> getAllRootChild(int root_type) {

        ArrayList<LivestockEntity> livestocks = new ArrayList<>();

        for (int i = 0; i < livestockMasters.size(); i++) {
            int root_id = getRootParentId(livestockMasters.get(i).getLivestock_Type_ID());
            if (root_id == root_type && livestockMasters.get(i).getLivestock_Type_Parent() != -1) { //remove parent
                LivestockEntity livestockEntity=new LivestockEntity(livestockMasters.get(i));
                livestocks.add(livestockEntity);
            }
        }

        return livestocks;
    }


    private void getAllParent() {
        for (int i = 0; i < livestockMasters.size(); i++) {
            if (livestockMasters.get(i).getLivestock_Type_Parent() == -1) {
                parentTabs.add(livestockMasters.get(i));
            }
        }
    }

    private void initTab() {
        tab.removeAllViews();
        current_tab_pos=0;

        for(int i=0;i<parentTabs.size();i++){
            View view = getLayoutInflater().inflate(R.layout.item_tab_livestock, null);
            MyNoHLTextView txt_title = (MyNoHLTextView) view.findViewById(R.id.txt_title);
            final LinearLayout btn_tab = (LinearLayout) view.findViewById(R.id.btn_tab);
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);

            final int i_=i;
            final ScreenUtils screenUtils=new ScreenUtils(getActivity());

            titles.add(txt_title);

            btn_tab.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    int scrollX = (btn_tab.getLeft() - (screenUtils.getScreenWidth()/ 2)) + (btn_tab.getWidth() / 2);
                    scr_h.smoothScrollTo(scrollX, 0);
                    current_tab_pos=i_;
                    //update livestock inside array farmer before change livestock
                    getBus().post(new CallUpdateLivestock(current_tab_pos));
                    //change livestock
                    getListLivestock(parentTabs.get(current_tab_pos).getLivestock_Type_ID(),((FarmActivity)getActivity()).getCurrent_FarmID());
                    setTextColor(i_);
                }
            });

            txt_title.setText(parentTabs.get(i).getLivestock_Type_Name());
            setIcon(imageView,parentTabs.get(i).getLivestock_Type_ID());
            tab.addView(view);
        }
        setTextColor(0);
        if (parentTabs.size()>0) {
            getListLivestock(parentTabs.get(current_tab_pos).getLivestock_Type_ID(),((FarmActivity)getActivity()).getCurrent_FarmID());
        }
    }

    private void updateTabColor(){

    }

    public class CallUpdateLivestock {
        int pos;

        public CallUpdateLivestock(int pos) {
            this.pos = pos;
        }

        public int getName() {
            return pos;
        }

        public void setName(int pos) {
            this.pos = pos;
        }
    }


    private void getLiveStockInFarm(final int farmId) {

        progressBar.setVisibility(View.VISIBLE);

        Api api = AppUtils.getApiService();
        Call<ResponseLivestock> call = api.getLivestock(Integer.toString(farmId));
        call.enqueue(new Callback<ResponseLivestock>() {
            @Override
            public void onResponse(Call<ResponseLivestock> call, Response<ResponseLivestock> response) {
                progressBar.setVisibility(View.GONE);
                if(isAdded()) {
                    if (response.body() != null) {
                        Farm farm_=((FarmActivity)getActivity()).getFarmById(farmId);

                        if(farm_.getLivestock()==null||farm_.getLivestock().size()==0){ //get from api when first time (first time livestock is null)
                            RealmList<Livestock> livestocks=new RealmList<Livestock>();
                            livestocks.addAll(response.body().getData());
                            farm_.setLivestock(livestocks);
                        }
                        initTab();
                    } else {
                        try {
                            Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseLivestock> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }

        });
    }

    private boolean haveLivestock(int typeID){

        int farmID=((FarmActivity)getActivity()).getCurrent_FarmID();

        Farm farm=((FarmActivity)getActivity()).getFarmById(farmID);

        ArrayList<LivestockEntity> childs = new ArrayList<>();

        childs=getAllRootChild(typeID);

        for(int i=0;i<farm.getLivestock().size();i++){
            for(int j=0;j<childs.size();j++) {
                if (childs.get(j).getLivestock_Type_ID()==(farm.getLivestock().get(i).getLivestockTypeID())) {
                    return true;
                }
            }
        }

        return false;
    }

    private void setIcon(ImageView imageView,int type_id){
        if(type_id==2){
            imageView.setImageResource(R.drawable.meat);
        }else if(type_id==19){
            imageView.setImageResource(R.drawable.cow_milk);
        }else if(type_id==27){
            imageView.setImageResource(R.drawable.buf);
        }else if(type_id==38){
            imageView.setImageResource(R.drawable.pig);
        }else if(type_id==45){
            imageView.setImageResource(R.drawable.chicken);
        }else if(type_id==51){
            imageView.setImageResource(R.drawable.duck);
        }else if(type_id==57){
            imageView.setImageResource(R.drawable.goat);
        }else if(type_id==67){
            imageView.setImageResource(R.drawable.group);
        }else{
            imageView.setImageResource(R.drawable.group);
        }
    }

    private void setTextColor(int position){
        for(int i=0;i<titles.size();i++){
            if(i!=position){
                if(haveLivestock(parentTabs.get(i).getLivestock_Type_ID())){
                    titles.get(i).setTextColor(ContextCompat.getColor(getContext(), R.color.txt_green));
                }else {
                    titles.get(i).setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
                }
            }else{
                titles.get(i).setTextColor(ContextCompat.getColor(getContext(), R.color.txt_blue2));
            }
        }
    }

    private int getRootParentId(int type_id){
        int id=0;
        for(int i=0;i<livestockMasters.size();i++){
            if(livestockMasters.get(i).getLivestock_Type_ID()==type_id){
                if(livestockMasters.get(i).getLivestock_Type_Parent()==-1) {
                    return livestockMasters.get(i).getLivestock_Type_ID();
                }else{
                    id= getRootParentId(livestockMasters.get(i).getLivestock_Type_Parent());
                }
            }
        }
        return id;
    }

//    @Subscribe
//    public void recieveUpdateTabColor(CallUpdateLivestock even) {
//        updateTabColor();
//    }

//    private ArrayList<Livestocks> filterGroup(int root_type){
//
//        ArrayList<Livestocks> livestocks=new ArrayList<>();
//
//        for(int i=0;i<livestockses.size();i++){
//            if(getRootParentId(livestockses.get(i).getLivestock_Type_ID())==root_type){
//                livestocks.add(livestockses.get(i));
//            }
//        }
//
//        return livestocks;
//    }

    private int checkLevel(int type_id,int level){
        for(int i=0;i<livestockMasters.size();i++){
            if(livestockMasters.get(i).getLivestock_Type_ID()==type_id){
                if(livestockMasters.get(i).getLivestock_Type_Parent()==-1){
                    return level;
                }else {
                    level+=1;
                    checkLevel(livestockMasters.get(i).getLivestock_Type_Parent(),level);
                }
            }
        }
        return level;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /*
         * Save Instance State Here
         */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }


}
