package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by fanqfang on 9/28/2017 AD.
 */

@Parcel
public class FeedModel implements APIResponse{
    //DBHelper
    public static final String TABLE = "feed";

    String responseCode;
    String responseMessage;
    ArrayList<FeedEntity> data;

    public FeedModel() { }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<FeedEntity> getData() {
        return data;
    }

    public void setData(ArrayList<FeedEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class FeedEntity{
        String createBy;
        String createDate;
        int farm_ID;
        BigDecimal feed_Area_Ngan;
        BigDecimal feed_Area_Rai;
        BigDecimal feed_Area_Wa;
        int feed_ID;
        int feed_Kind_ID;
        String feed_Kind_Name;
        String feed_Kind_Other;
        int feed_SubType_Format_ID;
        int feed_SubType_ID;
        String feed_SubType_Name;
        int feed_Type_ID;
        String feed_Type_Name;
        String feed_Type_Other;
        int feed_Water_Type_ID;
        String feed_Water_Type_Name;
        String MS_Feed_SubType_Format_Name;
        int orderIndex;
        int status_ID;
        String status_Name;
        String updateBy;
        String updateDate;
        boolean localOnly;

        public boolean isLocalOnly() {
            return localOnly;
        }

        public void setLocalOnly(boolean localOnly) {
            this.localOnly = localOnly;
        }

        public FeedEntity(){ }

        public String getCreateBy() {
            return createBy;
        }

        public void setCreateBy(String createBy) {
            this.createBy = createBy;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public int getFarm_ID() {
            return farm_ID;
        }

        public void setFarm_ID(int farm_ID) {
            this.farm_ID = farm_ID;
        }

        public BigDecimal getFeed_Area_Ngan() {
            return feed_Area_Ngan;
        }

        public void setFeed_Area_Ngan(BigDecimal feed_Area_Ngan) {
            this.feed_Area_Ngan = feed_Area_Ngan;
        }

        public BigDecimal getFeed_Area_Rai() {
            return feed_Area_Rai;
        }

        public void setFeed_Area_Rai(BigDecimal feed_Area_Rai) {
            this.feed_Area_Rai = feed_Area_Rai;
        }

        public BigDecimal getFeed_Area_Wa() {
            return feed_Area_Wa;
        }

        public void setFeed_Area_Wa(BigDecimal feed_Area_Wa) {
            this.feed_Area_Wa = feed_Area_Wa;
        }

        public int getFeed_ID() {
            return feed_ID;
        }

        public void setFeed_ID(int feed_ID) {
            this.feed_ID = feed_ID;
        }

        public int getFeed_Kind_ID() {
            return feed_Kind_ID;
        }

        public void setFeed_Kind_ID(int feed_Kind_ID) {
            this.feed_Kind_ID = feed_Kind_ID;
        }

        public String getFeed_Kind_Name() {
            return feed_Kind_Name;
        }

        public void setFeed_Kind_Name(String feed_Kind_Name) {
            this.feed_Kind_Name = feed_Kind_Name;
        }

        public String getFeed_Kind_Other() {
            return feed_Kind_Other;
        }

        public void setFeed_Kind_Other(String feed_Kind_Other) {
            this.feed_Kind_Other = feed_Kind_Other;
        }

        public int getFeed_SubType_Format_ID() {
            return feed_SubType_Format_ID;
        }

        public void setFeed_SubType_Format_ID(int feed_SubType_Format_ID) {
            this.feed_SubType_Format_ID = feed_SubType_Format_ID;
        }

        public int getFeed_SubType_ID() {
            return feed_SubType_ID;
        }

        public void setFeed_SubType_ID(int feed_SubType_ID) {
            this.feed_SubType_ID = feed_SubType_ID;
        }

        public String getFeed_SubType_Name() {
            return feed_SubType_Name;
        }

        public void setFeed_SubType_Name(String feed_SubType_Name) {
            this.feed_SubType_Name = feed_SubType_Name;
        }

        public int getFeed_Type_ID() {
            return feed_Type_ID;
        }

        public void setFeed_Type_ID(int feed_Type_ID) {
            this.feed_Type_ID = feed_Type_ID;
        }

        public String getFeed_Type_Name() {
            return feed_Type_Name;
        }

        public void setFeed_Type_Name(String feed_Type_Name) {
            this.feed_Type_Name = feed_Type_Name;
        }

        public String getFeed_Type_Other() {
            return feed_Type_Other;
        }

        public void setFeed_Type_Other(String feed_Type_Other) {
            this.feed_Type_Other = feed_Type_Other;
        }

        public int getFeed_Water_Type_ID() {
            return feed_Water_Type_ID;
        }

        public void setFeed_Water_Type_ID(int feed_Water_Type_ID) {
            this.feed_Water_Type_ID = feed_Water_Type_ID;
        }

        public String getFeed_Water_Type_Name() {
            return feed_Water_Type_Name;
        }

        public void setFeed_Water_Type_Name(String feed_Water_Type_Name) {
            this.feed_Water_Type_Name = feed_Water_Type_Name;
        }

        public String getMS_Feed_SubType_Format_Name() {
            return MS_Feed_SubType_Format_Name;
        }

        public void setMS_Feed_SubType_Format_Name(String MS_Feed_SubType_Format_Name) {
            this.MS_Feed_SubType_Format_Name = MS_Feed_SubType_Format_Name;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        public String getStatus_Name() {
            return status_Name;
        }

        public void setStatus_Name(String status_Name) {
            this.status_Name = status_Name;
        }

        public String getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(String updateBy) {
            this.updateBy = updateBy;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String CREATE_BY = "createBy";
        public static final String CREATE_DATE = "createDate";
        public static final String FARM_ID = "farm_ID";
        public static final String FEED_AREA_NGAN = "feed_Area_Ngan";
        public static final String FEED_AREA_RAI = "feed_Area_Rai";
        public static final String FEED_AREA_WA = "feed_Area_Wa";
        public static final String FEED_ID = "feed_ID";
        public static final String FEED_KIND_ID = "feed_Kind_ID";
        public static final String FEED_KIND_NAME = "feed_Kind_Name";
        public static final String FEED_KIND_OTHER = "feed_Kind_Other";
        public static final String FEED_SUBTYPE_FORMAT_ID = "feed_SubType_Format_ID";
        public static final String FEED_SUBTYPE_ID = "feed_SubType_ID";
        public static final String FEED_SUBTYPE_NAME = "feed_SubType_Name";
        public static final String FEED_TYPE_ID = "feed_Type_ID";
        public static final String FEED_TYPE_NAME = "feed_Type_Name";
        public static final String FEED_TYPE_OTHER = "feed_Type_Other";
        public static final String FEED_WATER_TYPE_ID = "feed_Water_Type_ID";
        public static final String FEED_WATER_TYPE_NAME = "feed_Water_Type_Name";
        public static final String MS_FEED_SUBTYPE_FORMAT_NAME = "MS_Feed_SubType_Format_Name";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
        public static final String STATUS_NAME = "status_Name";
        public static final String UPDATE_BY = "updateBy";
        public static final String UPDATE_DATE = "updateDate";
        public static final String LOCAL_ONLY = "localOnly";
    }
}
