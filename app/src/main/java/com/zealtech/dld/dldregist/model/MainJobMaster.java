package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/27/2017 AD.
 */
@Parcel
public class MainJobMaster implements APIResponse{
    //DBHelper
    public static final String TABLE = "main_job";

    String responseCode;
    String responseMessage;
    ArrayList<JobEntity> data;

    public MainJobMaster(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<JobEntity> getData() {
        return data;
    }

    public void setData(ArrayList<JobEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class JobEntity {
        int main_Job_ID;
        String main_Job_Name;
        int orderIndex;
        int status_ID;

        public JobEntity() { }

        public int getMain_Job_ID() {
            return main_Job_ID;
        }

        public void setMain_Job_ID(int main_Job_ID) {
            this.main_Job_ID = main_Job_ID;
        }

        public String getMain_Job_Name() {
            return main_Job_Name;
        }

        public void setMain_Job_Name(String main_Job_Name) {
            this.main_Job_Name = main_Job_Name;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String MAIN_JOB_ID = "main_Job_ID";
        public static final String MAIN_JOB_NAME = "main_Job_Name";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
    }
}
