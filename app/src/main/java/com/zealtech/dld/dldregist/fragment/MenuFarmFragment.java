//package com.android.fanqfang.dldregist.fragment;
//
//import android.os.Bundle;
//import android.supports.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//
//
//
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//
//public class MenuFarmFragment extends Fragment {
//
//
//    private LinearLayout llFarmData;
//    private LinearLayout llFeed;
//    private LinearLayout llHusbandry;
//
//    public MenuFarmFragment() {
//        super();
//    }
//
//    public static MenuFarmFragment newInstance() {
//        MenuFarmFragment fragment = new MenuFarmFragment();
//        Bundle args = new Bundle();
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View rootView = inflater.inflate(R.layout.fragment_menu_farm, container, false);
//        ButterKnife.bind(this, rootView);
//        initInstances(rootView);
//        return rootView;
//    }
//
//    private void initInstances(View rootView) {
//        // Init 'View' instance(s) with rootView.findViewById here
//
//        llFarmData = (LinearLayout) rootView.findViewById(R.id.llFarmData);
//        llFeed = (LinearLayout) rootView.findViewById(R.id.llFeed);
//        llHusbandry = (LinearLayout) rootView.findViewById(R.id.llHusbandry);
//    }
//
//
//    @OnClick(R.id.llFarmData)
//    public void gotoFarmData(){
//        FarmFragment farmFragment = new FarmFragment();
//        getActivity().getSupportFragmentManager().beginTransaction()
//                .replace(R.id.contentContainer,farmFragment)
//                .addToBackStack(null)
//                .commit();
//    }
//    @OnClick(R.id.llFeed)
//    public void gotoFeed(){
//        FeedFragment feedFragment = new FeedFragment();
//        getActivity().getSupportFragmentManager().beginTransaction()
//                .replace(R.id.contentContainer, feedFragment)
//                .addToBackStack(null)
//                .commit();
//    }
//    @OnClick(R.id.llHusbandry)
//    public void gotoHusbandry(){
//        FarmInfoFragment farmInfoFragment = new FarmInfoFragment();
//        getActivity().getSupportFragmentManager().beginTransaction()
//                .replace(R.id.contentContainer, farmInfoFragment)
//                .addToBackStack(null)
//                .commit();
//    }
//    @Override
//    public void onStart() {
//        super.onStart();
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//    }
//
//    /*
//     * Save Instance State Here
//     */
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        // Save Instance State here
//    }
//
//    /*
//     * Restore Instance State Here
//     */
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        if (savedInstanceState != null) {
//            // Restore Instance State here
//        }
//    }
//}
