package com.zealtech.dld.dldregist.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.FarmActivity;
import com.zealtech.dld.dldregist.model.newFarmer.Farm;
import com.zealtech.dld.dldregist.widget.MyNoHBTextView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedHeadFragment extends Fragment {
    @BindView(R.id.spn_farm)
    Spinner spn_farm;
    @BindView(R.id.plsAddFarm)
    MyNoHBTextView plsAddFarm;
    @BindView(R.id.contentContainerListFeed)
    FrameLayout contentContainerListFeed;

    ArrayList<Farm> farms = new ArrayList<>();
    ArrayList<String> farmNames = new ArrayList<>();

    FeedFragment feedFrag;

//    int currentFarmPos=0;


    public FeedHeadFragment() {
        super();
    }

    public static FeedHeadFragment newInstance() {
        FeedHeadFragment fragment = new FeedHeadFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    static class GUIDataHolder {
        static class FeedData {
            static class SubtypeData {
                // References to common structures for convenience.
                public LinearLayout layout;
                public LinearLayout button;
                // Data members unique to this instance.
                public HashMap<String, View> views = new HashMap<>();
            }
            public HashMap<Integer, SubtypeData> subtypes =
                    new HashMap<>();
        }
        public HashMap<Integer, FeedData> feeds = new HashMap<>();
    }
    HashMap<Integer, GUIDataHolder> guiData = new HashMap<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_feed_head, container, false);
        ButterKnife.bind(this, rootView);
        initInstances(rootView);

//
//        if(farms.size()!=0) {
//            contentContainerListFeed.setVisibility(View.VISIBLE);
//            plsAddFarm.setVisibility(View.GONE);
//
//            for (int i = 0; i < farms.size(); i++) {
//                farmNames.add(farms.get(i).getFarmName());
//            }
//            MySpinnerAdapter adapter = new MySpinnerAdapter(
//                    getContext(),
//                    android.R.layout.simple_dropdown_item_1line,
//                    farmNames);
//
//            spn_farm.setAdapter(adapter);
//            spn_farm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    currentFarmPos = position;
//                    filterListFeed(currentFarmPos);
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> parent) {
//
//                }
//            });
//        }else{
//            contentContainerListFeed.setVisibility(View.GONE);
//            plsAddFarm.setVisibility(View.VISIBLE);
//        }
//        if(farms.size()!=0) {
//            filterListFeed(((FarmActivity) getActivity()).getCurrent_FarmID());
//            contentContainerListFeed.setVisibility(View.VISIBLE);
//            plsAddFarm.setVisibility(View.GONE);
//        }else{
//            contentContainerListFeed.setVisibility(View.GONE);
//            plsAddFarm.setVisibility(View.VISIBLE);
//        }

        setUpData();

        return rootView;

    }

    public void setUpData() {
        farms.clear();
        farms.addAll(((FarmActivity)(getActivity())).getAllFarm());

        if(farms.size()!=0) {
            filterListFeed(((FarmActivity) getActivity()).getCurrent_FarmID());
            contentContainerListFeed.setVisibility(View.VISIBLE);
            plsAddFarm.setVisibility(View.GONE);
        }else{
            contentContainerListFeed.setVisibility(View.GONE);
            plsAddFarm.setVisibility(View.VISIBLE);
        }
    }

    void filterListFeed(int farmID){
//        int farmID = farms.get(pos).getFarmID();

        if (!guiData.containsKey(farmID)) {
            GUIDataHolder d = new GUIDataHolder();
            guiData.put(farmID, d);
        }

        FeedFragment feed = FeedFragment.newInstance(farmID, guiData.get(farmID).feeds);
        getFragmentManager().beginTransaction()
                .replace(R.id.contentContainerListFeed, feed)
                .commit();

//        if (feedFrag == null) {
//            feedFrag = FeedFragment.newInstance(farmID, guiData.get(farmID).feeds);
//            getFragmentManager().beginTransaction()
//                    .replace(R.id.contentContainerListFeed, feedFrag)
//                    .commit();
//        } else {
//            feedFrag.farmID = farmID;
//            feedFrag.guiData = guiData.get(farmID).feeds;
//        }


    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }
}
