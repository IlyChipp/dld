
package com.zealtech.dld.dldregist.model.ImageItem;

import com.google.gson.annotations.SerializedName;

public class UpdateDate {

    @SerializedName("date")
    private Integer date;
    @SerializedName("day")
    private Integer day;
    @SerializedName("hours")
    private Integer hours;
    @SerializedName("minutes")
    private Integer minutes;
    @SerializedName("month")
    private Integer month;
    @SerializedName("nanos")
    private Integer nanos;
    @SerializedName("seconds")
    private Integer seconds;
    @SerializedName("time")
    private Integer time;
    @SerializedName("timezoneOffset")
    private Integer timezoneOffset;
    @SerializedName("year")
    private Integer year;

}
