package com.zealtech.dld.dldregist.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.zealtech.dld.dldregist.R;
import com.zealtech.dld.dldregist.activity.FarmerListActivity;
import com.zealtech.dld.dldregist.activity.FindFarmerActivity;
import com.zealtech.dld.dldregist.config.Api;
import com.zealtech.dld.dldregist.manager.Contextor;
import com.zealtech.dld.dldregist.manager.MyPreferenceManager;
import com.zealtech.dld.dldregist.model.SearchModel;
import com.zealtech.dld.dldregist.model.farmerList.FarmerSearchList;
import com.zealtech.dld.dldregist.model.newFarmer.Permission;
import com.zealtech.dld.dldregist.util.AppUtils;
import com.zealtech.dld.dldregist.widget.FrameLayoutDisable;
import com.zealtech.dld.dldregist.widget.MyEditNoHLTextView;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FarmerCorporateFragment extends Fragment {

    @BindView(R.id.edt_pid)
    MyEditNoHLTextView edt_pid;
    @BindView(R.id.edt_compname)
    MyEditNoHLTextView edt_compname;

    @BindView(R.id.progressBar)
    FrameLayoutDisable progressBar;

    Permission permission;
    SearchModel searchModel;
    int mode;

    public FarmerCorporateFragment() {
        super();
    }

    public static FarmerCorporateFragment newInstance() {
        FarmerCorporateFragment fragment = new FarmerCorporateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_farmer_corporate, container, false);
        ButterKnife.bind(FarmerCorporateFragment.this, rootView);
        initInstances(rootView);

        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        MyPreferenceManager myPreferenceManager=new MyPreferenceManager(getContext());

        permission=((FindFarmerActivity)getActivity()).getPermission();


        int organizeLevelID = permission.getOrganizeLevelID();
        String orgCode = permission.getOrgCode();
        int parentID = permission.getOrgParentID();

        if (organizeLevelID == 3) {//province
            myPreferenceManager.setPROID(Integer.parseInt(orgCode));
            myPreferenceManager.setAMID(-99);
            myPreferenceManager.setTAMID(-99);
        } else if (organizeLevelID == 4) {//amphur
            myPreferenceManager.setPROID(parentID);
            myPreferenceManager.setAMID(Integer.parseInt(orgCode));
            myPreferenceManager.setTAMID(-99);
        }
    }

    @OnClick(R.id.btn_pid)
    public void submitPID(){
        if(edt_pid.getText().length()!=0) {
            mode=0;
            searchModel=new SearchModel();
            searchModel.setCompPid(edt_pid.getText().toString());
            getListFarmer(null, null, edt_pid.getText().toString()
                    , null, null, null, null
                    , 2,null,null,null,null,null,null);
        }
    }

    @OnClick(R.id.btn_compname)
    public void submitCompname(){
        if(edt_compname.getText().length()!=0) {
            mode=1;
            searchModel=new SearchModel();
            searchModel.setCompName( edt_compname.getText().toString());
            getListFarmer(null, null, edt_compname.getText().toString()
                    , null, null, null, null
                    , 2,null,null,null,null,null,null);
        }
    }

    private void getListFarmer(Integer iDisplayStart, Integer iDisplayLength, String sSearch, Integer ProvinceId
            , Integer AmphurId, Integer TambolId, Integer VillageId, int userType, String latitude
            ,String longitude,String distanceKilometer,String Pid,String FirstName,String LastName){
        progressBar.setVisibility(View.VISIBLE);
        Api api = AppUtils.getApiService();
        Call<FarmerSearchList> call = api.getListFarmer(iDisplayStart,iDisplayLength,sSearch,ProvinceId
                ,AmphurId,TambolId,VillageId,userType,latitude,longitude,distanceKilometer,Pid,FirstName,LastName);
        call.enqueue(new Callback<FarmerSearchList>() {
            @Override
            public void onResponse(Call<FarmerSearchList> call, Response<FarmerSearchList> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
//                    if (response.body().getResponseCode().equals(getString(R.string.success))) {

                    if(response.body().getAaData().size()!=0) {
                        Intent intent = new Intent(getContext(), FarmerListActivity.class);
                        intent.putExtra("farmersList", Parcels.wrap(response.body().getAaData()));
                        intent.putExtra("searchModel", Parcels.wrap(searchModel));
                        intent.putExtra("mode", mode);
                        intent.putExtra("userType", 2);
                        startActivity(intent);
                    }else{
                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.notfound), Toast.LENGTH_SHORT).show();
                    }
//                    } else {
//                        Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
//                    }
                } else {
                    Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<FarmerSearchList> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }
}
