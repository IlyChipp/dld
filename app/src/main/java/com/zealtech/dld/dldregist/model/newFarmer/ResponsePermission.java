package com.zealtech.dld.dldregist.model.newFarmer;

/**
 * Created by Jirat on 10/31/2017.
 */

public class ResponsePermission {

    private String responseCode;
    private String responseMessage;
    private Permission data;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Permission getData() {
        return data;
    }

    public void setData(Permission data) {
        this.data = data;
    }

}
