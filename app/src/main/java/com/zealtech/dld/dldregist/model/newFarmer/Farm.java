
package com.zealtech.dld.dldregist.model.newFarmer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Farm extends RealmObject {

    @Expose
    @SerializedName("government_Help_ID")
    private Integer government_Help_ID=1;

    @Expose
    @SerializedName("debtAmount")
    private Double debtAmount=0.0;

    @Expose
    @SerializedName("debtsAmountIn")
    private Double debtsAmountIn=0.0;

    @Expose
    @SerializedName("debtsAmountOut")
    private Double debtsAmountOut=0.0;

    @Expose
    @SerializedName("holding_Equipment")
    private String holding_Equipment="";

    @Expose
    @SerializedName("holding_Equipment_Name")
    private String holding_Equipment_Name="";

    @Expose
    @SerializedName("area_Wa")
    private Double area_Wa=0.0;

    @Expose
    @SerializedName("farm_ID")
    private Integer farmID=0;

    @Expose
    @SerializedName("amphur_ID")
    private Integer amphurID=0;

    @Expose
    @SerializedName("animalWorth")
    private Double animalWorth=0.0;

    @Expose
    @SerializedName("area_Ngan")
    private Double areaNgan=0.0;

    @Expose
    @SerializedName("area_Rai")
    private Double areaRai=0.0;

    @Expose
    @SerializedName("area_Type_ID")
    private String areaTypeID="";

    @Expose
    @SerializedName("cancelBy")
    private String cancelBy="";

    @Expose
    @SerializedName("cancelDate")
    private String cancelDate="";

    @Expose
    @SerializedName("createBy")
    private String createBy="";

    @Expose
    @SerializedName("createDate")
    private String createDate="";

    @Expose
    @SerializedName("email")
    private String email="";

    @Expose
    @SerializedName("farm_Name")
    private String farmName="";

    @Expose
    @SerializedName("farm_Owner_Type_ID")
    private Integer farmOwnerTypeID=0;

    @Expose
    @SerializedName("farm_Problem_Desc")
    private String farmProblemDesc="";

    @Expose
    @SerializedName("farm_Standard_Number")
    private String farmStandardNumber="";

    @Expose
    @SerializedName("homeNo")
    private String homeNo="";

    @Expose
    @SerializedName("isCancel")
    private Integer isCancel=0;
    @Expose
    @SerializedName("isFarm")
    private Integer isFarm=1;

    @Expose
    @SerializedName("latitude")
    private String latitude="";

    @Expose
    @SerializedName("longitude")
    private String longitude="";

    @Expose
    @SerializedName("mobile")
    private String mobile="";

    @Expose
    @SerializedName("orderIndex")
    private Integer orderIndex=0;

    @Expose
    @SerializedName("phone")
    private String phone="";

    @Expose
    @SerializedName("postCode")
    private String postCode="";

    @Expose
    @SerializedName("province_ID")
    private Integer provinceID=0;

    @Expose
    @SerializedName("revenueOfLivestock")
    private Double revenueOfLivestock=0.0;

    @Expose
    @SerializedName("road")
    private String road="";

    @Expose
    @SerializedName("soi")
    private String soi="";

    @Expose
    @SerializedName("standard_ID")
    private Integer standardID=0;

    @Expose
    @SerializedName("status_ID")
    private Integer statusID=0;

    @Expose
    @SerializedName("support_Standard_ID")
    private Integer supportStandardID=0;

    @Expose
    @SerializedName("tambol_ID")
    private Integer tambolID=0;

    @Expose
    @SerializedName("updateBy")
    private String updateBy="";

    @Expose
    @SerializedName("updateDate")
    private String updateDate="";

    @Expose
    @SerializedName("moo")
    private String moo="";

    @Expose
    @SerializedName("village_ID")
    private Integer villageID=0;

    @Expose
    @SerializedName("worker_ID")
    private String workerID="0";

    @Expose
    @SerializedName("farmExpire")
    private String farmExpire="";

    @Expose
    private Worker worker = new Worker();
    @Expose
    private RealmList<FarmStandard> farm_standard =new RealmList<>();
    @Expose
    private RealmList<Livestock> livestock =new RealmList<>();
    @Expose
    private RealmList<Feed> feed =new RealmList<>();
    @Expose
    private RealmList<FarmImage> farm_image =new RealmList<>();

    public Double getDebtAmount() {
        return debtAmount;
    }

    public void setDebtAmount(Double debtAmount) {
        this.debtAmount = debtAmount;
    }

    public Double getDebtsAmountIn() {
        return debtsAmountIn;
    }

    public void setDebtsAmountIn(Double debtsAmountIn) {
        this.debtsAmountIn = debtsAmountIn;
    }

    public Double getDebtsAmountOut() {
        return debtsAmountOut;
    }

    public void setDebtsAmountOut(Double debtsAmountOut) {
        this.debtsAmountOut = debtsAmountOut;
    }

    public Integer getGovernment_Help_ID() {
        return government_Help_ID;
    }

    public void setGovernment_Help_ID(Integer government_Help_ID) {
        this.government_Help_ID = government_Help_ID;
    }

    public String getHolding_Equipment_Name() {
        return holding_Equipment_Name;
    }

    public void setHolding_Equipment_Name(String holding_Equipment_Name) {
        this.holding_Equipment_Name = holding_Equipment_Name;
    }

    public String getHolding_Equipment() {
        return holding_Equipment;
    }

    public void setHolding_Equipment(String holding_Equipment) {
        this.holding_Equipment = holding_Equipment;
    }

    public Double getArea_Wa() {
        return area_Wa;
    }

    public void setArea_Wa(Double area_Wa) {
        this.area_Wa = area_Wa;
    }

  /*  public RealmList<FarmImage> getFarm_image() {
        return farm_image;
    }

    public void setFarm_image(RealmList<FarmImage> farm_image) {
        this.farm_image = farm_image;
    }*/

    public Integer getFarmID() {
        return farmID;
    }

    public void setFarmID(Integer farmID) {
        this.farmID = farmID;
    }

    public Integer getAmphurID() {
        return amphurID;
    }

    public void setAmphurID(Integer amphurID) {
        this.amphurID = amphurID;
    }

    public Double getAnimalWorth() {
        return animalWorth;
    }

    public void setAnimalWorth(Double animalWorth) {
        this.animalWorth = animalWorth;
    }

    public Double getAreaNgan() {
        return areaNgan;
    }

    public void setAreaNgan(Double areaNgan) {
        this.areaNgan = areaNgan;
    }

    public Double getAreaRai() {
        return areaRai;
    }

    public void setAreaRai(Double areaRai) {
        this.areaRai = areaRai;
    }

    public String getAreaTypeID() {
        return areaTypeID;
    }

    public void setAreaTypeID(String areaTypeID) {
        this.areaTypeID = areaTypeID;
    }

    public String getCancelBy() {
        return cancelBy;
    }

    public Integer getIsFarm() {
        return isFarm;
    }

    public void setIsFarm(Integer isFarm) {
        this.isFarm = isFarm;
    }

    public void setCancelBy(String cancelBy) {
        this.cancelBy = cancelBy;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFarmName() {
        return farmName;
    }

    public void setFarmName(String farmName) {
        this.farmName = farmName;
    }

    public Integer getFarmOwnerTypeID() {
        return farmOwnerTypeID;
    }

    public void setFarmOwnerTypeID(Integer farmOwnerTypeID) {
        this.farmOwnerTypeID = farmOwnerTypeID;
    }

    public String getFarmProblemDesc() {
        return farmProblemDesc;
    }

    public void setFarmProblemDesc(String farmProblemDesc) {
        this.farmProblemDesc = farmProblemDesc;
    }

    public String getFarmStandardNumber() {
        return farmStandardNumber;
    }

    public void setFarmStandardNumber(String farmStandardNumber) {
        this.farmStandardNumber = farmStandardNumber;
    }


    public String getHomeNo() {
        return homeNo;
    }

    public void setHomeNo(String homeNo) {
        this.homeNo = homeNo;
    }

    public Integer getIsCancel() {
        return isCancel;
    }

    public void setIsCancel(Integer isCancel) {
        this.isCancel = isCancel;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public Integer getProvinceID() {
        return provinceID;
    }

    public void setProvinceID(Integer provinceID) {
        this.provinceID = provinceID;
    }

    public Double getRevenueOfLivestock() {
        return revenueOfLivestock;
    }

    public void setRevenueOfLivestock(Double revenueOfLivestock) {
        this.revenueOfLivestock = revenueOfLivestock;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getSoi() {
        return soi;
    }

    public void setSoi(String soi) {
        this.soi = soi;
    }

    public Integer getStandardID() {
        return standardID;
    }

    public void setStandardID(Integer standardID) {
        this.standardID = standardID;
    }

    public Integer getStatusID() {
        return statusID;
    }

    public void setStatusID(Integer statusID) {
        this.statusID = statusID;
    }

    public Integer getSupportStandardID() {
        return supportStandardID;
    }

    public void setSupportStandardID(Integer supportStandardID) {
        this.supportStandardID = supportStandardID;
    }

    public Integer getTambolID() {
        return tambolID;
    }

    public void setTambolID(Integer tambolID) {
        this.tambolID = tambolID;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getMoo() {
        return moo;
    }

    public void setMoo(String moo) {
        this.moo = moo;
    }

    public Integer getVillageID() {
        return villageID;
    }

    public void setVillageID(Integer villageID) {
        this.villageID = villageID;
    }

    public String getWorkerID() {
        return workerID;
    }

    public void setWorkerID(String workerID) {
        this.workerID = workerID;
    }

    public RealmList<FarmStandard> getFarmStandard() {
        return farm_standard;
    }

    public void setFarmStandard(RealmList<FarmStandard> farmStandard) {
        this.farm_standard = farmStandard;
    }

    public RealmList<Livestock> getLivestock() {
        return livestock;
    }

    public void setLivestock(RealmList<Livestock> livestock) {
        this.livestock = livestock;
    }

    public RealmList<Feed> getFeed() {
        return feed;
    }

    public void setFeed(RealmList<Feed> feed) {
        this.feed = feed;
    }

    public RealmList<FarmImage> getFarmImage() {
        return farm_image;
    }

    public void setFarmImage(RealmList<FarmImage> farmImage) {
        this.farm_image = farmImage;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }


    public String getFarmExpire() {
        return farmExpire;
    }

    public void setFarmExpire(String farmExpire) {
        this.farmExpire = farmExpire;
    }
}
