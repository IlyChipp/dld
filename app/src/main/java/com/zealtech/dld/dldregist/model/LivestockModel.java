package com.zealtech.dld.dldregist.model;

import android.provider.BaseColumns;

import com.zealtech.dld.dldregist.model.newFarmer.Livestock;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 10/3/2017 AD.
 */
@Parcel
public class LivestockModel implements APIResponse{

    //DBHelper
    public static final String TABLE = "livestock";

    String responseCode ;
    String responseMessage;
    ArrayList<Livestock> data;

    public LivestockModel(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<Livestock> getData() {
        return data;
    }

    public void setData(ArrayList<Livestock> data) {
        this.data = data;
    }


    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String LIVESTOCK_ID = "livestock_ID";
        public static final String CREATE_BY ="createBy";
        public static final String CREATE_DATE = "createDate";
        public static final String FARM_ID ="farm_ID";
        public static final String LIVESTOCK_AMOUNT = "livestock_Amount";
        public static final String LIVESTOCK_TYPE_ID ="livestock_Type_ID";
        public static final String LIVESTOCK_VALUES = "livestock_Values";
        public static final String UPDATE_BY = "updateBy";
        public static final String UPDATE_DATE = "updateDate";
    }
}
